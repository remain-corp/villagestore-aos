package com.remain.villagestore;

import android.content.Context;
import android.content.Intent;

import com.google.android.gcm.GCMBaseIntentService;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.pages.p1_intro.activity.IntroActivity;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.manager.AlarmManager;
import com.remain.villagestore.util.ActivityManager;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by woongjaelee on 2015. 12. 6..
 */
public class GCMIntentService extends GCMBaseIntentService {

    //================================================================================
    // XXX Constants
    //================================================================================
    public static final String PROJECT_ID = "1007335227529";

    public static final String PUSH_TYPE_NOTICE = "1";
    public static final String PUSH_TYPE_ALARM = "2";

    //================================================================================
    // XXX Variables
    //================================================================================

    //================================================================================
    // XXX Constructors
    //================================================================================
    public GCMIntentService() {
        this(PROJECT_ID);
        Log.e("GCMIntentService");
    }

    public GCMIntentService(String projectId) {
        super(projectId);
        Log.e("GCMIntentService"+projectId);
    }

    //================================================================================
    // XXX Override Method
    //================================================================================
    @Override
    protected void onError(Context context, String errorId) {
        Log.e("onError : "+errorId);
    }

    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.e("onMessage");

        if (intent == null) {
            return;
        }

        String info = intent.getStringExtra("info");
        Log.e("info = "+intent.getStringExtra("info"));

        JSONObject object = null;

        if(info == null)
            return;

        try {
            object = new JSONObject(info);

            if (object != null)
                processActivity(context, object);
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRegistered(Context context, String regId) {
        Log.e("onRegistered");
    }

    @Override
    protected void onUnregistered(Context context, String regId) {
        Log.e("onUnregistered : "+regId);
    }

    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================

    /**
     * Activity 메시지
     * @param context
     */
    private void processActivity(Context context,JSONObject object) throws JSONException {

        // 노티 알림
        String message;
        boolean isCatePush;

        Log.e(object.toString());

        int push_seq = object.getInt(ParamInfo.PUSH_SEQ);
        int push_info_seq = object.getInt("push_info_seq");
        String title = object.getString("title");
        String content = object.getString("content");
        //0일경우 매장없음
        int link_st_seq = object.getInt("link_st_seq");

        Log.e("push_seq = "+push_seq);
        Log.e("push_info_seq = "+push_info_seq);
        Log.e("title = "+title);
        Log.e("content = "+content);
        Log.e("link_st_seq = "+link_st_seq);

        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(ParamInfo.PUSH_SEQ, push_seq);
        intent.putExtra(ParamInfo.STORE_SEQ, link_st_seq);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        AlarmManager.getInstance(context).sendNotification(context, intent, title, content, push_info_seq);
    }
}

