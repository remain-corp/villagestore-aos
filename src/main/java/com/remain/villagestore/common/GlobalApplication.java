package com.remain.villagestore.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.os.AsyncTask;
import android.os.Build;

import com.google.android.gcm.GCMRegistrar;
import com.kakao.auth.KakaoSDK;
import com.kakao.util.helper.log.Logger;
import com.remain.villagestore.GCMIntentService;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.manager.DataManager;
import com.remain.villagestore.pages.p4_other.sign.adapter.KakaoSDKAdapter;


/**
 * Created by woongjaelee on 2015. 12. 6..
 */
public class GlobalApplication extends Application {
    private static volatile GlobalApplication instance = null;
    private static volatile Activity currentActivity = null;

    public static Activity getCurrentActivity() {
        Logger.d("++ currentActivity : " + (currentActivity != null ? currentActivity.getClass().getSimpleName() : ""));
        return currentActivity;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        GlobalApplication.currentActivity = currentActivity;
    }

    /**
     * singleton 애플리케이션 객체를 얻는다.
     * @return singleton 애플리케이션 객체
     */
    public static GlobalApplication getGlobalApplicationContext() {
        if(instance == null)
            throw new IllegalStateException("this application does not inherit com.kakao.GlobalApplication");
        return instance;
    }

    /**
     * 이미지 로더, 이미지 캐시, 요청 큐를 초기화한다.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        KakaoSDK.init(new KakaoSDKAdapter());
        executeRegisterGcm();
    }

    /**
     * 애플리케이션 종료시 singleton 어플리케이션 객체 초기화한다.
     */
    @Override
    public void onTerminate() {
        super.onTerminate();
        instance = null;
    }

    /**
     * GCM Id 등록 비동기 작업
     */
    @SuppressLint("NewApi")
    public void executeRegisterGcm() {

        RegisterGcmAsyncTask registerGcmAsyncTask = new RegisterGcmAsyncTask();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            registerGcmAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            registerGcmAsyncTask.execute();
        }
    }

    /**
     * GCM Id 등록 비동기 작업
     */
    class RegisterGcmAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... doParams) {

            GCMRegistrar.checkDevice(GlobalApplication.this);
            GCMRegistrar.checkManifest(GlobalApplication.this);

            String pushId = GCMRegistrar.getRegistrationId(GlobalApplication.this);

            if (AppInfo.DEFAULT_STRING.equals(pushId)) {

                GCMRegistrar.register(GlobalApplication.this, GCMIntentService.PROJECT_ID);
                DataManager.getInstance().setPushID(GCMRegistrar.getRegistrationId(GlobalApplication.this));
            } else {
                DataManager.getInstance().setPushID(pushId);
            }
            return null;
        }
    }


}
