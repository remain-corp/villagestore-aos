package com.remain.villagestore.common;

import android.animation.ObjectAnimator;
import android.app.Notification;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.remain.villagestore.util.Log;

import java.security.MessageDigest;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by remain on 2016. 4. 18..
 */
public class Utils {


    /*
    * 16.04.18
    * 신주소에서 도로이름이 구,읍 등 이전에 나오는 위치변경
    * */
    public static String changeSubAddress(String address, boolean sub) {
        String changedAddress = "";
        String[] adresses = address.split(" ");
        if (sub) {
            //서브 주소일 경우
            if (adresses.length > 1) {
                int lastnum = adresses[0].length();
                String lastText = adresses[0].substring(lastnum - 1, lastnum);
                if (lastText.equals("로") || lastText.equals("길")) {
                    changedAddress = adresses[1] + " " + adresses[0];
                } else {
                    changedAddress = address;
                }
            } else {
                changedAddress = adresses[0];
            }
        } else {
            //전체 주소일 경우
            String road = "";
            StringBuffer addressBuffer = new StringBuffer();
            for (int a = 0; a < adresses.length; a++) {
                if (a > adresses.length - 3) {
                    String msg = adresses[a];
                    int lastnum = msg.length();
                    String lastText = msg.substring(lastnum - 1, lastnum);
                    if (lastText.equals("로") || lastText.equals("길")) {
                        road = msg;
                    } else {
                        addressBuffer.append(msg);
                        addressBuffer.append(" ");
                    }
                } else {
                    addressBuffer.append(adresses[a]);
                    addressBuffer.append(" ");
                }
            }
            addressBuffer.append(road);
            changedAddress = addressBuffer.toString().trim();
        }
        return changedAddress;
    }

    //해쉬키 가져오는 메서드
    public static String getAppKeyHash(Context context) {
        String key = "";
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
        }
        return key;
    }

    /*
    * 핸드폰 진동
    * */
    public static void vibrate(Context context, int duration) {
        Vibrator vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(duration);
    }

    /*
    * 뷰 흔들기
    * */
    public static void shakeView(final View v) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(v, "alpha", 0f, 1f);
        anim.setRepeatCount(1);
        anim.setDuration(200);
        anim.start();
    }

    /*
    * Email형식체크
    * */
    public static boolean isValidEmail(String email) {
        boolean err = false;
        String regex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(email);
        if (m.matches()) {
            err = true;
        }
        return err;
    }

    /*
    * 프리퍼런스처리
    * */
    public static Object getPreferenceData(Context context, String key, Object defValue) {
        if (defValue != null && context != null) {
            //Log.v("", "type : " + defValue.getClass());
            if (defValue.getClass() == String.class) {
                return context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).getString(key, (String) defValue);
            } else if (defValue.getClass() == Integer.class) {
                return context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).getInt(key, (Integer) defValue);
            } else if (defValue.getClass() == Long.class) {
                return context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).getLong(key, (Long) defValue);
            } else if (defValue.getClass() == Boolean.class) {
                return context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).getBoolean(key, (Boolean) defValue);
            } else if (defValue.getClass() == Float.class) {
                return context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).getFloat(key, (Float) defValue);
            }
            return defValue;
        } else return null;
    }

    public static void setPreferenceData(Context context, String key, Object value) {
        if (value != null) {
            if (value.getClass() == String.class) {
                context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).edit().remove(key).commit();
                context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).edit().putString(key, (String) value).commit();
            } else if (value.getClass() == Integer.class) {
                context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).edit().remove(key).commit();
                context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).edit().putInt(key, (Integer) value).commit();
            } else if (value.getClass() == Long.class) {
                context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).edit().remove(key).commit();
                context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).edit().putLong(key, (Long) value).commit();
            } else if (value.getClass() == Boolean.class) {
                context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).edit().remove(key).commit();
                context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).edit().putBoolean(key, (Boolean) value).commit();
            } else if (value.getClass() == Float.class) {
                context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).edit().remove(key).commit();
                context.getSharedPreferences(C.pref.PREF_NAME, context.MODE_PRIVATE).edit().putFloat(key, (Float) value).commit();
            }
        }
    }

    //랜덤 영문 숫자조합 8글자
    public static String getRandomPassword() {
        Random rnd = new Random();
        StringBuffer buf = new StringBuffer();

        for (int i = 0; i < 6; i++) {
            if (rnd.nextBoolean()) {
                buf.append((char) ((int) (rnd.nextInt(26)) + 97));
            } else {
                buf.append((rnd.nextInt(10)));
            }
        }
        return buf.toString();
    }

    /*
    * 시스템 노티텍스트가져오기
    * */
    public static int getDefaultNotiTitleColor(Context context) {
        int textColor;
        try {
            Notification ntf = new Notification();
            ntf.setLatestEventInfo(context, "sameText", "sameText", null);
            LinearLayout group = new LinearLayout(context);
            ViewGroup event = (ViewGroup) ntf.contentView.apply(context, group);
            textColor = recurseGroup(event);
            group.removeAllViews();
            Log.e("try로오는지");
        } catch (Exception e) {
            Log.e("캐취로오는지");
            textColor = android.R.color.white;
        }
        return textColor;
    }

    public static int recurseGroup(ViewGroup gp) {
        final int count = gp.getChildCount();
        Log.e("count = " + count);
        for (int i = 0; i < count; ++i) {
            Log.e("텍스트뷰인지 테스트 = " + (gp.getChildAt(i) instanceof TextView));
            if (gp.getChildAt(i) instanceof TextView) {
                TextView text = (TextView) gp.getChildAt(i);
                Log.e("getText = " + text.getText().toString());
                String szText = text.getText().toString();
                if (szText.equals("sameText")) {
                    Log.e("디폴트노티 = " + text.getTextColors().getDefaultColor());
                    Log.e("겟텍스트 = " + text.getTextColors());
                    return text.getTextColors().getDefaultColor();
                }
            } else if (gp.getChildAt(i) instanceof ViewGroup)
                return recurseGroup((ViewGroup) gp.getChildAt(i));
        }
        return -1;
    }

    public static int DPToPX(Context context, int dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }

    public static String getNetworkState(Context context){
        String result = "";
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); // 3G나 LTE등 데이터 네트워크에 연결된 상태
        NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); // 와이파이에 연결된 상태

        if (wifi.isConnected()) { // 와이파이에 연결된 경우
            result = "wifi";
        } else if (mobile.isConnected()) { // 데이터 네트워크에 연결된 경우
            result = "data";
        } else { // 인터넷에 연결되지 않은 경우
            result = "no";
        }
        return result;
    }

}
