package com.remain.villagestore.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.remain.villagestore.R;
import com.remain.villagestore.util.Log;


public class CircleIndicator extends LinearLayout implements OnPageChangeListener {

	//================================================================================
    // XXX Constants
    //================================================================================
	/** Indicator 기본 크기 */
    private final static int DEFAULT_INDICATOR_WIDTH = 15;
    /** Indicator 기본 margin */
    private final static int DEFAULT_INDICATOR_MARGIN = 7;
    /** 활성 Indicator alpha 값 */
    private final static float MAX_INDICATOR_ALPHA = 1.0f;
    /** 비활성 Indicator alpha 값 */
    private final static float MIN_INDICATOR_ALPHA = 0.5f;
    /** 활성 Indicator 크기 확대값 */
    private final static float MAX_INDICATOR_SCALE = 1.0f;
    /** 비활성 Indicator 크기 축소값 */
    private final static float MIN_INDICATOR_SCALE = 1.0f;
    
	//================================================================================
	// XXX Variables
	//================================================================================
	// App
	
	// Content

	// Widget
	
	// View
    private ViewPager mViewPager;
	
	// Apdater
	
	// Array
	
	// Lang
    private int mIndicatorMargin;
    private int mIndicatorWidth;
    private int mIndicatorHeight;
    private int mIndicatorBackground = R.drawable.store_indicator_on;
    private boolean isCreate;
    
	// Component

    // Listener
    private OnPageChangeListener mViewPagerOnPageChangeListener;
    
	// Manager
	
	// Util
	
	// Interface

	//================================================================================
	// XXX Constructor
	//================================================================================
    public CircleIndicator(Context context) {
        super(context);
        
        init(context, null);
    }

    public CircleIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        init(context, attrs);
    }

	//================================================================================
    // XXX Override Method
    //================================================================================
    @Override
    public void onPageSelected(int position) {
    	
    	// 페이지 위치에 따른 indicator 떄문에 추가 
        if (mViewPagerOnPageChangeListener != null) {
            mViewPagerOnPageChangeListener.onPageSelected(position);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    	
        if (mViewPagerOnPageChangeListener != null) {
            mViewPagerOnPageChangeListener.onPageScrollStateChanged(state);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    	
        if (mViewPagerOnPageChangeListener != null) {
            mViewPagerOnPageChangeListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }
        
        if (!isCreate) {
			return;
		}
        
//        Log.e("position : " + position);
//        Log.e("positionOffset : " + positionOffset);
//        Log.e("positionOffsetP2ixels : " + positionOffsetPixels);
        
        View vCurrent = null;
        View vNext = null;
        
    	vCurrent = getChildAt(position);
    	vCurrent.setAlpha(Math.max((MAX_INDICATOR_ALPHA - (MIN_INDICATOR_ALPHA * positionOffset)), MIN_INDICATOR_ALPHA));
    	
    	// Scale 사용 (view 크기는 변하지 않아서 겹치는 문제 발생)
//    	vCurrent.setScaleX(Math.max((MAX_INDICATOR_SCALE - (MIN_INDICATOR_SCALE * positionOffset)), MIN_INDICATOR_SCALE));
//    	vCurrent.setScaleY(Math.max((MAX_INDICATOR_SCALE - (MIN_INDICATOR_SCALE * positionOffset)), MIN_INDICATOR_SCALE));
    	
    	// LayoutParams 사용
    	LayoutParams lpCurrent = (LayoutParams) vCurrent.getLayoutParams();
    	lpCurrent.width = (int) ((float) mIndicatorWidth * Math.max((MAX_INDICATOR_SCALE - (MIN_INDICATOR_SCALE * positionOffset)), MIN_INDICATOR_SCALE));
    	lpCurrent.height = (int) ((float) mIndicatorHeight * Math.max((MAX_INDICATOR_SCALE - (MIN_INDICATOR_SCALE * positionOffset)), MIN_INDICATOR_SCALE));
    	vCurrent.setLayoutParams(lpCurrent);
    	
//    	Log.e("vCurrent.getWidth() : " + vCurrent.getWidth());
//    	Log.e("vCurrent.getHeight() : " + vCurrent.getHeight());
    	
    	vNext = getChildAt(position + 1);
    	if (vNext != null) {
    		
    		vNext.setAlpha(Math.min((MIN_INDICATOR_ALPHA + (MIN_INDICATOR_ALPHA * positionOffset)), MAX_INDICATOR_ALPHA));
    		
    		// Scale 사용 (view 크기는 변하지 않아서 겹치는 문제 발생)
//    		vNext.setScaleX(Math.min((MIN_INDICATOR_SCALE + (MIN_INDICATOR_SCALE * positionOffset)), MAX_INDICATOR_SCALE));
//    		vNext.setScaleY(Math.min((MIN_INDICATOR_SCALE + (MIN_INDICATOR_SCALE * positionOffset)), MAX_INDICATOR_SCALE));
    		
    		// LayoutParams 사용
    		LayoutParams lpNext = (LayoutParams) vNext.getLayoutParams();
    		lpNext.width = (int) ((float) mIndicatorWidth * Math.min((MIN_INDICATOR_SCALE + (MIN_INDICATOR_SCALE * positionOffset)), MAX_INDICATOR_SCALE));
    		lpNext.height = (int) ((float) mIndicatorHeight * Math.min((MIN_INDICATOR_SCALE + (MIN_INDICATOR_SCALE * positionOffset)), MAX_INDICATOR_SCALE));
    		vNext.setLayoutParams(lpNext);
    		
//    		Log.e("vNext.getWidth() : " + vNext.getWidth());
//    		Log.e("vNext.getHeight() : " + vNext.getHeight());
    	}
    }
    
	//================================================================================
    // XXX Etc Method
    //================================================================================
    private void init(Context context, AttributeSet attrs) {
    	
        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        handleTypedArray(context, attrs);
    }

    private void handleTypedArray(Context context, AttributeSet attrs) {
    	
        if (attrs != null) {
        	
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleIndicator);
            mIndicatorWidth = typedArray.getDimensionPixelSize(R.styleable.CircleIndicator_ci_width, -1);
            mIndicatorHeight = typedArray.getDimensionPixelSize(R.styleable.CircleIndicator_ci_height, -1);
            mIndicatorMargin = typedArray.getDimensionPixelSize(R.styleable.CircleIndicator_ci_margin, -1);
            mIndicatorMargin = mIndicatorMargin / 2 > 0 ? mIndicatorMargin / 2 : 1;
            mIndicatorBackground = typedArray.getResourceId(R.styleable.CircleIndicator_ci_drawable, R.drawable.store_indicator_on);
            typedArray.recycle();
        }

        mIndicatorWidth = (mIndicatorWidth == -1) ? dipPx(DEFAULT_INDICATOR_WIDTH) : mIndicatorWidth;
        mIndicatorHeight = (mIndicatorHeight == -1) ? dipPx(DEFAULT_INDICATOR_WIDTH) : mIndicatorHeight;
        mIndicatorMargin = (mIndicatorMargin == -1) ? dipPx(DEFAULT_INDICATOR_MARGIN) : mIndicatorMargin;
    }

    /**
     * ViewPager 연결
     * @param viewPager
     */
    public void setViewPager(ViewPager viewPager) {
    	
    	isCreate = false;
    	
        mViewPager = viewPager;
        mViewPager.setOnPageChangeListener(this);
    	createIndicators(viewPager);
    	
    	// 현재 페이저 위치의 인디케이터를 활성 상태로 설정
    	Log.e("ViewPager Indicator currentItemPos = " + viewPager.getCurrentItem());
    	View vCurrent = getChildAt(viewPager.getCurrentItem());
        
        // 데이터가 없음
    	if (vCurrent == null) {
    		return;
    	}
    	vCurrent.setAlpha(MAX_INDICATOR_ALPHA);
    	
    	// LayoutParams 사용
    	LayoutParams lpCurrent = (LayoutParams) vCurrent.getLayoutParams();
    	lpCurrent.width = mIndicatorWidth;
    	lpCurrent.height = mIndicatorHeight;
    	vCurrent.setLayoutParams(lpCurrent);
    	
    	isCreate = true;
    }

    /**
     * Indicator 생성 및 초기화
     */
    private void createIndicators(ViewPager viewPager) {
    	
        removeAllViews();
        
        int count = viewPager.getAdapter().getCount();

        for (int i = 0; i < count; i++) {
        	
        	// Indicator 생성
            View indicator = new View(getContext());
            indicator.setBackgroundResource(mIndicatorBackground);
            indicator.setAlpha(MIN_INDICATOR_ALPHA);
            // Scale 사용 (view 크기는 변하지 않아서 겹치는 문제 발생)
//            indicator.setScaleX(MIN_INDICATOR_SCALE);
//            indicator.setScaleY(MIN_INDICATOR_SCALE);
            addView(indicator, (int) (mIndicatorWidth * MIN_INDICATOR_SCALE), (int) ((mIndicatorHeight * MIN_INDICATOR_SCALE)));
            
            // margin 추가
            LayoutParams lp = (LayoutParams) indicator.getLayoutParams();
            lp.leftMargin = mIndicatorMargin;
            lp.rightMargin = mIndicatorMargin;
            indicator.setLayoutParams(lp);
        }
    }
    
    public int dipPx(float dpValue) {
    	
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale);
    }
    
    public void setOnPageChangeListener(OnPageChangeListener onPageChangeListener) {

        if (mViewPager == null) {
            throw new NullPointerException("can not find Viewpager , setViewPager first");
        }

        mViewPagerOnPageChangeListener = onPageChangeListener;
        mViewPager.setOnPageChangeListener(this);
    }
}
