package com.remain.villagestore.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.manager.DisplayManager;

/**
 * Created by woongjaelee on 2015. 11. 11..
 */
public class DetailMenuView extends FrameLayout {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    private Context context;

    // Lang
    private String menuTitle;
    private int arrayRes;

    // Widget
    private LinearLayout linearLayout;
    private ImageView imageViewIcon;
    private TextView textView;

    // Util
    private DisplayManager displayManager;
    private OnStoreMenuItemClickListener storeMenuItemClickListener;

    // Enum
    private DetailMenu detailMenu;

    //================================================================================
    // XXX Enum
    //================================================================================

    public enum DetailMenu {
        /* 메뉴보기 */
        SHOW_MENU(0),
        /* 좋아요 */
        LIKE(1),
        /* 제보하기 */
        REPORT(2),
        /* 전화걸기 */
        CALL(3),
        /* 위치보기 */
        LOCATION(4);

        private int menuCode;

        DetailMenu(int menuCode) {
            this.menuCode = menuCode;
        }

        public int getMenuCode() {
            return menuCode;
        }
    }

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    public DetailMenuView(Context context,int menuTypeCode,String menuTitle,int arrayRes) {
        super(context);

        this.context = context;
        this.arrayRes = arrayRes;
        this.menuTitle = menuTitle;

        detailMenu = DetailMenu.values()[menuTypeCode];

        init();
    }

    public DetailMenuView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        init();
    }


    //================================================================================
    // XXX Listener
    //================================================================================

    private OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            if(storeMenuItemClickListener != null)
                storeMenuItemClickListener.onMenuItemClick(detailMenu);
        }
    };
    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    public void setOnStoreMenuItemClickListener(OnStoreMenuItemClickListener storeMenuItemClickListener) {

        this.storeMenuItemClickListener = storeMenuItemClickListener;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================


    private void init() {

        displayManager = new DisplayManager(context);

        inflate(context, R.layout.content_detail_menu_view, this);
        setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT,1f));

        initUI();
        initUISize();
        setEventListener();
        setView(arrayRes);

        LayoutParams linearParams = (LayoutParams) linearLayout.getLayoutParams();

        if(detailMenu != DetailMenu.LOCATION) {
            linearParams.rightMargin = 16;
        }

    }

    private void initUI() {

        linearLayout = (LinearLayout)findViewById(R.id.content_detail_menu_linearlayout_bg);
        imageViewIcon = (ImageView)findViewById(R.id.content_detail_menu_imageview_icon);
        textView = (TextView)findViewById(R.id.content_detail_menu_textview);

    }

    private void initUISize() {

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) imageViewIcon.getLayoutParams();
        displayManager.changeWidthHeightSameRate(linearParams,75,75);
        linearParams.topMargin = displayManager.getScaleSizeSameRate(20);

        linearParams = (LinearLayout.LayoutParams) textView.getLayoutParams();
        linearParams.topMargin = displayManager.getScaleSizeSameRate(6);

    }

    private void setEventListener() {

        linearLayout.setOnClickListener(clickListener);
    }

    private void setView(int menuArrayRes) {

        textView.setText(menuTitle);
        TypedArray typedArrayIcon =  context.getResources().obtainTypedArray(menuArrayRes);
        imageViewIcon.setBackgroundResource(typedArrayIcon.getResourceId(detailMenu.getMenuCode(),0));

    }

    public void setSelect(boolean isSelected) {

        imageViewIcon.setSelected(isSelected);
    }


    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    public interface OnStoreMenuItemClickListener {
        public void onMenuItemClick(DetailMenu detailMenu);
    }

    //================================================================================
    // XXX Debug
    //================================================================================


}
