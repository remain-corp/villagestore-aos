package com.remain.villagestore.component;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.manager.DisplayManager;

/**
 * Created by woongjaelee on 2015. 11. 9..
 */
public class HorizontalMenuView extends LinearLayout {


    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    private Context context;

    // Lang
    private int menuPosition;

    // View

    private SparseArray<View> arrayListView = new SparseArray<View>();

    private OnHorizontalItemClickListener horizontalItemClickListener;

    private LayoutInflater layoutInflater;

    // Util
    private DisplayManager displayManager;

    //================================================================================
    // XXX Constructor
    //================================================================================

    public HorizontalMenuView(Context context) {
        super(context);

        this.context = context;

        init();
    }

    public HorizontalMenuView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        init();
    }

    //================================================================================
    // XXX Override Method
    //================================================================================

    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    public void setOnHorizontalItemClickListener(OnHorizontalItemClickListener horizontalItemClickListener) {

        this.horizontalItemClickListener = horizontalItemClickListener;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================


    private void init() {

        setOrientation(LinearLayout.HORIZONTAL);
        displayManager = new DisplayManager(context);

        setGravity(Gravity.CENTER_VERTICAL);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String[] arrayMenuItem = context.getResources().getStringArray(R.array.array_main_menu_item);

        for (int i = 0; i < arrayMenuItem.length; i++) {

            View view = layoutInflater.inflate(R.layout.content_menu_item, null);

            /*
            * 탭메뉴 관공서 빌딩 위치변경
            * */
            switch (i) {
                case 8:
                    initUI(view, arrayMenuItem, 9);
                    addView(view);
                    initUISize(view);
                    arrayListView.put(9, view);
                    break;
                case 9:
                    initUI(view, arrayMenuItem, 8);
                    addView(view);
                    initUISize(view);
                    arrayListView.put(8, view);
                    break;
                default:
                    initUI(view, arrayMenuItem, i);
                    addView(view);
                    initUISize(view);
                    arrayListView.put(i, view);
                    break;
            }
        }

        setEventListener();

    }

    private void initUI(View view, String[] arrayMenuItem, int position) {

        TextView textView = (TextView) view.findViewById(R.id.content_menu_textview);
        View viewLine = (View) view.findViewById(R.id.content_menu_view_line);

        if (position == 0) {

            textView.setSelected(true);
            viewLine.setAlpha(1.0f);

        } else {

            viewLine.setAlpha(0.0f);

        }

        textView.setText(arrayMenuItem[position]);
        view.setTag(position);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = (int) v.getTag();

                if (horizontalItemClickListener != null)
                    horizontalItemClickListener.onItemClick(pos);

                setPageSelected(pos);
            }
        });
    }

    private void initUISize(View view) {

        displayManager.changeWidthHeightSameRate(view.findViewById(R.id.content_menu_item_framelayout).getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 100);
        view.findViewById(R.id.content_menu_textview).setPadding(displayManager.getScaleSizeSameRate(18), 0, displayManager.getScaleSizeSameRate(18), 0);
        displayManager.changeWidthHeightSameRate(findViewById(R.id.content_menu_view_line).getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 4);

    }

    private void setEventListener() {

    }

    public void setPageSelected(int position) {

        if (position == menuPosition) return;

        // 선택된 페이지로 상태 바꿈
        setPageSelected(position, true);
        // 이전에 선택되어있던 페이지 상태 바꿈
        setPageSelected(menuPosition, false);

        menuPosition = position;
    }

    public int getMenuPos() {
        return menuPosition;
    }

    private void setPageSelected(int position, boolean isSelected) {

        View view = arrayListView.get(position);
        TextView textView = (TextView) view.findViewById(R.id.content_menu_textview);
        View viewLine = (View) view.findViewById(R.id.content_menu_view_line);

        if (isSelected) {

            textView.setSelected(true);
            viewLine.setAlpha(1.0f);

        } else {

            textView.setSelected(false);
            viewLine.setAlpha(0.0f);
        }
    }

    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    public interface OnHorizontalItemClickListener {
        public void onItemClick(int position);
    }

    //================================================================================
    // XXX Debug
    //================================================================================

}
