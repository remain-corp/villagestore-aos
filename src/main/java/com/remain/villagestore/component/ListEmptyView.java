package com.remain.villagestore.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.manager.DisplayManager;

/**
 * Created by woongjaelee on 15. 8. 31..
 */
public class ListEmptyView extends FrameLayout {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    private Context context;

    private ImageView imageView;
    private TextView textView;

    // Util
    private DisplayManager displayManager;

    //================================================================================
    // XXX Constructor
    //================================================================================

    public ListEmptyView(Context context) {
        super(context);

        this.context = context;
        init();
    }

    public ListEmptyView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        init();
    }


    //================================================================================
    // XXX Override Method
    //================================================================================

    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================


    private void init() {

        displayManager = new DisplayManager(context, AppInfo.STANDARD_WIDTH,AppInfo.STANDARD_HEIGHT);

        inflate(context, R.layout.content_list_empty_view,this);

        setLayoutParams(new AbsListView.LayoutParams(displayManager.getWidth(720), displayManager.getHeight(1000 )));


        initUI();
        initUISize();
        setEventListener();

    }

    private void initUI() {

        textView = (TextView)findViewById(R.id.content_list_empty_view_textview);
        imageView = (ImageView)findViewById(R.id.content_list_empty_view_imageview);

    }

    private void initUISize() {

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imageView.getLayoutParams();
        displayManager.changeWidthSameRate(params,212,177);

        params.bottomMargin = displayManager.getScaleSizeSameRate(40);
    }

    private void setEventListener() {

    }

    public void setMessage(String message) {

        if(textView != null)
            textView.setText(message);
    }
    public void setMessage(int redId) {

        if(textView != null)
            textView.setText(redId);
    }

    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================
}
