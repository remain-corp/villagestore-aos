package com.remain.villagestore.component.PagingScrollView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

/**
 * Created by JcobPark on 2015. 11. 21..
 */
public class LockableHScrollView extends HorizontalScrollView {
    public LockableHScrollView( Context context ) {
        super( context );
    }

    public LockableHScrollView( Context context, AttributeSet attrs ) {
        super( context, attrs );
    }

    public LockableHScrollView( Context context, AttributeSet attrs, int defStyleAttr ) {
        super( context, attrs, defStyleAttr );
    }

    private boolean mScrollable = true;

    public void setScrollingEnabled( boolean enabled ) {
        mScrollable = enabled;
    }

    public boolean isScrollable( ) {
        return mScrollable;
    }

    @Override
    public boolean onTouchEvent( MotionEvent ev ) {
        switch ( ev.getAction( ) ) {
            case MotionEvent.ACTION_DOWN:
                if ( mScrollable ) return super.onTouchEvent( ev );
                return mScrollable;
            default:
                return super.onTouchEvent( ev );
        }
    }

    @Override
    public boolean onInterceptTouchEvent( MotionEvent ev ) {
        if ( !mScrollable ) return false;
        else return super.onInterceptTouchEvent( ev );
    }
}
