package com.remain.villagestore.component.PagingScrollView;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

/**
 * Created by JcobPark on 2015. 11. 20..
 */
public class PagingScrollView extends RelativeLayout {

    private Context context;
    private LockableHScrollView hListView;
    private LinearLayout itemWrapper;

    private ArrayList< View > objects;

    private int page = 0;

    private int itemWidth = 0;


    OnPagineScrollViewListener onPagineScrollViewListener;

    public PagingScrollView( Context context, AttributeSet attrs ) {
        super( context, attrs );
        this.context = context;
        viewInit( );
    }

    public PagingScrollView( Context context ) {
        super( context );
        this.context = context;
        viewInit( );
    }

    private void viewInit( ) {

        hListView = new LockableHScrollView( context );
        hListView.setScrollingEnabled( false );
        hListView.setHorizontalScrollBarEnabled( false );
        hListView.setVerticalScrollBarEnabled( false );

        this.addView( hListView, new LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT ) );

        itemWrapper = new LinearLayout( context );
        itemWrapper.setOrientation( LinearLayout.HORIZONTAL );
        hListView.addView( itemWrapper, new HorizontalScrollView.LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT ) );

    }


    public ArrayList< View > getObjects( ) {
        return objects;
    }

    public void setObjects( ArrayList< View > objects ) {
        this.objects = objects;
        this.page = 0;


        DisplayMetrics metrics = new DisplayMetrics( );
        ( (FragmentActivity) context ).getWindowManager( ).getDefaultDisplay( ).getMetrics( metrics );
        int width = metrics.widthPixels;

        itemWidth = width;

        for ( int i = 0; i < objects.size( ); i++ ) {

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams( width, LayoutParams.MATCH_PARENT );
            objects.get( i ).setLayoutParams( params );
            itemWrapper.addView( objects.get( i ) );

        }

    }

    public int getPage( ) {
        return page;
    }

    public void setPage( int page ) {

        this.setPage( page, 200 );
    }


    public void setPage( int page, int duration ) {
        if ( objects != null ) {
            if ( page < 0 ) {
                page = 0;
            } else if ( page >= objects.size( ) ) {
                page = objects.size( ) - 1;
            }
            this.page = page;
        }

        ObjectAnimator animator = ObjectAnimator.ofInt(hListView, "scrollX", itemWidth * this.page);
        animator.setInterpolator( new AccelerateDecelerateInterpolator( ) );
        animator.setDuration( duration );
        animator.addListener( new Animator.AnimatorListener( ) {
            @Override
            public void onAnimationStart( Animator animation ) {

            }

            @Override
            public void onAnimationEnd( Animator animation ) {
                if ( onPagineScrollViewListener != null ) {
                    onPagineScrollViewListener.onPagingChanged( PagingScrollView.this.page );
                }
            }

            @Override
            public void onAnimationCancel( Animator animation ) {

            }

            @Override
            public void onAnimationRepeat( Animator animation ) {

            }
        } );
        animator.start( );

    }


    public void notifyDataChanged( ) {

    }

    public interface OnPagineScrollViewListener {
        void onPagingChanged(int page);
    }

    public OnPagineScrollViewListener getOnPagineScrollViewListener( ) {
        return onPagineScrollViewListener;
    }

    public void setOnPagineScrollViewListener( OnPagineScrollViewListener onPagineScrollViewListener ) {
        this.onPagineScrollViewListener = onPagineScrollViewListener;
    }
}
