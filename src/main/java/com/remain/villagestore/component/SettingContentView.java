package com.remain.villagestore.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.info.PushInfo;
import com.remain.villagestore.manager.DisplayManager;
import com.remain.villagestore.util.ToggleAnimation;


/**
 * Created by woongjaelee on 15. 8. 1..
 */
public class SettingContentView extends LinearLayout {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    private Context context;

    // Widget
    private TextView textView;
    private RelativeLayout relativeLayoutMain;
    private RelativeLayout relativeLayoutSwitch;
    private ImageView imageViewSwitchPanel;
    private ImageView imageViewSwitch;
    private ImageView imageViewSearch;

    // Lang
    private boolean isOn;

    // Util
    private DisplayManager displayManager;

    private OnSettingClickListener settingClickListener;

    private SettingMenu settingMenu;
    private PushInfo pushInfo;


    public enum SettingMenu {

        /** 우리 단지  */
        MY_LOC,
        /** 푸쉬 알림 */
        PUSH_ALARM,
        /** 공지사항 */
        NOTICE,
        /** 제보하기 */
        REPORT,
        /** 로그 아웃 */
        LOGOUT,

    }
    //================================================================================
    // XXX Constructor
    //================================================================================

    public SettingContentView(Context context, SettingMenu settingMenu) {
        super(context);

        this.context = context;
        this.settingMenu = settingMenu;
        init();
    }

    public SettingContentView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        init();
    }


    //================================================================================
    // XXX Override Method
    //================================================================================

    //================================================================================
    // XXX Listener
    //================================================================================

    private OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            if(settingClickListener != null)settingClickListener.onClick(settingMenu);

            if(pushInfo != null && settingMenu == SettingMenu.PUSH_ALARM) {
                int direct =  isOn ? ToggleAnimation.LEFT : ToggleAnimation.RIGHT;
                int startPos = (Boolean) imageViewSwitch.getTag() ? ToggleAnimation.RIGHT : ToggleAnimation.LEFT;

                ToggleAnimation toggleAnimation = new ToggleAnimation(imageViewSwitch, imageViewSwitchPanel, direct, startPos, ToggleAnimation.SETTING_FROM_TO_RATIO);
                toggleAnimation.setBgOnId(R.drawable.switch_panel_on);
                imageViewSwitch.startAnimation(toggleAnimation);

                isOn = !isOn;
                pushInfo.setIsOn(settingMenu, isOn);
                pushInfo.saveIsOn();
            }
        }
    };

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    public void setOnSettingClickListener(OnSettingClickListener settingClickListener) {
        this.settingClickListener = settingClickListener;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================


    private void init() {

        inflate(context, R.layout.content_setting,this);

        displayManager = new DisplayManager(context);

        setOrientation(LinearLayout.HORIZONTAL);

        initUI();
        initUISize();
        setEventListener();

    }

    private void initUI() {

        relativeLayoutMain = (RelativeLayout)findViewById(R.id.content_setting_relativelayout_main);
        textView = (TextView)findViewById(R.id.content_setting_textview);
        relativeLayoutSwitch = (RelativeLayout)findViewById(R.id.content_setting_relativelayout_switch_panel);
        imageViewSwitch = (ImageView)findViewById(R.id.content_setting_imageview_switch);
        imageViewSwitchPanel = (ImageView)findViewById(R.id.content_setting_imageview_switch_panel);
        imageViewSearch = (ImageView)findViewById(R.id.content_setting_imageview_search);
    }

    private void initUISize() {

        LayoutParams linearParams = (LayoutParams) relativeLayoutMain.getLayoutParams();
        displayManager.changeWidthHeightSameRate(linearParams, ViewGroup.LayoutParams.MATCH_PARENT,75);
        linearParams.leftMargin = displayManager.getScaleSizeSameRate(35);
        linearParams.rightMargin = displayManager.getScaleSizeSameRate(35);

        RelativeLayout.LayoutParams relativeParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
        relativeParams.leftMargin = displayManager.getScaleSizeSameRate(86);

        RelativeLayout.LayoutParams param3 = (RelativeLayout.LayoutParams) relativeLayoutSwitch.getLayoutParams();
        param3.width = displayManager.getScaleSizeSameRate(113);
        param3.height = displayManager.getScaleSizeSameRate(50);
        param3.rightMargin = displayManager.getScaleSizeSameRate(56);

        RelativeLayout.LayoutParams param4 = (RelativeLayout.LayoutParams) imageViewSwitchPanel.getLayoutParams();
        param4.width = displayManager.getScaleSizeSameRate(113);
        param4.height = displayManager.getScaleSizeSameRate(44);

        RelativeLayout.LayoutParams param5 = (RelativeLayout.LayoutParams) imageViewSwitch.getLayoutParams();
        param5.width = displayManager.getScaleSizeSameRate(50);
        param5.height = displayManager.getScaleSizeSameRate(50);

        RelativeLayout.LayoutParams param6 = (RelativeLayout.LayoutParams) imageViewSearch.getLayoutParams();
        param6.width = displayManager.getScaleSizeSameRate(48);
        param6.height = displayManager.getScaleSizeSameRate(47);
    }

    private void setEventListener() {

        relativeLayoutMain.setOnClickListener(clickListener);

    }

    public void setData(String contentText, int topMargin) {

        relativeLayoutSwitch.setVisibility(View.GONE);

        if(settingMenu == SettingMenu.MY_LOC) {
            imageViewSearch.setVisibility(View.VISIBLE);
        }

        textView.setText(contentText);

        LayoutParams params = (LayoutParams)relativeLayoutMain.getLayoutParams();
        params.topMargin = displayManager.getScaleSizeSameRate(topMargin);

        RelativeLayout.LayoutParams relativeParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
        relativeParams.addRule(RelativeLayout.CENTER_IN_PARENT);
    }

    public void setPushData(String contentText, int topMargin) {

        relativeLayoutSwitch.setVisibility(View.VISIBLE);

        textView.setText(contentText);

        LayoutParams params = (LayoutParams)relativeLayoutMain.getLayoutParams();
        params.topMargin = displayManager.getScaleSizeSameRate(topMargin);

        pushInfo = new PushInfo(context);
        setIsOn();
    }

    public void setIsOn() {

        int switchId = 0;
        int switchBgId = 0;

        RelativeLayout.LayoutParams param1 = (RelativeLayout.LayoutParams) imageViewSwitch.getLayoutParams();
        if (pushInfo.isOn(settingMenu)) {

            switchId = R.drawable.switch_push;
            switchBgId = R.drawable.switch_panel_on;
            param1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        } else {

            switchId = R.drawable.switch_push;
            switchBgId = R.drawable.switch_panel_off;
            param1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        }

        if (pushInfo.isOn(settingMenu)) {
            param1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        } else {

        }
        imageViewSwitch.setBackgroundResource(switchId);
        displayManager.changeWidthHeightSameRate(param1, 50, 50);
        imageViewSwitch.setLayoutParams(param1);

        RelativeLayout.LayoutParams param2 = (RelativeLayout.LayoutParams) imageViewSwitchPanel.getLayoutParams();
        imageViewSwitchPanel.setBackgroundResource(switchBgId);
        displayManager.changeWidthHeightSameRate(param2, 113, 44);
        param2 = (RelativeLayout.LayoutParams) imageViewSwitchPanel.getLayoutParams();
        imageViewSwitchPanel.setLayoutParams(param2);

        imageViewSwitch.setTag(pushInfo.isOn(settingMenu));
        isOn = pushInfo.isOn(settingMenu);
    }

    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    public interface OnSettingClickListener {
        public void onClick(SettingMenu settingMenu);
    }

    //================================================================================
    // XXX Debug
    //================================================================================
}
