package com.remain.villagestore.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.manager.DisplayManager;

/**
 * Created by woongjaelee on 2015. 11. 11..
 */
public class StoreDetailView extends LinearLayout {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    private Context context;

    // Widget;
    private TextView textViewTitle;
    private TextView textViewContent;

    // Util
    private DisplayManager displayManager;

    //================================================================================
    // XXX Constructor
    //================================================================================

    public StoreDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        init();
    }

    public StoreDetailView(Context context) {
        super(context);

        this.context = context;
        init();
    }

    //================================================================================
    // XXX Override Method
    //================================================================================

    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================


    private void init() {

        setOrientation(LinearLayout.VERTICAL);
        inflate(context, R.layout.content_store_detail_view, this);

        displayManager = new DisplayManager(context);

        initUI();
        initUISize();
        setEventListener();

        setBackgroundResource(R.color.white);
    }

    private void initUI() {

        textViewTitle = (TextView)findViewById(R.id.content_store_detail_textview_title);
        textViewContent = (TextView)findViewById(R.id.content_store_detail_textview_content);

    }

    private void initUISize() {

        textViewTitle.setPadding(displayManager.getScaleSizeSameRate(10),displayManager.getScaleSizeSameRate(10),displayManager.getScaleSizeSameRate(10),displayManager.getScaleSizeSameRate(10));
        textViewContent.setPadding(displayManager.getScaleSizeSameRate(10),displayManager.getScaleSizeSameRate(10),displayManager.getScaleSizeSameRate(10),displayManager.getScaleSizeSameRate(10));

    }

    private void setEventListener() {

    }

    public void setView(String title,String content,int marginBottom) {

        textViewTitle.setText(title);
        textViewContent.setText(content);

        LayoutParams params = (LayoutParams) getLayoutParams();
        params.bottomMargin = displayManager.getScaleSizeSameRate(marginBottom);

        setLayoutParams(params);

    }

    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================

}
