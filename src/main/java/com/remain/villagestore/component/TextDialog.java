package com.remain.villagestore.component;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.manager.DisplayManager;

/*
	사용법)
	
	각 데이터를 set 하지 않을 경우 해당 뷰가 보이지 않음
	
	title : 제목
	message : 내용
	cancel : 취소 버튼명 재설정, 기본 버튼명 Cancel
	confirm : 확인 버튼명 재설정, 기본 버튼명 OK
	
	-
	
	setOnTextCancelClickListener : 취소 버튼 클릭 리스너 (이 리스너를 사용해야 취소 버튼이 보임)
	setOnTextConfirmClickListener : 확인 버튼 클릭 리스너 (이 리스너를 사용해야 확인 버튼이 보임)
	

	
	private void showDialogXXX(String title, String message, String cancel, String confirm) {
		
		if (dialogText == null || !dialogText.isShowing()) {
			
			dialogText = new TextDialog(context);
			dialogText.setOnTextCancelClickListener();
			dialogText.setOnTextConfirmClickListener();
			dialogText.setTitle(title);
			dialogText.setMessage(message);
			dialogText.setCancel(cancel);
			dialogText.setConfirm(confirm);
			dialogText.show();
		}
	}
*/
public class TextDialog extends Dialog {

	//================================================================================
    // XXX Constants
    //================================================================================
	
	//================================================================================
    // XXX Variables
    //================================================================================
	// Content
	private Context context;
	
	// Widget
	private LinearLayout linearLayoutTopPanel;
	private LinearLayout linearLayoutButtonPanel;
	private TextView textViewTitle;
	private TextView textViewMessage;
	private TextView textViewCancel;
	private TextView textViewConfirm;
	private View viewTitleDivider;
	private View viewButtonTopDivider;
	private View viewButtonDivider;

	// Lang
	private String title;
	private String message;
	/** 취소 버튼 텍스트 */
	private String cancel;
	/** 확인 버튼 텍스트 */
	private String confirm;
	/** 취소 버튼 사용 여부 */
	private boolean isShowCancel;
	/** 확인 버튼 사용 여부 */
	private boolean isShowConfirm;
	
	// Interface
	private OnTextCancelClickListener textCancelClickListener;
	private OnTextConfirmClickListener textConfirmClickListener;
	
	// Manager
	private DisplayManager displayManager;
	
	//================================================================================
    // XXX Constructor
    //================================================================================
	public TextDialog(Context context) {
		super(context);
		
		this.context = context;
		displayManager = new DisplayManager(context, AppInfo.STANDARD_WIDTH, AppInfo.STANDARD_HEIGHT);
	}
	
	//================================================================================
    // XXX Override Method
    //================================================================================
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_text);
		
		Window window = getWindow();
		window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		init();
	}

	//================================================================================
    // XXX Listener
    //================================================================================
	private View.OnClickListener clickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.dialog_text_textview_cancel:
				
				dismiss();
				
				if (textCancelClickListener != null) {
					textCancelClickListener.onClick();
				}
				break;
				
			case R.id.dialog_text_textview_confirm:
				
				dismiss();
				
				if (textConfirmClickListener != null) {
					textConfirmClickListener.onClick();
				}
				break;
			}
		}
	};

	//================================================================================
    // XXX Getter, Setter
    //================================================================================
	public void setTitle(int titleId) {
		setTitle(context.getResources().getString(titleId));
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setMessage(int messageId) {
		setMessage(context.getResources().getString(messageId));
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setCancel(int cancelId) {
		setCancel(context.getResources().getString(cancelId));
	}
	
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	
	public void setConfirm(int confirmId) {
		setConfirm(context.getResources().getString(confirmId));
	}
	
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}
	
	public void setOnTextCancelClickListener() {
		this.isShowCancel = true;
	}
	
	public void setOnTextConfirmClickListener() {
		this.isShowConfirm = true;
	}
	
	public void setOnTextCancelClickListener(OnTextCancelClickListener textCancelClickListener) {
		
		this.textCancelClickListener = textCancelClickListener;
		setOnTextCancelClickListener();
	}

	public void setOnTextConfirmClickListener(OnTextConfirmClickListener textConfirmClickListener) {
		
		this.textConfirmClickListener = textConfirmClickListener;
		setOnTextConfirmClickListener();
	}
	
	//================================================================================
    // XXX Etc Method
    //================================================================================
	private void init() {
		
		initUI();
		setEventListener();
		setUISize();
		
		setTitle();
		setMessage();
		setCancel();
		setConfirm();
		setVisibility();
	}

	private void initUI() {

		linearLayoutTopPanel = (LinearLayout) findViewById(R.id.dialog_text_linearlayout_top_panel);
		linearLayoutButtonPanel = (LinearLayout) findViewById(R.id.dialog_text_linearlayout_button_panel);
		textViewTitle = (TextView) findViewById(R.id.dialog_text_textview_title);
		textViewMessage = (TextView) findViewById(R.id.dialog_text_textview_message);
		textViewCancel = (TextView) findViewById(R.id.dialog_text_textview_cancel);
		textViewConfirm = (TextView) findViewById(R.id.dialog_text_textview_confirm);
		viewTitleDivider = (View) findViewById(R.id.dialog_text_view_title_divider);
		viewButtonTopDivider = (View) findViewById(R.id.dialog_text_view_button_top_divider);
		viewButtonDivider = (View) findViewById(R.id.dialog_text_view_button_divider);
	}
	
	private void setEventListener() {

		textViewCancel.setOnClickListener(clickListener);
		textViewConfirm.setOnClickListener(clickListener);
	}
	
	private void setUISize() {
		
		displayManager.changeWidthHeightSameRate(linearLayoutTopPanel.getLayoutParams(), 620, LayoutParams.WRAP_CONTENT);
		displayManager.changeWidthHeightSameRate(linearLayoutButtonPanel.getLayoutParams(), LayoutParams.MATCH_PARENT, 104);
		displayManager.changeWidthHeightSameRate(textViewTitle.getLayoutParams(), LayoutParams.MATCH_PARENT, 140);
		displayManager.changeWidthHeightSameRate(viewTitleDivider.getLayoutParams(), LayoutParams.MATCH_PARENT, 1);
		displayManager.changeWidthHeightSameRate(viewButtonTopDivider.getLayoutParams(), LayoutParams.MATCH_PARENT, 1);
		displayManager.changeWidthHeightSameRate(viewButtonDivider.getLayoutParams(), 1, LayoutParams.MATCH_PARENT);
	
		textViewTitle.setPadding((int)displayManager.getScaleSizeSameRate(42), 0, (int)displayManager.getScaleSizeSameRate(42), 0);
		textViewMessage.setMinHeight((int)displayManager.getScaleSizeSameRate(178));
		textViewMessage.setPadding((int)displayManager.getScaleSizeSameRate(42), (int)displayManager.getScaleSizeSameRate(42),(int) displayManager.getScaleSizeSameRate(42), (int)displayManager.getScaleSizeSameRate(42));
	}
	
	/**
	 * 제목 출력
	 */
	private void setTitle() {
		
		if (title != null) {
			
			textViewTitle.setVisibility(View.VISIBLE);
			viewTitleDivider.setVisibility(View.VISIBLE);
			textViewTitle.setText(title);
		}
	}

	/**
	 * 메시지 출력
	 */
	private void setMessage() {
		
		if (message != null) {
			
			textViewMessage.setVisibility(View.VISIBLE);
			textViewMessage.setText(message);
		}
	}
	
	/**
	 * 취소 버튼 텍스트 출력
	 */
	private void setCancel() {
		if (cancel != null) textViewCancel.setText(cancel);
	}
	
	/**
	 * 확인 버튼 텍스트 출력
	 */
	private void setConfirm() {
		if (confirm != null) textViewConfirm.setText(confirm);
	}
	
	/**
	 * 취소, 확인 버튼 설정
	 */
	private void setVisibility() {
		
		if (isShowCancel) {
			textViewCancel.setVisibility(View.VISIBLE);
		}
		
		if (isShowConfirm) {
			textViewConfirm.setVisibility(View.VISIBLE);
		}
		
		if (isShowCancel || isShowConfirm) {
			
			viewButtonTopDivider.setVisibility(View.VISIBLE);
			linearLayoutButtonPanel.setVisibility(View.VISIBLE);
		}
		
		if (isShowCancel && isShowConfirm) {
			viewButtonDivider.setVisibility(View.VISIBLE);
		}
	}
	
	//================================================================================
	// XXX Interface
	//================================================================================
	public interface OnTextCancelClickListener {
		public void onClick();
	}
	
	public interface OnTextConfirmClickListener {
		public void onClick();
	}
}
