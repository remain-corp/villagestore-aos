package com.remain.villagestore.component;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ProgressBar;

import com.remain.villagestore.R;

public class TransProgressDialog extends Dialog {
	
	private static TransProgressDialog dialog;

	public static TransProgressDialog show(Context context, boolean cancelable) {
		return show(context, cancelable, null);
	}

	public static TransProgressDialog show(Context context, boolean cancelable, OnCancelListener cancelListener) {
		
		// 다이얼로그를 띄울 액티비티가 종료되면 작업을 중지함
		if (((Activity) context).isFinishing()) {
			return null;
		}
		
		dialog = new TransProgressDialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(cancelable);
		dialog.setOnCancelListener(cancelListener);
		dialog.addContentView(new ProgressBar(context), new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		
		dialog.show(); 

		return dialog;
	}

	public void setLoadingTitle(String title) {
		dialog.setTitle(title);
	}

	private TransProgressDialog(Context context) {
		super(context, R.style.TransProgressDialog);
	}

	public void dismiss() {
		
		if (isShowing()) {
			super.dismiss();
		}
	}
}
