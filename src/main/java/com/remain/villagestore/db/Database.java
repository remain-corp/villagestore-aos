package com.remain.villagestore.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.remain.villagestore.dto.Delivery;
import com.remain.villagestore.dto.Eatery;
import com.remain.villagestore.dto.Store;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.DateFormat;
import com.remain.villagestore.util.Log;

import java.util.ArrayList;


public class Database implements BaseColumns {

    //================================================================================
    // XXX Constants
    //================================================================================
    // Singleton
    /**
     * Singleton을 위한 객체
     */
    private static Database database;

    // Database
    private static DatabaseHelper db;

    // String
    public static final String DOT = ".";
    public static final String EQUAL = " = ";
    public static final String AND = " AND ";
    public static final String SINGLE_QUOT = "'";
    public static final String SELECT_COUNT = "Select count(*) from ";

    // Lang
    public static final int FALSE = 0;
    public static final int TRUE = 1;
    public static final int MAX_STORE_SAVE_COUNT = 3;

    // Table Name
    public static final String TB_USER = "tb_user";
    public static final String TB_FAVORITE = "tb_favorite";
    public static final String TB_STORE = "tb_store";

    // Column Name
    public static final String _ID = "_id";

    // 유저
    /**
     * 유저 정보
     */
    public static final String TB_CREATE_USER =
            "CREATE TABLE " + TB_USER + " (" +
                    _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ParamInfo.USER_ID + " TEXT, " +
                    ParamInfo.DEVICE_ID + " TEXT, " +
                    ParamInfo.LATITUDE + " REAL, " +
                    ParamInfo.LONGITUDE + " REAL," +
                    ParamInfo.REG_DATE + " TEXT," +
                    ParamInfo.ADDRESS + " TEXT," +
                    ParamInfo.POINT + " INTEGER," +
                    ParamInfo.ADDRESS2 + " TEXT," +
                    ParamInfo.LOCAL_SEQ + " INTEGER);";

    public static final String TB_CREATE_FAVORITE =
            "CREATE TABLE " + TB_FAVORITE + " (" +
                    _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ParamInfo.SEQ + " TEXT);";

    public static final String TB_CREATE_STORE =
            "CREATE TABLE " + TB_STORE + " (" +
                    _ID + " INTEGER PRIMARY KEY, " +
                    ParamInfo.ST_NAME + " TEXT, " +
                    ParamInfo.ST_PHONE + " TEXT, " +
                    ParamInfo.ST_CATE + " INTEGER DEFAULT 1, " +
                    ParamInfo.PRICE + " INTEGER DEFAULT 0, " +
                    ParamInfo.IMAGE + " TEXT, " +
                    ParamInfo.CP_INFO + " TEXT, " +
                    ParamInfo.CP_LIMITE + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                    ParamInfo.REG_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP);";

    // Create Table List
    public static final String[] CREATE_TB_LIST = {

            TB_CREATE_USER,
            TB_CREATE_FAVORITE,
            TB_CREATE_STORE,
    };

    // Drop Table List
    public static final String[] DROP_TB_LIST = {

            "DROP TABLE IF EXISTS " + TB_USER,
            "DROP TABLE IF EXISTS " + TB_FAVORITE,
            "DROP TABLE IF EXISTS " + TB_STORE,
    };

    //================================================================================
    // XXX Variables
    //================================================================================

    //================================================================================
    // XXX Constructors
    //================================================================================

    /**
     * Database Singleton instance
     *
     * @return Database instance
     */
    public static Database getInstance() {

        if (database == null) {
            database = new Database();
        }

        return database;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================
    // 공통

    /**
     * 테이블의 모든 Row를 본다.
     *
     * @param table
     */
    public void selectTable(Context context, String table) {

        db = DatabaseHelper.getInstance(context);

        Cursor cursor = db.query(table, null);
        int colum = cursor.getColumnCount();
        Log.i(table);
        while (cursor.moveToNext()) {
            String a = "";
            for (int i = 0; i < colum; i++) {
                a += cursor.getString(i);
                a += "\t";
            }
            Log.i(a);
        }
        cursor.close();
    }

    /**
     * 테이블의 모든 Row를 본다.
     *
     * @param table
     */
    public int selectColumnCount(Context context, String table) {

        db = DatabaseHelper.getInstance(context);

        Cursor cursor = db.query(table, null);
        return cursor.getColumnCount();
    }

    /**
     * DB의 row에 값이 저장되어 있는지 확인한다.
     *
     * @return : DB에 저장되어 있으면 true, 없으면 false
     */
    public boolean isExistRow(Context context, String table) {

        db = DatabaseHelper.getInstance(context);

        String sql = SELECT_COUNT + table;
        Cursor cursor = db.rawQuery(sql);

        if (cursor.getCount() > 0) {
            cursor.close();
            return true;
        } else {
            cursor.close();
            return false;
        }
    }

    /**
     * Table drop 후 create
     *
     * @param context
     */
    private void createAfterDropTable(Context context, String dropTable, String createTable) {

        db = DatabaseHelper.getInstance(context);
        db.createAfterdrop(dropTable, createTable);
    }

    /**
     * long형을 int형으로 변환. (값이 바뀔 경우, exception 발생)
     *
     * @param l : 형변환을 할 long형 값
     * @return int형 값
     */
    private int convertSafeLongToInt(long l) {

        int i = (int) l;
        if ((long) i != l) {
            throw new IllegalArgumentException(l + " cannot be cast to int without changing its value.");
        }
        return i;
    }

    /**
     * Database 내용 모두를 삭제한 후, 다시 생성한다.
     */
    public void removeAll(Context context) {

        db = DatabaseHelper.getInstance(context);

        db.drop(null);
        db.create(null);
    }

    // XXX 유저
    public void checkUser(Context context, User user) {
        boolean isExist = isExistUser(context);
        if (isExist) {
            Log.i("DB 유저 업데이트");
            updateUser(context, user);
        } else {
            Log.i("DB 유저 저장");
            insertUser(context, user);
        }
    }

    /**
     * 유저가 DB에 저장되어 있는지 확인한다.
     *
     * @return : DB에 저장되어 있으면 true, 없으면 false
     */
    public boolean isExistUser(Context context) {

        db = DatabaseHelper.getInstance(context);

        String[] columns = {_ID};
        Cursor cursor = db.query(TB_USER, columns);
        if (cursor.moveToNext()) {
            cursor.close();
            return true;
        }
        cursor.close();
        return false;
    }

    public void deleteUser(Context context) {

        db = DatabaseHelper.getInstance(context);
        db.delete(TB_USER, null);

    }

    /**
     * 유저의 정보를 저장한다.
     *
     * @param context
     * @param user
     * @return
     */
    private int insertUser(Context context, User user) {

        db = DatabaseHelper.getInstance(context);

        ContentValues values = new ContentValues();
        values.put(_ID, user.getSeq());
        values.put(ParamInfo.USER_ID, user.getUserId());
        values.put(ParamInfo.DEVICE_ID, user.getDeviceId());
        values.put(ParamInfo.LATITUDE, user.getLatitude());
        values.put(ParamInfo.LONGITUDE, user.getLongitude());
        values.put(ParamInfo.REG_DATE, user.getAddress());
        values.put(ParamInfo.ADDRESS, user.getAddress());
        values.put(ParamInfo.POINT, user.getPoint());
        values.put(ParamInfo.ADDRESS2, user.getUserNickName());
        values.put(ParamInfo.LOCAL_SEQ, user.getLocalSeq());

        long id = db.insert(TB_USER, values);
        return convertSafeLongToInt(id);
    }

    /**
     * 유저의 정보를 업데이트한다.
     *
     * @param user
     */
    public void updateUser(Context context, User user) {

        db = DatabaseHelper.getInstance(context);

        ContentValues values = convertUserToContentValues(user);

        int count = db.update(TB_USER, values, null);
        Log.i("update = " + count);
        if (count < 1) {
            Log.e("DB 저장 안된 유저 _id = ");
        }
    }

    private ContentValues convertUserToContentValues(User user) {

        ContentValues values = new ContentValues();

        values.put(_ID, user.getSeq());
        values.put(ParamInfo.USER_ID, user.getUserId());
        values.put(ParamInfo.DEVICE_ID, user.getDeviceId());
        values.put(ParamInfo.LATITUDE, user.getLatitude());
        values.put(ParamInfo.LONGITUDE, user.getLongitude());
        values.put(ParamInfo.REG_DATE, user.getRegDate());
        values.put(ParamInfo.ADDRESS, user.getAddress());
        values.put(ParamInfo.POINT, user.getPoint());
        values.put(ParamInfo.ADDRESS2, user.getDetailAddress());
        values.put(ParamInfo.LOCAL_SEQ, user.getLocalSeq());

        return values;
    }

    /**
     * 일련번호로 유저의 정보를 가져온다.
     *
     * @return 일련번호에 일치하는 정보가 없으면 null을 반환
     */
    public User getUser(Context context) {

        db = DatabaseHelper.getInstance(context);

        User user = null;
        Cursor cursor = db.query(TB_USER, null);
        if (cursor.moveToNext()) {
            user = convertCursorToUser(cursor);
        } else {
            Log.e("DB 저장 안된 유저 _id = ");
        }
        cursor.close();

        return user;
    }

    /**
     * 현재 커서에서 유저의 정보를 가져온다.
     *
     * @param cursor
     * @return : 커서에 있는 유저의 정보
     */
    private User convertCursorToUser(Cursor cursor) {

        int seq = getInt(cursor, _ID);
        String userId = getString(cursor, ParamInfo.USER_ID);
        String deviceId = getString(cursor, ParamInfo.DEVICE_ID);
        double latitude = getDouble(cursor, ParamInfo.LATITUDE);
        double longitude = getDouble(cursor, ParamInfo.LONGITUDE);
        String regDate = getString(cursor, ParamInfo.REG_DATE);
        String address = getString(cursor, ParamInfo.ADDRESS);
        int point = getInt(cursor, ParamInfo.POINT);
        String detailAddress = getString(cursor, ParamInfo.ADDRESS2);
        int localSeq = getInt(cursor, ParamInfo.LOCAL_SEQ);

        return new User(seq, userId, deviceId, latitude, longitude, regDate, address, point, detailAddress, localSeq);
    }

    private int getStoreCount(Context context) {

        int count = 0;
        db = DatabaseHelper.getInstance(context);

        String[] columns = {_ID};

        Cursor cursor = db.query(TB_STORE, columns, null);
        if (cursor.moveToNext()) {
            count++;
        }

        cursor.close();
        return count;

    }

    // 매장 저장
    public void checkStore(Context context, Store store) {

        boolean isExist = isExistStore(context, store.getSeq());

        if (isExist) {
            Log.i("DB 매장 업데이트");
            updateStore(context, store);
        } else {

            Log.i("DB 매장 저장");

            if (getStoreCount(context) >= MAX_STORE_SAVE_COUNT) {

                Log.i("최대 갯수 이미 도달 가장 오래된 데이터 지우고 추가 ");
                storeRemoveAndInsert(context, store);
            } else {

                Log.i("새로 매장 추가 ");
                insertStore(context, store);
            }
        }
    }

    /**
     * 매장가 DB에 저장되어 있는지 확인한다.
     *
     * @return : DB에 저장되어 있으면 true, 없으면 false
     */
    public boolean isExistStore(Context context, int storeSeq) {

        db = DatabaseHelper.getInstance(context);

        String[] columns = {_ID};
        String where = _ID + EQUAL + storeSeq;
        Cursor cursor = db.query(TB_STORE, columns, where);
        if (cursor.moveToNext()) {
            cursor.close();
            return true;
        }
        cursor.close();
        return false;
    }

    private void storeRemoveAndInsert(Context context, Store store) {

        boolean isDelete = false;
        db = DatabaseHelper.getInstance(context);

        String[] columns = {_ID};
        String order = " datetime(reg_date) ";

        Cursor cursor = db.query(TB_STORE, columns, null, null, null, null, order);
        if (!isDelete && cursor.moveToNext()) {

            deleteStore(context, getInt(cursor, _ID));
            isDelete = true;
            cursor.close();
        }

        insertStore(context, store);


    }

    /**
     * 매장의 정보를 저장한다.
     *
     * @param context
     * @return
     */
    private int insertStore(Context context, Store store) {

        db = DatabaseHelper.getInstance(context);

        ContentValues values = new ContentValues();
        values.put(_ID, store.getSeq());
        values.put(ParamInfo.ST_NAME, store.getName());
        values.put(ParamInfo.ST_PHONE, store.getPhoneNumber());
        values.put(ParamInfo.ST_CATE, store.getStoreType().ordinal());
        if (store.getArrayListImgUri() != null && store.getArrayListImgUri().size() > 0) {

            values.put(ParamInfo.IMAGE, store.getArrayListImgUri().get(0));

        } else {

            values.put(ParamInfo.IMAGE, "");
        }
        values.put(ParamInfo.CP_INFO, store.getCouponInfo());
        values.put(ParamInfo.CP_LIMITE, store.getCouponLimit());
        values.put(ParamInfo.REG_DATE, DateFormat.getCurrentTime());
        values.put(ParamInfo.PRICE, store.getPrice());

        Log.i("디비에서 넣어줄  : seq : " + store.getSeq());
        Log.i("디비에서 넣어줄 : name : " + store.getName());
        Log.i("디비에서 넣어줄 : cate : " + store.getStoreType().ordinal());
        //Log.i("디비에서 넣어줄 : img : " + store.getArrayListImgUri().get(0));
        Log.i("디비에서 넣어줄 : cpInfo : " + store.getCouponInfo());
        Log.i("디비에서 넣어줄 : cpLimite : " + store.getCouponLimit());
        Log.i("디비에서 넣어줄 : regDate : " + DateFormat.getCurrentTime());
        Log.i("디비에서 넣어줄 : price : " + store.getPrice());

        long id = db.insert(TB_STORE, values);
        return convertSafeLongToInt(id);
    }

    /**
     * 매장의 정보를 업데이트한다.
     */
    public void updateStore(Context context, Store store) {

        db = DatabaseHelper.getInstance(context);

        ContentValues values = convertStoreToContentValues(store);

        String where = _ID + EQUAL + store.getSeq();

        int count = db.update(TB_STORE, values, where);
        Log.i("update = " + count);
        if (count < 1) {
            Log.e("DB 저장 안된 매장 _id = ");
        }
    }

    private ContentValues convertStoreToContentValues(Store store) {

        ContentValues values = new ContentValues();

        values.put(_ID, store.getSeq());
        values.put(ParamInfo.ST_NAME, store.getName());
        values.put(ParamInfo.ST_PHONE, store.getPhoneNumber());
        values.put(ParamInfo.ST_CATE, store.getStoreType().ordinal());
        if (store.getArrayListImgUri() != null && store.getArrayListImgUri().size() > 0) {

            values.put(ParamInfo.IMAGE, store.getArrayListImgUri().get(0));

        }
        values.put(ParamInfo.CP_INFO, store.getCouponInfo());
        values.put(ParamInfo.CP_LIMITE, store.getCouponLimit());
        values.put(ParamInfo.REG_DATE, DateFormat.getCurrentTime());
        values.put(ParamInfo.PRICE, store.getPrice());

        return values;
    }

    public ArrayList<Store> getStoreList(Context context) {

        ArrayList<Store> arrayListStore = new ArrayList<Store>();
        String orderBy = " datetime(reg_date) ";

        Cursor cursorStore = db.query(TB_STORE, null, null, null, null, null, orderBy);
        while (cursorStore.moveToNext()) {
            arrayListStore.add(convertSursorToStore(cursorStore));
        }
        cursorStore.close();

        return arrayListStore;
    }

    private Delivery convertSursorToStore(Cursor cursor) {

        int seq = getInt(cursor, _ID);
        String name = getString(cursor, ParamInfo.ST_NAME);
        String phone = getString(cursor, ParamInfo.ST_PHONE);
        int cate = getInt(cursor, ParamInfo.ST_CATE);
        String img = getString(cursor, ParamInfo.IMAGE);
        String cpInfo = getString(cursor, ParamInfo.CP_INFO);
        String cpLimite = getString(cursor, ParamInfo.CP_LIMITE);
        String regDate = getString(cursor, ParamInfo.REG_DATE);
        int price = getInt(cursor, ParamInfo.PRICE);

        Log.i("디비에서 꺼내온 : seq : " + seq);
        Log.i("디비에서 꺼내온 : name : " + name);
        Log.i("디비에서 꺼내온 : cate : " + cate);
        Log.i("디비에서 꺼내온 : img : " + img);
        Log.i("디비에서 꺼내온 : cpInfo : " + cpInfo);
        Log.i("디비에서 꺼내온 : cpLimite : " + cpLimite);
        Log.i("디비에서 꺼내온 : regDate : " + regDate);
        Log.i("디비에서 꺼내온 : price : " + price);
        return new Delivery(seq, name, phone, cate, img, cpInfo, cpLimite, regDate, price);
    }

    public void deleteStore(Context context, int storeSeq) {

        db = DatabaseHelper.getInstance(context);

        String where = _ID + EQUAL + storeSeq;

        db.delete(TB_STORE, where);

    }

    public void deleteStore(Context context) {

        db = DatabaseHelper.getInstance(context);

        db.delete(TB_STORE, null);

    }


    // XXX 컬럼 값 가져오기

    /**
     * 현재 커서에서 column 이름에 해당하는 값을 가져온다.
     *
     * @param cursor
     * @param column
     * @return : 해당 컬럼에 해당하는 int값
     */
    private int getInt(Cursor cursor, String column) {
        return cursor.getInt(cursor.getColumnIndex(column));
    }

    /**
     * 현재 커서에서 column 이름에 해당하는 값을 가져온다.
     *
     * @param cursor
     * @param column
     * @return : 해당 컬럼에 해당하는 long값
     */
    private long getLong(Cursor cursor, String column) {
        return cursor.getLong(cursor.getColumnIndex(column));
    }

    /**
     * 현재 커서에서 column 이름에 해당하는 값을 가져온다.
     *
     * @param cursor
     * @param column
     * @return : 해당 컬럼에 해당하는 boolean값
     */
    private boolean getBoolean(Cursor cursor, String column) {
        int trueFalse = getInt(cursor, column);
        if (trueFalse == FALSE) return false;
        else return true;
    }

    /**
     * 현재 커서에서 column 이름에 해당하는 값을 가져온다.
     *
     * @param cursor
     * @param column
     * @return : 해당 컬럼에 해당하는 String값
     */
    private double getDouble(Cursor cursor, String column) {
        return cursor.getDouble(cursor.getColumnIndex(column));
    }

    /**
     * 현재 커서에서 column 이름에 해당하는 값을 가져온다.
     *
     * @param cursor
     * @param column
     * @return : 해당 컬럼에 해당하는 String값
     */
    private String getString(Cursor cursor, String column) {
        return cursor.getString(cursor.getColumnIndex(column));
    }
}
