package com.remain.villagestore.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import com.remain.villagestore.util.Log;

import java.util.ArrayList;


public class DatabaseHelper extends SQLiteOpenHelper {
	 
	//================================================================================
    // XXX Variables
    //================================================================================
	/** Singleton을 위한 객체 */
	private static DatabaseHelper helperInstance;
	/** a read/write database object (until close() is called) */
	private static SQLiteDatabase db;
	
	//================================================================================
    // XXX Constants
    //================================================================================
	/** _ID */
	public static final String _ID = "_id";
	/** Database Name */
	public static final String DB_NAME = "villagestore.db";		// FIXME db파일이름 입력 ex) appname.db
	/** Database Version */
	public static final int DB_VERSION = 1;					// FIXME db버전 입력 ex) 1
	
	//================================================================================
    // XXX Constructors
    //================================================================================
	private DatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}
	
	//================================================================================
    // XXX Getter, Setter
    //================================================================================
	/**
	 * DatabaseHelper Singleton instance
	 * @param context
	 * @return DatabaseHelper instance
	 */
	public static DatabaseHelper getInstance(Context context) {
		if (helperInstance == null) {
			helperInstance = new DatabaseHelper(context);
			db = helperInstance.getWritableDatabase();
		}
		return helperInstance;
	}
	
	//================================================================================
    // XXX Override Method
    //================================================================================
	/**
	 * 데이터베이스가 처음 만들어질때 불리는 메소드.
	 * Table 초기 세팅을 담당한다.
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		create(db);
	}

	/**
	 * 데이터베이스 업데이트가 필요할때 불리는 메소드.
	 * 기존 DB를 삭제하고 다시 만들거나, DB를 수정한다.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		drop(db);
		onCreate(db);
	}
	
	//================================================================================
    // XXX Etc Method
    //================================================================================
	/**
	 * SQLiteDatabase 읽고 쓸 수 있는 객체 닫기
	 */
	public void close() {
		if (helperInstance != null) {
			db.close();
			helperInstance = null;
		}
	}
	
	/**
	 * 하나의 SQL문을 실행시켜주는 역할을 한다.
	 * @param db
	 * @param sql : 실행할 SQL문(String)
	 */
	private void execMultipleSQL(SQLiteDatabase db, String sql){
		if (sql.trim().length() > 0) db.execSQL(sql);
	}
	
	/**
	 * 여러개의 SQL문을 차례대로 실행시켜주는 역할을 한다.
	 * @param db
	 * @param sql : 실행할 SQL문(String)의 배열
	 */
	private void execMultipleSQL(SQLiteDatabase db, String[] sql){
		for (String str : sql) {
			if (str.trim().length() > 0) db.execSQL(str);
		}
	}
	
	/**
	 * 여러개의 SQL문을 차례대로 실행시켜주는 역할을 한다.
	 * @param db
	 * @param sql : 실행할 SQL문(String)의 배열
	 */
	private void execMultipleSQL(SQLiteDatabase db, ArrayList<String> sql){
		for (String str : sql) {
			if (str.trim().length() > 0) db.execSQL(str);
		}
	}
	
	/**
	 * 모든 테이블 삭제
	 * @param db
	 */
	public void drop(SQLiteDatabase db) {
		if (db == null) db = DatabaseHelper.db;
		db.beginTransaction();
		try {
//			Table Drop문 여러개 생성후  dropTables에 넣어준다

//			Table Drop문의 예)
//			String dropEmoticon = "DROP TABLE IF EXISTS " + DatabaseManager.TB_EMOTICON;
			
			execMultipleSQL(db, Database.DROP_TB_LIST);
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			Log.e("SQL Error (DBHelper drop)");
			e.printStackTrace();
			return;
		} finally {
			db.endTransaction();
		}
	}
	
	/**
	 * 테이블 삭제 후 생성
	 */
	public void createAfterdrop(String dropTable, String createTable) {
		db.beginTransaction();
		try {
			String dropCountry = "DROP TABLE IF EXISTS " + dropTable;
			execMultipleSQL(db, dropCountry);
			execMultipleSQL(db, createTable);
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			Log.e("SQL Error (DBHelper drop)");
			e.printStackTrace();
			return;
		} finally {
			db.endTransaction();
		}
	}
	
	/**
	 * 테이블 생성
	 * @param db
	 */
	public void create(SQLiteDatabase db) {
		if (db == null) db = DatabaseHelper.db;
		
//		Table Create문 여러개 생성후  createTables에 넣어준다

//		Table Create문의 예)
//		String createUserAlbumImage = "CREATE TABLE " + DatabaseManager.TB_USER_ALBUM_IMAGE + " (" +
//				DatabaseManager._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
//				DatabaseManager.USER_SEQ + " INTEGER NOT NULL REFERENCES " + DatabaseManager.TB_USER + "(" + DatabaseManager._ID + "), " +
//				DatabaseManager.BLOG_SEQ + " INTEGER, " +
//				DatabaseManager.REG_DATE + " DATE, " +
//				DatabaseManager.IMAGE_ORIGIN + " TEXT, " +
//				DatabaseManager.IMAGE_THUMB + " TEXT, " +
//				DatabaseManager.BLOG_CONTENT + " TEXT);";
		
		db.beginTransaction();
		try {
			execMultipleSQL(db, Database.CREATE_TB_LIST);
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			Log.e("SQL Error (DBHelper create)");
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
	}

	// Query 관련 Method
	/**
	 * DB에서 Select하는 메소드. 조건(where문)없이 테이블명과 컬럼명만으로 검색한다.
	 * @param table : table name
	 * @param columns : column name array
	 * @return : cursor
	 */
	public Cursor query(String table, String[] columns) {
		return db.query(table, columns, null, null, null, null, null);
	}
	
	/**
	 * DB에서 Select하는 메소드. 조건은 테이블의 PK로 해당하는 값으로 _ID로 고정된다.
	 * 테이블명과 컬럼명, 찾을 레코드의 PK값으로 겁색한다.
	 * @param table : table name
	 * @param columns : column name array (null이면 모든 컬럼을 반환한다.)
	 * @param id : record id (pk 컬러명은 "_id" 만 가능함)
	 * @return : cursor
	 */
	public Cursor query(String table, String[] columns, long id) {
		return query(table, columns, _ID + "=" + id, null, null, null, null);
	}
	
	/**
	 * DB에서 Select하는 메소드. 조건을 String값으로 설정한다.
	 * @param table : table name
	 * @param columns : column name array (null이면 모든 컬럼을 반환한다.)
	 * @param where : 조건절(ex.phone_number='01011112222')
	 * @return : cursor
	 */
	public Cursor query(String table, String[] columns, String where) {
		return query(table, columns, where, null, null, null, null);
	}
	
	/**
	 * DB에서 Select하는 메소드. 쿼리에 넣을 수 있는 모든 내용을 넣어 검색한다.
	 * @param table : table name
	 * @param columns : column name array (null이면 모든 컬럼을 반환한다.)
	 * @param selection : where절
	 * @param selectionArgs : where절에 해당하는 값
	 * @param groupBy : 그룹 지음
	 * @param having : groupBy의 조건
	 * @param orderBy : 정렬
	 * @return : cursor
	 */
	public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, 
			String groupBy, String having, String orderBy) {
		return query(table, columns, selection, selectionArgs, groupBy, having, orderBy, null);
	}
	
	/**
	 * DB에서 Select하는 메소드. 쿼리에 넣을 수 있는 모든 내용을 넣어 검색한다.
	 * @param table : table name
	 * @param columns : column name array (null이면 모든 컬럼을 반환한다.)
	 * @param selection : where절
	 * @param selectionArgs : where절에 해당하는 값
	 * @param groupBy : 그룹 지음
	 * @param having : groupBy의 조건
	 * @param orderBy : 정렬
	 * @param limit : 제한
	 * @return
	 */
	public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, 
			String groupBy, String having, String orderBy, String limit) {
		return db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
	}
	
	/**
	 * SQLiteQueryBuilder를 이용하여 쿼리를 실행한다.
	 * @param table : table name. join 등을 포함할 수 있다.
	 * @param columns : column name array (null이면 모든 컬럼을 반환한다.)
	 * @param selection : where절
	 * @param selectionArgs : where절에 해당하는 값
	 * @param groupBy : 그룹 지음
	 * @param having : groupBy의 조건
	 * @param sortOrder orderBy : 정렬
	 * @param limit : 제한
	 * @return : cursor
	 */
	public Cursor queryFromBuilder(String table,String[] columns, String selection, String[] selectionArgs,
			String groupBy, String having, String sortOrder, String limit) {
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		builder.setTables(table);
		return builder.query(db, columns, selection, selectionArgs, groupBy, having, sortOrder, limit);
	}

	/**
	 * String으로 된 쿼리문을 실행시킨다.
	 * @param sql : sql statements
	 * @return : cursor
	 */
	public Cursor rawQuery(String sql) {
		return db.rawQuery(sql, null);
	}
	
	/**
	 * String으로 된 쿼리문을 실행시킨다.
	 * @param sql : sql statements
	 * @param selection : ?에 해당하는 값들
	 * @return : cursor
	 */
	public Cursor rawQuery(String sql, String[] selection) {
		return db.rawQuery(sql, selection);
	}
	
	/**
	 * 레코드를 해당 테이블에 삽입한다.
	 * @param table : table name
	 * @param values : ContentValues instance
	 * @return : rowId
	 */
	public long insert(String table, ContentValues values) {
		return db.insert(table, null, values);
	}

	/**
	 * 해당 테이블의 id값을 가진 레코드의 값을 수정한다.
	 * @param table : table name
	 * @param values : ContentValues instance
	 * @param id : record id
	 * @return : 바뀐 레코드의 수
	 */
	public int update(String table, ContentValues values, long id) {
		return db.update(table, values, _ID + "=" + id, null);
	}

	/**
	 * 해당 테이블에서 조건절에 해당하는 레코드의 값을 수정한다.
	 * @param table : table name
	 * @param values : ContentValues instance
	 * @param whereClause : Where Clause
	 * @return : 바뀐 레코드 수
	 */
	public int update(String table, ContentValues values, String whereClause) {
		return db.update(table, values, whereClause, null);
	}

	/**
	 * 해당 테이블에서 조건절에 해당하는 레코드를 삭제한다.
	 * @param table : table name
	 * @param whereClause : Where Clause
	 * @return : 삭제된 레코드 수
	 */
	public int delete(String table, String whereClause) {
		return db.delete(table, whereClause, null);
	}

	/**
	 * 해당 테이블의 id값을 가진 레코드를 삭제한다.
	 * @param table : table name
	 * @param id : record id
	 * @return : 삭제된 레코드 수
	 */
	public int delete(String table, long id) {
		return db.delete(table, _ID + "=" + id, null);
	}
}
