package com.remain.villagestore.dto;

import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by woongjaelee on 2016. 10. 1..
 */
public class Address {

    /** 주소 seq */
    private int addr_seq;
    /** 이름 */
    private String name;
    /** 부모 seq */
    private int p_addr_seq;

    public enum AddrType{
        SIDO,
        GUGUN,
        DONG
    }

    public Address () {}

    public Address(int addr_seq, String name) {
        this.addr_seq = addr_seq;
        this.name = name;
    }

    public Address(JSONObject object) throws JSONException {

        if(!object.isNull(ParamInfo.NAME)){

            this.addr_seq = object.getInt(ParamInfo.SEARCH_SEQ);
            this.name = object.getString(ParamInfo.NAME);

        }
        Log.e("pAddrSeq : "+object.getInt(ParamInfo.P_ADDR_SEQ));
        if(!object.isNull(ParamInfo.P_ADDR_SEQ)) {
            this.p_addr_seq = object.getInt(ParamInfo.P_ADDR_SEQ);
        }
    }

    public int getAddr_seq() {
        return addr_seq;
    }

    public void setAddr_seq(int addr_seq) {
        this.addr_seq = addr_seq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getP_addr_seq() {
        return p_addr_seq;
    }

    public void setP_addr_seq(int p_addr_seq) {
        this.p_addr_seq = p_addr_seq;
    }
}
