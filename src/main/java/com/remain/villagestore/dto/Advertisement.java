package com.remain.villagestore.dto;

import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.ParamInfo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by woongjaelee on 15. 9. 11..
 */
public class Advertisement {

    private int seq;
    private String prvwUrl;
    private String linkUrl;

    public Advertisement(JSONObject object) throws JSONException {

        if(!object.isNull(ParamInfo.SEQ)) seq = object.getInt(ParamInfo.SEQ);
        if(!object.isNull("fileName")) {
            String fileName = object.getString("fileName");

            StringBuffer stringBuffer = new StringBuffer(AppInfo.IMG_AD_PATH);

            if(fileName != null && !fileName.equals(AppInfo.DEFAULT_STRING)) {
                prvwUrl = stringBuffer.append(fileName).toString();
            }else {
                prvwUrl = null;
            }
        }

        if(!object.isNull(ParamInfo.FILEURL)) {
            linkUrl = object.getString(ParamInfo.FILEURL);
        }
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getPrvwUrl() {
        return prvwUrl;
    }

    public void setPrvwUrl(String prvwUrl) {
        this.prvwUrl = prvwUrl;
    }
}
