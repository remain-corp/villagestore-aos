package com.remain.villagestore.dto;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by woongjaelee on 2015. 12. 3..
 */
public class Cafe extends Eatery {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    //================================================================================
    // XXX Constructor
    //================================================================================

    public Cafe() {}


    public Cafe(JSONObject object) throws JSONException {

        super(object);
        storeType = StoreType.CAFE;

    }

    public Cafe(Parcel parcel) {

        super(parcel);

    }

    public static final Creator<Cafe> CREATOR = new Creator<Cafe>() {

        @Override
        public Cafe[] newArray(int size) {
            return new Cafe[size];
        }

        @Override
        public Cafe createFromParcel(Parcel source) {
            return new Cafe(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        super.writeToParcel(dest, flags);
    }


    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================
}
