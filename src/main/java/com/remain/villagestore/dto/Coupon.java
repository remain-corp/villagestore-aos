package com.remain.villagestore.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.remain.villagestore.info.ParamInfo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by woongjaelee on 2015. 11. 25..
 */
public class Coupon implements Parcelable{

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    private int couponSeq;
    /* 사용 기간 */
    private String startDate;
    /* 사용 기간 */
    private String endDate;
    /* 쿠폰 내용 */
    private String couponDetail;
    /* 쿠폰 사용 조건 */
    private String useTerm;

    //================================================================================
    // XXX Constructor
    //================================================================================

    public Coupon() {

    }

    public Coupon(int couponSeq, String startDate, String endDate, String couponDetail, String useTerm) {
        this.couponSeq = couponSeq;
        this.startDate = startDate;
        this.endDate = endDate;
        this.couponDetail = couponDetail;
        this.useTerm = useTerm;
    }

    public Coupon(JSONObject object) throws JSONException {

        if(!object.isNull(ParamInfo.CP_SEQ)) {
            couponSeq = object.getInt(ParamInfo.CP_SEQ);
        }

        if(!object.isNull(ParamInfo.CP_INFO)) {
            couponDetail = object.getString(ParamInfo.CP_INFO);
        }

        if(!object.isNull(ParamInfo.CP_STARTDATE)) {
            startDate = object.getString(ParamInfo.CP_STARTDATE);
        }

        if(!object.isNull(ParamInfo.CP_ENDDATE)) {
            endDate = object.getString(ParamInfo.CP_ENDDATE);
        }
    }

    public Coupon(Parcel parcel) {

        couponSeq = parcel.readInt();
        startDate = parcel.readString();
        endDate = parcel.readString();
        couponDetail = parcel.readString();
        useTerm = parcel.readString();
    }

    public static final Creator<Coupon> CREATOR = new Creator<Coupon>() {

        @Override
        public Coupon[] newArray(int size) {
            return new Coupon[size];
        }

        @Override
        public Coupon createFromParcel(Parcel source) {
            return new Coupon(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(couponSeq);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeString(couponDetail);
        dest.writeString(useTerm);
    }

    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================


    public int getCouponSeq() {
        return couponSeq;
    }

    public void setCouponSeq(int couponSeq) {
        this.couponSeq = couponSeq;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCouponDetail() {
        return couponDetail;
    }

    public void setCouponDetail(String couponDetail) {
        this.couponDetail = couponDetail;
    }

    public String getUseTerm() {
        return useTerm;
    }

    public void setUseTerm(String useTerm) {
        this.useTerm = useTerm;
    }
}
