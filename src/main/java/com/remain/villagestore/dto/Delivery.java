package com.remain.villagestore.dto;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by woongjaelee on 2016. 1. 18..
 */
public class Delivery extends Store {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    /* 메뉴 */
    protected ArrayList<StoreMenu> arrayListMenu;
    /* 쿠폰 */
    protected ArrayList<Coupon> arrayListCoupon;

    //================================================================================
    // XXX Constructor
    //================================================================================

    public Delivery() {}

    public Delivery(int seq , String name , String phone, int cate,String img, String couponInfo , String couponLimit,String regDate,int price) {

        super(seq, name, phone, cate, img, couponInfo, couponLimit, regDate,price);
    }

    public Delivery(JSONObject object) throws JSONException {

        super(object);
        storeType = StoreType.DELIVERY;

    }

    public Delivery(Parcel parcel) {

        super(parcel);

        this.arrayListMenu = new ArrayList<StoreMenu>();
        parcel.readTypedList(this.arrayListMenu, StoreMenu.CREATOR);

        this.arrayListCoupon = new ArrayList<Coupon>();
        parcel.readTypedList(this.arrayListCoupon, Coupon.CREATOR);
    }

    public static final Creator<Delivery> CREATOR = new Creator<Delivery>() {

        @Override
        public Delivery[] newArray(int size) {
            return new Delivery[size];
        }

        @Override
        public Delivery createFromParcel(Parcel source) {
            return new Delivery(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        super.writeToParcel(dest,flags);
        dest.writeTypedList(arrayListMenu);
        dest.writeTypedList(arrayListCoupon);
    }


    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    public ArrayList<StoreMenu> getArrayListMenu() {
        return arrayListMenu;
    }

    public void setArrayListMenu(ArrayList<StoreMenu> arrayListMenu) {
        this.arrayListMenu = arrayListMenu;
    }

    public int getTotalPrice() {

        int totalPrice = 0;
        for (StoreMenu storeMenu : arrayListMenu) {
            if (storeMenu.getCount() > 0)
                totalPrice += storeMenu.getCount() * storeMenu.getPrice();
        }

        return totalPrice;
    }

    public ArrayList<Coupon> getArrayListCoupon() {
        return arrayListCoupon;
    }

    public void setArrayListCoupon(ArrayList<Coupon> arrayListCoupon) {
        this.arrayListCoupon = arrayListCoupon;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================
}
