package com.remain.villagestore.dto;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by woongjaelee on 2015. 12. 3..
 */
public class Eatery extends Store {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    /* 메뉴 */
    protected ArrayList<StoreMenu> arrayListMenu;
    /* 쿠폰 */
    protected ArrayList<Coupon> arrayListCoupon;

    //================================================================================
    // XXX Constructor
    //================================================================================

    public Eatery() {}

    public Eatery(JSONObject object) throws JSONException {

        super(object);
        storeType = StoreType.EATERY;

    }

    public Eatery(Parcel parcel) {

        super(parcel);

        this.arrayListMenu = new ArrayList<StoreMenu>();
        parcel.readTypedList(this.arrayListMenu, StoreMenu.CREATOR);

        this.arrayListCoupon = new ArrayList<Coupon>();
        parcel.readTypedList(this.arrayListCoupon, Coupon.CREATOR);
    }

    public static final Creator<Eatery> CREATOR = new Creator<Eatery>() {

        @Override
        public Eatery[] newArray(int size) {
            return new Eatery[size];
        }

        @Override
        public Eatery createFromParcel(Parcel source) {
            return new Eatery(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        super.writeToParcel(dest,flags);
        dest.writeTypedList(arrayListMenu);
        dest.writeTypedList(arrayListCoupon);
    }


    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    public ArrayList<StoreMenu> getArrayListMenu() {
        return arrayListMenu;
    }

    public void setArrayListMenu(ArrayList<StoreMenu> arrayListMenu) {
        this.arrayListMenu = arrayListMenu;
    }

    public int getTotalPrice() {

        int totalPrice = 0;
        for (StoreMenu storeMenu : arrayListMenu) {
            if (storeMenu.getCount() > 0)
                totalPrice += storeMenu.getCount() * storeMenu.getPrice();
        }

        return totalPrice;
    }

    public ArrayList<Coupon> getArrayListCoupon() {
        return arrayListCoupon;
    }

    public void setArrayListCoupon(ArrayList<Coupon> arrayListCoupon) {
        this.arrayListCoupon = arrayListCoupon;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================
}
