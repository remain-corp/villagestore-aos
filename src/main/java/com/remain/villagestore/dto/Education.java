package com.remain.villagestore.dto;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by woongjaelee on 2015. 12. 3..
 */
public class Education extends Store {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    /* 메뉴 */
    protected ArrayList<StoreMenu> arrayListMenu;

    //================================================================================
    // XXX Constructor
    //================================================================================

    public Education() {}


    public Education(JSONObject object) throws JSONException {

        super(object);
        storeType = StoreType.EDUCATION;

    }

    public Education(Parcel parcel) {

        super(parcel);

    }

    public static final Creator<Education> CREATOR = new Creator<Education>() {

        @Override
        public Education[] newArray(int size) {
            return new Education[size];
        }

        @Override
        public Education createFromParcel(Parcel source) {
            return new Education(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        super.writeToParcel(dest, flags);
    }


    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================
}
