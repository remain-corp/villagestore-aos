package com.remain.villagestore.dto;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by woongjaelee on 2015. 12. 14..
 */
public class Life extends Store {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    //================================================================================
    // XXX Constructor
    //================================================================================

    public Life() {}


    public Life(JSONObject object) throws JSONException {

        super(object);
        storeType = StoreType.LIFE;
    }

    public Life(Parcel parcel) {

        super(parcel);

    }

    public static final Creator<Life> CREATOR = new Creator<Life>() {

        @Override
        public Life[] newArray(int size) {
            return new Life[size];
        }

        @Override
        public Life createFromParcel(Parcel source) {
            return new Life(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        super.writeToParcel(dest, flags);
    }


    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================
}
