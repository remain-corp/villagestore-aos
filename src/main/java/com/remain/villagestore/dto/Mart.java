package com.remain.villagestore.dto;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by woongjaelee on 2016. 2. 6..
 */
public class Mart extends Store{

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    //================================================================================
    // XXX Constructor
    //================================================================================

    public Mart() {}


    public Mart(JSONObject object) throws JSONException {

        super(object);
        storeType = StoreType.MART;
    }

    public Mart(Parcel parcel) {

        super(parcel);

    }

    public static final Parcelable.Creator<Mart> CREATOR = new Parcelable.Creator<Mart>() {

        @Override
        public Mart[] newArray(int size) {
            return new Mart[size];
        }

        @Override
        public Mart createFromParcel(Parcel source) {
            return new Mart(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        super.writeToParcel(dest, flags);
    }


    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================
}
