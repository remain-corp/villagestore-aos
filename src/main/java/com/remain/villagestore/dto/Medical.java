package com.remain.villagestore.dto;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by woongjaelee on 2015. 12. 14..
 */
public class Medical extends Store {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    //================================================================================
    // XXX Constructor
    //================================================================================

    public Medical() {}


    public Medical(JSONObject object) throws JSONException {

        super(object);
        storeType = StoreType.MEDICAL;
    }

    public Medical(Parcel parcel) {

        super(parcel);

    }

    public static final Creator<Medical> CREATOR = new Creator<Medical>() {

        @Override
        public Medical[] newArray(int size) {
            return new Medical[size];
        }

        @Override
        public Medical createFromParcel(Parcel source) {
            return new Medical(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        super.writeToParcel(dest, flags);
    }


    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================
}
