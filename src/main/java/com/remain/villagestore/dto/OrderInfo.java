package com.remain.villagestore.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.remain.villagestore.info.ParamInfo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by woongjaelee on 2015. 11. 25..
 */
public class OrderInfo implements Parcelable{

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================
    /* 쿠폰 발급 번호 */
    private int couponSeq;
    /* 매장 쿠폰 번호 */
    private int storeSeq;
    /* 최초 주문 금액 */
    private int firstPrice;
    /* 주문 시간 */
    private String orderDate;
    /* 쿠폰 상태 정보
     * N = 적립처리 안함 Y = 적립 처리됨 E = 적립기간 만료 P = 포인트 사용 주문
      * */
    private String couponInfo;

    //================================================================================
    // XXX Constructor
    //================================================================================

    public OrderInfo() {

    }

    public OrderInfo(int couponSeq, int storeSeq, int firstPrice, String orderDate, String couponInfo) {
        this.couponSeq = couponSeq;
        this.storeSeq = storeSeq;
        this.firstPrice = firstPrice;
        this.orderDate = orderDate;
        this.couponInfo = couponInfo;
    }

    public OrderInfo(JSONObject object) throws JSONException {

        if(!object.isNull(ParamInfo.ORDER_SEQ)) {
            couponSeq = object.getInt(ParamInfo.ORDER_SEQ);
        }

        if(!object.isNull(ParamInfo.STORE_CP_SEQ)) {
            storeSeq = object.getInt(ParamInfo.STORE_CP_SEQ);
        }

        if(!object.isNull(ParamInfo.ORDER_PRICE)) {
            firstPrice = object.getInt(ParamInfo.ORDER_PRICE);
        }
        if(!object.isNull(ParamInfo.ORDER_DATE)) {
            orderDate = object.getString(ParamInfo.ORDER_DATE);
        }
        if(!object.isNull(ParamInfo.ORDER_INFO)) {
            couponInfo = object.getString(ParamInfo.ORDER_INFO);
        }


    }

    public OrderInfo(Parcel parcel) {

        couponSeq = parcel.readInt();
        storeSeq = parcel.readInt();
        firstPrice = parcel.readInt();
        orderDate = parcel.readString();
        couponInfo = parcel.readString();
    }

    public static final Creator<OrderInfo> CREATOR = new Creator<OrderInfo>() {

        @Override
        public OrderInfo[] newArray(int size) {
            return new OrderInfo[size];
        }

        @Override
        public OrderInfo createFromParcel(Parcel source) {
            return new OrderInfo(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(couponSeq);
        dest.writeInt(storeSeq);
        dest.writeInt(firstPrice);
        dest.writeString(orderDate);
        dest.writeString(couponInfo);
    }

    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================


    public int getCouponSeq() {
        return couponSeq;
    }

    public void setCouponSeq(int couponSeq) {
        this.couponSeq = couponSeq;
    }

    public int getStoreSeq() {
        return storeSeq;
    }

    public void setStoreSeq(int storeSeq) {
        this.storeSeq = storeSeq;
    }

    public int getFirstPrice() {
        return firstPrice;
    }

    public void setFirstPrice(int firstPrice) {
        this.firstPrice = firstPrice;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }
}
