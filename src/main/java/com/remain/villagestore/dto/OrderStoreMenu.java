package com.remain.villagestore.dto;

import java.util.ArrayList;

/**
 * Created by woongjaelee on 2015. 11. 27..
 */
public class OrderStoreMenu {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Enum
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    /* 매장 seq */
    private int storeSeq;
    /* 매장 이름 */
    private String storeName;
    /* 선택한 메뉴 */
    private ArrayList<StoreMenu> arrayListStoreMenu;
    /* 총가격 */
    private int totalPrice;

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    public OrderStoreMenu() {}

    public OrderStoreMenu(int storeSeq, String storeName, ArrayList<StoreMenu> arrayListStoreMenu) {

        this.storeSeq = storeSeq;
        this.storeName = storeName;
        this.arrayListStoreMenu = arrayListStoreMenu;

        setCulcuTotalPrice();
    }

    public int getStoreSeq() {
        return storeSeq;
    }

    public void setStoreSeq(int storeSeq) {
        this.storeSeq = storeSeq;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public ArrayList<StoreMenu> getArrayListStoreMenu() {
        return arrayListStoreMenu;
    }

    public void setArrayListStoreMenu(ArrayList<StoreMenu> arrayListStoreMenu) {
        this.arrayListStoreMenu = arrayListStoreMenu;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================

    private int setCulcuTotalPrice() {
        int totalPrice = 0;

        for(StoreMenu storeMenu : arrayListStoreMenu) {
            totalPrice += storeMenu.getPrice() * storeMenu.getCount();
        }

        return totalPrice;
    }
}
