package com.remain.villagestore.dto;

/**
 * Created by woongjaelee on 2015. 12. 16..
 */
public class SearchPlace {

    private String subLocality;
    private String fullLocality;
    private String etc;

    public SearchPlace() {
    }

    public SearchPlace(String subLocality, String fullLocality) {
        this.subLocality = subLocality;
        this.fullLocality = fullLocality;
    }

    public String getSubLocality() {
        return subLocality;
    }

    public void setSubLocality(String subLocality) {
        this.subLocality = subLocality;
    }

    public String getFullLocality() {
        return fullLocality;
    }

    public void setFullLocality(String fullLocality) {
        this.fullLocality = fullLocality;
    }

    public String getEtc() {
        return etc;
    }

    public void setEtc(String etc) {
        this.etc = etc;
    }
}
