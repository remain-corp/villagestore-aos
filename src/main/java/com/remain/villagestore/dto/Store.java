package com.remain.villagestore.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.StringTokenizerManager;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by woongjaelee on 2015. 11. 6..
 */

public class Store implements Parcelable {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    /* seq */
    protected int seq;
    /* 매장명 */
    protected String name;
    /* 매장 전달 */
    protected String announce;
    /* 영업시간 */
    protected String businessHour;
    /* 전화번호 */
    protected String phoneNumber;
    /* 구주소 */
    protected String address;
    /* 신주소 */
    protected String address2;
    /* 이미지 */
    protected ArrayList<String> arrayListImgUri;
    /* 매장 타입 */
    protected StoreType storeType;
    /* 위도 */
    protected double latitude;
    /* 경도 */
    protected double longitude;
    /* 좋아요 수 */
    protected int likeCount;
    /* 매장 인증 키 */
    protected int authKey;
    /* 매장 서브 카테고리 */
    protected int subCate;
    /* 매장 서브 카테고리 */
    protected String url;

    // DB 전용
    private int price;
    private String couponInfo;
    private String couponLimit;
    private String regDate;

    //================================================================================
    // XXX Enum
    //================================================================================

    public enum StoreType {
        ALL(0),
        /* 배달*/
        DELIVERY(1),
        /* 외식 */
        EATERY(2),
        /* 학원/과외 */
        EDUCATION(3),
        /* 병원/약국 */
        MEDICAL(4),
        /* 카페/주점 */
        CAFE(5),
        /* 생활/편의 */
        LIFE(6),
        /* 마트 */
        MART(7),
        /* 관공서   */
        PUBLIC_OFFICE(8),
        /* 프라자  */
        BUILDING(9);


        private int storeTypeCode;

        StoreType(int storeTypeCode) {
            this.storeTypeCode = storeTypeCode;
        }

        public int getStoreTypeCode() {
            return storeTypeCode;
        }

        public void setStoreTypeCode(int storeTypeCode) {
            this.storeTypeCode = storeTypeCode;
        }

        public static StoreType getStoreType(int storeTypeCode) {
            for (StoreType storeType : StoreType.values()) {
                if (storeType.storeTypeCode == storeTypeCode) return storeType;
            }
            throw new IllegalArgumentException("enum 값이 없습니다.");
        }
    }

    //================================================================================
    // XXX Constructor
    //================================================================================

    public Store() {
    }

    public Store(int seq, String name, String phone, int cate, String img, String couponInfo, String couponLimit, String regDate, int price) {

        this.arrayListImgUri = new ArrayList<String>();

        this.seq = seq;
        this.name = name;
        this.phoneNumber = phone;
        this.arrayListImgUri.add(img);
        this.storeType = StoreType.getStoreType(cate);
        this.couponInfo = couponInfo;
        this.couponLimit = couponLimit;
        this.regDate = regDate;
        this.price = price;
    }

    public Store(JSONObject object) throws JSONException {

        storeType = StoreType.ALL;

        if (!object.isNull(ParamInfo.SEQ)) {
            seq = object.getInt(ParamInfo.SEQ);
        }

        if (!object.isNull(ParamInfo.ST_NAME)) {
            name = object.getString(ParamInfo.ST_NAME);
        }

        if (!object.isNull(ParamInfo.ST_PHONE)) {
            phoneNumber = object.getString(ParamInfo.ST_PHONE);
        }

        if (!object.isNull(ParamInfo.ST_COMMENT)) {
            announce = object.getString(ParamInfo.ST_COMMENT);
        }

        if (!object.isNull(ParamInfo.ST_HOURS)) {
            businessHour = object.getString(ParamInfo.ST_HOURS);
        }

        if (!object.isNull(ParamInfo.ST_ADDRESS)) {
            address = object.getString(ParamInfo.ST_ADDRESS);
        }

        if (!object.isNull(ParamInfo.ST_ADDRESS2)) {
            address2 = object.getString(ParamInfo.ST_ADDRESS2);
        }

        if (!object.isNull(ParamInfo.ST_LAT)) {
            latitude = object.getDouble(ParamInfo.ST_LAT);
        }

        if (!object.isNull(ParamInfo.ST_LNG)) {
            longitude = object.getDouble(ParamInfo.ST_LNG);
        }

        if (!object.isNull(ParamInfo.ST_LIKE)) {
            likeCount = object.getInt(ParamInfo.ST_LIKE);
        }

        if (!object.isNull(ParamInfo.ST_AUTH_KEY)) {
            authKey = object.getInt(ParamInfo.ST_AUTH_KEY);
        }

        if (!object.isNull(ParamInfo.SUB_CATE)) {
            subCate = object.getInt(ParamInfo.SUB_CATE);
        }

        if (!object.isNull(ParamInfo.STORE_URL)) {
            url = object.getString(ParamInfo.STORE_URL);
        }


//        if(!object.isNull(ParamInfo.COUPONSEQ)) {
//            String copon = object.getString(ParamInfo.COUPONSEQ);
//
//            if(copon != null && !copon.equals("")) {
//                isCouponExist = true;
//            } else {
//                isCouponExist = false;
//            }
//        }
//        else {
//            isCouponExist = false;
//        }

        arrayListImgUri = new ArrayList<String>();

        if (!object.isNull(ParamInfo.FILENAME)) {

            String imgPath = object.getString(ParamInfo.FILENAME);
            String fileInfos[] = StringTokenizerManager.getInstance().split(imgPath, AppInfo.TAG_DELIMITER_TO_IMG);

            if (fileInfos != null && fileInfos.length != 0) {

                for (int i = 0; i < fileInfos.length; i++) {

                    arrayListImgUri.add(fileInfos[i]);
                }
            }

        }

//        Log.e("인증번호 : " + getVerificationCode());

    }

    public Store(Parcel parcel) {

        this.seq = parcel.readInt();
        int storeTypeInt = parcel.readInt();
        if (storeTypeInt != -1)
            this.storeType = StoreType.values()[storeTypeInt];
        this.name = parcel.readString();
        this.announce = parcel.readString();
        this.businessHour = parcel.readString();
        this.phoneNumber = parcel.readString();
        this.url = parcel.readString();
        this.address = parcel.readString();
        this.likeCount = parcel.readInt();
        this.subCate = parcel.readInt();
        this.latitude = parcel.readDouble();
        this.longitude = parcel.readDouble();
//        int couponExist = parcel.readInt();
//        this.isCouponExist = (couponExist == 1) ? true : false ;
        arrayListImgUri = new ArrayList<>();
        parcel.readStringList(this.arrayListImgUri);
        this.price = parcel.readInt();

    }

    public static final Creator<Store> CREATOR = new Creator<Store>() {

        @Override
        public Store[] newArray(int size) {
            return new Store[size];
        }

        @Override
        public Store createFromParcel(Parcel source) {
            return new Store(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(seq);
        int storeTypeInt = storeType == null ? -1 : storeType.ordinal();
        dest.writeInt(storeTypeInt);
        dest.writeString(name);
        dest.writeString(announce);
        dest.writeString(businessHour);
        dest.writeString(phoneNumber);
        dest.writeString(url);
        dest.writeString(address);
        dest.writeInt(likeCount);
        dest.writeInt( subCate );
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
//        int couponExist = isCouponExist ? 1 : 0;
//        dest.writeInt(couponExist);
        dest.writeStringList(arrayListImgUri);
        dest.writeInt(price);
    }

    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnnounce() {
        return announce;
    }

    public void setAnnounce(String announce) {
        this.announce = announce;
    }

    public String getBusinessHour() {
        return businessHour;
    }

    public void setBusinessHour(String businessHour) {
        this.businessHour = businessHour;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<String> getArrayListImgUri() {
        return arrayListImgUri;
    }

    public void setArrayListImgUri(ArrayList<String> arrayListImgUri) {
        this.arrayListImgUri = arrayListImgUri;
    }

    public StoreType getStoreType() {
        return storeType;
    }

    public void setStoreType(StoreType storeType) {
        this.storeType = storeType;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

//    public String getVerificationCode() {
//
//        StringBuffer stringBuffer = new StringBuffer(getPhoneNumber());
//        try {
//
//            return stringBuffer.substring(stringBuffer.length() - 4);
//        } catch (StringIndexOutOfBoundsException e) {
//            return "0";
//        }
//
//    }

    public String getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }

    public String getCouponLimit() {
        return couponLimit;
    }

    public void setCouponLimit(String couponLimit) {
        this.couponLimit = couponLimit;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAuthKey() {
        return authKey;
    }

    public void setAuthKey(int authKey) {
        this.authKey = authKey;
    }

    public int getSubCate() {
        return subCate;
    }

    public void setSubCate(int subCate) {
        this.subCate = subCate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================


}
