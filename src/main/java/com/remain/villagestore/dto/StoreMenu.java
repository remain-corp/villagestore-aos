package com.remain.villagestore.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.ParamInfo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by woongjaelee on 2015. 11. 21..
 */
public class StoreMenu implements Parcelable {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    /* 메뉴 이름 */
    private String name;
    /* 메뉴 주문 갯수 */
    private int count;
    /* 메뉴 가격 */
    private int price;
    /* 이미지 */
    private String imgPath;
    /* 정보 */
    private String info;
    /* 디폴트 이미지인지 */
    private String isDefault;

    //================================================================================
    // XXX Enum
    //================================================================================
    public enum MenuType {
        NONE(0),
        /* 중식  */
        CHINESE(1),
        /* 치킨 */
        CHICKEN(2),
        /* 피자/햄버거 */
        PIZZA(3),
        /* 족발/보쌈/야식 */
        MEAT(4),
        /* 분식 */
        FLOUR(5),
        /* 한식 */
        KOREAN(6),
        /* 기타 */
        ETC(7);

        private int menuTypeCode;

        MenuType(int menuTypeCode) {
            this.menuTypeCode = menuTypeCode;
        }

        public int getMenuTypeCode() {
            return menuTypeCode;
        }

        public void setMenuTypeCode(int menuTypeCode) {
            this.menuTypeCode = menuTypeCode;
        }

        public static MenuType getMenuType(int menuTypeCode) {
            for (MenuType menuType : MenuType.values()) {
                if (menuType.menuTypeCode == menuTypeCode) return menuType;
            }
            throw new IllegalArgumentException("enum 값이 없습니다.");
        }
    }


    //================================================================================
    // XXX Constructor
    //================================================================================

    public StoreMenu() {
    }

    public StoreMenu(JSONObject object) throws JSONException {

        if (!object.isNull(ParamInfo.NAME)) {
            name = object.getString(ParamInfo.NAME);
        }

        if (!object.isNull(ParamInfo.PRICE)) {
            price = object.getInt(ParamInfo.PRICE);
        }

        if (!object.isNull(ParamInfo.INFO)) {
            info = object.getString(ParamInfo.INFO);
        }

        if (!object.isNull(ParamInfo.IMAGE)) {

//            StringBuffer stringBuffer = new StringBuffer(AppInfo.IMG_THUM_PATH);

//            imgPath = stringBuffer.append(object.getString(ParamInfo.IMAGE)).toString();
            imgPath = object.getString(ParamInfo.IMAGE);
        }

        if (!object.isNull(ParamInfo.IS_DEFAULT)) {
            isDefault = object.getString(ParamInfo.IS_DEFAULT);
        }
    }

    public StoreMenu(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public StoreMenu(Parcel parcel) {

        this.name = parcel.readString();
        this.price = parcel.readInt();
        this.count = parcel.readInt();
        this.imgPath = parcel.readString();
        this.info = parcel.readString();
        this.isDefault = parcel.readString();
    }

    //================================================================================
    // XXX Override Method
    //================================================================================

    public static final Creator<StoreMenu> CREATOR = new Creator<StoreMenu>() {

        @Override
        public StoreMenu[] newArray(int size) {
            return new StoreMenu[size];
        }

        @Override
        public StoreMenu createFromParcel(Parcel source) {
            return new StoreMenu(source);
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(name);
        dest.writeInt(price);
        dest.writeInt(count);
        dest.writeString(imgPath);
        dest.writeString(info);
        dest.writeString(isDefault);
    }

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        if (count < 0) {
            this.count = 0;
        } else {
            this.count = count;
        }
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }
}
