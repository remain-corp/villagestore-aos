package com.remain.villagestore.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.StringTokenizerManager;
import com.remain.villagestore.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by woongjaelee on 2015. 11. 6..
 */

public class SubCate implements Parcelable {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    /* seq */
    protected String seq;
    /* 이미지 */
    protected String imageName;

    //================================================================================
    // XXX Enum
    //================================================================================



    //================================================================================
    // XXX Constructor
    //================================================================================

    public SubCate() {
    }

    public SubCate(String seq, String imageName) {

        this.seq = seq;
        this.imageName = imageName;

    }

    public SubCate(JSONObject object) throws JSONException {

        if (!object.isNull(ParamInfo.SUB_CATE_SEQ)) {
            seq = object.getString(ParamInfo.SUB_CATE_SEQ);
        }

        if (!object.isNull(ParamInfo.IMAGE)) {
            imageName = object.getString(ParamInfo.IMAGE);
        }

    }

    public SubCate(Parcel parcel) {

        this.seq = parcel.readString();
         this.imageName = parcel.readString();

    }

    public static final Creator<SubCate> CREATOR = new Creator<SubCate>() {

        @Override
        public SubCate[] newArray(int size) {
            return new SubCate[size];
        }

        @Override
        public SubCate createFromParcel(Parcel source) {
            return new SubCate(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(seq);
        dest.writeString(imageName);

    }

    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================


    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
//================================================================================
    // XXX Etc Method
    //================================================================================


}
