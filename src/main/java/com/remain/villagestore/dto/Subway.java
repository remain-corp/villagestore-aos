package com.remain.villagestore.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by woongjaelee on 2015. 11. 6..
 */

public class Subway implements Parcelable {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    /* 역이름 */
    private String name;
    /** 위도 */
    private double lat;
    /** 경도 */
    private double lng;
    /** 호선명 */
    private ArrayList<String> arrayListLineName;
    /** 호선 색깔 */
    private ArrayList<String> arrayListLineColor;

    //================================================================================
    // XXX Enum
    //================================================================================



    //================================================================================
    // XXX Constructor
    //================================================================================

    public Subway() { }

    public Subway(JSONObject object) throws JSONException {

        arrayListLineName = new ArrayList<>();
        arrayListLineColor = new ArrayList<>();

        if(!object.isNull(ParamInfo.STN_NAME)) {
            this.name = object.getString(ParamInfo.STN_NAME);
        }
        if(!object.isNull(ParamInfo.LAT)) {
//            this.lat = object.getDouble(ParamInfo.LAT);
            try {

                this.lat = Double.parseDouble(object.getString(ParamInfo.LAT));

            }catch (NumberFormatException e) {

                this.lat = 0;

            }
        }
        if(!object.isNull(ParamInfo.LNG)) {
//            this.lng = object.getDouble(ParamInfo.LNG);
            try {

                this.lng = Double.parseDouble(object.getString(ParamInfo.LNG));

            }catch (NumberFormatException e) {

                this.lng = 0;

            }
        }
        if(!object.isNull(ParamInfo.INFO)) {
            JSONArray infoArray = object.getJSONArray(ParamInfo.INFO);
            JSONObject infoObject = null;
            for(int i = 0 ; i < infoArray.length(); i ++ ) {
                infoObject = infoArray.getJSONObject(i);
                if(infoObject != null) {

                    if(!infoObject.isNull(ParamInfo.COLOR_CD)) {

                        arrayListLineColor.add(infoObject.getString(ParamInfo.COLOR_CD));
                    }

                    if(!infoObject.isNull(ParamInfo.LN_NAME)) {

                        arrayListLineName.add(infoObject.getString(ParamInfo.LN_NAME));
                    }
                }
            }
        }
    }

    public Subway(Parcel parcel) {

        this.name = parcel.readString();
        this.lat = parcel.readDouble();
        this.lng = parcel.readDouble();

    }

    public static final Creator<Subway> CREATOR = new Creator<Subway>() {

        @Override
        public Subway[] newArray(int size) {
            return new Subway[size];
        }

        @Override
        public Subway createFromParcel(Parcel source) {
            return new Subway(source);
        }
    };

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(name);
        dest.writeDouble(lat);
        dest.writeDouble(lng);

    }

    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public ArrayList<String> getArrayListLineName() {
        return arrayListLineName;
    }

    public void setArrayListLineName(ArrayList<String> arrayListLineName) {
        this.arrayListLineName = arrayListLineName;
    }

    public ArrayList<String> getArrayListLineColor() {
        return arrayListLineColor;
    }

    public void setArrayListLineColor(ArrayList<String> arrayListLineColor) {
        this.arrayListLineColor = arrayListLineColor;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================


}
