package com.remain.villagestore.dto;

import android.content.Context;

import com.remain.villagestore.db.Database;
import com.remain.villagestore.info.ParamInfo;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by woongjaelee on 15. 8. 16..
 */
public class User {

    //================================================================================
    // XXX Constants
    //================================================================================

    private static User myUser;

    //================================================================================
    // XXX Variables
    //================================================================================

    /** 유저 seq*/
    private int seq;
    /** 유저 아이디 */
    private String userId;
    /** 유저 닉네임 */
    private String userNickName;
    /** 유저 비밀번호 */
    private String userPass;
    /** 디바이스 아이디 */
    private String deviceId;
    /** 위도 */
    private double latitude;
    /** 경도 */
    private double longitude;
    /** 주소 */
    private String address;
    /** 상세 주소만  */
    private String detailAddress;
    /** 최종 접속 시간 */
    private String regDate;
    /** 이벤트 쿠폰 */
    private int eventCNT;
    /** 포인트 */
    private int point;
    /** 로컬seq */
    private int localSeq;



    public static User getMyInfo(Context context) {

        if (myUser == null) {

            myUser = new User();
        }
        myUser.loadMyInfo(context);

        return myUser;
    }

    public User() {}

    public User(JSONObject object) throws JSONException {

        if(!object.isNull(ParamInfo.SEQ)) {
            seq = object.getInt(ParamInfo.SEQ);
        }
        if(!object.isNull(ParamInfo.ID)) {
            userId = object.getString(ParamInfo.ID);
        }

        if(!object.isNull(ParamInfo.DEVICE_ID)) {
            deviceId = object.getString(ParamInfo.DEVICE_ID);
        }

        if(!object.isNull(ParamInfo.ADDRESS1)) {
            address = object.getString(ParamInfo.ADDRESS1);
        }

        if(!object.isNull(ParamInfo.ADDRESS2)) {
            userNickName = object.getString(ParamInfo.ADDRESS2);
        }

        if(!object.isNull(ParamInfo.PASS)) {
            userPass = object.getString(ParamInfo.PASS);
        }

        if(!object.isNull(ParamInfo.EVENT_CNT)) {
            eventCNT = object.getInt(ParamInfo.EVENT_CNT);
        }

        if(!object.isNull(ParamInfo.POINT)) {
            point = object.getInt(ParamInfo.POINT);
        }
        if(!object.isNull(ParamInfo.LOCAL_SEQ)) {
            localSeq = object.getInt(ParamInfo.LOCAL_SEQ);
        }

    }

    public User(int seq, String userId, String deviceId, double latitude, double longitude, String regDate, String address, int point, String detailAddress, int localSeq) {

        this.seq = seq;
        this.userId = userId;
        this.deviceId = deviceId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.regDate = regDate;
        this.address = address;
        this.point = point;
        this.userNickName = detailAddress;
        this.localSeq = localSeq;
    }

    public void loadMyInfo(Context context) {
        myUser = Database.getInstance().getUser(context);
    }

    public void clearMyInfo() {
        myUser = null;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;

    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getDetailAddress() {
        return userNickName;
    }

    public void setDetailAddress(String detailAddress) {
        this.userNickName = detailAddress;
    }

    public int getEventCNT() {
        return eventCNT;
    }

    public void setEventCNT(int eventCNT) {
        this.eventCNT = eventCNT;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getLocalSeq() {
        return localSeq;
    }

    public void setLocalSeq(int localSeq) {
        this.localSeq = localSeq;
    }
}
