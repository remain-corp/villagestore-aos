package com.remain.villagestore.info;

/**
 * Created by Administrator on 2015-06-20.
 */
public class AppInfo {

    public static final String PACKAGE_NAME = "com.remain.villagestore";

    public static final String MARKET_LINK_APPSTORE = "market://details?id=" + PACKAGE_NAME;
    public static final String MARKET_LINK = "https://play.google.com/store/apps/details?id="+PACKAGE_NAME;
    public static final String KAKAO_PLUS_LINK = "http://plus.kakao.com/home/kg6vw6ey";


    public static final int VERSION_APP_SEQ = 3;

    public static final int STANDARD_WIDTH = 720;
    public static final int STANDARD_HEIGHT = 1280;

    public static final int NOTHING_INT = -1;
    public static final int DEFAULT_INT = 0;

    public static final String DEFAULT_STRING = "";
    /** 이미지 구분자 */
    public static final String TAG_DELIMITER_TO_IMG = "^";
    /** null */
    public static final String NULL = "null";
    /** 콤마 */
    public static final String COMMA = ",";
    /** img thum path */
    public static final String IMG_THUM_PATH = "remain.synology.me:7070/Image/thumb/";
    /** img path */
    public static final String IMG_PATH = "remain.synology.me:7070/Image/resize/";
    /** img path */
    public static final String IMG_AD_PATH = "remain.synology.me:7070/Image/ad/";
    /** menu img path */
    public static final String IMG_MENU_PATH = "remain.synology.me:7070/Image/menu/";
    public static final String IMG_D_MENU_PATH = "remain.synology.me:7070/Image/menu_deft/";
    /** file path */
    public static final String FILE_PATH = "screenshot";

    public static final int TUTORIAL_PAGES = 5;
}
