package com.remain.villagestore.info;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

import java.util.Locale;

public class DeviceInfo {
	
	//================================================================================
    // Variables
    //================================================================================
	// This
	private static DeviceInfo mDeviceInfo;
	
	//================================================================================
    // Constructors
    //================================================================================
	
	//================================================================================
    // Getter, Setter
    //================================================================================
	public static DeviceInfo getInstance() {
		if (mDeviceInfo == null) {
			mDeviceInfo = new DeviceInfo();
		}
		return mDeviceInfo;
	}
	
	//================================================================================
    // Etc Method
    //================================================================================
	/**
	 * 휴대전화 전화번호 가져오기
	 * @return : 전화번호
	 */
	public String getPhoneNumber(Context context) {
		TelephonyManager mgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return mgr.getLine1Number();
	}
	
	/**
	 * 디바이스 고유 아이디 가져오기
	 * @return : 디바이스 아이디
	 */
	public String getDeviceId(Context context) {
		TelephonyManager mgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

		if(mgr.getDeviceId() == null) {

			return "";
			
		} else {

			return mgr.getDeviceId();
		}
	}

	/**
	 * 디바이스 모델명 가져오기
	 * @return : 디바이스 모델명
	 */
	public String getModelName() {
		return Build.MODEL;
	}

	/**
	 * 디바이스 펌웨어 버전 정보 가져오기
	 * @return 디바이스 버전
	 */
	public String getFirmwareVersion() {
		return Build.VERSION.RELEASE;
	}

	/**
	 * Tablet 인지 유무 파악
	 * @return 기본 : 1 / 태블릿 : 2
	 */
	public int getDeviceType(Context context) {
		
		int deviceType = 1;
		int portrait_width_pixel = Math.min(context.getResources().getDisplayMetrics().widthPixels, context.getResources().getDisplayMetrics().heightPixels);
		int dots_per_virtual_inch = context.getResources().getDisplayMetrics().densityDpi;
		float virutal_width_inch = portrait_width_pixel / dots_per_virtual_inch;

		if (virutal_width_inch > 2)
			deviceType = 2;
		else
			deviceType = 1;

		return deviceType;
	}
	
	/**
	 * 국가 코드 가져오기
	 * @return 국가 코드
	 */
	public String getCountry(Context context) {
		
		Locale systemLocale = context.getResources().getConfiguration().locale;
		String strCountry = systemLocale.getCountry(); // output : KR
		return strCountry;
	}
	
	/**
	 * 언어 코드 가져오기
	 * @return 언어 코드
	 */
	public String getLanguage(Context context) {
		
		Locale systemLocale = context.getResources().getConfiguration().locale;
		String strLanguage = systemLocale.getLanguage(); // output : ko
		return strLanguage;
	}
}
