package com.remain.villagestore.info;

/**
 * 에러 코드와 에러 내용을 담는 클래스
 */
public class ErrorInfo {

	//================================================================================
    // Constants
    //================================================================================
	/** 정상 종료 */
	public static final int NORMAL_TERMINATION 		=	101;
	/** 비정상 종료 */
	public static final int ABNORMAL_TERMINATION 	=	102;
	/** 잘못된 주소 */
	public static final int INVALID_ADDRESS			=	103;
	/** 연결 오류 */
	public static final int FAILED_CONNECTED		=	104;
	/** JSON Parsing Error */
	public static final int JSON_ERROR				=	105;
	/** Socket Error */
	public static final int SOCKET_ERROR			=	106;
	/** Not Connect */
	public static final int NOT_CONNECT				=	107;
	/** SignVerify */
	public static final int FAIL_VERIFY				=	108;
	//================================================================================
    // Variables
    //================================================================================
	private int errorCode;
	private String error;
	
	//================================================================================
    // Constructors
    //================================================================================
	public ErrorInfo(int errorCode, String error) {
		this.setErrorCode(errorCode);
		this.setError(error);
	}

	//================================================================================
    // Getter, Setter
    //================================================================================
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}