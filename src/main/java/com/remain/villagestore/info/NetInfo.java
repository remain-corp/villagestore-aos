package com.remain.villagestore.info;

/**
 * Created by woongjaelee on 15. 9. 23..
 */
public class NetInfo {

    public static final String IP_ADDRESS = "http://remain1991.cafe24.com";
//    public static final String IP_ADDRESS1 = "http://remain1992.cafe24.com";
//    public static final String IP_ADDRESS1 = "http://remain1992.cafe24.com/aos";
    public static final String IP_ADDRESS1 = "http://remain1992.cafe24.com/test";
    public static final String IP_ADDRESS2 = "http://remain.synology.me:7070";
    public static final String HASH_KEY = "21101d45c8084aeppd38c6114ffaec05";

    public final static String NOTICE_URL = "http://remain1991.cafe24.com/wordpress/dongne-notice/";
    public final static String ADMIN_URL = "http://remain1992.cafe24.com";
    public final static String POLICY_URL = "http://www.remain-ent.com/#!blank/wy4bj";
    public static final String GOOGLE_SUPPLY_URL = "http://remain1991.cafe24.com/wordpress/contact-us/";
    public static final String DAUM_MAP_DOWNLOAD_URL = "market://details?id=net.daum.android.map";
    public static final String TERMS_URL = "http://remain1992.cafe24.com/terms";
    public static final String SUBCATE_URL = IP_ADDRESS2+"/Image/sub_cate_menu/";

    //우체국 api인증키
    public static final String POST_API_KEY = "5d154ff3eb4a07ff81464683877903";

    //================================================================================
    // XXX Store
    //================================================================================

    /**
     *  구버전 매장 리스트 정보 받아오기
     */
    public static final String SELECT_ADMIN = "/dapi/selectAdmin";

    /**
     * 매장 리스트 정보 받아오기
     */
    public static final String SEARCH_STORE = "/dapi/getStoreSchList";

    //================================================================================
    // XXX User
    //================================================================================

    /**
     * 유저 정보 넣기
     */
    public static final String INSERT_USER = "/dapi/insertUser"; //
    /**
     * 유저 검색
     */
    public static final String SELECT_USER = "/dapi/selectUser";
    /**
     * 유저 검색
     */
    public static final String LOGIN_USER = "/dapi/login"; //
    /**
     * 유저 업데이트
     */
    public static final String UPDATE_USER = "/dapi/updateUser";
    /**
     * d_user 모든컬럼 업데이트 가능
     */
    public static final String UPDATE_ALL_CUL_USER = "/dapi/userModify";
    /**
     * 매장 좋아요 여부확인
     */
    public static final String READ_LIKE = "/dapi/readLike"; //
    /**
     * 매장 좋아요
     */
    public static final String UPDATE_LIKE = "/dapi/updateLike"; //
    /**
     * 매장 쿠폰 받아오기
     */
    public static final String SELECT_COUPON = "/dapi/selectCoupon"; //
    /**
     * 매장 쿠폰 가감하기
     */
    public static final String DISCOUNT_COUPON = "/dapi/discountCoupon"; //
    /**
     * 주문 내역 전송
     */
    public static final String INSERT_COUPONUSE = "/dapi/insertCouponUse"; //
    /**
     * 나의 좋아요한 매장 읽기
     */
    public static final String READ_MYLIKE = "/dapi/readMyLike";
    /**
     * 매장 메뉴 읽기
     */
    public static final String READ_MENU = "/dapi/readMenu"; //
    /**
     * 광고 리스트 받기
     */
    public static final String READ_AD = "/dapi/readAdfile"; //
    /**
     * 콜수 올리기 받기
     */
    public static final String CALL_CNT = "/dapi/callCnt"; //
    /**
     * 앱 버젼 받아오기
     */
    public static final String READ_VERSION = "/dapi/readVersion"; //

    /**
     * 아이디 닉네임체크
     */
    public static final String ID_CHECK = "/dapi/id_nick_check"; //
    /**
     * 주문 쿠폰정보 검색
     */
    public static final String SELECT_ORDER = "/dapi/getOrderInfo";
    /**
     * 포인트 적립
     */
    public static final String SAVE_POINT = "/dapi/finalPay";
    /**
     * 포인트 로그 셀렉트
     */
    public static final String GET_POINT_LOG = "/dapi/myPointLog";
    /**
     * 아이디 존재여부 조회
     */
    public static final String USER_ID_CHECK = "/dapi/id_check";
    /**
     * 비밀번호 변경
     */
    public static final String UPDATE_PASSWORD = "/dapi/mod_u_pass";
    /**
     * 포인트 환급
     */
    public static final String POINT_REFUND = "/dapi/reg_refund";
    /**
     * 포인트 환급 내역
     */
    public static final String POINT_REFUND_LOG = "/dapi/user_reflist";
    /**
     * 유저 주소지 입력
     */
    public static final String INSERT_USER_ADDRESS = "/dapi/ins_user_addr";
    /**
     * 약정서 리스트 받아오기
     */
    public static final String SELECT_CONTRACT = "/dapi/get_resi_cont";
    /**
     * 약정등록지점, 주소지설정 , 직접입력
     * user_locl_info_seq ( 10:주소,20:약정,30:기타)
     * isPre - 대표설정여부 (Y:대표,N:안대표)
     */
    public static final String INSERT_ADDRESS = "/dapi/set_user_local";
    /**
     * push_seq 받아서 해당 push의 click_cnt +1
     * st_seq 가 넘어왔을경우 해당 매장정보 return
     */
    public static final String PUSH_UPDATE = "/dapi/pushIntro";
    /**
     * searchActivity 지역리스트
     */
    public static final String SELECT_ADDRLIST = "/dapi/getAddrInfo";
    /**
     * searchActivity 지하철리스트
     */
    public static final String SEARCH_STATION = "/dapi/schStation";

    //================================================================================
    // XXX Handler What
    //================================================================================
    /**
     * 구버전 매장 리스트 받아오기
     */
    public static final int WHAT_SEARCH_STORE_SUB = 1001;
    /**
     * 매장 리스트 정보 받아오기
     */
    public static final int WHAT_SEARCH_STORE = 1002;
    /**
     * 유저 입력
     */
    public static final int WHAT_INSERT_USER = 1003;
    /**
     * 유저 검색
     */
    public static final int WHAT_SELECT_USER = 1004;
    /**
     * 매장 좋아요 확인
     */
    public static final int WHAT_READ_LIKE = 1005;
    /**
     * 매장 좋아요
     */
    public static final int WHAT_UPDATE_LIKE = 1006;
    /**
     * 매장 쿠폰 받아오기
     */
    public static final int WHAT_SELECT_COUPON = 1007;
    /**
     * 매장 쿠폰 가감하기
     */
    public static final int WHAT_DISCOUNT_COUPON = 1008;
    /**
     * 주문 내역 전송
     */
    public static final int WHAT_INSERT_COUPONUSE = 1009;
    /**
     * 나의 좋아요한 매장 읽기
     */
    public static final int WHAT_READ_MYLIKE = 1010;
    /**
     * 유저 업데이트
     */
    public static final int WHAT_UPDATE_USER = 1011;
    /**
     * d_user 모든컬럼 업데이트 가능
     */
    public static final int WHAT_UPDATE_ALL_CUL_USER = 1011;
    /**
     * 매장 메뉴 읽기
     */
    public static final int WHAT_READ_MENU = 1012;
    /**
     * 광고 리스트 받기
     */
    public static final int WHAT_READ_AD = 1013;
    /**
     * 로그인 성공
     */
    public static final int WHAT_LOGIN_USER = 1014;
    /**
     * 콜수 올리기 받기
     */
    public static final int WHAT_CALL_CNT = 1015;
    /**
     * 주문 쿠폰정보 검색
     */
    public static final int WHAT_SELECT_ORDER = 1016;
    /**
     * 포인트 적립
     */
    public static final int WHAT_SAVE_POINT = 1017;
    /**
     * 포인트 로그 셀렉트
     */
    public static final int WHAT_GET_POINT_LOG = 1018;
    /**
     * 아이디 존재여부 조회
     */
    public static final int WHAT_USER_ID_CHECK = 1019;
    /**
     * 비밀번호 변경
     */
    public static final int WHAT_UPDATE_PASSWORD = 1020;
    /**
     * 포인트 환급
     */
    public static final int WHAT_POINT_REFUND = 1021;
    /**
     * 포인트 환급 내역
     */
    public static final int WHAT_POINT_REFUND_LOG = 1022;
    /**
     * 유저 주소지 입력
     */
    public static final int WHAT_INSERT_USER_ADDRESS = 1023;
    /**
     * 약정서 주소 가저오기
     */
    public static final int WHAT_SELECT_CONTRACT = 1024;
    /**
     * 약정등록지점, 주소지설정 , 직접입력
     * user_locl_info_seq ( 10:주소,20:약정,30:기타)
     */
    public static final int WHAT_INSERT_ADDRESS = 1025;

    /**
     * push_seq 받아서 해당 push의 click_cnt +1
     * st_seq 가 넘어왔을경우 해당 매장정보 return
     */
    public static final int WHAT_PUSH_UPDATE = 1026;

    /**
     * 서브카테 생성
     */
    public static final int WHAT_MAKE_SUBCATE = 1027;
    /**
     * 아이디 , 닉네임체크
     */
    public static final int WHAT_ID_CHECK = 1028;
    /**
     * searchActivity 지역리스트
     */
    public static final int WHAT_SELECT_ADDRLIST = 1029;
    /**
     * searchActivity 지하철리스트
     */
    public static final int WHAT_SEARCH_STATION = 1030;


}
