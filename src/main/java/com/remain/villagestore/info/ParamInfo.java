package com.remain.villagestore.info;

public class ParamInfo {

    //================================================================================
    // Common
    //================================================================================

    public final static String HASHKEY = "hashKey";
    public final static String LAT = "lat";
    public final static String LNG = "lng";
    public final static String LAT_LNG = "lat_lng";
    public final static String IS_CATE_PUSH = "is_cate_push";
    public final static String APP_VERSION = "appVersion";
    public final static String IS_OK = "isOk";
    public final static String OK = "ok";
    public final static String NO = "no";
    public final static String COLUMN = "column";
    public final static String IS_END = "isEnd";
    public final static String DATA = "data";
    public final static String PUSH_SEQ = "push_seq";

    //================================================================================
    // User
    //================================================================================

    public final static String ID = "id";
    public final static String PASS = "pass";
    public final static String USER_SEQ = "user_seq";
    public final static String USER_ID = "user_id";
    public final static String USER_PASS = "user_pass";
    public final static String DEVICE_ID = "device";
    public final static String LATITUDE = "latitude";
    public final static String LONGITUDE = "longitude";
    public final static String REG_DATE = "reg_date";
    public final static String NICKNAME = "nickname";
    public final static String FAVORITES = "favorites";
    public final static String PUSHID = "pushid";
    public final static String USE_LOC = "use_loc";
    public final static String USERLIST = "UserList";
    public final static String EVENT_CNT = "event_cnt";
    public final static String POINT = "point";
    public final static String LOCAL_SEQ = "local_seq";

    //================================================================================
    // Store
    //================================================================================
    public final static String SEARCH_TYPE = "search_type";
    public final static String SEARCH_REFRESH_CNT = "refresh_cnt";
    public final static String SEARCH_GPS = "GPS";
    public final static String SEARCH_GPS_DIS = "search_dis";
    public final static String SEARCH_ADDRESS = "ADDRESS";
    public final static String SEARCH_LIKE = "LIKE";
    public final static String SEARCH_SUBWAY = "SUBWAY";
    public final static String STORE = "store";
    public final static String STORE_LIST = "store_list";
    public final static String SUB_CATE_SEQ = "sub_cate_seq";
    public final static String SUB_CATE_LIST = "sub_cate_list";
    public final static String IS_SUBCATE = "isSubCate";
    public final static String STORE_SEQ = "store_seq";
    public final static String ADDRESS = "address";
    public final static String ADDRESS1 = "address1";
    public final static String ADDRESS2 = "address2";
    public final static String ADLIST = "adList";
    public final static String SELECTED_POS = "selected_pos";
    public final static String SEQ = "seq";
    public final static String ST_CATE = "st_cate";
    public final static String ST_SEQ = "st_seq";
    public final static String ST_LIKE = "st_like";
    public final static String ST_LAT = "st_lat";
    public final static String ST_LNG = "st_lng";
    public final static String ST_COMMENT = "st_comment";
    public final static String ST_HOURS = "st_hours";
    public final static String ST_ADDRESS = "st_address";
    public final static String ST_ADDRESS2 = "st_address2";
    public final static String ST_AUTH_KEY = "st_auth_key";
    public final static String Y = "Y";
    public final static String N = "N";
    public final static String ST_NAME = "st_name";
    public final static String ST_PHONE = "st_phone";
    public final static String CATE = "cate";
    public final static String SUB_CATE = "sub_cate_seq";
    public final static String STORE_URL = "URL";
    public final static String FILENAME = "filename";
    public final static String MAIN_IMAGES = "main_images";
    public final static String FAVORITE = "favorite";
    public final static String MAP_TITLE_TEXT = "map_title_text";
    public final static String RESULT = "result";
    public final static String TUTORIAL = "tutorial";
    public final static String SINGLE_MAP_MODE = "single_map_mode";
    public final static String DETAIL_MODE = "detail_mode";
    public final static String IS_LOC_UPDATED = "is_loc_updated";
    public final static String SITE_ADDRESS = "site_address";

    //================================================================================
    // ADDRESS
    //================================================================================
    /*
         user_locl_info_seq - 주소/약정/기타 여부( 10:주소,20:약정,30:기타)
		isPre - 대표설정여부 (Y:대표,N:안대표)
		user_locl_seq - 로컬의 시퀸스 ( 안넘어올경우 insert,넘어올경우 해당 seq Update)
	*/
    public final static String ADDRESS_INFO = "user_locl_info_seq";
    //10:주소
    public final static String ADDRESS_SIDO = "sidoStr";
    public final static String ADDRESS_SIGUNGU = "sigunguStr";
    public final static String ADDRESS_ZONECODE = "zcd";
    public final static String ADDRESS_FULL = "full_addr";
    public final static String ADDRESS_DORO_DETAIL = "road_det";
    public final static String ADDRESS_JIBUN_DETAIL = "jbn_det";
    public final static String ADDRESS_USER_DETAIL = "user_det";
    //20:약정
    public final static String ADDRESS_CONT_SEQ = "cont_seq";
    //30:기타
    public final static String ADDRESS_TEXT = "text";
    //공통
    public final static String ADDRESS_ISPRE = "isPre";
    public final static String ADDRESS_PRIO = "prio";
    //기존에 값이있어서 업데이트해야하는 경우
    public final static String ADDRESS_LOCAL_SEQ = "user_locl_seq";

    //================================================================================
    // Coupon
    //================================================================================
    public final static String ORDER_SEQ = "seq";
    public final static String STORE_CP_SEQ = "cp_seq";
    public final static String ORDER_PRICE = "price";
    public final static String ORDER_DATE = "createDate";
    public final static String ORDER_INFO = "cp_info";

    //================================================================================
    // SavePoint
    //================================================================================
    public final static String CP_USE_SEQ = "cp_use_seq";
    public final static String FINAL_PRICE = "r_price";

    //================================================================================
    // OrderInfo
    //================================================================================
    public final static String CP_SEQ = "cp_seq";
    public final static String CP_INFO = "cp_info";
    public final static String CP_STARTDATE = "cp_startDate";
    public final static String CP_ENDDATE = "cp_endDate";
    public final static String CP_LIMITE = "cp_limit";
    public final static String COUPONSEQ = "couponSeq";
    public final static String CNT = "cnt";

    //================================================================================
    // Menu
    //================================================================================
    public final static String STORE_MENU = "store_menu";
    public final static String NAME = "name";
    public final static String PRICE = "price";
    public final static String INFO = "info";
    public final static String MENU_LIST = "menuList";
    public final static String IMAGE = "image";
    public final static String STORE_MENU_LIST = "store_menu_list";
    public final static String IS_DEFAULT = "isDeft";

    //================================================================================
    // PointRefund
    //================================================================================
    public final static String REFUND_NAME = "user_name";
    public final static String REFUND_BANK = "bank_name";
    public final static String REFUND_ACCOUNT_NUM = "account_number";
    public final static String REFUND_PRICE = "reg_bill";
    public final static String REFUND_PHONE_NUM = "phone_number";


    //================================================================================
    // SEARCH
    //================================================================================
    public final static String SEARCH_SEQ = "addr_seq";
    public final static String SEL_ADDR_SEQ = "sel_addr_seq";
    public final static String SEARCH_IS_SIDO = "isSido";
    public final static String ADDRESS_LIST = "addr_list";
    public final static String IS_SIGUNGU = "is_sigungu";
    public final static String SEARCH_TEXT = "sch_text";
    public final static String DONG_LIST = "dong_list";
    public final static String GU_LIST = "gu_list";
    public final static String IS_RESEARCH = "is_resch";
    public final static String STN_SEQ = "stn_seq";
    public final static String STN_NAME = "stn_name";
    public final static String STN_LIST = "stn_list";
    public final static String COLOR_CD = "color_cd";
    public final static String LINE_SEQ = "line_seq";
    public final static String LN_NAME = "ln_name";
    public final static String IS_RESCH = "is_resch";
    public final static String P_ADDR_SEQ = "p_addr_seq";

    //================================================================================
    // Etc
    //================================================================================

    public final static String AD_LIST = "adList";
    public final static String FILEURL = "fileURL";
    public final static int RESULT_UPDATE = 101;
    public final static int REQUEST_UPDATE = 1001;
    public final static int REQUEST_LOGIN_UPDATE = 2001;

    //================================================================================
    // Preference
    //================================================================================
    /**
     * 전체 알림 여부
     */
    public final static String ALL_ALARM = "all_alarm";
    /**
     * 이용약관 동의 여부
     */
    public final static String AGREE_TERM_OF_SERVICE = "agree_term_of_service";
    /**
     * 닉네임 입력 여부
     */
    public final static String EDIT_NICKNAME = "edit_nickname";
    /**
     * 닉네임 입력 여부
     */
    public final static String IS_MAIN_TUTORIAL = "is_main_tutorial";
    /**
     * 닉네임 입력 여부
     */
    public final static String IS_DETAIL_TUTORIAL = "is_detail_tutorial";
    /**
     * 닉네임 입력 여부
     */
    public final static String IS_MENU_TUTORIAL = "is_menu_tutorial";
    /**
     * 닉네임 입력 여부
     */
    public final static String IS_CATE_TUTORIAL = "is_cate_tutorial";
    /**
     * 위치 저장
     */
    public final static String SEARCH_LOC = "search_loc";

}
