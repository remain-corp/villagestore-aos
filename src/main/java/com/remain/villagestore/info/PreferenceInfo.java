package com.remain.villagestore.info;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.remain.villagestore.R;

public class PreferenceInfo {

	//================================================================================
    // XXX Constants
    //================================================================================
	/** 프리퍼런스 이름 */
	private static final String PREFERENCE_NAME = "VillageStore";
	//================================================================================
    // XXX Etc Method
    //================================================================================
	/**
	 * 프리퍼런스 가져오기
	 * @return
	 */
	private static SharedPreferences getPrefer(Context context) {
		SharedPreferences prefer = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
		return prefer;
	}
	
	/**
	 * 모든 저장된 preference 삭제
	 */
	public static void clear(Context context) {
		
		Editor editor = getPrefer(context).edit();
		
		editor.clear();
		editor.commit();
	}

	//================================================================================
	// XXX 알림 관련
	//================================================================================

	/**
	 * 전체 알림 여부 저장
	 * @param context
	 * @param isAll
	 */
	public static void saveAllAlarm(Context context, boolean isAll) {

		Editor editor = getPrefer(context).edit();
		editor.putBoolean(com.remain.villagestore.info.ParamInfo.ALL_ALARM, isAll);
		editor.commit();
	}

	/**
	 * 전체 알림 여부 꺼내기
	 * @param context
	 * @return
	 */
	public static boolean loadAllAlarm(Context context) {
		return getPrefer(context).getBoolean(com.remain.villagestore.info.ParamInfo.ALL_ALARM, true);
	}
	/**
	 * 이용 약관 동의 여부 저장
	 * @param context
	 * @param isAgree
	 */
	public static void saveAgreeTermOfService(Context context, boolean isAgree) {

		Editor editor = getPrefer(context).edit();
		editor.putBoolean(com.remain.villagestore.info.ParamInfo.AGREE_TERM_OF_SERVICE, isAgree);
		editor.commit();
	}

	/**
	 * 이용 약관 동의 여부 꺼내기
	 * @param context
	 * @return
	 */
	public static boolean loadAgreeTermOfService(Context context) {
		return getPrefer(context).getBoolean(com.remain.villagestore.info.ParamInfo.AGREE_TERM_OF_SERVICE, false);
	}
	/**
	 * 닉네임 수정 여부 저장
	 * @param context
	 * @param isEdit
	 */
	public static void saveEditNickname(Context context, boolean isEdit) {

		Editor editor = getPrefer(context).edit();
		editor.putBoolean(com.remain.villagestore.info.ParamInfo.EDIT_NICKNAME, isEdit);
		editor.commit();
	}

	/**
	 * 닉네임 수정 여부 꺼내기
	 * @param context
	 * @return
	 */
	public static boolean loadEditNickname(Context context) {
		return getPrefer(context).getBoolean(com.remain.villagestore.info.ParamInfo.EDIT_NICKNAME, false);
	}
	/**
	 * 닉네임 수정 여부 저장
	 * @param context
	 */
	public static void savePushId(Context context, String pushId) {

		Editor editor = getPrefer(context).edit();
		editor.putString(com.remain.villagestore.info.ParamInfo.PUSHID, pushId);
		editor.commit();
	}

	/**
	 * 닉네임 수정 여부 꺼내기
	 * @param context
	 * @return
	 */
	public static String loadPushId(Context context) {
		return getPrefer(context).getString(com.remain.villagestore.info.ParamInfo.PUSHID, com.remain.villagestore.info.AppInfo.DEFAULT_STRING);
	}

	//================================================================================
	// XXX 튜토리얼 관련
	//================================================================================

	/**
	 * 메인 튜토 여부 저장
	 * @param context
	 */
	public static void saveTutorialMain(Context context, boolean isShow) {

		Editor editor = getPrefer(context).edit();
		editor.putBoolean(com.remain.villagestore.info.ParamInfo.IS_MAIN_TUTORIAL, isShow);
		editor.commit();
	}

	/**
	 * 메인 튜토 여부 꺼내기
	 * @param context
	 * @return
	 */
	public static boolean loadTutorialMain(Context context) {
		return getPrefer(context).getBoolean(com.remain.villagestore.info.ParamInfo.IS_MAIN_TUTORIAL, false);
	}

	/**
	 * 위치 정보 저장
	 * @param context
	 */
	public static void saveLocationSearch(Context context, String searchLoc) {

		Editor editor = getPrefer(context).edit();
		editor.putString(com.remain.villagestore.info.ParamInfo.SEARCH_LOC, searchLoc);
		editor.commit();
	}

	/**
	 * 위치 정보 꺼내기
	 * @param context
	 * @return
	 */
	public static String loadLocationSearch(Context context) {
		return getPrefer(context).getString(com.remain.villagestore.info.ParamInfo.SEARCH_LOC, context.getResources().getString(R.string.default_search));
	}


	/**
	 * 위치 정보 저장
	 * @param context
	 */
	public static void saveFullLocationSearch(Context context, String searchLoc) {

		Editor editor = getPrefer(context).edit();
		editor.putString(com.remain.villagestore.info.ParamInfo.SEARCH_LOC, searchLoc);
		editor.commit();
	}

	/**
	 * 위치 정보 꺼내기
	 * @param context
	 * @return
	 */
	public static String loadLatitude(Context context) {
		return getPrefer(context).getString(com.remain.villagestore.info.ParamInfo.SEARCH_LOC, com.remain.villagestore.info.AppInfo.DEFAULT_STRING);
	}

}
