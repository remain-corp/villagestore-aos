package com.remain.villagestore.info;

import android.content.Context;

import com.remain.villagestore.component.SettingContentView;
import com.remain.villagestore.pages.p4_other.setting.activity.UserSettingActivity;


/**
 * Created by woongjaelee on 15. 8. 15..
 */
public class PushInfo {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================
    // App

    // Content
    private Context context;

    // Widget

    // View

    // Apdater

    // Array

    // Lang
    private boolean isAllPushAlarm;

    // Component

    // Manager

    // Util

    // Interface

    //================================================================================
    // XXX Constructor
    //================================================================================
    public PushInfo(Context context) {
        this.context = context;

        initIsOn();
    }

    //================================================================================
    // XXX Enum
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================
    public boolean isOn(SettingContentView.SettingMenu settingMenu) {

        switch (settingMenu) {

            case PUSH_ALARM:
                return isAllPushAlarm;
            default:
                return false;
        }
    }

    public boolean setIsOn(SettingContentView.SettingMenu settingMenu, boolean isOn) {

        switch (settingMenu) {
            case PUSH_ALARM:
                return isAllPushAlarm = isOn;
            default:
                return false;
        }
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================
    public void initIsOn() {

        this.isAllPushAlarm = com.remain.villagestore.info.PreferenceInfo.loadAllAlarm(context);

    }
    public void saveIsOn() {

        PreferenceInfo.saveAllAlarm(context, isAllPushAlarm);
    }

    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================
}