package com.remain.villagestore.manager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.text.format.*;
import android.text.format.DateFormat;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.remain.villagestore.R;
import com.remain.villagestore.common.C;
import com.remain.villagestore.common.Utils;

/**
 * Created by woongjaelee on 15. 8. 21..
 */
/*
 * 알람을 관리하는 클래스
 */
public class AlarmManager {

    //================================================================================
    // XXX Constants
    //================================================================================
    /** Notification의 공통 ID */
    public final int NOTI_ID = 1;

    /** 글자 수 제한 */
    public final int NOTI_MAX_LENGTH = 100;

    //================================================================================
    // XXX Variables
    //================================================================================
    // This
    private static AlarmManager notiManager_instance;

    // App
    private NotificationManager notificationManager;

    // Content
    private Context context;

    //================================================================================
    // XXX Constructors
    //================================================================================
    public AlarmManager(Context context) {

        this.context = context;
        // 노티피케이션 매니저에 대한 레퍼런스를 얻기
        String ns = Context.NOTIFICATION_SERVICE;
        notificationManager = (NotificationManager) context.getSystemService(ns);
    }

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    /**
     * 전체 알림 여부 가져오기
     * @return
     */
    public boolean isNoticeAlarm() {
        return (boolean) Utils.getPreferenceData(context, C.pref.PUSH_SWITCH, true);
    }

    /**
     * 전체 알림 여부
     * @return
     */
    public void setNoticeAlarm(boolean isNoti) {
        Utils.setPreferenceData(context, C.pref.PUSH_SWITCH, isNoti);
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================
    /**
     * 자신의 객체를 싱글턴으로 만들어 넘겨주는 메소드
     * @return 자신의 객체
     */
    public static AlarmManager getInstance(Context context) {

        if (notiManager_instance == null) {
            notiManager_instance = new AlarmManager(context);
        }
        return notiManager_instance;
    }

    /**
     * NotificationManager 객체반환(시스템서비스)
     * @return NotificationManager 객체
     */
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    /**
     *
     * @param context
     *            현재 context
     * @param intent
     *            이동시킬 인텐트
     */
    public void sendNotification(Context context, Intent intent,String title ,String message, int cateInfo) {

        if(context == null) this.context = context;

        // 전체 알림 껏을때 메시지 안보이기
        if(!isNoticeAlarm()) {
            return;
        }

        Bitmap largeIcon ;
        int smallIconRes = R.mipmap.ic_launcher;


//        Bitmap originBitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
//        if(originBitmap != null) {
//            largeIcon = ImageUtil.getInstance().resizeBitmapWithWidth(originBitmap,300);
//            originBitmap.recycle();
//        }

        String ticker = context.getResources().getString(R.string.all_push_ticker);
        smallIconRes = R.mipmap.push_icon_small;

        switch (cateInfo){
            case 1 :
                largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ico_push_all);
                break;
            case 2 :
                largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ico_push_contract);
                break;
            case 3 :
                largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ico_push_contract);
                break;
            case 4 :
                largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ico_push_like);
                break;
            default:
                largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ico_push_all);
                break;
        }

/*

        if(isCatePush) {


            largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ico_push_like);
            text = String.format(context.getResources().getString(R.string.cate_push_message_form),storeName,message);
            title = context.getResources().getString(R.string.cate_push_title);
            ticker = context.getResources().getString(R.string.cate_push_ticker);

        } else {

            smallIconRes = R.mipmap.push_icon_small;
            largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
            title = context.getResources().getString(R.string.all_push_title);

        }*/

        // 노티피케이션의 확장 메시지와 인텐트를 정의
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(smallIconRes)
                .setTicker(ticker)
                .setLargeIcon(largeIcon)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(contentIntent);

        builder.setDefaults(Notification.DEFAULT_ALL);
        NotificationCompat.BigTextStyle bigTextStyle = null;

        bigTextStyle = new NotificationCompat.BigTextStyle(builder)
                .bigText(message);

        // 노티피케이션 매니저에게 노티피케이션을 전달
        notificationManager.notify(NOTI_ID, bigTextStyle.build());

    }

    /**
     * 노티 날리기
     */
    public void checkNotification() {
        notificationManager.cancel(NOTI_ID);
    }

}
