package com.remain.villagestore.manager;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.remain.villagestore.pages.p2_main.activity.MainActivity;

/**
 * Created by remain on 2016. 7. 18..
 */
public class BackPressCloseHandler {
    private long backKeyPressedTime = 0;
    private Toast toast;

    private Activity activity;

    public BackPressCloseHandler(Activity context) {
        this.activity = context;
    }

    public void onBackPressed() {
        if (System.currentTimeMillis() > backKeyPressedTime + 3000) {
            backKeyPressedTime = System.currentTimeMillis();
            showGuide();
            return;
        }
        if (System.currentTimeMillis() <= backKeyPressedTime + 3000) {
            toast.cancel();

            /*Intent t = new Intent(activity, MainActivity.class);
            activity.startActivity(t);*/

            activity.moveTaskToBack(true);
            activity.finish();
            //android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    public void showGuide() {
        toast = Toast.makeText(activity, "뒤로 버튼을 한번 더 누르시면 앱이 종료됩니다.", Toast.LENGTH_SHORT);
        toast.show();
    }

}
