package com.remain.villagestore.manager;

import com.remain.villagestore.dto.Delivery;
import com.remain.villagestore.dto.Store;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.util.Log;

/**
 * Created by remain on 2016. 4. 24..
 */
public class DataManager {
    private static DataManager dataManager = new DataManager();
    private String pushID = "";
    private User user;
    private Store store;
    private Delivery delivery;

    public static DataManager getInstance(){
        return dataManager;
    }

    public String getPushID() {
        return pushID;
    }

    public void setPushID(String pushID) {
        this.pushID = pushID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }
}
