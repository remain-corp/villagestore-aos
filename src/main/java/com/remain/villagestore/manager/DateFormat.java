package com.remain.villagestore.manager;

import android.content.Context;

import com.remain.villagestore.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateFormat {

		
	//================================================================================
    // XXX Constants
    //================================================================================
	/** GMT 시간대 */
	public static final String GMT_TIME = "+0000";
	
	/** Date의 기본 Format */
	public static final String FORMAT_DATE_DEFAULT = "yyyy-MM-dd HH:mm:ss";
	/** Date의 GMT가 포함된 Format */
	public static final String FORMAT_DATE_GMT = "yyy-MM-dd HH:mm:ssZ";
	/** 날짜, 시간을 표시하는 Format (2014.1.1 오전 10:00) */
	public static final String FORMAT_DATE_DATE_TIME = "yyyy.M.d a hh:mm";
	/** 날짜, 시간을 표시하는 Format (2014/01/01 오전 10:00) */
	public static final String FORMAT_FULL_DATE_TIME = "yyyy/MM/dd a hh:mm";
	/** 시간부분만 표시하는 Format (10:00) */
	public static final String FORMAT_DATE_ONLY_TIME = "HH:mm";
	/** 날짜부분만 표시하는 Format (2014/01/01) */
	public static final String FORMAT_DATE_ONLY_DATE = "yyyy/MM/dd EEEE";
	/** 채팅방 공지에 사용되는 Format (1. 1. 0:03am) */
	public static final String FORMAT_CHAT_NOTICE_DATE = "M. d. h:mm a";
	/** 앨범 파일에 사용되는 Format (2014. 1. 1) */
	public static final String FORMAT_CHAT_ALBUM_DATE = "yyyy. M. d";
	/** birth Format (2014-1-1) */
	public static final String FORMAT_REG_DATE = "yyyy-MM-dd";
	/** birth */
	public static final String FORMAT_DATE_YEAR = "yyyy";
	/** 쿠폰 사용일 format */
	public static final String FORMAT_DATE_COUPON_USE_DATE = "yyyy년 MM월dd일";
	/** 요일 가져올떄 */
	public static final String FORMAT_DATE_WEEKDAY = "EEE";
	/** 쿠폰 발급일 사용시 보여줄   */
	public static final String FORMAT_COUPON_REG_DATE = "yyyy-MM-dd a hh시 mm분";

	public final static long MpSec = 1000;
	public final static long MpMin = 60 * 1000;
	public final static long MpHou = 60 * 60 * 1000;
	public final static long MpDay = 24 * 60 * 60 * 1000;
	
	//================================================================================
    // XXX Enum
    //================================================================================
	public enum MpType {
		
		/** 초 */
		SEC,
		/** 초 2 */
		SEC2,
		/** 분 */
		MIN,
		/** 시 */
		HOU,
		/** 일 */
		DAY
	}
	
	//================================================================================
	// XXX Etc Method
	//================================================================================
	/**
	 * GMT 표준시간을 기기의 시간대로 변환한다.
	 * @param gmtTime : Server의 시간 (GMT 표준시간대)
	 * @return : 변환된 시간대
	 */
	public static String parseGMTTime(String gmtTime) {
		
		// gmtTime 값이 유효한지 확인
		if (gmtTime == null) return null;
		SimpleDateFormat dateFormatDefault = new SimpleDateFormat(FORMAT_DATE_DEFAULT);
		try {
			dateFormatDefault.parse(gmtTime);
		} catch (ParseException e) {
			Log.e("Date Format ParseException : " + gmtTime + " is not format \"" + FORMAT_DATE_DEFAULT + "\"");
			return null;
		}
		
		// gmt 시간대에서 기기 시간으로 다시 formatting
		String time = gmtTime + GMT_TIME;
		String convertTime = null;
		SimpleDateFormat dateFormatGMT = new SimpleDateFormat(FORMAT_DATE_GMT);
		try {
			convertTime = dateFormatDefault.format(dateFormatGMT.parse(time));
		} catch (ParseException e) {
			Log.e("Date Format ParseException : " + time + " is not format \"" + FORMAT_DATE_GMT + "\"");
		}
		
		return convertTime;
	}
	
	/**
	 * 현재시간을 GMT시간대에 맞게 변경한다 (ex.한국시간 -9시)
	 * @return GMT시간대로 바꾼 현재시간 (Format : FORMAT_DATE_DEFAULT)
	 */
	public static String parseNowTimeToGMT() {
		
		String gmtTime = null;
		SimpleDateFormat dateFormatDefault = new SimpleDateFormat(FORMAT_DATE_DEFAULT);
		dateFormatDefault.setTimeZone(TimeZone.getTimeZone("GMT"));
		gmtTime = dateFormatDefault.format(new Date());
		return gmtTime;
	}

	/**
	 * 현재시간을 GMT시간대에 맞게 변경한다 (ex.한국시간 -9시)
	 * @return GMT시간대로 바꾼 현재시간 long타입 (Format : FORMAT_DATE_DEFAULT)
	 */
	public static long parseNowTimeToGMTLong() {
		
		String gmtTime = null;
		SimpleDateFormat dateFormatDefault = new SimpleDateFormat(FORMAT_DATE_DEFAULT);
		dateFormatDefault.setTimeZone(TimeZone.getTimeZone("GMT"));
		gmtTime = dateFormatDefault.format(new Date());
		Date d = null;
		try {
			d = dateFormatDefault.parse(gmtTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return d.getTime();
	}
	
	/**
	 * String으로 되어 있는 Date값을 Date 형태로 바꿔준다. 
	 * 단, String은 "yyyy-MM-dd HH:mm:ss" 형태여야 한다.
	 * @param dateStr : String 형태의 날짜
	 * @return : String 형태의 날짜를 Date로 바꾼 객체. String 형태의 날짜가 기본 포맷에 맞지 않으면 null을 리턴
	 */
	public static Date parseStringToDate(String dateStr) {
		
		if (dateStr == null) {
			return null;
		}
		
		SimpleDateFormat dateFormatDefault = new SimpleDateFormat(FORMAT_DATE_DEFAULT);
		Date parseDate = null;
		try {
			parseDate = dateFormatDefault.parse(dateStr);
		} catch (ParseException e) {
			Log.e("Date Format ParseException : " + dateStr + " is not format \"" + FORMAT_DATE_DEFAULT + "\"");
		}
		
		return parseDate;
	}
	
	public static Date parseYearToData(String dateStr) {
		
		if (dateStr == null) {
			return null;
		}
		
		SimpleDateFormat dateFormatDefault = new SimpleDateFormat(FORMAT_DATE_YEAR);
		Date parseDate = null;
		try {
			parseDate = dateFormatDefault.parse(dateStr);
		} catch (ParseException e) {
			Log.e("Date Format ParseException : " + dateStr + " is not format \"" + FORMAT_DATE_YEAR + "\"");
		}
		
		return parseDate;
	}
	
	/**
	 * Long으로 되어 있는 GMT 표준시간(millisecond)을 String 형태의 기기의 시간대로 변환한다.
	 * @param milliseconds millisecond 시간
	 * @return
	 */
	public static String parseLongToString(long milliseconds) {

		Date parseDate = new Date(milliseconds);
		return parseDateToString(parseDate);
	}

	/**
	 * Date 형식의 날짜를 문자열로 변환한다.
	 * @param date 변환할 날짜
	 * @return 변환된 문자열
	 */
	public static String parseDateToString(Date date) {

		SimpleDateFormat dateFormatDefault = new SimpleDateFormat(FORMAT_DATE_DEFAULT);
		
		return dateFormatDefault.format(date);
	}
	/**
	 * Date 형식의 날짜를 문자열로 변환한다.
	 * @param date 변환할 날짜
	 * @return 변환된 문자열
	 */
	public static String parseDateToString(Date date, String format,Locale locale) {
		
		SimpleDateFormat dateFormatDefault = new SimpleDateFormat(format, locale);
		
		return dateFormatDefault.format(date);
	}
	/**
	 * Date 형식의 날짜를 문자열로 변환한다.
	 * @param date 변환할 날짜
	 * @return 변환된 문자열
	 */
	public static String parseDateToString(Date date, String format) {
		
		SimpleDateFormat dateFormatDefault = new SimpleDateFormat(format);
		
		return dateFormatDefault.format(date);
	}
	
	/**
	 * 날짜형식의 String을 지정한 format의 String으로 변환해준다.
	 * @param dateStr : 기본 날짜 형식의 String ("yyyy-MM-dd HH:mm:ss")
	 * @param format : 변경하기 원하는 format
	 * @return : 원하는 format으로 변경된 날짜 String, 중도 에러나면 null을 리턴
	 */
	public static String formatDate(String dateStr, String format) {
		
		if (dateStr == null) {
			return null;
		}
		
		String formatDate = null;
		
		Date date = parseStringToDate(dateStr);
		if (date != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			formatDate = dateFormat.format(date);
		}
		
		return formatDate;
	}
	
	/**
	 * 날짜형식의 String을 지정한 format의 String으로 변환해준다.
	 * @param dateStr : 기본 날짜 형식의 String ("yyyy-MM-dd )
	 * @param format : 변경하기 원하는 format
	 * @return : 원하는 format으로 변경된 날짜 String, 중도 에러나면 null을 리턴
	 */
	public static String regDate(String dateStr, String format) {
		
		if (dateStr == null) {
			return null;
		}
		
		String formatDate = null;
		
		if (dateStr == null) {
			return null;
		}
		
		SimpleDateFormat dateFormatDefault = new SimpleDateFormat(FORMAT_REG_DATE);
		Date parseDate = null;
		try {
			parseDate = dateFormatDefault.parse(dateStr);
		} catch (ParseException e) {
			Log.e("Date Format ParseException : " + dateStr + " is not format \"" + FORMAT_REG_DATE + "\"");
		}
		
		if (parseDate != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			formatDate = dateFormat.format(parseDate);
		}
		
		return formatDate;
	}
	
	/**
	 * 현재 시간 가져오기
	 * @return 현재 시간 "yyyy-MM-dd HH:mm:ss" 형태
	 */
	public static String getCurrentTime() {

		SimpleDateFormat dateFormatDefault = new SimpleDateFormat(DateFormat.FORMAT_DATE_DEFAULT);
		Date date = new Date(System.currentTimeMillis());
		String time = dateFormatDefault.format(date);
		
		return time;
	}
	/**
	 * 현재 시간 가져오기
	 * @return 현재 시간 "yyyy-MM-dd HH:mm:ss" 형태
	 */
	public static String getCurrentTime(String format) {

		SimpleDateFormat dateFormatDefault = new SimpleDateFormat(format);
		Date date = new Date(System.currentTimeMillis());
		String time = dateFormatDefault.format(date);

		return time;
	}
	
	
	/**
	 * 시간 전, 후 확인 (기준은 첫번째 값)
	 * @param context
	 * @param arrayTime
	 * @return
	 */
	public static boolean getTimeAfter(Context context, String[] arrayTime) {
		return getTimeAfter(context, arrayTime, FORMAT_DATE_DEFAULT);
	}
	
	public static boolean getTimeAfter(Context context, String[] arrayTime, String format) {
		
		if (arrayTime == null || arrayTime[0] == null || arrayTime[1] == null) {
			return false;
		}
		
//		Log.e("arrayTime[0] : " + arrayTime[0] + ", arrayTime[1] : " + arrayTime[1]);
		
		Date previousDate = parseStringToDate(arrayTime[0]);
		Date presentDate = parseStringToDate(arrayTime[1]);
		
		if (previousDate.after(presentDate)) {
			return true;
		} else {
			return false;
		}
	}
}
