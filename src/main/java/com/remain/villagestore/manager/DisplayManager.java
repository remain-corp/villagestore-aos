package com.remain.villagestore.manager;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.util.TypedValue;
import android.view.Display;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.remain.villagestore.info.AppInfo;


/**
 * 디바이스 화면 정보 클래스
 */
public class DisplayManager {

    //================================================================================
    // XXX Variables
    //================================================================================
    /** App의 컨택스트 */
    private Context context;
    /** 디스플래이 객체 */
    private Display display;
    /** 기준이 되는 화면 크기 (width) */
    private int displayWidth;
    /** 기준이 되는 화면 크기 (height) */
    private int displayHeight;
    /** 현재 디바이스 화면의 가로크기 (width) */
    private float standardWidth;
    /** 현재 디바이스 화면의 세로크기 (height) */
    private float standardHeight;
    /** 기준 디바이스 화면 크기 대비 현재 디바이스 화면의 가로비율 (width) */
    private float scaleX;
    /** 기준 디바이스 화면 크기 대비 현재 디바이스 화면의 세로비율 (height) */
    private float scaleY;

    //================================================================================
    // XXX Constructors
    //================================================================================
    public DisplayManager(Context context) {
        this(context, AppInfo.STANDARD_WIDTH, AppInfo.STANDARD_HEIGHT);
    }

    public DisplayManager(Context context, float standardWidth, float standardHeight) {

        this.context = context;
        this.standardWidth = standardWidth;
        this.standardHeight = standardHeight;

        getDisplay();
    }

    //================================================================================
    // XXX Getter, Setter
    //================================================================================
    /**
     * 현재 디바이스 화면의 가로크기를 가져온다
     * @return
     */
    public int getDisplayWidth() {
        return displayWidth;
    }

    /**
     * 현재 디바이스 화면의 세로크기를 가져온다
     * @return
     */
    public int getDisplayHeight() {
        return displayHeight;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @SuppressWarnings("deprecation")
    /**
     * Display 객체 가져오기 (디바이스 크기 초기화 포함)
     * @return
     */
    public Display getDisplay() {

        if (display == null) {

            display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

            // 디스플레이 크기 설정
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {

                // API 13 이전버전
                displayWidth = display.getWidth();
                displayHeight = display.getHeight();
                if (displayWidth > displayHeight) {
                    displayWidth = display.getHeight();
                    displayHeight = display.getWidth();
                }

            } else {

                // API 13 이상버전
                Point size = new Point();
                display.getSize(size);
                displayWidth = size.x;
                displayHeight = size.y;
                if (displayWidth > displayHeight) {
                    displayWidth = size.y;
                    displayHeight = size.x;
                }
            }
        }

        return display;
    }

    /**
     * <p>
     * 	{@link #getScaleSameRate} 사용을 권장
     * </p>
     *
     * 기준크기에 대한 현재 디바이스 비율 가져오기 (width)
     * @return 가로 비율
     */
    public float getScaleX() {

        if (scaleX == 0f) {
            scaleX = (float) displayWidth / standardWidth;
        }
        return scaleX;
    }

    /**
     * <p>
     * 	{@link #getScaleSameRate} 사용을 권장
     * </p>
     *
     * 기준크기에 대한 현재 디바이스 비율 가져오기 (height)
     * @return 세로 비율
     */
    public float getScaleY() {

        if (scaleY == 0f) {
            scaleY = (float) displayHeight / standardHeight;
        }
        return scaleY;
    }

    /**
     * 기준크기에 대한 현재 디바이스 비율 가져오기
     * @return 가로세로 중 큰 사이즈의 기준크기에 대한 현재 디바이스 비율
     */
    public float getScaleSameRate() {

        float scale = 0;
        if (getScaleX() < getScaleY()) scale = getScaleX();
        else scale = getScaleY();

        return scale;
    }

    /**
     * 기준크기에 대한 현재 디바이스 비율 가져오기 (가로세로 중 큰 사이즈로 맞춤)
     * @return 가로세로 중 큰 사이즈로 맞춰진 사이즈
     */
    @SuppressWarnings("deprecation")
    public int getScaleSizeSameRate(int size) {

        if (size == LayoutParams.FILL_PARENT
                || size == LayoutParams.MATCH_PARENT
                || size == LayoutParams.WRAP_CONTENT) {
            return size;
        }

        float scale = 0;
        if (getScaleX() < getScaleY()) scale = getScaleX();
        else scale = getScaleY();

        return (int) (scale * size) <= 0 ? 1 : (int) (scale * size);
    }

    /**
     * <p>
     * 	{@link #getScaleSizeSameRate} 사용을 권장
     * </p>
     *
     * 기준 크기의 width 사이즈를 디바이스 크기에 맞게 조절해준다.
     * @param width 기준크기에서의 width 크기
     * @return 수정한 width 값
     */
    @SuppressWarnings("deprecation")
    public int getWidth(int width) {

        if (width == LayoutParams.FILL_PARENT
                || width == LayoutParams.MATCH_PARENT
                || width == LayoutParams.WRAP_CONTENT) {
            return width;
        }

        return (int) (getScaleX() * width) <= 0 ? 1 : (int) (getScaleX() * width);
    }

    /**
     * <p>
     * 	{@link #getScaleSizeSameRate} 사용을 권장
     * </p>
     *
     * 기준 크기의 height 사이즈를 디바이스 크기에 맞게 조절해준다.
     * @param height 기준크기에서의 height 크기
     * @return 수정한 height 값
     */
    @SuppressWarnings("deprecation")
    public int getHeight(int height) {

        if (height == LayoutParams.FILL_PARENT
                || height == LayoutParams.MATCH_PARENT
                || height == LayoutParams.WRAP_CONTENT) {
            return height;
        }

        return (int) (getScaleY() * height) <= 0 ? 1 : (int) (getScaleY() * height);
    }

    /**
     * <p>
     * 	{@link #changeWidthHeightSameRate} 사용을 권장
     * </p>
     *
     * 레이아웃의 가로와 세로를 한번에 디바이스 크기에 맞게 고쳐준다.
     * @param param 수정할 레이아웃의 LayoutParams
     * @param width 수정할 이미지 가로 크기
     * @param height 수정할 이미지 세로 크기
     */
    @Deprecated
    public void changeWidthHeight(LayoutParams param, int width, int height) {
        changeWidthHeightSameRate(param, width, height);
    }

//	/**
//	 * <p>
//	 * 	{@link #changeWidthHeightSameRate} 사용을 권장
//	 * </p>
//	 * 
//	 * 레이아웃의 가로와 세로를 한번에 디바이스 크기에 맞게 고쳐준다.
//	 * @param param 수정할 레이아웃의 LayoutParams
//	 * @param width 수정할 이미지 가로 크기
//	 * @param height 수정할 이미지 세로 크기
//	 */
//	public void changeWidthHeight(LayoutParams param, int width, int height) {
//		
//		param.width = getWidth(width);
//		param.height = getHeight(height);
//	}

    /**
     * 레이아웃의 가로와 세로를 한번에 디바이스 크기에 맞게 고쳐준다.
     * 가로 비율에 맞춰 수정한다.
     * @param param 수정할 레이아웃의 LayoutParams
     * @param width 수정할 이미지 가로 크기
     * @param height 수정할 이미지 세로 크기
     */
    @SuppressWarnings("deprecation")
    public void changeWidthSameRate(LayoutParams param, int width, int height) {

        float scale = getScaleX();

        if (width == LayoutParams.FILL_PARENT
                || width == LayoutParams.MATCH_PARENT
                || width == LayoutParams.WRAP_CONTENT) {
            param.width = width;
        } else {
            param.width = (int) (scale * width) <= 0 ? 1 : (int) (scale * width);
        }

        if (height == LayoutParams.FILL_PARENT
                || height == LayoutParams.MATCH_PARENT
                || height == LayoutParams.WRAP_CONTENT) {
            param.height = height;
        } else {
            param.height = (int) (scale * height) <= 0 ? 1 : (int) (scale * height);
        }
    }

    /**
     * 레이아웃의 가로와 세로를 한번에 디바이스 크기에 맞게 고쳐준다.
     * 가로세로 중 더 작은 비율에 맞춰 수정한다.
     * @param param 수정할 레이아웃의 LayoutParams
     * @param width 수정할 이미지 가로 크기
     * @param height 수정할 이미지 세로 크기
     */
    @SuppressWarnings("deprecation")
    public LinearLayout.LayoutParams changeWidthHeightSameRate(LayoutParams param, int width, int height) {

        float scale = 0;
        if (getScaleX() < getScaleY()) scale = getScaleX();
        else scale = getScaleY();

        if (width == LayoutParams.FILL_PARENT
                || width == LayoutParams.MATCH_PARENT
                || width == LayoutParams.WRAP_CONTENT) {
            param.width = width;
        } else {
            param.width = (int) (scale * width) <= 0 ? 1 : (int) (scale * width);
        }

        if (height == LayoutParams.FILL_PARENT
                || height == LayoutParams.MATCH_PARENT
                || height == LayoutParams.WRAP_CONTENT) {
            param.height = height;
        } else {
            param.height = (int) (scale * height) <= 0 ? 1 : (int) (scale * height);
        }
        return null;
    }

    /**
     * DP 수치 구하기
     * @return  DP 수치
     */
    public float getDip() {
        return context.getResources().getDisplayMetrics().density;
    }

    /**
     * Pixel을 DP로 변경해주는 메서드
     * @param dp
     * @return
     */
    public int getPxFromDp(int px) {

        int dp = 0;
        dp = (int) (px * getDip());
        return dp;
    }

    /**
     * DP를 Pixel로 변경해주는 메서드
     * @param dp
     * @return
     */
    public int getDpFromPx(int dp) {

        int px = 0;
        px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
        return px;
    }

    /**
     * 스테이터스 바(status bar) 높이 구하기
     * @return
     */
    public int getStatusBarHeight() {

        int statusHeight = 0;
        int screenSizeType = (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK);

        if (screenSizeType != Configuration.SCREENLAYOUT_SIZE_XLARGE) {

            int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");

            if (resourceId > 0) {
                statusHeight = context.getResources().getDimensionPixelSize(resourceId);
            }
        }

        return statusHeight;
    }
}
