package com.remain.villagestore.manager;

import android.content.Context;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

/**************************************************
 *
 * ImageLoaderManager
 * 이미지를 불러오는데 사용하는 라이브로리인 ImageLoader를
 * 간편하게 사용하기 위한 매니저 클래스
 *
 **************************************************/
public class ImageLoaderManager {

	// Acceptable URIs
	/** from Web */
	public static final String URIS_WEB = "http://";
	/** from SD card */
	public static final String URIS_SD_CARD = "file://";
	/** from content provider */
	public static final String URIS_CONTENT_PROVIDER = "content://";
	/** from assets */
	public static final String URIS_ASSETS = "assets://";
	/** from drawables (non-9patch images) */
	public static final String URIS_DRAWABLES = "drawable://";
	
	private static ImageLoaderManager instance;
	private DisplayImageOptions displayImageOptions;
	private DisplayImageOptions displayImageOptionsCache;
	private DisplayImageOptions displayImageOptionsExif;
	private DisplayImageOptions displayImageOptionsGallery;
	private DisplayImageOptions displayImageOptionsPost;
	private DisplayImageOptions displayImageOptionsListPost;
	private DisplayImageOptions displayImageOptionsPostProfile;
	private DisplayImageOptions displayImageOptionsPostListProfile;
	private DisplayImageOptions displayImageOptionsCommentListProfile;
	private DisplayImageOptions displayImageOptionsProfileBackground;
	private DisplayImageOptions displayImageOptionsProfileEditProfile;
	private DisplayImageOptions displayImageOptionsLoadPostImage;
	
	private int imageWidth, imageHeight;
	public static boolean booleanCheck;
	
	private ImageLoaderManager(Context context) {
		
		imageWidth = 480;
		imageHeight = 800;
		
		File cacheDir = StorageUtils.getCacheDirectory(context);
		
		ImageLoaderConfiguration imageLoaderConfiguration = new ImageLoaderConfiguration.Builder(context)
		.denyCacheImageMultipleSizesInMemory()
		.writeDebugLogs()
		.build();
		
		ImageLoader.getInstance().init(imageLoaderConfiguration);
		
		/**
		 * 기본
		 */
		displayImageOptions = new DisplayImageOptions.Builder()
		.cacheInMemory(false)
		.cacheOnDisk(false)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.EXACTLY)
		.build(); 
		
		/**
		 * 캐쉬사용
		 */
		displayImageOptionsCache = new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.EXACTLY)
		.build(); 
		
		/**
		 * 사진회전 복원
		 */
		displayImageOptionsExif = new DisplayImageOptions.Builder()
		.cacheInMemory(false)
		.cacheOnDisk(false)
		.imageScaleType(ImageScaleType.EXACTLY)
		.considerExifParams(true)
		.build();
		
		/**
		 * 게시글 이미지
		 */
		displayImageOptionsPost = new DisplayImageOptions.Builder()
		.cacheInMemory(false)
		.cacheOnDisk(false)
		.considerExifParams(true)
		.build();
		
		
		/**
		 * 프로필 편집 프로필 배경 이미지
		 */
		displayImageOptionsProfileBackground = new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.EXACTLY)
		.build();
		
		/**
		 * 수정 이미지 로드 캐쉬사용
		 */
		displayImageOptionsLoadPostImage = new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.imageScaleType(ImageScaleType.NONE)
		.considerExifParams(true)
		.build();
	}
	
	private ImageLoaderManager(Context context, boolean isPreview) {
		
		ImageLoaderConfiguration imageLoaderConfiguration = new ImageLoaderConfiguration.Builder(context)
        .denyCacheImageMultipleSizesInMemory()
        .writeDebugLogs()
        .build();
		
		ImageLoader.getInstance().init(imageLoaderConfiguration);
		
		/**
		 * 기본
		 */
		displayImageOptions = new DisplayImageOptions.Builder()
		.cacheInMemory(false)
		.cacheOnDisk(false)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.EXACTLY)
		.build(); 
		
		/**
		 * 캐쉬사용
		 */
		displayImageOptionsCache = new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.EXACTLY)
		.build(); 
		
		/**
		 * 게시글 이미지
		 */
		displayImageOptionsPost = new DisplayImageOptions.Builder()
		.cacheInMemory(false)
		.cacheOnDisk(false)
		.considerExifParams(true)
		.build();
		
		
		/**
		 * 수정 이미지 로드 캐쉬사용
		 */
		displayImageOptionsLoadPostImage = new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.imageScaleType(ImageScaleType.NONE)
		.considerExifParams(true)
		.build();
	}

	/**
	 * 기본
	 */
	public DisplayImageOptions getDisplayImageOptions() {
		return displayImageOptions;
	}
	
	/**
	 * 캐쉬사용
	 */
	public DisplayImageOptions getDisplayImageOptionsCache() {
		return displayImageOptionsCache;
	}
	
	/**
	 * 앨범
	 */
	public DisplayImageOptions getDisplayImageOptionsGallery() {
		return displayImageOptionsGallery;
	}

	/**
	 * 사진회전 복원
	 */
	public DisplayImageOptions getDisplayImageOptionsExif() {
		return displayImageOptionsExif;
	}

	/**
	 * 게시글 이미지
	 */
	public DisplayImageOptions getDisplayImageOptionsPost() {
		return displayImageOptionsPost;
	}
	
	/**
	 * 게시글 리스트 이미지
	 */
	public DisplayImageOptions getDisplayImageOptionsListPost() {
		return displayImageOptionsListPost;
	}
	
	/**
	 * 게시글 프로필 이미지
	 */
	public DisplayImageOptions getDisplayImageOptionsPostProfile() {
		return displayImageOptionsPostProfile;
	}
	
	/**
	 * 게시글 리스트 프로필 이미지
	 */
	public DisplayImageOptions getDisplayImageOptionsPostListProfile() {
		return displayImageOptionsPostListProfile;
	}
	
	/**
	 * 게시글 댓글 프로필 이미지
	 */
	public DisplayImageOptions getDisplayImageOptionsCommentListProfile() {
		return displayImageOptionsCommentListProfile;
	}
	
	/**
	 * 프로필 편집 프로필 이미지
	 */
	public DisplayImageOptions getDisplayImageOptionsProfileBackground() {
		return displayImageOptionsProfileBackground;
	}
	
	/**
	 * 프로필 편집 프로필 이미지
	 */
	public DisplayImageOptions getDisplayImageOptionsProfileEditProfile() {
		return displayImageOptionsProfileEditProfile;
	}
	
	/**
	 * 프로필 편집 프로필 이미지
	 */
	public DisplayImageOptions getDisplayImageOptionsLoadPostImage() {
		return displayImageOptionsLoadPostImage;
	}
	
	public DisplayImageOptions getDisplayImageOptionCache(int failRes) {
		
		DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.showImageOnFail(failRes)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.EXACTLY)
		.build(); 
		
		return displayImageOptions;
	}
	
	public DisplayImageOptions getDisplayImageOption(int failRes) {
		
		DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
		.cacheInMemory(false)
		.cacheOnDisk(false)
		.showImageOnFail(failRes)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.EXACTLY)
		.build(); 
		
		return displayImageOptions;
	}
	
	public static ImageLoaderManager getInstance(Context context) {
		
		if (instance == null) {
			instance = new ImageLoaderManager(context);
		} 
		
		return instance;
	}
	
	public static ImageLoaderManager getInstance(Context context, boolean isPreview) {
		
		if (instance == null) {
			instance = new ImageLoaderManager(context, isPreview);
		} 
		
		return instance;
	}
}
