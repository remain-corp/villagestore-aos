package com.remain.villagestore.manager;

import java.util.StringTokenizer;

/**************************************************
*
* StringTokenizerManager
* 문자열 자르기 및 특정문자 변경 및 추가, 제거 등이 가능한 클래스
*
**************************************************/
public class StringTokenizerManager {

	//================================================================================
    // XXX Constants
    //================================================================================
	// This
	public static StringTokenizerManager stringTokenizerManager;
	
	//================================================================================
	// XXX Variables
	//================================================================================
	// App
	
	// Content
	
	// Widget
	
	// View
	
	// Apdater
	
	// Array
	
	// Lang
	
	// Component
	
	// Util
	
	// Interface
	
	//================================================================================
	// XXX Constructor
	//================================================================================
	public static StringTokenizerManager getInstance() {
		
		if (stringTokenizerManager == null) {
			stringTokenizerManager = new StringTokenizerManager();
		}
		
		return stringTokenizerManager;
	}
	
	//================================================================================
    // XXX Override Method
    //================================================================================
	
	//================================================================================
    // XXX Listener
    //================================================================================
	
	//================================================================================
    // XXX Thread, Handler
    //================================================================================
	
	//================================================================================
    // XXX Getter, Setter
    //================================================================================
	
	//================================================================================
    // XXX Etc Method
    //================================================================================
	/**
	 * 문자열을 나눌 횟수 구하기
	 * 
	 * 나눈 문자를 저장하기 위한 배열의 크기를 초기에 선언할 때 필요 단, 배열의 크기로 사용할 때는 count값에 +1을 해주어야
	 * 한다. (1번 자르게되면 나눠지는 문자는 2글자가 되기 때문이다.)
	 * 
	 * @param str
	 *            나눌 문자열
	 * @param delimiter
	 *            구분자
	 */
	public int splitCount(String str, String delimiter) {

		StringTokenizer tokenizer = new StringTokenizer(str, delimiter);

		int count = 0;

		while (tokenizer.hasMoreTokens()) {
			tokenizer.nextToken();
			count++;
		}

		return count;
	}

	/**
	 * 문자열을 나누어 배열에 저장한 후 리턴
	 * 
	 * @param str
	 *            나눌 문자열
	 * @param delimiter
	 *            구분자
	 */
	public String[] split(String str, String delimiter) {

		if (str == null) {
			return null;
		}
		
		StringTokenizer tokenizer = new StringTokenizer(str, delimiter);

		int splitCount = splitCount(str, delimiter);
		String[] resultSplit = new String[splitCount];

		int count = 0;

		try {
			while (tokenizer.hasMoreTokens()) {
				resultSplit[count] = tokenizer.nextToken();
				count++;
			}
			return resultSplit;
		} catch (ArrayIndexOutOfBoundsException aioobe) {
		}

		return null;
	}
	
	/**
	 * 문자열을 나누어 배열에 저장한 후 리턴 (짝수 위치)
	 * 
	 * @param str
	 *            나눌 문자열
	 * @param delimiter
	 *            구분자
	 */
	public String evenSplit(String str, String delimiter) {

		StringTokenizer tokenizer;

		if (delimiter.equals(""))
			tokenizer = new StringTokenizer(str);
		else
			tokenizer = new StringTokenizer(str, delimiter);

		String resultSplit = "";
		int count = 0;

		try {
			while (tokenizer.hasMoreTokens()) {
				if (count % 2 == 1)
					resultSplit += tokenizer.nextToken();
				else
					tokenizer.nextToken();
				count++;
			}
			return resultSplit;
		} catch (ArrayIndexOutOfBoundsException aioobe) {
		}

		return null;
	}

	/**
	 * 문자열을 나누어 배열에 저장한 후 리턴 (홀수 위치)
	 * 
	 * @param str
	 *            나눌 문자열
	 * @param delimiter
	 *            구분자
	 */
	public String oddSplit(String str, String delimiter) {

		StringTokenizer tokenizer;

		if (delimiter.equals(""))
			tokenizer = new StringTokenizer(str);
		else
			tokenizer = new StringTokenizer(str, delimiter);

		String resultSplit = "";
		int count = 0;

		try {
			while (tokenizer.hasMoreTokens()) {

				if (count % 2 == 0)
					resultSplit += tokenizer.nextToken();
				else
					tokenizer.nextToken();
				count++;
			}

			return resultSplit;
		} catch (ArrayIndexOutOfBoundsException aioobe) {
		}

		return null;
	}

	/**
	 * 문자열 처음 문자 알아내기
	 * 
	 * @param str
	 * @return
	 */
	public String getFirstChar(String str) {
		
		if (str.length() <= 0) {
			return null;
		}
		
		String lastChar = null;
		lastChar = String.valueOf(str.charAt(0));
		
		return lastChar;
	}
	
	/**
	 * 문자열 마지막 문자 알아내기
	 * 
	 * @param str
	 * @return
	 */
	public String getLastChar(String str) {
		
		if (str.length() <= 0) {
			return null;
		}
		
		String lastChar = null;
		lastChar = String.valueOf(str.charAt(str.length() - 1));
		
		return lastChar;
	}
	
	/**
	 * 특정위치 문자 삭제
	 * @param str
	 * @param index
	 * @return
	 */
	public String split(String str, int index) {
	
		if (str.length() <= 0) {
			return null;
		}
		
		StringBuffer stringBuffer = null;
		stringBuffer = new StringBuffer(str);
		stringBuffer.delete(index - 1, index);
		
		return stringBuffer.toString();
	}
	
	public String emptySplit(String str, String delimiter) {
		
		if (str == null) {
			return null;
		}

		String[] emptyChar = split(str, delimiter);
		
		StringBuffer stringBuffer = null;
		stringBuffer = new StringBuffer();
		
		int count = 0;
		
		for (int i = 0; i < emptyChar.length; i++) {
			
			if (!emptyChar[i].trim().equals("")) {
				
				if (count != 0)	stringBuffer.append(",");
				stringBuffer.append(emptyChar[i].trim());
				count++;
			}
		}
		
		return stringBuffer.toString();
	}
	
	/**
	 * 텍스트 정렬
	 * 
	 * @param str
	 *            정렬할 문자열
	 * @param enterPosition
	 *            enter를 넣을 위치
	 */
	public String setAlign(String str, int enterPosition) {

		String arrayAlign[];
		String strAlign = "";

		arrayAlign = StringTokenizerManager.getInstance().split(str, ",");

		for (int i = 0; i < arrayAlign.length; i++) {
			if (i > 0) {
				if (i == enterPosition)
					strAlign += ",\n";
				else
					strAlign += ", ";
			}
			strAlign += arrayAlign[i];
		}

		return strAlign;
	}
	
	/**
	 * 문자열에 문자 추가
	 * 
	 * @param str 문자를 추가할 문자열
	 * @param addChar 추가할 문자
	 * @param addPosition 문자를 추가할 위치의 증가치
	 * @param isUnitTransform 단위변환을 할것인지(10000 -> 10k)
	 * 
	 * (금액의 경우 1,000,000의
	 * 	str : 1000000
	 * 	addChar : ,
	 * 	addPosition : 3)
	 */
	public String addChar(int count, String addChar, int addPosition, boolean isUnitTransform) {
		return addChar(String.valueOf(count), addChar, addPosition, isUnitTransform);
	}
	
	public String addChar(String str, String addChar, int addPosition, boolean isUnitTransform) {
		
		boolean isUnitTransformComplete = false;
		//단위 변환이면
		if(isUnitTransform && Integer.parseInt(str) >= 10000){
			int count = Integer.parseInt(str);
			str = String.valueOf(count / 1000);
			isUnitTransformComplete = true;
		}
		
		boolean isMinus = false;

		if (Integer.parseInt(str) < 0) {
			
			isMinus = true;
			str = String.valueOf(Math.abs(Integer.parseInt(str)));
		}
		
		str.replaceAll("\\p{Space}", ""); // 공백 제거
		int strLength = str.length(); // 문자열의 길이
		
		StringBuffer stringBuffer = new StringBuffer(str);
		
		// 문자열의 길이를 addPosition으로 나눈다.
		// 몫은 문자를 추가할 횟수이며,
		// 나머지는 처음 추가할 위치가 된다.
		// 나머지가 0이면 처음에 추가하지 않는다.
		// 문자를 추가했다면 문자열의 길이가 1증가했기 때문에 다음에 추가할 위치값에 1을 더해야한다.
		int addValue = strLength / addPosition; // 추가 횟수
		int startPosition = strLength % addPosition; // 시작 위치
		
		for (int i = 0; i < addValue; i++) {
			
			if (startPosition != 0) {
				
				stringBuffer.insert(startPosition, addChar);
				startPosition++;
			}
			
			startPosition += addPosition;
		}
		
		if (isMinus) {
			stringBuffer.insert(0, "-");
		}
		
		if(isUnitTransformComplete){
			stringBuffer.append("k");
		}
		
		return stringBuffer.toString();
	}
	//================================================================================
	// XXX Inner Class
	//================================================================================
	
	//================================================================================
	// XXX Interface
	//================================================================================
	
	//================================================================================
    // XXX Debug
    //================================================================================
}
