package com.remain.villagestore.pages.commonActivity;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.component.TransProgressDialog;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.util.Log;

/**
 * Created by remain on 2016. 7. 6..
 */
public class WebviewActivity extends FragmentActivity {

    private WebView webview;
    private TransProgressDialog progressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        String storeSiteAddress = getIntent().getStringExtra(ParamInfo.SITE_ADDRESS);

        webview = (WebView) findViewById(R.id.store_webView);
        WebSettings webSetting = webview.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setJavaScriptCanOpenWindowsAutomatically(true);   // javascript가 window.open()을 사용할 수 있도록 설정
        webSetting.setBuiltInZoomControls(false); // 안드로이드에서 제공하는 줌 아이콘을 사용할 수 있도록 설정
        //webSetting.setPluginState(WebSettings.PluginState.ON_DEMAND); // 플러그인을 사용할 수 있도록 설정
        webSetting.setSupportZoom(true); // 확대,축소 기능을 사용할 수 있도록 설정
        webSetting.setBlockNetworkImage(false); // 네트워크의 이미지의 리소스를 로드하지않음
        //webSetting.setLoadsImagesAutomatically(true); // 웹뷰가 앱에 등록되어 있는 이미지 리소스를 자동으로 로드하도록 설정
        //webSetting.setUseWideViewPort(true); // wide viewport를 사용하도록 설정
        //webSetting.setCacheMode(WebSettings.LOAD_NO_CACHE); // 웹뷰가 캐시를 사용하지 않도록 설정

        webview.setWebViewClient(new WebViewClient());

        webview.loadUrl(storeSiteAddress);

        webview.setWebViewClient(new WebViewClient() {
            // 페이지 로딩 시작시 호출
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Log.e("시작 여기오나");
                if(progressDialog == null){
                    progressDialog = TransProgressDialog.show(WebviewActivity.this, false);
                }

            }

            //페이지 로딩 종료시 호출
            @Override
            public void onPageFinished(WebView view, String Url) {
                Log.e("끝 여기오나");
                if(progressDialog != null){
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, //페이지 로딩 실패
                                        String description, String failingUrl) {

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                webview.loadUrl(failingUrl);
                Toast.makeText(WebviewActivity.this, "네트워크 환경을 확인해 주세요." + description,
                        Toast.LENGTH_LONG).show();

                super.onReceivedError(view, errorCode, description, failingUrl);
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        /*
        * 이전화면이 있을경우 이전 페이지
        * */
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (webview.canGoBack()) {
                webview.goBack();
                return false;
            } else {
                finish();
            }

        }

        return super.onKeyDown(keyCode, event);
    }

    private class WebViewClient extends android.webkit.WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }
    }
}
