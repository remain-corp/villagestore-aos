package com.remain.villagestore.pages.p1_intro.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.TextDialog;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.info.PreferenceInfo;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.RecycleUtil;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;


/**
 * Created by woongjaelee on 2015. 11. 6..
 */
public class IntroActivity extends Activity {


    //================================================================================
    // XXX Constants
    //================================================================================

    /**
     * 인트로 보여줄 시간
     */
    private final static int SHOW_INTRO_TIME = 2000;

    /**
     * 핸들러
     */
    private final static int WHAT_VERSION_OK = 2001;
    private final static int WHAT_VERSION_NEED = 2002;
    private final static int WHAT_SHOW_INTRO = 2003;

    //================================================================================
    // XXX Variables
    //================================================================================

    private Context context;
    private Intent intent;

    // Component
    private TextDialog textDialog;

    private ImageView imageView;
    private ServerRequest server;

    //업데이트
    private RelativeLayout wrapperUpdate;
    private TextView textUpdate;

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro);

        init();
        Log.e("네트워크 상태 = "+Utils.getNetworkState(this));
        if(Utils.getNetworkState(this).equals("no")){
            Toast.makeText(this, "네트워크 상태를 확인후 다시 이용해 주세요.", Toast.LENGTH_SHORT).show();
        }else {
            uiHandler.sendEmptyMessageDelayed(WHAT_SHOW_INTRO, SHOW_INTRO_TIME);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        RecycleUtil.recursiveRecycle(imageView);
    }

    //================================================================================
    // XXX Listener
    //================================================================================

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {

            if (error != null) {
                Log.e("에러 = "+error.getErrorCode());
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.READ_VERSION) {

                String result;
                if (!object.isNull(ParamInfo.RESULT)) try {

                    result = object.getString(ParamInfo.RESULT);

                    String checkVersion = object.getString("checkVersion");

                    //서버 점검 중일 경우 점검화면
                    if (checkVersion != null) {
                        if (checkVersion.equals("Y")) {
                            Intent i = new Intent(context, ServerCheckActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            finish();
                            return;
                        }
                    }

                    if (result != null) {

                        if (result.equals("Y")) {

                            uiHandler.sendEmptyMessage(WHAT_VERSION_NEED);
                        } else if (result.equals("N")) {

                            uiHandler.sendEmptyMessage(WHAT_VERSION_OK);
                        } else {

                            uiHandler.sendEmptyMessage(WHAT_VERSION_OK);
                        }
                    } else {

                        uiHandler.sendEmptyMessage(WHAT_VERSION_OK);

                    }

                } catch (JSONException e) {
                    Log.e("exception = "+e);
                    e.printStackTrace();
                }
            }else{

            }
        }
    };
    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    @SuppressLint("HandlerLeak")
    private Handler uiHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    /*if (PreferenceInfo.loadTutorialMain(context)) {

                        // 메인 페이지로
                        intent = new Intent(context, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                        startActivity(intent);

                    } else {

                        intent = new Intent(context, TutorialActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                        startActivity(intent);

                    }

                    finish();
                    //overridePendingTransition(0, 0);
                    */
                    Intent i = new Intent(context, ServerCheckActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(i);
                    finish();

                    break;
                case WHAT_VERSION_OK:

                    if (PreferenceInfo.loadTutorialMain(context)) {

                        // 메인 페이지로
                        intent = new Intent(context, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                        startActivity(intent);

                    } else {

                        intent = new Intent(context, TermConfirmActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                        startActivity(intent);

                    }

                    finish();
                    overridePendingTransition(0, 0);

                    break;
                case WHAT_VERSION_NEED:

                    /*
                    * 업데이트시 유저컬럼 변동으로 인한 앱 크래쉬 방지
                    * 추후 안정화시 제거 필요.
                    * */
                    Database.getInstance().removeAll(IntroActivity.this);

                    wrapperUpdate.setVisibility(View.VISIBLE);

                   /* textDialog = new TextDialog(context);
                    textDialog.setConfirm("확인");
                    textDialog.setCancelable(false);
                    textDialog.setMessage("최신 버젼으로 업데이트가 필요합니다.");
                    textDialog.setOnTextConfirmClickListener(new TextDialog.OnTextConfirmClickListener() {
                        @Override
                        public void onClick() {
                            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppInfo.MARKET_LINK_APPSTORE));

                            startActivity(intent);

                            finish();
                        }
                    });
                    textDialog.show();*/

                    break;
                case WHAT_SHOW_INTRO:

                    requsetCheckVersionToServer();

                    break;
            }


        }
    };

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================


    private void init() {

        context = this;

        server = new ServerRequest(networkResultListener);

        initUI();
        initUISize();
        setEventListener();

    }

    private void initUI() {

        imageView = (ImageView) findViewById(R.id.activity_intro_imageview);
        wrapperUpdate = (RelativeLayout) findViewById(R.id.intro_wrapper_update);
        textUpdate = (TextView) findViewById(R.id.intro_text_update);

    }

    private void initUISize() {

    }

    private void setEventListener() {
        textUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppInfo.MARKET_LINK_APPSTORE));

                startActivity(intent);

                finish();
            }
        });
    }


    /**
     * 앱 버젼 체크
     */
    private void requsetCheckVersionToServer() {

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        PackageInfo pInfo = null;
        try {

            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.APP_VERSION, String.valueOf(pInfo.versionCode));

        server.execute(NetInfo.READ_VERSION, params);
    }

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================

}
