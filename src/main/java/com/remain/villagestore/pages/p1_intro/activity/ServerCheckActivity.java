package com.remain.villagestore.pages.p1_intro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;

/**
 * Created by remain on 2016. 6. 8..
 */
public class ServerCheckActivity extends FragmentActivity {
    private int count = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_check);

        StretchImageView image = (StretchImageView) findViewById(R.id.check_image);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(count > 20){
                    Intent intent = new Intent(ServerCheckActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    startActivity(intent);
                }else{
                    count++;
                }
            }
        });


    }


}
