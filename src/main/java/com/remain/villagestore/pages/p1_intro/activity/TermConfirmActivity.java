package com.remain.villagestore.pages.p1_intro.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;

/**
 * Created by woongjaelee on 2016. 3. 7..
 */
public class TermConfirmActivity extends Activity {

    private StretchImageView textViewConfirm;
    private StretchImageView textViewCancel;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_term);

        init();
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.term_textview_confirm:

                    Intent intent = new Intent(context,TutorialActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    startActivity(intent);

                    finish();

                    break;
                case R.id.term_textview_cancel:
                    finish();
                    break;
            }
        }
    };

    private void init() {

        context = this;
        initUI();
        setEventListener();
    }

    private void initUI() {

        textViewConfirm = (StretchImageView)findViewById(R.id.term_textview_confirm);
        textViewCancel = (StretchImageView)findViewById(R.id.term_textview_cancel);

    }
    private void setEventListener() {

        textViewConfirm.setOnClickListener(clickListener);
        textViewCancel.setOnClickListener(clickListener);
    }
}
