package com.remain.villagestore.pages.p1_intro.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.remain.villagestore.R;
import com.remain.villagestore.common.GlobalApplication;
import com.remain.villagestore.component.CircleIndicator;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.PreferenceInfo;
import com.remain.villagestore.manager.DisplayManager;
import com.remain.villagestore.pages.p1_intro.adapter.TutorialEmptyPagerAdapter;
import com.remain.villagestore.pages.p1_intro.adapter.TutorialImgPagerAdapter;
import com.remain.villagestore.pages.p1_intro.adapter.TutorialTextPagerAdapter;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.util.RecycleUtil;

/**
 * Created by woongjaelee on 2016. 3. 1..
 */
public class TutorialActivity extends Activity {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Enum
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    // App

    private Context context;
    private Intent intent;

    // Widget
    private ImageView imageViewPhone;
    private ImageView imageViewFinger;
    private TextView textViewClose;
    private ViewPager viewPagerEmpty;
    private ViewPager viewPagerImg;
    private ViewPager viewPagerText;
    private LinearLayout linearLayougBg;

    // View

    // Adapter
    private TutorialEmptyPagerAdapter adapterTutorialEmptyPager;
    private TutorialImgPagerAdapter adapterTutorialImgPager;
    private TutorialTextPagerAdapter adapterTutorialTextPager;

    // Array

    // Lang
    private float textPageRatia;
    private float imgPageRatio;

    // Component
    private CircleIndicator circleIndicator;

    // Util
    private DisplayManager displayManager;

    // Interface
    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tutorial);
        init();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onDestroy() {

        if (adapterTutorialImgPager != null)
            adapterTutorialImgPager.recycle();

        RecycleUtil.recursiveRecycle(imageViewFinger);
        RecycleUtil.recursiveRecycle(imageViewPhone);
        RecycleUtil.recursiveRecycle(linearLayougBg);

        super.onDestroy();
    }

    //================================================================================
    // XXX Listener
    //================================================================================

    private View.OnClickListener clickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.tutorial_textview_close:

                    PreferenceInfo.saveTutorialMain(context, true);
                    // 닫기
                    intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                    finish();
                    break;
            }
        }
    };
    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================

    private void init() {

        context = this;
        displayManager = new DisplayManager(context);
        imgPageRatio = (float) displayManager.getScaleSizeSameRate(413) / (float) displayManager.getDisplayWidth();
        textPageRatia = displayManager.getDisplayWidth() / displayManager.getDisplayWidth();

        initUI();
        initUISize();
        setEventListener();
        setPagerAdapter();
    }

    private void initUI() {

        linearLayougBg = (LinearLayout) findViewById(R.id.activity_tutorial_linearlayout_bg);
        imageViewPhone = (ImageView) findViewById(R.id.tutorial_imageview_img);
        imageViewFinger = (ImageView) findViewById(R.id.tutorial_imageview_finger);
        circleIndicator = (CircleIndicator) findViewById(R.id.tutorial_circleindicator);
        textViewClose = (TextView) findViewById(R.id.tutorial_textview_close);
        viewPagerEmpty = (ViewPager) findViewById(R.id.tutorial_viewpager_empty);
        viewPagerImg = (ViewPager) findViewById(R.id.tutorial_viewpager_img);
        viewPagerText = (ViewPager) findViewById(R.id.tutorial_viewpager_text);

    }

    private void initUISize() {

        displayManager.changeWidthHeightSameRate(findViewById(R.id.tutorial_framelayout_text_panel).getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 300);
        displayManager.changeWidthHeightSameRate(circleIndicator.getLayoutParams(), ViewGroup.LayoutParams.WRAP_CONTENT, 110);
        displayManager.changeWidthHeightSameRate(findViewById(R.id.tutorial_imageview_img).getLayoutParams(), 482, 858);
        displayManager.changeWidthHeightSameRate(viewPagerImg.getLayoutParams(), 413, 763);

        FrameLayout.LayoutParams param1 = (FrameLayout.LayoutParams) textViewClose.getLayoutParams();
        param1.rightMargin = displayManager.getScaleSizeSameRate(30);
        param1.bottomMargin = displayManager.getScaleSizeSameRate(40);

    }

    private void setEventListener() {

        textViewClose.setOnClickListener(clickListener);
    }

    private void setPagerAdapter() {
        //
        adapterTutorialEmptyPager = new TutorialEmptyPagerAdapter(context);
        viewPagerEmpty.setOffscreenPageLimit(AppInfo.TUTORIAL_PAGES - 1);
        viewPagerEmpty.setPageMargin(0);
        viewPagerEmpty.setAdapter(adapterTutorialEmptyPager);

        circleIndicator.setViewPager(viewPagerEmpty);
        circleIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {

                    imageViewFinger.setVisibility(View.VISIBLE);
                } else {

                    imageViewFinger.setVisibility(View.GONE);
                }
                if (position == (AppInfo.TUTORIAL_PAGES - 1)) {
                    textViewClose.setVisibility(View.VISIBLE);
                } else {
                    textViewClose.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                viewPagerText.scrollTo((int) (positionOffsetPixels * textPageRatia) + (position * (int) (displayManager.getDisplayWidth() * textPageRatia)), 0);
                viewPagerImg.scrollTo((int) (positionOffsetPixels * imgPageRatio) + (position * (int) (displayManager.getDisplayWidth() * imgPageRatio)), 0);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        // 이미지
        adapterTutorialImgPager = new TutorialImgPagerAdapter(context, Glide.with(this));
        viewPagerImg.setOffscreenPageLimit(AppInfo.TUTORIAL_PAGES - 1);
        viewPagerImg.setPageMargin(0);
        viewPagerImg.setAdapter(adapterTutorialImgPager);

        // 내용
        adapterTutorialTextPager = new TutorialTextPagerAdapter(context);
        viewPagerText.setOffscreenPageLimit(AppInfo.TUTORIAL_PAGES - 1);
        viewPagerText.setPageMargin(0);
        viewPagerText.setAdapter(adapterTutorialTextPager);
    }


    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================

}
