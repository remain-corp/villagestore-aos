package com.remain.villagestore.pages.p1_intro.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.remain.villagestore.R;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.util.RecycleUtil;

/**
 * Created by woongjaelee on 2016. 3. 1..
 */
public class TutorialEmptyPagerAdapter extends PagerAdapter {

    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================
    // App
    private Context context;

    // Content

    // View

    // Array

    // Lang
    private LayoutInflater layoutInflater;

    //================================================================================
    // XXX Constructor
    //================================================================================

    public TutorialEmptyPagerAdapter(Context context) {

        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    //================================================================================
    // XXX Override Method
    //================================================================================
    @Override
    public int getCount() {
        return AppInfo.TUTORIAL_PAGES;
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = layoutInflater.inflate(R.layout.content_tutorial_empty,null);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        RecycleUtil.recursiveRecycle((View) object);
        ((ViewPager) container).removeView((View) object);

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }


    //================================================================================
    // XXX Etc Method
    //================================================================================

}
