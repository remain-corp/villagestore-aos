package com.remain.villagestore.pages.p1_intro.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.RequestManager;
import com.remain.villagestore.R;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.util.RecycleUtil;

/**
 * Created by woongjaelee on 2016. 3. 1..
 */
public class TutorialImgPagerAdapter extends PagerAdapter {

    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================
    // App
    private Context context;


    // Content

    // View
    private ImageView imageView;

    // Array

    private SparseArray<View> views = new SparseArray<View>();

    // Lang

    private LayoutInflater layoutInflater;

    // Imageloader
    private RequestManager glideRequestManager;


    //================================================================================
    // XXX Constructor
    //================================================================================

    public TutorialImgPagerAdapter(Context context, RequestManager glideRequestManager) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.glideRequestManager = glideRequestManager;
    }


    //================================================================================
    // XXX Override Method
    //================================================================================
    @Override
    public int getCount() {
        return AppInfo.TUTORIAL_PAGES;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = layoutInflater.inflate(R.layout.content_tutorial_img, null);

        init(view, position);
        container.addView(view);

        views.put(position, view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        RecycleUtil.recursiveRecycle((View) object);
        ((ViewPager) container).removeView((View) object);

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================

    private void init(View view, int position) {


        initUi(view);

        TypedArray typedArrayImg = context.getResources().obtainTypedArray(R.array.tutorial_img);

        glideRequestManager
                .fromResource()
                .load(typedArrayImg.getResourceId(position, 0))
                .error(R.drawable.image_default_photo)
                .thumbnail(0.1f)
                .into(imageView);
    }

    private void initUi(View view) {

        imageView = (ImageView) view.findViewById(R.id.tutorial_img_imageview);
    }

    public void recycle() {

        RecycleUtil.recursiveRecycle(imageView);
        RecycleUtil.recursiveRecycle(views);
    }
}
