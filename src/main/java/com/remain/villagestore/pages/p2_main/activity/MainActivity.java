package com.remain.villagestore.pages.p2_main.activity;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.google.android.gcm.GCMRegistrar;
import com.remain.villagestore.GCMIntentService;
import com.remain.villagestore.R;
import com.remain.villagestore.common.GlobalApplication;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.HorizontalMenuView;
import com.remain.villagestore.component.ListEmptyView;
import com.remain.villagestore.component.image.CircleImageView;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.dto.Advertisement;
import com.remain.villagestore.dto.Cafe;
import com.remain.villagestore.dto.Delivery;
import com.remain.villagestore.dto.Eatery;
import com.remain.villagestore.dto.Education;
import com.remain.villagestore.dto.Life;
import com.remain.villagestore.dto.Mart;
import com.remain.villagestore.dto.Medical;
import com.remain.villagestore.dto.Store;
import com.remain.villagestore.dto.StoreMenu;
import com.remain.villagestore.dto.SubCate;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.info.PreferenceInfo;
import com.remain.villagestore.manager.BackPressCloseHandler;
import com.remain.villagestore.manager.DataManager;
import com.remain.villagestore.manager.DisplayManager;
import com.remain.villagestore.pages.commonActivity.WebviewActivity;
import com.remain.villagestore.pages.p2_main.adapter.StoreListAdapter;
import com.remain.villagestore.pages.p3_stores.activity.CafeStoreActivity;
import com.remain.villagestore.pages.p3_stores.activity.DeliveryActivity;
import com.remain.villagestore.pages.p3_stores.activity.EateryStoreActivity;
import com.remain.villagestore.pages.p3_stores.activity.MartStoreActivity;
import com.remain.villagestore.pages.p3_stores.activity.OtherStoreActivity;
import com.remain.villagestore.pages.p4_other.address.activity.SelectAddressActivity;
import com.remain.villagestore.pages.p4_other.local.activity.SearchActivity;
import com.remain.villagestore.pages.p4_other.local.activity.SearchActivityi;
import com.remain.villagestore.pages.p4_other.point.activity.PointActivity;
import com.remain.villagestore.pages.p4_other.setting.activity.UserSettingActivity;
import com.remain.villagestore.pages.p4_other.sign.activity.SelectSignActivity;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.FlipAnimation;
import com.remain.villagestore.util.LocationUtil;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.RecycleUtil;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity implements View.OnClickListener {


    //================================================================================
    // XXX Constants
    //================================================================================
    /**
     * GPS 체크 텀(sec)
     */
    private static final int CHECK_GPS_TERM = 1000;
    /**
     * animation term
     */
    private static final int AD_ANIM_TERM = 1000 * 5;
    /* GPS 잡기까지 기다리는 최대 시간*/
    private static final int MAX_SEARCH_GPS_TIME = 10;
    /* 시간 초로 변환을 위한 값 */
    private static final double FOR_SEC_TRANS = 1000000000.0;
    /* GPS FAIL 시 메시지 보여주기  */
    private static final int WHAT_GPS_FAIL = 5001;

    //================================================================================
    // XXX Variables
    //================================================================================

    // Content
    private Context context;
    private Intent intent;

    // Widget
    private ViewMapper viewMapper;
    private FrameLayout frameLayoutMenu;
    private ListView listView;
    private HorizontalScrollView horizontalScrollView;
    private DrawerLayout drawerLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView imageViewAd;
    private TextView textViewTitle;
    private TextView textViewLike;
    private TextView textViewLikeTitle;
    private HorizontalMenuView horizontalMenuView;

    // Lang
    private User me;
    private boolean isGpsUpdated;
    private boolean isMenuOpen;
    private boolean isGpsOK;
    private boolean isGpsWating;
    private long holdStartTime;
    private int currentAnimPos;
    /** 현재 선택된 구/군/시 seq*/
    private int currentAddrSeq = AppInfo.DEFAULT_INT;
    private double subwayLat;
    private double subwayLng;

    private Timer gpsTimer;

    // Component
    private ListEmptyView listEmptyView;
    private RequestManager glideRequestManager;

    //Adapter
    private StoreListAdapter storeListAdapter;

    private ArrayList<Store> arrayListStore = new ArrayList<Store>();
    //private ArrayList<Store> arrayListStoreBackUp = new ArrayList<Store>();
    private ArrayList<Advertisement> arrayListAD = new ArrayList<>();
    private ArrayList<SubCate> subCateList = new ArrayList<>();
    private String subCateStyle = "";
    private boolean isSubCateMode = false;

    // Util
    private DisplayManager displayManager;
    private ServerRequest server;
    private LocationUtil locationUtil;
    private FlipAnimation flipAnimation;

    // Enum
    private Store.StoreType storeType;
    private StoreListMode storeListMode;
    private StoreMenu.MenuType menuType;

    // 좋아요한 매장시 MainString 백업
    private String mainTextBackUp;

    //백키 두번눌렸을때 앱종료
    private BackPressCloseHandler backPressCloseHandler;

    //gps반경설정
    private int gpsDis = 3;

    //매장 리스트요청
    private boolean isEnabledRefresh = true;
    private int refreshCount = 1;
    private boolean beforeProgressShowing;
    private String beforeSerchType;
    private String beforeIsSigungu;
    private int beforeCate;
    private double beforeLat;
    private double beforeLng;
    private int beforeAddress;
    private int beforeUser_seq;
    private int beforeSubcate_seq;

    //================================================================================
    // XXX Enum
    //================================================================================

    public enum StoreListMode {
        GPS,
        SEARCH,
        FAVORITE,
        SUBWAY
    }

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        viewMapper = new ViewMapper();

        init();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        //super.onNewIntent(intent);


        //푸쉬로진입
        if (intent.getIntExtra(ParamInfo.PUSH_SEQ, 0) != 0) {
            requestPushUpdateToServer(intent.getIntExtra(ParamInfo.PUSH_SEQ, 0), intent.getIntExtra(ParamInfo.STORE_SEQ, 0));
        }

        if (intent.getBooleanExtra("signUp", false)) {
            me = User.getMyInfo(this);
            setLogin(true);
        }
        if (intent.getBooleanExtra("saving", false)) {
            Log.e("포인트" + me.getPoint());
            me = User.getMyInfo(this);
            Log.e("포인트2" + me.getPoint());
            viewMapper.textUserPoint.setText("동네로 포인트 : " + me.getPoint());
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        if (me == null)
            me = User.getMyInfo(context);

        if (isGpsOK) {

            isGpsOK = false;

            locationUtil = LocationUtil.getInstance(context);
            locationUtil.setOnLocationChangedListener(locationChangedListener);

            if (locationUtil.canGetLocation()) {
                // GPS 시작 시간 체크
                startGpsTimer();
                serchGpsView(true);
                isGpsWating = true;
            } else {
                Log.e("설정 갔다와도 안켰으면 개색키 ");
                // 설정 갔다왔는데도 gps 를 안켯으면 카테고리로 검색 또는 빈화면 띄우기
                setGpsCancel();

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // 주소로 검색하기
        if (requestCode == ParamInfo.REQUEST_UPDATE) {

            if (data != null) {
                switch (resultCode) {
                    case NetInfo.WHAT_SELECT_ADDRLIST:
                        int selAdSeq = data.getIntExtra(ParamInfo.SEL_ADDR_SEQ,AppInfo.DEFAULT_INT);
                        viewMapper.wrapperGpsFail.setVisibility(View.GONE);

                        if (selAdSeq != AppInfo.DEFAULT_INT) {

                            String newAddress = data.getStringExtra(ParamInfo.ADDRESS);
                            int isAll = data.getIntExtra(ParamInfo.IS_SIGUNGU,AppInfo.DEFAULT_INT);
                            textViewLike.setText(getResources().getString(R.string.main_button_like_store_message));

                            horizontalMenuView.setPageSelected(0);
                            storeType = Store.StoreType.ALL;
                            menuType = StoreMenu.MenuType.NONE;

                            textViewTitle.setText(newAddress);
                            viewMapper.textLocation.setText(newAddress);
                            requestToStoreListToServer(true, ParamInfo.SEARCH_ADDRESS, storeType.getStoreTypeCode(),selAdSeq,(isAll == 1)? ParamInfo.Y:ParamInfo.N);
                        } else {
                            Log.e("검색 취소 ");
                        }
                        break;
                    case NetInfo.WHAT_SEARCH_STATION:

                        String stationName = data.getStringExtra(ParamInfo.STN_NAME);
                        textViewTitle.setText(stationName);

                        subwayLat = data.getDoubleExtra(ParamInfo.LAT,AppInfo.DEFAULT_INT);
                        subwayLng = data.getDoubleExtra(ParamInfo.LNG,AppInfo.DEFAULT_INT);

                        if(subwayLat != AppInfo.DEFAULT_INT
                                && subwayLng != AppInfo.DEFAULT_INT) {

                            storeType = Store.StoreType.ALL;
                            menuType = StoreMenu.MenuType.NONE;

                            requestToStoreListToServer(true,ParamInfo.SEARCH_SUBWAY,storeType.getStoreTypeCode(),subwayLat,subwayLng);
                        }
                        break;
                    case RESULT_OK:
                        Log.e("취소");
                        break;
                }
            } else {
//                if (textViewTitle.getText().toString().equals(getResources().getString(R.string.main_button_like_store_message))) {
//                    storeListMode = StoreListMode.FAVORITE;
//                } else if (textViewTitle.getText().toString().equals(getResources().getString(R.string.store_map_title))) {
//                    storeListMode = StoreListMode.GPS;
//                } else {
//                    storeListMode = StoreListMode.SEARCH;
//                }
                Log.e("모드는 = " + storeListMode);
            }

        }
        // 로그인하기 후
        else if (requestCode == ParamInfo.REQUEST_LOGIN_UPDATE) {

            Log.e("로그인하기 후 ~ ");
            me = User.getMyInfo(context);

            if (me != null && me.getDeviceId() != null && !me.getDeviceId().equals(""))
                setLogin(true);
            else
                setLogin(false);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    //gps설정창이 on일경우
                    if (viewMapper.wrapperGpsAlert.getVisibility() == View.VISIBLE) {
                        isGpsOK = false;
                        setGpsCancel();
                        viewMapper.wrapperGpsAlert.setVisibility(View.GONE);
                        return true;
                    }
                    //gps 반경 설정캉이 on일경우
                    if (viewMapper.wrapperGpsDis.getVisibility() == View.VISIBLE) {
                        viewMapper.wrapperGpsDis.setVisibility(View.GONE);
                        return true;
                    }

                    Log.e("getVisibility() ="+viewMapper.subCateScroll.getVisibility() +"isSubCateMode"+isSubCateMode);
                    //if (beforeSubcate_seq != 0) {
                        if (viewMapper.subCateScroll.getVisibility() == View.GONE && isSubCateMode) {
                            Log.e("여기1111");
                            viewMapper.subCateScroll.setAlpha(1);
                            viewMapper.subCateScroll.setVisibility(View.VISIBLE);
                        } else {
                            Log.e("여기2222");
                            backPressCloseHandler.onBackPressed();
                        }
                    //}
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        clearReferences();

        if (locationUtil != null) {
            locationUtil.stopLocationUpdates();
        }

        if (gpsTimer != null) {
            gpsTimer.cancel();
            gpsTimer = null;
        }

        if (storeListAdapter != null)
            storeListAdapter.recycle();

        RecycleUtil.recursiveRecycle(listEmptyView);
        RecycleUtil.recursiveRecycle(listView);

        System.gc();

        super.onDestroy();
    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    //================================================================================
    // XXX Listener
    //================================================================================

    private HorizontalMenuView.OnHorizontalItemClickListener horizontalItemClickListener = new HorizontalMenuView.OnHorizontalItemClickListener() {
        @Override
        public void onItemClick(int position) {
            viewMapper.subCateScroll.setVisibility(View.GONE);

            //gpsFail창제거
            if (viewMapper.wrapperGpsFail.getVisibility() == View.VISIBLE) {
                viewMapper.wrapperGpsFail.setVisibility(View.GONE);
            }

            storeType = Store.StoreType.getStoreType(position);
            menuType = StoreMenu.MenuType.NONE;

            if (storeListMode == StoreListMode.GPS) {
                if (checkGps(true)) {
                    requestToStoreListToServer(true, ParamInfo.SEARCH_GPS, storeType.getStoreTypeCode(), locationUtil.getLatitude(), locationUtil.getLongitude());
                }

            } else if (storeListMode == StoreListMode.SEARCH) {
                requestToStoreListToServer(true, ParamInfo.SEARCH_ADDRESS, storeType.getStoreTypeCode(), beforeAddress,beforeIsSigungu);
            }
            else if(storeListMode == StoreListMode.SUBWAY) {
                requestToStoreListToServer(true, ParamInfo.SEARCH_SUBWAY, storeType.getStoreTypeCode(), subwayLat, subwayLng);
            }
            else {
                // 좋아요한 매장 보기 refresh
                Log.e("else horizontal");
            }

        }
    };

    private LocationUtil.OnLocationChangedListener locationChangedListener = new LocationUtil.OnLocationChangedListener() {
        @Override
        public void onLocationChanged() {

            if (isGpsWating) {
                isGpsUpdated = true;
                Log.e("GPS 찾음 !");
                stopGpsTimer(false);

                requestToStoreListToServer(true, ParamInfo.SEARCH_GPS, storeType.getStoreTypeCode(), locationUtil.getLatitude(), locationUtil.getLongitude());
            }

        }
    };

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            Log.e("스토어 리스트 모드 =" + storeListMode);

            refreshCount = 0;
            // GPS 상태에서 refresh
            if (storeListMode == StoreListMode.GPS) {

                if (checkGps(false)) {

                    requestToStoreListToServer(false, ParamInfo.SEARCH_GPS, storeType.getStoreTypeCode(), locationUtil.getLatitude(), locationUtil.getLongitude());

                } else {

                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            // 검색
            else if (storeListMode == StoreListMode.SEARCH) {

                requestToStoreListToServer(false, ParamInfo.SEARCH_ADDRESS, storeType.getStoreTypeCode(), beforeAddress,beforeIsSigungu);

            }
            // 지하철 검색
            else if(storeListMode == StoreListMode.SUBWAY) {
                requestToStoreListToServer(false, ParamInfo.SEARCH_SUBWAY, storeType.getStoreTypeCode(), subwayLat, subwayLng);
            }
            // 좋아요한 매장 보기 refresh
            else  {
                requestToStoreListToServer(false, ParamInfo.SEARCH_LIKE, me.getSeq());
            }

        }
    };

    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.e("StoreSize " + arrayListStore.size());
            Log.e("position " + position);
            if (arrayListStore.size() > 0) {
                Log.e("카테고리 타입 " + arrayListStore.get(position).getStoreType().ordinal());
                switch (arrayListStore.get(position).getStoreType()) {

                    case DELIVERY:
                        intent = new Intent(context, DeliveryActivity.class);
                        break;
                    case EATERY:
                        intent = new Intent(context, EateryStoreActivity.class);
                        break;
                    case EDUCATION:
                        intent = new Intent(context, OtherStoreActivity.class);
                        break;
                    case CAFE:
                        intent = new Intent(context, CafeStoreActivity.class);
                        break;
                    case LIFE:
                        intent = new Intent(context, OtherStoreActivity.class);
                        break;
                    case MEDICAL:
                        intent = new Intent(context, OtherStoreActivity.class);
                        break;
                    case MART:
                        intent = new Intent(context, MartStoreActivity.class);
                        break;
                    default:
                        intent = new Intent(context, OtherStoreActivity.class);
                        break;
                }

                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(ParamInfo.STORE, arrayListStore.get(position));
                startActivity(intent);
            }
        }
    };


    private DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerOpened(View drawerView) {
            isMenuOpen = true;
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            isMenuOpen = false;
        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {

            if (error != null) {
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.PUSH_UPDATE) {
                try {
                    JSONObject result = object.getJSONObject("result");
                    Log.e("result = " + result.toString());
                    if (!result.isNull("store")) {
                        JSONObject storeObject = result.getJSONObject("store");
                        Log.e("store = " + storeObject.toString());
                        Store store;
                        // 카테고리 있으면 카테고리 별로 객체 생성
                        if (!storeObject.isNull(ParamInfo.ST_CATE)) {
                            switch (storeObject.getInt(ParamInfo.ST_CATE)) {
                                // 외식
                                case 1:
                                    intent = new Intent(context, DeliveryActivity.class);
                                    store = new Delivery(storeObject);
                                    break;
                                //배달
                                case 2:
                                    intent = new Intent(context, EateryStoreActivity.class);
                                    store = new Eatery(storeObject);
                                    break;
                                // 과외/학원
                                case 3:
                                    intent = new Intent(context, OtherStoreActivity.class);
                                    store = new Education(storeObject);
                                    break;
                                // 병원/약국
                                case 4:
                                    intent = new Intent(context, OtherStoreActivity.class);
                                    store = new Medical(storeObject);
                                    break;
                                // 카페/주점
                                case 5:
                                    intent = new Intent(context, CafeStoreActivity.class);
                                    store = new Cafe(storeObject);
                                    break;
                                // 생활/편의
                                case 6:
                                    intent = new Intent(context, OtherStoreActivity.class);
                                    store = new Life(storeObject);
                                    break;
                                case 7:
                                    intent = new Intent(context, MartStoreActivity.class);
                                    store = new Mart(storeObject);
                                    break;
                                default:
                                    intent = new Intent(context, OtherStoreActivity.class);
                                    store = new Store(storeObject);
                                    break;

                            }
                        } else {
                            intent = new Intent(context, OtherStoreActivity.class);
                            store = new Store(storeObject);
                        }

                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra(ParamInfo.STORE, store);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            if (url == NetInfo.SEARCH_STORE) {
                try {
                    if(refreshCount == 1) {
                        arrayListStore.clear();
                    }
                    subCateList.clear();

                    //서브카테가 있을경우 서브카테 생성
                    JSONObject resultObject = object.getJSONObject(ParamInfo.RESULT);
                    Log.e("결과 object = " + object.toString());
                    Log.e("결과 resultObject = " + resultObject.toString());

                    if (resultObject.getString(ParamInfo.IS_OK).equals(ParamInfo.Y)) {

                        Log.e("결과 IS_OK = " + resultObject.getString(ParamInfo.IS_OK).toString());
                        Log.e("결과 IS_SUBCATE = " + resultObject.getString(ParamInfo.IS_SUBCATE));

                        if (resultObject.getString(ParamInfo.IS_SUBCATE).equals(ParamInfo.Y)) {
                            Log.e("서브카테 true Y");

                            if(!resultObject.isNull("sub_cate")){
                                //Log.e("결과 sub_cate = " + resultObject.getJSONObject("sub_cate").toString());
                                JSONObject subcate = resultObject.getJSONObject("sub_cate");
                                Log.e("style code = " + subcate.getString("style_code"));

                                JSONArray subList = subcate.getJSONArray(ParamInfo.SUB_CATE_LIST);
                                Log.e("서브카테리스트 사이즈는 ?? = " + subList.length());
                                if (subList != null) {

                                    for (int i = 0; i < subList.length(); i++) {
                                        SubCate sub = new SubCate((JSONObject) subList.get(i));
                                        Log.e("sub_cate_seq = " + sub.getSeq());
                                        Log.e("image = " + sub.getImageName());

                                        subCateList.add(sub);
                                    }
                                } else {
                                    Log.e("subCateList = null");
                                }

                                subCateStyle = subcate.getString("style_code");

                                Log.e("subCateStyle = " + subCateStyle);

                            }else{
                                Log.e("널");
                                uiHandler.sendEmptyMessage(NetInfo.WHAT_SEARCH_STORE);
                            }

                            uiHandler.sendEmptyMessage(NetInfo.WHAT_SEARCH_STORE_SUB);

                        } else {
                            Log.e("서브카테 true N");

                            if (resultObject.getString(ParamInfo.IS_END).equals(ParamInfo.Y)) {
                                //다음데이터가 있는지 여부
                                Log.e("다음데이터가 없음");
                                isEnabledRefresh = false;
                            } else {
                                Log.e("다음데이터가 있어");
                                isEnabledRefresh = true;
                            }

                            //스토어 생성
//                            beforeSubcate_seq = 0;
                            JSONArray storeList = resultObject.getJSONArray(ParamInfo.STORE_LIST);
                            Store store;
                            JSONObject storeObject;
                            for (int i = 0; i < storeList.length(); i++) {
                                storeObject = storeList.getJSONObject(i);

                                // 카테고리 있으면 카테고리 별로 객체 생성
                                if (!storeObject.isNull(ParamInfo.ST_CATE)) {
                                    switch (storeObject.getInt(ParamInfo.ST_CATE)) {
                                        //배달
                                        case 1:
                                            store = new Delivery(storeList.getJSONObject(i));
                                            break;
                                        // 외식
                                        case 2:
                                            store = new Eatery(storeList.getJSONObject(i));
                                            break;
                                        // 과외/학원
                                        case 3:
                                            store = new Education(storeList.getJSONObject(i));
                                            break;
                                        // 병원/약국
                                        case 4:
                                            store = new Medical(storeList.getJSONObject(i));
                                            break;
                                        // 카페/주점
                                        case 5:
                                            store = new Cafe(storeList.getJSONObject(i));
                                            break;
                                        // 생활/편의
                                        case 6:
                                            store = new Life(storeList.getJSONObject(i));
                                            break;
                                        case 7:
                                            store = new Mart(storeList.getJSONObject(i));
                                            break;
                                        default:
                                            store = new Store(storeList.getJSONObject(i));
                                            break;
                                    }
                                } else {
                                    // 카테고리 없으면 일반 객체 생성
                                    store = new Store(storeList.getJSONObject(i));
                                }
                                arrayListStore.add(store);
                            }
                            Log.e("여기는?=======");
                            uiHandler.sendEmptyMessage(NetInfo.WHAT_SEARCH_STORE);
                        }

                    } else {
                        uiHandler.sendEmptyMessage(NetInfo.WHAT_SEARCH_STORE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            } else if (url == NetInfo.READ_AD) {

                if (!object.isNull(ParamInfo.AD_LIST)) {
                    try {
                        arrayListAD = new ArrayList<Advertisement>();

                        JSONArray adList = null;
                        adList = object.getJSONArray(ParamInfo.AD_LIST);
                        for (int i = 0; i < adList.length(); i++) {
                            arrayListAD.add(new Advertisement(adList.getJSONObject(i)));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                uiHandler.sendEmptyMessage(NetInfo.WHAT_READ_AD);
            }
        }
    };

    /*
    * gps검색 실패시, 지역검색 실패시
    * */
    float searchXPosition;
    float refreshXPosition;
    View.OnTouchListener imageTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(final View v, MotionEvent event) {

            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    searchXPosition = viewMapper.imageGpsFailSearch.getX();
                    refreshXPosition = viewMapper.imageGpsFailRefresh.getX();

                    RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) v.getLayoutParams();
                    param.width = Utils.DPToPX(MainActivity.this, 48);
                    v.setLayoutParams(param);

                    break;

                case MotionEvent.ACTION_MOVE:

                    final int touchPointX = (int) event.getRawX();
                    final int gap = Utils.DPToPX(MainActivity.this, 40);
                    final int imageHalfWidth = Utils.DPToPX(MainActivity.this, 24);

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (touchPointX > ((searchXPosition + imageHalfWidth) - gap)) {
                                v.setX(searchXPosition);
                                ((StretchImageView) v).setImageResource(R.drawable.ico_gps_fail_search_p);
                            } else if (touchPointX < ((refreshXPosition + imageHalfWidth) + gap)) {
                                v.setX(refreshXPosition);
                                ((StretchImageView) v).setImageResource(R.drawable.ico_gps_fail_refresh_p);
                            } else {
                                v.setX(touchPointX - imageHalfWidth);
                                ((StretchImageView) v).setImageResource(R.drawable.ico_gps_fail_bird);
                            }

                        }
                    });

                    break;

                case MotionEvent.ACTION_UP:
                    final int touchUpPointX = (int) event.getRawX();
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) v.getLayoutParams();
                    params.width = Utils.DPToPX(MainActivity.this, 70);

                    if (v.getX() == refreshXPosition) {
                        viewMapper.wrapperGpsFail.setVisibility(View.INVISIBLE);
                        v.animate().translationXBy(touchUpPointX).translationX(0).start();
                        ((StretchImageView) v).setImageResource(R.drawable.ico_gps_fail_bird);
                        v.setLayoutParams(params);
                        viewMapper.wrapperGpsFail.setVisibility(View.GONE);

                        //내주변매장
                        horizontalMenuView.setPageSelected(0);
                        storeType = Store.StoreType.ALL;
                        menuType = StoreMenu.MenuType.NONE;
                        textViewTitle.setText(getResources().getString(R.string.store_map_title));
                        textViewLike.setText(getResources().getString(R.string.main_button_like_store_message));
                        horizontalScrollView.setVisibility(View.VISIBLE);
                        textViewLikeTitle.setVisibility(View.GONE);

                        if (checkGps(true)) {
                            isGpsWating = true;
                        } else {
                            // 빈화면 띄우기
                            setEmptyView(R.string.empty_list_message);
                        }

                    } else if (v.getX() == searchXPosition) {
                        viewMapper.wrapperGpsFail.setVisibility(View.INVISIBLE);
                        v.animate().translationXBy(touchUpPointX).translationX(0).start();
                        ((StretchImageView) v).setImageResource(R.drawable.ico_gps_fail_bird);
                        v.setLayoutParams(params);

                        intent = new Intent(context, SearchActivity.class);
                        intent.putExtra(ParamInfo.SEARCH_SEQ,beforeAddress);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivityForResult(intent, ParamInfo.REQUEST_UPDATE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                viewMapper.wrapperGpsFail.setVisibility(View.VISIBLE);
                            }
                        }, 1000);
                    } else {
                        v.setLayoutParams(params);
                        v.animate().translationXBy(touchUpPointX).translationX(0).setDuration(400).start();
                    }

                    break;
                default:

                    break;
            }
            return true;
        }
    };

    /*
    * gps 반경설정
    * */
    float circle1XPos;
    float circle2XPos;
    float circle3XPos;
    View.OnTouchListener gpsDisListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(final View v, MotionEvent event) {

            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    circle1XPos = viewMapper.gpsDisImage1.getX();
                    circle2XPos = viewMapper.gpsDisImage2.getX();
                    circle3XPos = viewMapper.gpsDisImage3.getX();

                    RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) v.getLayoutParams();
                    param.width = Utils.DPToPX(MainActivity.this, 46);
                    v.setLayoutParams(param);

                    break;

                case MotionEvent.ACTION_MOVE:

                    final int touchPointX = (int) event.getRawX();
                    final int gap = Utils.DPToPX(MainActivity.this, 32);
                    final int imageHalfWidth = Utils.DPToPX(MainActivity.this, 23);


                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (touchPointX > ((circle3XPos + imageHalfWidth) - gap)) {
                                v.setX(circle3XPos);
                                RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) v.getLayoutParams();
                                param.width = Utils.DPToPX(MainActivity.this, 42);
                                v.setLayoutParams(param);
                            } else if (touchPointX < ((circle1XPos + imageHalfWidth) + gap)) {
                                v.setX(circle1XPos);
                                RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) v.getLayoutParams();
                                param.width = Utils.DPToPX(MainActivity.this, 42);
                                v.setLayoutParams(param);
                            } else {
                                RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) v.getLayoutParams();
                                param.width = Utils.DPToPX(MainActivity.this, 42);
                                v.setLayoutParams(param);
                                v.setX(touchPointX - imageHalfWidth);
                            }

                        }
                    });

                    break;

                case MotionEvent.ACTION_UP:
                    final int touchUpPointX = (int) event.getRawX();
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) v.getLayoutParams();
                    params.width = Utils.DPToPX(MainActivity.this, 42);

                    if (v.getX() == circle1XPos) {
                        clickGpsDisImage(1);
                    } else if (v.getX() == circle3XPos) {
                        clickGpsDisImage(3);
                    } else {
                        v.setLayoutParams(params);
                        v.animate().translationXBy(touchUpPointX).translationX(0).setDuration(300).start();
                        clickGpsDisImage(2);
                    }
                    break;
                default:

                    break;
            }
            return true;
        }
    };

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    private Handler uiHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            Log.e("로딩뷰~ ");
            viewMapper.lodingView.setVisibility(View.GONE);

            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:

                    Toast.makeText(context, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    Log.e("FAILED_CONNECTED");
                    stopGpsTimer(false);
                    setAdapter(R.string.no_list_message);

                    swipeRefreshLayout.setRefreshing(false);
                    break;

                case NetInfo.WHAT_SEARCH_STORE:

                    Log.e("매장 size : " + arrayListStore.size());

                    if (storeListMode == StoreListMode.FAVORITE) {
                        Log.e("좋아요 모드");

                        swipeRefreshLayout.setRefreshing(false);

                    } else {
                        Log.e("서치모드 or GPS모드");

                        if (storeListMode == StoreListMode.GPS) {
                            viewMapper.textLocation.setText("지역을 검색해주세요");
                        } else if (storeListMode == StoreListMode.SEARCH) {
                            viewMapper.textLocation.setText(PreferenceInfo.loadLocationSearch(context));
                        }

                        if (arrayListStore.size() == 0) {
                            viewMapper.imageFailInfo.setImageResource(R.drawable.image_search_fail_info);
                            viewMapper.wrapperGpsFail.setVisibility(View.VISIBLE);
                        }

                        if (locationUtil != null) {
                            locationUtil.stopLocationUpdates();
                        }
                    }

                    swipeRefreshLayout.setRefreshing(false);
                    setAdapter(R.string.no_list_message);
                    break;

                case NetInfo.WHAT_SEARCH_STORE_SUB:
                    if (subCateStyle.equals("A")) {
                        Log.e("스타일코드가 A로 진입");
                        isSubCateMode = true;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setSubCateA(subCateList);
                            }
                        });
                    } else if (subCateStyle.equals("B")) {
                        Log.e("스타일코드가 B로 진입");
                        isSubCateMode = true;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setSubCateB(subCateList);
                            }
                        });
                    } else {
                        viewMapper.subCateScroll.setVisibility(View.GONE);
                        Log.e("스타일코드가 A B 둘다 아님");
                    }

                    break;
                case NetInfo.WHAT_READ_AD:

                    setAdView();
                    break;
                case WHAT_GPS_FAIL:

                    setGpsFailAdapter(R.string.gps_fail_message);
                    break;

            }
        }
    };

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================


    private void init() {
        //back Key
        backPressCloseHandler = new BackPressCloseHandler(this);

        context = this;
        glideRequestManager = Glide.with(this);
        storeType = Store.StoreType.ALL;
        menuType = StoreMenu.MenuType.NONE;

        storeListMode = StoreListMode.GPS;

        GlobalApplication.setCurrentActivity(this);
        server = new ServerRequest(networkResultListener);

        listEmptyView = new ListEmptyView(context);

        me = User.getMyInfo(context);
        displayManager = new DisplayManager(context);

        executeRegisterGcm();

        initUI();
        initUISize();
        setEventListener();
        setScrollMenuView();
        setAdapter(R.string.gps_find_message);
        textViewTitle.setText(getResources().getString(R.string.store_map_title));

        requestADToServer();

        //푸쉬로진입시 gps탐색 X
        if (getIntent().getIntExtra(ParamInfo.PUSH_SEQ, 0) != 0) {
            requestPushUpdateToServer(getIntent().getIntExtra(ParamInfo.PUSH_SEQ, 0), getIntent().getIntExtra(ParamInfo.STORE_SEQ, 0));
            if (getIntent().getIntExtra(ParamInfo.STORE_SEQ, 0) == 0) {
                //띄울 매장이 없는경우 gps탐색
                if (checkGps(true)) {
                    isGpsWating = true;
                } else {
                    // 빈화면 띄우기
                    setEmptyView(R.string.empty_list_message);
                }

            } else {
                viewMapper.imageFailInfo.setImageResource(R.drawable.image_search_guide);
                viewMapper.wrapperGpsFail.setVisibility(View.VISIBLE);
            }
        } else {
            if (checkGps(true)) {
                isGpsWating = true;
            } else {
                // 빈화면 띄우기
                setEmptyView(R.string.empty_list_message);
            }

        }

    }

    boolean lastItemVisibleFlag = false;

    private void initUI() {

        drawerLayout = (DrawerLayout) findViewById(R.id.activity_main_drawerlayout);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.main_swiperefreshlayout);


        listView = (ListView) findViewById(R.id.activity_main_listview);
        //하단에 닫은시점 찾기위해

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                //OnScrollListener.SCROLL_STATE_IDLE은 스크롤이 이동하다가 멈추었을때 발생되는 스크롤 상태입니다.
                //즉 스크롤이 바닦에 닿아 멈춘 상태에 처리를 하겠다는 뜻
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && lastItemVisibleFlag) {

                    Log.e("스크롤 넘어가기전 : "+beforeSubcate_seq);
                    if (isEnabledRefresh) {
                        requestToStoreListToServer(beforeProgressShowing, beforeSerchType, beforeCate, beforeLat, beforeLng, beforeAddress,beforeIsSigungu, beforeUser_seq, beforeSubcate_seq);
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //현재 화면에 보이는 첫번째 리스트 아이템의 번호(firstVisibleItem) + 현재 화면에 보이는 리스트 아이템의 갯수(visibleItemCount)가 리스트 전체의 갯수(totalItemCount) -1 보다 크거나 같을때
                if ((totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount)) {
                    lastItemVisibleFlag = true;
                } else {
                    lastItemVisibleFlag = false;
                }
            }
        });

        horizontalScrollView = (HorizontalScrollView) findViewById(R.id.activity_main_horizontal_scrollview);
        textViewTitle = (TextView) findViewById(R.id.activity_main_textview_title);

        textViewLikeTitle = (TextView) findViewById(R.id.activity_main_textview_like_title);

        if (me == null || me.getDeviceId() == null || me.getDeviceId().equals("")) {

            setLogin(false);

        } else {

            setLogin(true);

        }
    }

    private void initUISize() {

        displayManager.changeWidthHeightSameRate(findViewById(R.id.activity_main_top_panel).getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 96);
        FrameLayout.LayoutParams frameParams = (FrameLayout.LayoutParams) findViewById(R.id.activity_main_imageview_menu).getLayoutParams();
        displayManager.changeWidthHeightSameRate(frameParams, 55, 51);
        frameParams.leftMargin = displayManager.getScaleSizeSameRate(20);
        frameParams.rightMargin = displayManager.getScaleSizeSameRate(20);

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) textViewTitle.getLayoutParams();
        linearParams.leftMargin = displayManager.getScaleSizeSameRate(20);
        linearParams.rightMargin = displayManager.getScaleSizeSameRate(20);

        linearParams = (LinearLayout.LayoutParams) textViewLike.getLayoutParams();
        textViewLike.setPadding(displayManager.getScaleSizeSameRate(14), displayManager.getScaleSizeSameRate(14), displayManager.getScaleSizeSameRate(14), displayManager.getScaleSizeSameRate(14));
        linearParams.leftMargin = displayManager.getScaleSizeSameRate(18);

        displayManager.changeWidthHeightSameRate(horizontalScrollView.getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 100);
        displayManager.changeWidthHeightSameRate(textViewLikeTitle.getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 100);

        displayManager.changeWidthHeightSameRate(imageViewAd.getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 100);


        listView.setDividerHeight(displayManager.getScaleSizeSameRate(4));

        displayManager.changeWidthHeightSameRate(imageViewAd.getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 100);

    }

    final void setSubCateA(final ArrayList<SubCate> subCateList) {
        if (subCateList == null) {
            return;
        } else {
            if (subCateList.size() == 0) {
                return;
            } else {
                viewMapper.subCateScroll.setVisibility(View.VISIBLE);
            }
        }
        viewMapper.subCateWrapper.removeAllViews();

        int num = 0;
        View v = null;
        RelativeLayout imageWrapper1 = null;
        RelativeLayout imageWrapper2 = null;
        StretchImageView image = null;
        StretchImageView image2 = null;

        for (int i = 0; i < subCateList.size(); i++) {
            Log.e("카테고리 종류?" + subCateList.get(i));
            SubCate subCate = subCateList.get(i);

            if (num % 2 == 0) {
                v = getLayoutInflater().inflate(R.layout.item_container_subcate, null);
            }
            switch (num) {
                case 0:
                    if (i == 0) {
                        imageWrapper1 = (RelativeLayout) v.findViewById(R.id.subcate_wrapper1);
                        imageWrapper2 = (RelativeLayout) v.findViewById(R.id.subcate_wrapper2);
                        image = (StretchImageView) v.findViewById(R.id.subcate_image1);
                        imageWrapper2.setVisibility(View.GONE);

                        imageWrapper1.setTag(subCate.getSeq());
                        imageWrapper1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //전체매장보기
                                Log.e("서브카테 = "+v.getTag());
                                requestToStoreListToServer(beforeProgressShowing, beforeSerchType, beforeCate, beforeLat, beforeLng, beforeAddress,beforeIsSigungu, beforeUser_seq, Integer.parseInt((String) v.getTag()));
                            }
                        });
                        if (subCate.getImageName() != null) {
                            glideRequestManager
                                    .load(NetInfo.SUBCATE_URL + subCate.getImageName())
                                    .error(R.drawable.image_default_photo)
                                    .thumbnail(0.1f)
                                    .into(image);
                            Log.e("애드뷰 ");
                            viewMapper.subCateWrapper.addView(v);
                        }
                        num = 0;
                        break;

                    } else {
                        imageWrapper1 = (RelativeLayout) v.findViewById(R.id.subcate_wrapper1);
                        imageWrapper2 = (RelativeLayout) v.findViewById(R.id.subcate_wrapper2);
                        image = (StretchImageView) v.findViewById(R.id.subcate_image1);
                        image2 = (StretchImageView) v.findViewById(R.id.subcate_image2);

                        imageWrapper2.setVisibility(View.INVISIBLE);

                        imageWrapper1.setTag(subCate.getSeq());
                        imageWrapper1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.e("서브카테 = "+v.getTag());
                                requestToStoreListToServer(beforeProgressShowing, beforeSerchType, beforeCate, beforeLat, beforeLng, beforeAddress,beforeIsSigungu, beforeUser_seq, Integer.parseInt((String) v.getTag()) );

                            }
                        });
                        if (subCate.getImageName() != null) {
                            glideRequestManager
                                    .load(NetInfo.SUBCATE_URL + subCate.getImageName())
                                    .error(R.drawable.image_default_photo)
                                    .thumbnail(0.1f)
                                    .into(image);
                            Log.e("애드뷰 ");
                            viewMapper.subCateWrapper.addView(v);
                        }
                        num = 1;
                        break;
                    }
                case 1:
                    imageWrapper2.setVisibility(View.VISIBLE);
                    imageWrapper2.setTag(subCate.getSeq());
                    imageWrapper2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.e("서브카테 = "+v.getTag());
                            requestToStoreListToServer(beforeProgressShowing, beforeSerchType, beforeCate, beforeLat, beforeLng, beforeAddress,beforeIsSigungu, beforeUser_seq, Integer.parseInt((String) v.getTag()));
                    }
                    });
                    if (subCate.getImageName() != null) {
                        glideRequestManager
                                .load(NetInfo.SUBCATE_URL + subCate.getImageName())
                                .error(R.drawable.image_default_photo)
                                .thumbnail(0.1f)
                                .into(image2);
                    }
                    num = 0;
                    break;
            }
        }
        viewMapper.subCateScroll.setAlpha(1);
    }

    final void setSubCateB(final ArrayList<SubCate> subCateList) {
        if (subCateList == null) {
            return;
        } else {
            if (subCateList.size() == 0) {
                return;
            } else {
                viewMapper.subCateScroll.setVisibility(View.VISIBLE);
            }
        }
        viewMapper.subCateWrapper.removeAllViews();

        View v = null;
        RelativeLayout imageWrapper1 = null;
        RelativeLayout imageWrapper2 = null;
        StretchImageView image = null;
        StretchImageView image2 = null;
        for (int i = 0; i < subCateList.size(); i++) {
            Log.e("카테고리 종류?" + subCateList.get(i));
            SubCate subCate = subCateList.get(i);

            v = getLayoutInflater().inflate(R.layout.item_container_subcate, null);

            imageWrapper1 = (RelativeLayout) v.findViewById(R.id.subcate_wrapper1);
            imageWrapper2 = (RelativeLayout) v.findViewById(R.id.subcate_wrapper2);
            image = (StretchImageView) v.findViewById(R.id.subcate_image1);
            imageWrapper2.setVisibility(View.GONE);

            imageWrapper1.setTag(subCateList.get(i).getSeq());
            imageWrapper1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("서브카테 = "+v.getTag());
                    requestToStoreListToServer(beforeProgressShowing, beforeSerchType, beforeCate, beforeLat, beforeLng, beforeAddress,beforeIsSigungu, beforeUser_seq, Integer.parseInt((String) v.getTag()));
                }
            });
            if (subCate.getImageName() != null) {
                glideRequestManager
                        .load(NetInfo.SUBCATE_URL + subCate.getImageName())
                        .error(R.drawable.image_default_photo)
                        .thumbnail(0.1f)
                        .into(image);
                viewMapper.subCateWrapper.addView(v);
            }
        }
        viewMapper.subCateScroll.setAlpha(1);
    }

    private void setEventListener() {

        drawerLayout.setDrawerListener(drawerListener);
        swipeRefreshLayout.setOnRefreshListener(refreshListener);

    }


    private void setAdapter(int resId) {

        Log.e("store size = " + arrayListStore.size());
        if (arrayListStore == null || arrayListStore.size() == 0) {
            setEmptyView(resId);
        }
        if(refreshCount == 1) {
            storeListAdapter = new StoreListAdapter(context, arrayListStore, glideRequestManager);
            listView.setAdapter(storeListAdapter);
            listView.setOnItemClickListener(itemClickListener);
        }else {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
                    storeListAdapter.replace(arrayListStore);
//                }
//            });

        }
        if (arrayListStore != null && arrayListStore.size() > 0 && listView.getFooterViewsCount() > 0) {

            listView.removeFooterView(listEmptyView);
        }
    }

    private void setGpsFailAdapter(int resId) {

        setEmptyView(resId);
        storeListAdapter = new StoreListAdapter(context, new ArrayList<Store>(), glideRequestManager);
        listView.setAdapter(storeListAdapter);
        listView.setOnItemClickListener(null);

        viewMapper.imageFailInfo.setImageResource(R.drawable.image_gps_fail_info);
        Log.e("gps fail이라 생성");
        viewMapper.wrapperGpsFail.setVisibility(View.VISIBLE);
    }

    private void setScrollMenuView() {

        horizontalMenuView = new HorizontalMenuView(context);
        horizontalMenuView.setOnHorizontalItemClickListener(horizontalItemClickListener);
        horizontalScrollView.addView(horizontalMenuView);

    }

    /**
     * GPS 실행 여부 체크하고
     */
    private boolean checkGps(boolean isShowProgress) {

        if (arrayListStore != null && arrayListStore.size() > 0) {

            listView.removeFooterView(listEmptyView);
        } else {

            setEmptyView(R.string.gps_find_message);
        }

        locationUtil = LocationUtil.getInstance(context);
        locationUtil.setOnLocationChangedListener(locationChangedListener);

        if (locationUtil.canGetLocation()) {
            if (isShowProgress) {
                serchGpsView(true);
            }
            // GPS 만 켜있는데 이전 좌표가 없을때
            if (!locationUtil.isNetWorkEnable() &&
                    locationUtil.getLocation() == null &&
                    (me == null || (me.getLatitude() == LocationUtil.DEFAULT_LATITUDE && me.getLongitude() == LocationUtil.DEFAULT_LONGITUDE))) {

                Log.i("GPS 만 켜있는데 이전 좌표가 없을때 향상된 알림창 띄움 ");
                serchGpsView(false);

                showDialogGPS(R.string.gps_improve_message);
                return false;
            }

            storeListMode = StoreListMode.GPS;
            startGpsTimer();
            Log.e("GPS 사용가능 ");

            return true;
        } else {
            showDialogGPS(R.string.gps_message);
            return false;
        }

    }


    private void showDialogGPS(int messageId) {
        if (viewMapper.wrapperGpsAlert.getVisibility() == View.GONE) {
            viewMapper.wrapperGpsAlert.setVisibility(View.INVISIBLE);
            viewMapper.wrapperGpsAlert.setAlpha(0);
            viewMapper.wrapperGpsAlert.setVisibility(View.VISIBLE);
            viewMapper.wrapperGpsAlert.animate().alpha(1).setDuration(400).start();
        }
    }

    /**
     * GPS 시간 체크 키기
     */
    private void startGpsTimer() {
        if (gpsTimer != null)
            gpsTimer.cancel();

        TimerTask gpsTimerTask = new TimerTask() {
            @Override
            public void run() {
                Log.e("gps 타이머 동작중 " + isGpsWating);

                if ((int) ((System.nanoTime() - holdStartTime) / FOR_SEC_TRANS) >= MAX_SEARCH_GPS_TIME) {

                    Log.e("기다리기 지쳐서 끄기 ");
                    stopGpsTimer(true);
                }
            }
        };

        Log.e("GPS 타이머 시작 ! ");
        holdStartTime = System.nanoTime();
        gpsTimer = new Timer();
        gpsTimer.schedule(gpsTimerTask, 1, CHECK_GPS_TERM);

    }

    /**
     * GPS 시간 체크 끄기 및 GPS 찾기 종료 ,프로그래스바 끄기, 못찾음 띄우기
     */
    private void stopGpsTimer(boolean isShowEmpty) {

        if (gpsTimer != null) {
            Log.e("타이머 끄기");
            gpsTimer.cancel();
            gpsTimer = null;
        }

        serchGpsView(false);

        if (locationUtil != null) {

            locationUtil.stopLocationUpdates();
            locationUtil.setOnLocationChangedListener(null);
        }

        if (isShowEmpty) {
            uiHandler.sendEmptyMessage(WHAT_GPS_FAIL);
        }


    }

    /**
     * 아무것도 없을때 화면 설정
     */
    private void setEmptyView(int resId) {
        Log.e("listview footer = " + listView.getFooterViewsCount());

        if (listView.getFooterViewsCount() > 0)
            listView.removeFooterView(listEmptyView);

        listEmptyView.setMessage(resId);
        listView.addFooterView(listEmptyView);

    }

    /**
     * 로그인 했을떄 , 로그아웃 했을때 처리
     *
     * @param isLogin
     */
    private void setLogin(boolean isLogin) {
        if (isLogin) {
            if (me != null) {

                if (me.getUserNickName() != null) {
                    viewMapper.textUserNickname.setText(me.getUserNickName());
                }
                if (me.getAddress() == null || me.getAddress().equals("")) {
                    Log.e("0" + me.getAddress() + "0");
                    viewMapper.textUserAddress.setText(getResources().getString(R.string.main_user_no_address));
                } else {
                    Log.e("주소진입");
                    viewMapper.textUserAddress.setText(me.getAddress());
                }
                viewMapper.textUserPoint.setText("동네로 포인트 : " + me.getPoint());
            }
            viewMapper.btnDrawerSignIn.setVisibility(View.GONE);
            viewMapper.btnDrawerUserInfo.setVisibility(View.VISIBLE);
        } else {
            viewMapper.btnDrawerUserInfo.setVisibility(View.GONE);
            viewMapper.btnDrawerSignIn.setVisibility(View.VISIBLE);
        }
    }

    /**
     * gps취소했을때 이전 좌표로 띄워주거나 기본 카테고리로 띄워줌
     */
    private void setGpsCancel() {

        storeListMode = StoreListMode.SEARCH;

        intent = new Intent(context, SearchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(ParamInfo.SEARCH_SEQ,beforeAddress);
        startActivityForResult(intent, ParamInfo.REQUEST_UPDATE);
//        intent = new Intent(context, SearchActivityi.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        startActivityForResult(intent, ParamInfo.REQUEST_UPDATE);

    }

    private void setAdView() {

        if (arrayListAD == null || arrayListAD.size() < 1) {

            RelativeLayout.LayoutParams linearParams = (RelativeLayout.LayoutParams) viewMapper.subCateScroll.getLayoutParams();
            linearParams.bottomMargin = 0;
            imageViewAd.setVisibility(View.GONE);
            return;

        } else {

            RelativeLayout.LayoutParams linearParams = (RelativeLayout.LayoutParams) swipeRefreshLayout.getLayoutParams();
            linearParams.bottomMargin = displayManager.getScaleSizeSameRate(100);
            RelativeLayout.LayoutParams linearParams2 = (RelativeLayout.LayoutParams) viewMapper.subCateScroll.getLayoutParams();
            linearParams2.bottomMargin = displayManager.getScaleSizeSameRate(100);

            imageViewAd.setVisibility(View.VISIBLE);

        }

        flipAnimation = new FlipAnimation();
        flipAnimation.setDuration(500);
        flipAnimation.setStartOffset(AD_ANIM_TERM);
        flipAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

                currentAnimPos++;
                if (arrayListAD.size() <= currentAnimPos) {
                    currentAnimPos = 0;
                }

                imageViewAd.post(new Runnable() {
                    @Override
                    public void run() {
                        glideRequestManager
                                .load("http://" + arrayListAD.get(currentAnimPos).getPrvwUrl())
                                .error(R.drawable.image_default_photo)
                                .into(imageViewAd);
                    }
                });
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageViewAd.startAnimation(flipAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        imageViewAd.post(new Runnable() {
            @Override
            public void run() {
                glideRequestManager
                        .load("http://" + arrayListAD.get(0).getPrvwUrl())
                        .error(R.drawable.image_default_photo)
                        .into(imageViewAd);
            }
        });
        imageViewAd.startAnimation(flipAnimation);

    }

    private void serchGpsView(final boolean startAnimation) {
        final ObjectAnimator animator = ObjectAnimator.ofFloat(viewMapper.birdSearchGps, "translationX", 0, Utils.DPToPX(MainActivity.this, 110));
        animator.setDuration(3000);
        animator.setRepeatCount(2);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (startAnimation) {
                    //gps찾는중
                    viewMapper.wrapperSearchGps.setVisibility(View.VISIBLE);
                    animator.start();
                } else {
                    //애니메이션 끄기
                    viewMapper.wrapperSearchGps.setVisibility(View.INVISIBLE);
                    viewMapper.birdSearchGps.setX(Utils.DPToPX(MainActivity.this, 110));
                }
            }
        });
    }

    private void clearReferences() {
        Activity currActivity = GlobalApplication.getCurrentActivity();
        if (currActivity != null && currActivity.equals(this)) {
            GlobalApplication.setCurrentActivity(null);
        }
    }

    private boolean checkChangeParams(boolean progressShowing, String serchType, int cate, double lat, double lng,int addrSeq, String isSigu
            ,int user_seq, int subCate) {
        boolean isChanged = false;
        if (this.beforeProgressShowing != progressShowing) {
            isChanged = true;
        } else if (!this.beforeSerchType.equals(serchType)) {
            isChanged = true;
        } else if (this.beforeCate != cate) {
            isChanged = true;
        } else if (this.beforeLat != lat) {
            isChanged = true;
        } else if (this.beforeLng != lng) {
            isChanged = true;
        } else if (beforeAddress != addrSeq) {
            isChanged = true;
        }
        else if( beforeIsSigungu != null && !beforeIsSigungu.equals(isSigu)) {
            isChanged = true;
        }
        else if (this.beforeUser_seq != user_seq) {
            isChanged = true;
        } else if (this.beforeSubcate_seq != subCate) {
            isChanged = true;
        }
        //현재검색데이터 저장
        this.beforeProgressShowing = progressShowing;
        this.beforeSerchType = serchType;
        this.beforeCate = cate;
        this.beforeLat = lat;
        this.beforeLng = lng;
        this.beforeAddress = addrSeq;
        this.beforeIsSigungu = isSigu;
        this.beforeUser_seq = user_seq;
        this.beforeSubcate_seq = subCate;
        Log.e("저장할 addr : "+addrSeq);

        return isChanged;
    }

    /*
    * 1. GPS검색
    * 2. 주소로 검색
    * 3. 좋아요한 매장
    * */
    private void requestToStoreListToServer(boolean progressShowing, String serchType, int cate, double lat, double lng) {
        requestToStoreListToServer(progressShowing, serchType, cate, lat, lng,0,ParamInfo.N, 0, 0);
    }

    private void requestToStoreListToServer(boolean progressShowing, String serchType, int cate, int addr_seq,String isSigungu) {
        requestToStoreListToServer(progressShowing, serchType, cate, 0, 0, addr_seq,isSigungu, 0, 0);
    }

    private void requestToStoreListToServer(boolean progressShowing, String serchType, int user_seq) {
        requestToStoreListToServer(progressShowing, serchType, 0, 0, 0,beforeAddress,beforeIsSigungu, user_seq,0);
    }

    /**
     * 위도 경도로 매장 리스트 검색
     *
     */
    private void requestToStoreListToServer(boolean progressShowing, String serchType, int cate, double lat, double lng, int addrSeq,String isSigungu, int user_seq, int subCate) {
        //서브카테가 보일시 제거
        if (viewMapper.subCateScroll.getVisibility() == View.VISIBLE || viewMapper.subCateScroll.getVisibility() == View.INVISIBLE) {
            viewMapper.subCateScroll.setVisibility(View.GONE);
        }

        //프로그레스바
        if (progressShowing) {
            Log.e("들어오나?");
            viewMapper.lodingView.setVisibility(View.VISIBLE);
        }

        //before바뀌기전에 서브카테모드 변경
        if(beforeSerchType != serchType || beforeCate != cate){

            isSubCateMode = false;
        }

        //검색방법 변경시 refresh 가능하도록 검증 및 변수저장
        if (checkChangeParams(progressShowing, serchType, cate, lat, lng, addrSeq,isSigungu, user_seq, subCate)) {
            isEnabledRefresh = true;
            refreshCount = 1;
        } else {
            refreshCount += 1;
        }

        //좋아요한매장  VS  GPS or SEARCH
        if (serchType.equals(ParamInfo.SEARCH_LIKE)) {
            viewMapper.subCateScroll.setVisibility(View.GONE);
            horizontalScrollView.setVisibility(View.GONE);
            textViewLikeTitle.setVisibility(View.VISIBLE);
        } else {
            horizontalMenuView.setPageSelected(cate);
            textViewLikeTitle.setVisibility(View.GONE);
            horizontalScrollView.setVisibility(View.VISIBLE);
        }

        //서버 파람
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);

        // 지하철 검색은 gps 처럼
        if(serchType.equals(ParamInfo.SEARCH_SUBWAY)) {

            params.put(ParamInfo.SEARCH_TYPE, ParamInfo.SEARCH_GPS);
        }
        else {

            params.put(ParamInfo.SEARCH_TYPE, serchType);
        }
        params.put(ParamInfo.SEARCH_REFRESH_CNT, refreshCount);
        Log.e("요청 서브카테 = "+subCate);
        Log.e("refreshCount =" + refreshCount);
        params.put(ParamInfo.SUB_CATE_SEQ, subCate);

        //요청방식에 따른 로직 분기
        if (serchType.equals(ParamInfo.SEARCH_GPS)) {
            //GPS 요청방식 저장
            storeListMode = StoreListMode.GPS;

            //좋아요한 매장검색시 메뉴 VISIBLE
            if (horizontalScrollView.getVisibility() == View.GONE) {

                horizontalScrollView.setVisibility(View.VISIBLE);
                textViewLikeTitle.setVisibility(View.GONE);
                horizontalMenuView.setPageSelected(0);
            }

            stopGpsTimer(false);
            isGpsOK = false;
            isGpsWating = false;

            //param처리
            params.put(ParamInfo.SEARCH_GPS_DIS, gpsDis);
            params.put(ParamInfo.CATE, cate);
            params.put(ParamInfo.LAT, lat);
            params.put(ParamInfo.LNG, lng);

        } else if (serchType.equals(ParamInfo.SEARCH_ADDRESS)) {
            //SEARCH 요청방식 저장
            storeListMode = StoreListMode.SEARCH;

            //param처리
            params.put(ParamInfo.CATE, cate);
            if (addrSeq != AppInfo.DEFAULT_INT) {
                params.put(ParamInfo.SEARCH_SEQ, addrSeq);
                params.put(ParamInfo.IS_SIGUNGU,isSigungu);
            } else {
                Log.e("주소값이 올바르지 않음");
            }

        } else if (serchType.equals(ParamInfo.SEARCH_LIKE)) {
            //FAVORITE 요청방식 저장
            storeListMode = StoreListMode.FAVORITE;

            //param처리
            params.put(ParamInfo.USER_SEQ, user_seq);

        }
        else if(serchType.equals(ParamInfo.SEARCH_SUBWAY)) {

            storeListMode = StoreListMode.SUBWAY;

            //좋아요한 매장검색시 메뉴 VISIBLE
            if (horizontalScrollView.getVisibility() == View.GONE) {

                horizontalScrollView.setVisibility(View.VISIBLE);
                textViewLikeTitle.setVisibility(View.GONE);
                horizontalMenuView.setPageSelected(0);
            }

            //param처리
            params.put(ParamInfo.SEARCH_GPS_DIS, gpsDis);
            params.put(ParamInfo.CATE, cate);
            params.put(ParamInfo.LAT, lat);
            params.put(ParamInfo.LNG, lng);

        }
        else {
            Log.e("스토어리스트 요청방식 오류");
            return;
        }

        Log.e(" 서치 type = " + params.get(ParamInfo.SEARCH_TYPE));
        Log.e("cate =" + params.get(ParamInfo.CATE));
        Log.e("lat =" + params.get(ParamInfo.LAT));
        Log.e("lng =" + params.get(ParamInfo.LNG));
        Log.e("address =" + params.get(ParamInfo.ADDRESS));
        Log.e("user_seq =" + params.get(ParamInfo.USER_SEQ));


        //서버요청
        server.execute(NetInfo.IP_ADDRESS1, NetInfo.SEARCH_STORE, params);
    }

    /**
     * 푸쉬정보 update 매장정보가 있을경우 매장 정보가져오기
     */
    private void requestPushUpdateToServer(int pushSeq, int storeSeq) {
        viewMapper.lodingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.PUSH_SEQ, pushSeq);
        params.put(ParamInfo.ST_SEQ, storeSeq);

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.PUSH_UPDATE, params);
    }

    /**
     * 광고 배너 받아오기
     */
    private void requestADToServer() {

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);

        server.execute(NetInfo.READ_AD, params);

    }

    /**
     * GCM Id 등록 비동기 작업
     */
    @SuppressLint("NewApi")
    public void executeRegisterGcm() {

        RegisterGcmAsyncTask registerGcmAsyncTask = new RegisterGcmAsyncTask();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            registerGcmAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            registerGcmAsyncTask.execute();
        }
    }

    @Override
    public void onClick(View v) {
        //gps반경설정
        if (v == viewMapper.wrapperGpsDis) {
        }
        //로딩뷰
        if (v == viewMapper.lodingView) {
        }
        //Gps얼럿창
        if (v == viewMapper.wrapperGpsAlert) {
        }
        //Gps실패시
        if (v == viewMapper.wrapperGpsFail) {
        }
        //Gps수신시
        if (v == viewMapper.wrapperSearchGps) {
        }
        //드로어가 열렸을 시
        if (v == viewMapper.relDrawer) {
        }
        //드로어아이템 클릭
        if (v == viewMapper.btnDrawerSignIn || v == viewMapper.btnDrawerUserInfo ||
                v == viewMapper.btnDrawerNear || v == viewMapper.btnDrawerCoup ||
                v == viewMapper.btnDrawerSetting || v == viewMapper.btnDrawerSetting ||
                v == viewMapper.btnDrawerNotice || v == viewMapper.btnDrawerShare ||
                v == viewMapper.btnDrawerAsk || v == viewMapper.btnLocation || v == viewMapper.btnDrawerNearDis) {
            clickDrawerItem(v);
        }
        //메인
        if (v == frameLayoutMenu) {
            drawerLayout.openDrawer(viewMapper.relDrawer);
            isMenuOpen = true;
        } else if (v == textViewLike || v == imageViewAd) {

            clickMainItem(v);
        }
        //gps상태창
        if (v == viewMapper.textAlertConfirm) {
            isGpsOK = true;
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(intent);
            viewMapper.wrapperGpsAlert.setVisibility(View.GONE);
        } else if (v == viewMapper.textAlertCancel) {
            isGpsOK = false;
            setGpsCancel();
            viewMapper.wrapperGpsAlert.setVisibility(View.GONE);
        }
        //gps반경설정
        if (v == viewMapper.gpsDisImage1 || v == viewMapper.gpsDisImage2 || v == viewMapper.gpsDisImage3 || v == viewMapper.gpsDisConfirm) {
            if (v == viewMapper.gpsDisImage1) {
                clickGpsDisImage(1);
            } else if (v == viewMapper.gpsDisImage2) {
                clickGpsDisImage(2);
            } else if (v == viewMapper.gpsDisImage3) {
                clickGpsDisImage(3);
            } else if (v == viewMapper.gpsDisConfirm) {
                viewMapper.wrapperGpsDis.setVisibility(View.GONE);
                viewMapper.btnDrawerNearDis.setText(gpsDis + "km");
                Toast.makeText(this,"내주변검색하기 거리가 설정 되었습니다.",Toast.LENGTH_SHORT).show();
            }
        }
    }

    final void clickDrawerItem(View v) {
        if (v == viewMapper.btnDrawerSignIn) {
            //로그인
            if (me == null || me.getDeviceId() == null || me.getDeviceId().equals("")) {
                Intent intent = new Intent(this, SelectSignActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        } else if (v == viewMapper.btnDrawerUserInfo) {
            if (viewMapper.textUserAddress.getText().toString().equals(getResources().getString(R.string.main_user_no_address))) {
                //사용자가 주소설정을 하지 않았을 경우 local
                if (me != null) {
                    DataManager.getInstance().setUser(me);
                }
                Intent intent = new Intent(this, SelectAddressActivity.class);
                intent.putExtra(ParamInfo.DETAIL_MODE, true);
                intent.putExtra("isSignMode", true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                //주소있을경우 환경설정
                intent = new Intent(context, UserSettingActivity.class);
                intent.putExtra("isPointMode", true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(intent, ParamInfo.REQUEST_LOGIN_UPDATE);
                /*Intent intent = new Intent(this, SelectAddressActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);*/
            }
        } else if (v == viewMapper.btnDrawerNear) {
            if (viewMapper.wrapperGpsFail.getVisibility() == View.VISIBLE) {
                viewMapper.wrapperGpsFail.setVisibility(View.GONE);
            }
            //내주변매장
            horizontalMenuView.setPageSelected(0);
            storeType = Store.StoreType.ALL;
            menuType = StoreMenu.MenuType.NONE;
            textViewTitle.setText(getResources().getString(R.string.store_map_title));
            textViewLike.setText(getResources().getString(R.string.main_button_like_store_message));
            horizontalScrollView.setVisibility(View.VISIBLE);
            textViewLikeTitle.setVisibility(View.GONE);

            if (checkGps(true)) {
                isGpsWating = true;
            } else {
                // 빈화면 띄우기
                setEmptyView(R.string.empty_list_message);
            }
        } else if (v == viewMapper.btnDrawerCoup) {
            //최근본쿠폰목록
            if (me != null) {
                if (viewMapper.textUserAddress.getText().toString().equals(getResources().getString(R.string.main_user_no_address))) {
                    //주소지가 설정되어 있지 않을때
                    Toast.makeText(context, getResources().getString(R.string.fail_search_address), Toast.LENGTH_SHORT).show();
                } else {
                    intent = new Intent(context, PointActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
            } else {
                //로그인이 되어있지 않을때
                Toast.makeText(context, getResources().getString(R.string.fail_unlogin_message), Toast.LENGTH_SHORT).show();
            }
        } else if (v == viewMapper.btnDrawerSetting) {
            //환경설정
            intent = new Intent(context, UserSettingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, ParamInfo.REQUEST_LOGIN_UPDATE);
        } else if (v == viewMapper.btnDrawerNotice) {
            //공지사항
            intent = new Intent(context, WebviewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(ParamInfo.SITE_ADDRESS, NetInfo.NOTICE_URL);
            startActivity(intent);
        } else if (v == viewMapper.btnDrawerShare) {
            //카톡으로 공유하기
            intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, AppInfo.MARKET_LINK);
            startActivity(Intent.createChooser(intent, "공유"));
        } else if (v == viewMapper.btnDrawerAsk) {
            //문의하기
            /*
            웹뷰 카카오 플러스친구가 적용 안됨
            intent = new Intent(context, WebviewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(ParamInfo.SITE_ADDRESS, AppInfo.KAKAO_PLUS_LINK);
            startActivity(intent);*/

            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppInfo.KAKAO_PLUS_LINK));
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);

            /*intent = new Intent(context, BarcodeTestActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);*/

        } else if (v == viewMapper.btnLocation) {
            //위치설정
            intent = new Intent(context, SearchActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(ParamInfo.SEARCH_SEQ,beforeAddress);
            startActivityForResult(intent, ParamInfo.REQUEST_UPDATE);

        } else if (v == viewMapper.btnDrawerNearDis) {
            viewMapper.wrapperGpsDis.setVisibility(View.VISIBLE);
        }
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    private void clickGpsDisImage(int position) {
        /*
        * 1 == 이미지 1 == 1km
        * 2 == 이미지 2 == 3km
        * 3 == 이미지 3 == 5km
        * */
        circle1XPos = viewMapper.gpsDisImage1.getX();
        circle2XPos = viewMapper.gpsDisImage2.getX();
        circle3XPos = viewMapper.gpsDisImage3.getX();
        switch (position) {
            case 1:
                viewMapper.gpsDisImageSelector.setX(circle1XPos);
                gpsDis = 1;
                break;
            case 2:
                viewMapper.gpsDisImageSelector.setX(circle2XPos);
                gpsDis = 3;
                break;
            case 3:
                viewMapper.gpsDisImageSelector.setX(circle3XPos);
                gpsDis = 5;
                break;
        }
    }

    final void clickMainItem(View v) {
        if (v == textViewLike) {
            //좋아요 한 매장보기 클릭

            if (viewMapper.wrapperGpsFail.getVisibility() == View.VISIBLE) {
                if (me == null) {
                    Toast.makeText(context, getResources().getString(R.string.menu_login_message), Toast.LENGTH_SHORT).show();
                    return;
                }
                viewMapper.wrapperGpsFail.setVisibility(View.GONE);
            }

            // 좋아요 한 매장 상태일때는 다시 전체 매장보기로
            if (storeListMode == StoreListMode.FAVORITE) {

                horizontalMenuView.setPageSelected(0);
                storeType = Store.StoreType.ALL;
                menuType = StoreMenu.MenuType.NONE;

                if (mainTextBackUp != null) {
                    textViewTitle.setText(mainTextBackUp);
                }

                textViewLike.setText(getResources().getString(R.string.main_button_like_store_message));

                if (textViewTitle.getText().toString().equals(getResources().getString(R.string.store_map_title))) {
                    if (checkGps(true)) {
                        requestToStoreListToServer(true, ParamInfo.SEARCH_GPS, storeType.getStoreTypeCode(), locationUtil.getLatitude(), locationUtil.getLongitude());
                    }
                } else {
                    requestToStoreListToServer(true, ParamInfo.SEARCH_ADDRESS, storeType.getStoreTypeCode(), beforeAddress,beforeIsSigungu);
                }

            } else {
                if (me != null && me.getDeviceId() != null && !me.getDeviceId().equals("")) {
                    if (!textViewTitle.getText().toString().equals(getResources().getString(R.string.main_button_like_store_message))) {
                        mainTextBackUp = textViewTitle.getText().toString();
                    }

                    textViewTitle.setText(getResources().getString(R.string.main_button_like_store_message));

                    textViewLike.setText(getResources().getString(R.string.main_button_all_store_message));
                    requestToStoreListToServer(true, ParamInfo.SEARCH_LIKE, me.getSeq());
                } else {
                    Toast.makeText(context, getResources().getString(R.string.menu_login_message), Toast.LENGTH_SHORT).show();
                }
            }
        } else if (v == imageViewAd) {
            //광고보기 클릭
            if (arrayListAD == null || arrayListAD.size() < 1) return;
            String inputUrl = arrayListAD.get(currentAnimPos).getLinkUrl();
            if (inputUrl == null || inputUrl.equals(AppInfo.DEFAULT_STRING)) return;
            if (!inputUrl.contains("http://"))
                inputUrl = "http://" + inputUrl;

            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(inputUrl));
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);

           /* intent = new Intent(context, WebviewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(ParamInfo.SITE_ADDRESS, inputUrl);
            startActivity(intent);*/
        }

    }
//================================================================================
// XXX Inner Class
//================================================================================

    /**
     * GCM Id 등록 비동기 작업
     */
    class RegisterGcmAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... doParams) {

            GCMRegistrar.checkDevice(context);
            GCMRegistrar.checkManifest(context);

            String pushId = GCMRegistrar.getRegistrationId(context);

            if (AppInfo.DEFAULT_STRING.equals(pushId)) {

                GCMRegistrar.register(context, GCMIntentService.PROJECT_ID);
                Log.e("pushId1 : " + pushId);

            } else {
                Log.e("pushId : " + pushId);

            }
            return null;
        }
    }

    /*
    * 16.04.21 메인 드로어 변경작업
    * */
    protected class ViewMapper {
        //로딩뷰
        RelativeLayout lodingView;

        //드로어
        RelativeLayout relDrawer;

        //drawerWigets
        RelativeLayout btnDrawerUserInfo;
        LinearLayout btnDrawerSignIn;
        LinearLayout btnDrawerNear;
        TextView btnDrawerNearDis;
        LinearLayout btnDrawerCoup;
        LinearLayout btnDrawerSetting;
        LinearLayout btnDrawerNotice;
        LinearLayout btnDrawerShare;
        LinearLayout btnDrawerAsk;
        LinearLayout btnLocation;
        TextView textLocation;
        TextView textUserNickname;
        TextView textUserPoint;
        TextView textUserAddress;

        ScrollView subCateScroll;
        LinearLayout subCateWrapper;

        //gps탐색시
        RelativeLayout wrapperSearchGps;
        StretchImageView birdSearchGps;

        //gps실패
        RelativeLayout wrapperGpsFail;
        StretchImageView imageFailInfo;
        StretchImageView imageGpsFailBird;
        StretchImageView imageGpsFailRefresh;
        StretchImageView imageGpsFailSearch;
        View viewGpsFailBirdPosition;

        //gps설정확인
        RelativeLayout wrapperGpsAlert;
        TextView textAlertCancel;
        TextView textAlertConfirm;

        //gps반경설정
        RelativeLayout wrapperGpsDis;
        TextView gpsDisConfirm;
        CircleImageView gpsDisImage1;
        CircleImageView gpsDisImage2;
        CircleImageView gpsDisImage3;
        CircleImageView gpsDisImageSelector;


        public ViewMapper() {
            //로딩뷰
            lodingView = (RelativeLayout) findViewById(R.id.main_lodingView);

            relDrawer = (RelativeLayout) findViewById(R.id.main_linearlayout_left_drawer);

            //drawerWigets
            btnDrawerUserInfo = (RelativeLayout) relDrawer.findViewById(R.id.drawer_user_container1);
            btnDrawerSignIn = (LinearLayout) relDrawer.findViewById(R.id.drawer_user_container2);
            btnDrawerNear = (LinearLayout) relDrawer.findViewById(R.id.drawer_list_item1);
            btnDrawerNearDis = (TextView) relDrawer.findViewById(R.id.drawer_list_item1_dis);
            btnDrawerCoup = (LinearLayout) relDrawer.findViewById(R.id.drawer_list_item2);
            btnDrawerSetting = (LinearLayout) relDrawer.findViewById(R.id.drawer_list_item3);
            btnDrawerNotice = (LinearLayout) relDrawer.findViewById(R.id.drawer_list_item4);
            btnDrawerShare = (LinearLayout) relDrawer.findViewById(R.id.drawer_list_item5);
            btnDrawerAsk = (LinearLayout) relDrawer.findViewById(R.id.drawer_list_item6);
            btnLocation = (LinearLayout) relDrawer.findViewById(R.id.drawer_list_item7);
            textLocation = (TextView) relDrawer.findViewById(R.id.drawer_text_location);
            textUserNickname = (TextView) relDrawer.findViewById(R.id.drawer_user_text_nick);
            textUserPoint = (TextView) relDrawer.findViewById(R.id.drawer_user_text_point);
            textUserAddress = (TextView) relDrawer.findViewById(R.id.drawer_user_text_address);
            //메인위젯
            frameLayoutMenu = (FrameLayout) findViewById(R.id.activity_main_relativelayout_menu);
            textViewLike = (TextView) findViewById(R.id.activity_main_textview_like_store);
            imageViewAd = (ImageView) findViewById(R.id.activity_main_imageview_advertisement);

            subCateScroll = (ScrollView) findViewById(R.id.main_scroll_sub_cate);
            subCateWrapper = (LinearLayout) findViewById(R.id.main_wrapper_sub_cate);

            //gps탐색시
            wrapperSearchGps = (RelativeLayout) findViewById(R.id.main_wrapper_search_gps);
            birdSearchGps = (StretchImageView) findViewById(R.id.main_bird_search_gps);

            //gps실패
            wrapperGpsFail = (RelativeLayout) findViewById(R.id.main_wrapper_gps_fail);
            imageFailInfo = (StretchImageView) findViewById(R.id.main_image_fail_info);
            imageGpsFailBird = (StretchImageView) findViewById(R.id.main_gps_fail_bird);
            imageGpsFailRefresh = (StretchImageView) findViewById(R.id.main_gps_fail_refresh);
            imageGpsFailSearch = (StretchImageView) findViewById(R.id.main_gps_fail_search);
            viewGpsFailBirdPosition = findViewById(R.id.main_gps_fail_position_bird);

            //gps설정창
            wrapperGpsAlert = (RelativeLayout) findViewById(R.id.main_wrapper_alert);
            textAlertConfirm = (TextView) findViewById(R.id.main_text_alert_confirm);
            textAlertCancel = (TextView) findViewById(R.id.main_text_alert_cancel);

            //gps반경설정
            wrapperGpsDis = (RelativeLayout) findViewById(R.id.main_wrapper_gps_dis);
            gpsDisConfirm = (TextView) findViewById(R.id.main_gps_dis_confirm);
            gpsDisImage1 = (CircleImageView) findViewById(R.id.main_gps_dis_image1);
            gpsDisImage2 = (CircleImageView) findViewById(R.id.main_gps_dis_image2);
            gpsDisImage3 = (CircleImageView) findViewById(R.id.main_gps_dis_image3);
            gpsDisImageSelector = (CircleImageView) findViewById(R.id.main_gps_dis_image_selector);

            //setListener
            lodingView.setOnClickListener(MainActivity.this);
            relDrawer.setOnClickListener(MainActivity.this);
            btnDrawerUserInfo.setOnClickListener(MainActivity.this);
            btnDrawerSignIn.setOnClickListener(MainActivity.this);
            btnDrawerNear.setOnClickListener(MainActivity.this);
            btnDrawerNearDis.setOnClickListener(MainActivity.this);
            btnDrawerCoup.setOnClickListener(MainActivity.this);
            btnDrawerSetting.setOnClickListener(MainActivity.this);
            btnDrawerNotice.setOnClickListener(MainActivity.this);
            btnDrawerShare.setOnClickListener(MainActivity.this);
            btnDrawerAsk.setOnClickListener(MainActivity.this);
            btnLocation.setOnClickListener(MainActivity.this);
            wrapperGpsFail.setOnClickListener(MainActivity.this);
            imageGpsFailBird.setOnTouchListener(imageTouchListener);

            //메인
            frameLayoutMenu.setOnClickListener(MainActivity.this);
            textViewLike.setOnClickListener(MainActivity.this);
            imageViewAd.setOnClickListener(MainActivity.this);

            //gps탐색시
            wrapperSearchGps.setOnClickListener(MainActivity.this);

            //gps설정창
            wrapperGpsAlert.setOnClickListener(MainActivity.this);
            textAlertConfirm.setOnClickListener(MainActivity.this);
            textAlertCancel.setOnClickListener(MainActivity.this);

            //gps반경설정
            wrapperGpsDis.setOnClickListener(MainActivity.this);
            gpsDisConfirm.setOnClickListener(MainActivity.this);
            gpsDisImage1.setOnClickListener(MainActivity.this);
            gpsDisImage2.setOnClickListener(MainActivity.this);
            gpsDisImage3.setOnClickListener(MainActivity.this);
            gpsDisImageSelector.setOnTouchListener(gpsDisListener);
        }
    }

//================================================================================
// XXX Interface
//================================================================================

//================================================================================
// XXX Debug
//================================================================================

}
