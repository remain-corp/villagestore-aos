package com.remain.villagestore.pages.p2_main.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.remain.villagestore.R;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.dto.Store;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.manager.DisplayManager;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.RecycleUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by woongjaelee on 2015. 11. 6..
 */
public class StoreListAdapter extends BaseAdapter {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Store> arrayListStore;

    private List<WeakReference<View>> mRecycleList = new ArrayList<WeakReference<View>>();
    // Util
    private DisplayManager displayManager;
    //private DisplayImageOptions displayImageOptions;
    private RequestManager glideRequestManager;

    //================================================================================
    // XXX Constructor
    //================================================================================

    public StoreListAdapter(Context context, ArrayList<Store> arrayListStore, RequestManager glideRequestManager) {

        this.context = context;
        displayManager = new DisplayManager(context);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        displayManager = new DisplayManager(context);
        //displayImageOptions = ImageLoaderManager.getInstance(context).getDisplayImageOptionCache(R.drawable.image_default_photo);
        this.glideRequestManager = glideRequestManager;
        if (arrayListStore != null) {
            this.arrayListStore = new ArrayList<Store>(arrayListStore);
        }else {
            this.arrayListStore = new ArrayList<Store>();
        }
    }

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int getCount() {
        return arrayListStore.size();
    }

    @Override
    public Store getItem(int position) {
        return arrayListStore.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.content_store_list, null);
            holder = new ViewHolder();
            init(convertView, holder, position);
            convertView.setTag(holder);
            mRecycleList.add(new WeakReference<View>(convertView));

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        setView(holder, position);

        return convertView;
    }

    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================


    private void init(View convertView, ViewHolder holder, int position) {

        initUI(convertView, holder);
        initUISize(convertView, holder);

    }

    private void initUI(View convertView, ViewHolder holder) {

        holder.linearLayout = (LinearLayout) convertView.findViewById(R.id.content_store_linearlayout_background_panel);
        holder.imageView = (ImageView) convertView.findViewById(R.id.content_store_imageview_store);
        holder.textViewTitle = (TextView) convertView.findViewById(R.id.content_store_textview_title);
        holder.textViewTime = (TextView) convertView.findViewById(R.id.content_store_textview_time);
        holder.textViewContent = (TextView) convertView.findViewById(R.id.content_store_textview_content);

    }

    private void initUISize(View convertView, ViewHolder holder) {

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) holder.imageView.getLayoutParams();
        displayManager.changeWidthHeightSameRate(linearParams, 230, 140);
        linearParams.setMargins(displayManager.getScaleSizeSameRate(20), displayManager.getScaleSizeSameRate(20), displayManager.getScaleSizeSameRate(20), displayManager.getScaleSizeSameRate(20));

        linearParams = (LinearLayout.LayoutParams) convertView.findViewById(R.id.content_store_linearlayout_main).getLayoutParams();
        linearParams.setMargins(0, displayManager.getScaleSizeSameRate(30), displayManager.getScaleSizeSameRate(30), displayManager.getScaleSizeSameRate(30));

        linearParams = (LinearLayout.LayoutParams) convertView.findViewById(R.id.content_store_imageview_detail).getLayoutParams();
        displayManager.changeWidthHeightSameRate(linearParams, 15, 53);
        linearParams.rightMargin = displayManager.getScaleSizeSameRate(20);

    }

    private void setView(final ViewHolder holder, int position) {
        Store store = getItem(position);
        holder.textViewTitle.setText(store.getName());
        holder.textViewTime.setText(store.getBusinessHour());
        holder.textViewContent.setText(store.getAnnounce());

//        // 쿠폰 있을때 없을떄
//        if(store.isCouponExist()) {
//
//
//        } else {
//
//            holder.textViewContent.setText(context.getResources().getString(R.string.store_coupon_none_message));
//        }
        int imageWidth = Utils.DPToPX(context,120);
        int imageHeight = Utils.DPToPX(context,90);
        if (store.getArrayListImgUri() != null && store.getArrayListImgUri().size() > 0) {
            glideRequestManager
                    .load("http://" + AppInfo.IMG_THUM_PATH + store.getArrayListImgUri().get(0))
                    .error(R.drawable.image_default_photo)
                    .thumbnail(0.1f)
                    .override(imageWidth, imageHeight)
                    .into(holder.imageView);

            //ImageLoader.getInstance().displayImage(ImageLoaderManager.URIS_WEB + AppInfo.IMG_THUM_PATH + store.getArrayListImgUri().get(0) ,holder.imageView,displayImageOptions);
        } else {
            holder.imageView.setImageResource(R.drawable.image_default_photo);
            //ImageLoader.getInstance().displayImage(ImageLoaderManager.URIS_DRAWABLES + R.drawable.image_default_photo ,holder.imageView,displayImageOptions);
        }

        if (position % 2 == 0)
            holder.linearLayout.setBackgroundResource(R.drawable.main_list_white_panel);
        else
            holder.linearLayout.setBackgroundResource(R.drawable.main_list_gray_panel);
    }

    public void replace(ArrayList<Store> arrayListReplace) {

        ArrayList<Store> arrayListNew = new ArrayList<Store>(arrayListReplace);
        arrayListStore.clear();
        arrayListStore.addAll(arrayListNew);

        notifyDataSetChanged();
    }

    public void replaceOnlyList(ArrayList<Store> arrayListReplace) {

        ArrayList<Store> arrayListNew = new ArrayList<Store>(arrayListReplace);
        arrayListStore.clear();
        arrayListStore.addAll(arrayListNew);

    }


    public void recycle() {

        RecycleUtil.recursiveRecycle(mRecycleList);
    }

    //================================================================================
    // XXX Inner Class
    //================================================================================

    public class ViewHolder {
        private LinearLayout linearLayout;
        private ImageView imageView;
        private TextView textViewTitle;
        private TextView textViewTime;
        private TextView textViewContent;
    }
    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================

}
