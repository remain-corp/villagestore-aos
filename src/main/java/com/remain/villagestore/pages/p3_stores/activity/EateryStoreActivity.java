package com.remain.villagestore.pages.p3_stores.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.pages.commonActivity.WebviewActivity;
import com.remain.villagestore.pages.p4_other.menu.activitiy.MenuActivity;
import com.remain.villagestore.component.DetailMenuView;
import com.remain.villagestore.dto.Eatery;
import com.remain.villagestore.dto.StoreMenu;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by woongjaelee on 2015. 12. 3..
 */
public class EateryStoreActivity extends StoreActivity {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Enum
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    // App

    // Content

    // Widget


    // View

    // Adapter

    // Array

    // Lang
    private boolean isGetMenu;
    private Eatery eatery;
    private boolean isLiked;
    private ArrayList<StoreMenu> arrayListMenu = new ArrayList<StoreMenu>();


    // Component
    private DetailMenuView detailMenuLike;

    // Util
    private ServerRequest server;

    // Interface

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == ParamInfo.REQUEST_UPDATE) {

            if(data != null) {

                isGetMenu = true;
                arrayListMenu = data.getParcelableArrayListExtra(ParamInfo.STORE_MENU_LIST);
                eatery.setArrayListMenu(arrayListMenu);
            } else {
                Log.e("data null");
            }

        } else {

            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void init() {
        super.init();

        context = this;
        store = eatery = getIntent().getParcelableExtra(ParamInfo.STORE);

        if(store == null) {
            Toast.makeText(context, "매장정보가 없습니다.", Toast.LENGTH_SHORT).show();
            finish();
        }

        me = User.getMyInfo(this);
        server = new ServerRequest(networkResultListener);

        if(me != null && me.getSeq() != AppInfo.DEFAULT_INT)
            requestReadLikeToServer(me.getSeq(), eatery.getSeq());
        else
            requestMenuListToServer(eatery.getSeq());

        setCouponVisible(false);
    }

    @Override
    protected void setDetailView() {
        super.setDetailView();
    }

    @Override
    protected void setMenuView() {

        String[] arrayListTitle = getResources().getStringArray(R.array.array_detail_menu_item);

        for(int i = 0 ; i < arrayListTitle.length; i ++) {

            DetailMenuView detailMenuView = new DetailMenuView(context,i,arrayListTitle[i],R.array.array_detail_menu_res_item);
            detailMenuView.setOnStoreMenuItemClickListener(storeMenuItemClickListener);
            linearLayoutMenu.addView(detailMenuView);
            if(DetailMenuView.DetailMenu.LIKE.getMenuCode() == i)
                detailMenuLike = detailMenuView;
        }
    }

    @Override
    protected void couponClick() {

    }

    //================================================================================
    // XXX Listener
    //================================================================================

    private DetailMenuView.OnStoreMenuItemClickListener storeMenuItemClickListener = new DetailMenuView.OnStoreMenuItemClickListener() {
        @Override
        public void onMenuItemClick(DetailMenuView.DetailMenu detailMenu) {

            switch (detailMenu){

                case SHOW_MENU:

                    intent = new Intent(context, MenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra(ParamInfo.ST_NAME, eatery.getName());
                    intent.putExtra(ParamInfo.STORE_SEQ,eatery.getSeq());
                    intent.putExtra(ParamInfo.STORE, eatery);
                    intent.putExtra("isEnableDelivery", false);
                    // 메뉴를 성공적으로 받았을떄
                    if(isGetMenu) {

                        intent.putExtra(ParamInfo.STORE_MENU_LIST, eatery.getArrayListMenu());
                        startActivity(intent);
                    }
                    // 메뉴 받기를 실패했을때
                    else {

                        startActivityForResult(intent,ParamInfo.REQUEST_UPDATE);

                    }


                    break;
                case LIKE:

                    if(me != null && me.getSeq() != AppInfo.DEFAULT_INT) {

                        // 좋아요 상태였으면 취소
                        if(isLiked) {
                            requestUpdateLikeToServer(me.getSeq(),eatery.getSeq(),ParamInfo.N);
                        }
                        else {
                            requestUpdateLikeToServer(me.getSeq(),eatery.getSeq(),ParamInfo.Y);
                        }
                    }
                    else {

                        Toast.makeText(context,getResources().getString(R.string.fail_unlogin_message),Toast.LENGTH_SHORT).show();

                    }
                    break;
                case REPORT:

                    /*intent = new Intent (Intent.ACTION_VIEW, Uri.parse(NetInfo.GOOGLE_SUPPLY_URL));
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);*/
                    intent = new Intent(context, WebviewActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra(ParamInfo.SITE_ADDRESS, NetInfo.GOOGLE_SUPPLY_URL);
                    startActivity(intent);
                    break;
                case CALL:

                    requestCallCntToServer(eatery.getSeq());

                    intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + eatery.getPhoneNumber()));
                    startActivity(intent);
                    break;
                case LOCATION:

                    intent = new Intent(context,StoreMapActivity.class);
                    intent.putExtra(ParamInfo.SINGLE_MAP_MODE,true);
                    intent.putExtra(ParamInfo.STORE,eatery);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
            }
        }
    };

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {

            if (error != null) {

                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if(url == NetInfo.READ_LIKE) {

                if(!object.isNull(ParamInfo.RESULT)) {

                    try {

                        String result = object.getString(ParamInfo.RESULT);
                        if(result != null) {

                            if(result.equals(ParamInfo.Y)) {
                                isLiked = true;
                            } else {

                                isLiked = false;
                            }
                            uiHandler.sendEmptyMessage(NetInfo.WHAT_READ_LIKE);

                        } else {

                            uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {

                    uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                }


            }

            else if(url == NetInfo.UPDATE_LIKE) {


                uiHandler.sendEmptyMessage(NetInfo.WHAT_UPDATE_LIKE);

            }

            else if(url == NetInfo.READ_MENU) {

                if(!object.isNull(ParamInfo.MENU_LIST)) {

                    try {

                        arrayListMenu.clear();
                        JSONArray array = object.getJSONArray(ParamInfo.MENU_LIST);
                        for(int i = 0 ; i < array.length(); i++) {
                            arrayListMenu.add(new StoreMenu(array.getJSONObject(i)));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                uiHandler.sendEmptyMessage(NetInfo.WHAT_READ_MENU);

            }

        }
    };


    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            loadingview.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:

                    Toast.makeText(context, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    break;
                case NetInfo.WHAT_READ_LIKE:

                    if(isLiked) {

                        detailMenuLike.setSelect(true);
                    } else {

                        detailMenuLike.setSelect(false);
                    }

                    requestMenuListToServer(eatery.getSeq());

                    break;
                case NetInfo.WHAT_UPDATE_LIKE:

                    if(isLiked) {
                        eatery.setLikeCount(eatery.getLikeCount() - 1);
                        textViewLikeCount.setText(eatery.getLikeCount() + "");
                    }
                    else {
                        eatery.setLikeCount(eatery.getLikeCount() + 1);
                        textViewLikeCount.setText(eatery.getLikeCount() + "");
                    }
                    isLiked = !isLiked;

                    detailMenuLike.setSelect(isLiked);
                    break;
                case NetInfo.WHAT_READ_MENU:

                    isGetMenu = true;
                    eatery.setArrayListMenu(arrayListMenu);

                    break;
            }
        }
    };

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================

    private void requestReadLikeToServer(int userSeq,int storeSeq) {

        loadingview.setVisibility(View.VISIBLE);

        LinkedHashMap<String,Object> params = new LinkedHashMap<>();

        params.put(ParamInfo.HASHKEY,NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_SEQ,userSeq);
        params.put(ParamInfo.STORE_SEQ,storeSeq);

        server.execute(NetInfo.READ_LIKE,params);

    }

    private void requestUpdateLikeToServer(int userSeq,int storeSeq,String likeYN) {

        loadingview.setVisibility(View.VISIBLE);

        LinkedHashMap<String,Object> params = new LinkedHashMap<>();

        params.put(ParamInfo.HASHKEY,NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_SEQ,userSeq);
        params.put(ParamInfo.STORE_SEQ,storeSeq);
        params.put(ParamInfo.OK,likeYN);

        server.execute(NetInfo.UPDATE_LIKE,params);

    }

    private void requestMenuListToServer(int storeSeq) {

        loadingview.setVisibility(View.VISIBLE);

        LinkedHashMap<String,Object> params = new LinkedHashMap<>();

        params.put(ParamInfo.HASHKEY,NetInfo.HASH_KEY);
        params.put(ParamInfo.ST_SEQ,storeSeq);

        server.execute(NetInfo.READ_MENU,params);

    }

    private void requestCallCntToServer(int storeSeq) {

        LinkedHashMap<String,Object> params = new LinkedHashMap<>();

        params.put(ParamInfo.HASHKEY,NetInfo.HASH_KEY);
        params.put(ParamInfo.ST_SEQ,storeSeq);

        server.execute(NetInfo.CALL_CNT,params);

    }

    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================

}
