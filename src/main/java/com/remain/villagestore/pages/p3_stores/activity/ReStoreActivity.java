package com.remain.villagestore.pages.p3_stores.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.component.LinkEnabledTextView;
import com.remain.villagestore.dto.Store;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.pages.p3_stores.adapter.StoreImagePagerAdapter;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.RecycleUtil;

import org.w3c.dom.Text;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by woongjaelee on 2016. 10. 30..
 */
public class ReStoreActivity extends Activity {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Enum
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    // App

    // Content
    protected Context context;
    protected Intent intent;

    // Widget
    protected FrameLayout btnArrowBack;
    protected FrameLayout btnLocation;
    protected TextView textViewTitle;
    protected ViewPager viewPager;
    protected TextView textViewImgCnt;
    protected TextView textViewLikeCnt;
    protected LinkEnabledTextView textViewContent;
    protected TextView textViewTime;
    protected TextView textViewPhone;
    protected TextView textViewAddress;
    protected TextView textViewCoupon;

    // View

    // Adapter
    private StoreImagePagerAdapter storeImagePagerAdapter;

    // Array

    // Lang
    protected Store store;

    // Component

    // Util

    // Interface

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_re_store);

        init();
    }

    @Override
    protected void onDestroy() {

        RecycleUtil.recursiveRecycle(viewPager);

        super.onDestroy();


    }

    //================================================================================
    // XXX Listener
    //================================================================================

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 뒤로가기
                case R.id.activity_restore_arrow_back:
                    finish();
                    break;
                // 지도보기
                case R.id.activity_restore_loc_icon:

                    intent = new Intent(context,StoreMapActivity.class);
                    intent.putExtra(ParamInfo.SINGLE_MAP_MODE,true);
                    intent.putExtra(ParamInfo.STORE,store);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);

                    break;
                // 쿠폰
                case R.id.activity_store_textview_coupon:
                    break;
            }
        }
    };

    private StoreImagePagerAdapter.OnPageClickListener pageClickListener = new StoreImagePagerAdapter.OnPageClickListener() {
        @Override
        public void onPageClick(int position) {

            intent = new Intent(context, StoreImgDetailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(ParamInfo.FILENAME, store.getArrayListImgUri());
            intent.putExtra(ParamInfo.SELECTED_POS, position);
            startActivity(intent);
        }
    };

    private LinkEnabledTextView.TextLinkClickListener textLinkClickListener = new LinkEnabledTextView.TextLinkClickListener() {
        @Override
        public void onTextLinkClick(View view, String clickedString) {

            Log.e("Click url  = " + clickedString);

            String inputUrl = clickedString;
            if (!inputUrl.contains("http://") && !inputUrl.contains("https://"))
                inputUrl = "http://" + inputUrl;

            URL url;
            try {
                url = new URL(inputUrl);
//
            } catch (MalformedURLException e) {
                Toast.makeText(context, "올바른 형식의 주소가 아닙니다.", Toast.LENGTH_LONG).show();
                return;
            }

            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(inputUrl));
            startActivity(intent);

        }
    };

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================

    private void init() {

        context = this;

        store = getIntent().getParcelableExtra(ParamInfo.STORE);

        if (store == null) {
            Toast.makeText(context, "매장정보가 없습니다.", Toast.LENGTH_SHORT).show();
            finish();
        }

        initUI();
        initUISize();
        setEventListener();
        setAdapter();
        setDetailView();
    }

    private void initUI() {

        btnArrowBack = (FrameLayout)findViewById(R.id.activity_restore_arrow_back);
        btnLocation = (FrameLayout)findViewById(R.id.activity_restore_loc_icon);
        textViewTitle = (TextView)findViewById(R.id.activity_store_textview_imgcnt);
        viewPager = (ViewPager)findViewById(R.id.activity_restore_viewpager);
        textViewImgCnt = (TextView)findViewById(R.id.activity_store_textview_imgcnt);
        textViewLikeCnt = (TextView)findViewById(R.id.activity_store_textview_likecnt);
        textViewContent = (LinkEnabledTextView)findViewById(R.id.activity_store_textview_content_detail);
        textViewTime = (TextView)findViewById(R.id.activity_store_textview_content_time);
        textViewPhone = (TextView)findViewById(R.id.activity_store_textview_content_phone);
        textViewAddress = (TextView)findViewById(R.id.activity_store_textview_content_address);
        textViewCoupon = (TextView)findViewById(R.id.activity_store_textview_coupon);

    }

    private void initUISize() {

    }

    private void setEventListener() {

        btnArrowBack.setOnClickListener(clickListener);
        btnLocation.setOnClickListener(clickListener);
        textViewCoupon.setOnClickListener(clickListener);
        textViewContent.setOnTextLinkClickListener(textLinkClickListener);

    }

    private void setAdapter() {

        storeImagePagerAdapter = new StoreImagePagerAdapter(context,store.getArrayListImgUri(),false);
        storeImagePagerAdapter.setOnPageClickListener(pageClickListener);
        viewPager.setAdapter(storeImagePagerAdapter);
    }

    /**
     * URl 로 되어있는 문자열이 있는지 체크하여 셋팅
     */
    private void setUrlText() {

        textViewContent.setLinkForText(store.getAnnounce());

        MovementMethod moveMentMethod = textViewContent.getMovementMethod();
        if ((moveMentMethod == null) || !(moveMentMethod instanceof LinkMovementMethod)) {
            if (textViewContent.getLinksClickable()) {
                textViewContent.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }

    private void setDetailView() {

        textViewTitle.setText(store.getName());
        textViewImgCnt.setText((store.getArrayListImgUri() != null)? store.getArrayListImgUri().size()+"" : "0");
        textViewLikeCnt.setText(store.getLikeCount()+"");
        setUrlText();
        textViewTime.setText(store.getBusinessHour());
        textViewPhone.setText(store.getPhoneNumber());
        textViewAddress.setText(store.getAddress());
    }


    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================

}
