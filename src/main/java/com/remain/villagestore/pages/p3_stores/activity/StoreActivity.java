package com.remain.villagestore.pages.p3_stores.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.CircleIndicator;
import com.remain.villagestore.component.LinkEnabledTextView;
import com.remain.villagestore.component.StoreDetailView;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.dto.Store;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.DisplayManager;
import com.remain.villagestore.pages.commonActivity.WebviewActivity;
import com.remain.villagestore.pages.p3_stores.adapter.StoreImagePagerAdapter;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ImageUtil;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.RecycleUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by woongjaelee on 2015. 11. 9..
 */
public abstract class StoreActivity extends Activity {

    //================================================================================
    // XXX Constants
    //================================================================================
//396
    private static final int VIEWPAGER_HEIGHT = 460;

    //================================================================================
    // XXX Variables
    //================================================================================

    // Content
    protected Context context;
    protected Intent intent;

    protected RelativeLayout loadingview;

    // Widget
    protected FrameLayout relativeLayoutBack;
    protected FrameLayout relativeLayoutShare;
    protected TextView textViewTitle;
    protected ViewPager viewPager;
    protected CircleIndicator circleIndicator;
    protected TextView textViewLikeCount;
    protected LinearLayout linearLayoutContent;
    protected LinearLayout linearLayoutMenu;
    //protected LinearLayout linearLayoutCoupon;
    protected StretchImageView btnCoupon;
    protected LinkEnabledTextView linkEnabledTextView;
    protected StretchImageView imageButtonPlus;
    protected TextView btnGoHomePage;

    // Lang
    protected Store store;
    protected User me;

    // Adapter
    protected StoreImagePagerAdapter storeImagePagerAdapter;

    // Util
    protected DisplayManager displayManager;

    protected boolean isOpenButton = false;

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_store);

        init();
    }

    @Override
    protected void onDestroy() {

        if (storeImagePagerAdapter != null)
            storeImagePagerAdapter.recycle();

        RecycleUtil.recursiveRecycle(viewPager);

        super.onDestroy();

    }

    //================================================================================
    // XXX Listener
    //================================================================================

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == loadingview){}
            if (v == btnCoupon) {
                //쿠폰
                if (me != null && me.getSeq() != AppInfo.DEFAULT_INT)
                    couponClick();
                else
                    Toast.makeText(context, getResources().getString(R.string.fail_unlogin_message), Toast.LENGTH_SHORT).show();
            }else if ( v == imageButtonPlus){

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnOpen(isOpenButton);
                        isOpenButton = !isOpenButton;
                    }
                });

            } else if ( v == btnGoHomePage){
                String url = store.getUrl().trim();
                Log.e("디비유알엘 = "+url);
                if (!url.contains("http://") && !url.contains("https://")){
                    url = "http://"+url;
                }
                Log.e("변경 = "+url);
                intent = new Intent(context, WebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(ParamInfo.SITE_ADDRESS, url);
                startActivity(intent);
            }
            switch (v.getId()) {
                // 뒤로가기
                case R.id.activity_detail_relativelayout_back:

                    finish();
                    break;
                // 공유하기
                case R.id.activity_detail_relativelayout_share:

                    screenShot();
                    break;
            }
        }
    };

    private StoreImagePagerAdapter.OnPageClickListener pageClickListener = new StoreImagePagerAdapter.OnPageClickListener() {
        @Override
        public void onPageClick(int position) {

            intent = new Intent(context, StoreImgDetailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(ParamInfo.FILENAME, store.getArrayListImgUri());
            intent.putExtra(ParamInfo.SELECTED_POS, position);
            startActivity(intent);
        }
    };

    private LinkEnabledTextView.TextLinkClickListener textLinkClickListener = new LinkEnabledTextView.TextLinkClickListener() {
        @Override
        public void onTextLinkClick(View view, String clickedString) {

            Log.e("Click url  = " + clickedString);

            String inputUrl = clickedString;
            if (!inputUrl.contains("http://") && !inputUrl.contains("https://"))
                inputUrl = "http://" + inputUrl;

            URL url;
            try {
                url = new URL(inputUrl);
//
            } catch (MalformedURLException e) {
                Toast.makeText(context, "올바른 형식의 주소가 아닙니다.", Toast.LENGTH_LONG).show();
                return;
            }

            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(inputUrl));
            startActivity(intent);

        }
    };

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================


    protected void init() {

        context = this;
        displayManager = new DisplayManager(context);

        if (store == null)
            store = getIntent().getParcelableExtra(ParamInfo.STORE);

        if (store == null) {
            Toast.makeText(context, "매장정보가 없습니다.", Toast.LENGTH_SHORT).show();
            finish();
        }


        initUI();
        initUISize();
        setEventListener();
        setAdapter();
        setDetailView();
        setMenuView();

        //홈페이지가 있을경우
        if(store.getUrl() != null && !store.getUrl().equals("")){
            imageButtonPlus.setVisibility(View.VISIBLE);
            //버튼 숨기기
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    btnGoHomePage.animate().translationX(btnGoHomePage.getWidth() + Utils.DPToPX(StoreActivity.this, 14)).setDuration(150).start();
                    //btnGoHomePage.animate().translationX(0).setDuration(150).start();
                    //btnGoHomePage.setVisibility(View.VISIBLE);
                }
            },100);
        }

    }

    private void initUI() {

        loadingview = (RelativeLayout) findViewById(R.id.store_loadingview);
        relativeLayoutBack = (FrameLayout) findViewById(R.id.activity_detail_relativelayout_back);
        relativeLayoutShare = (FrameLayout) findViewById(R.id.activity_detail_relativelayout_share);
        textViewTitle = (TextView) findViewById(R.id.activity_detail_textview_title);
        viewPager = (ViewPager) findViewById(R.id.activity_store_viewpager);
        circleIndicator = (CircleIndicator) findViewById(R.id.activity_store_circleindicator);
        textViewLikeCount = (TextView) findViewById(R.id.activity_store_textview_like_count);
        linearLayoutContent = (LinearLayout) findViewById(R.id.activity_store_linearlayout_content);
        linearLayoutMenu = (LinearLayout) findViewById(R.id.activity_store_linearlayout_menu);
        //linearLayoutCoupon = (LinearLayout) findViewById(R.id.activity_store_linearlayout_coupon);
        btnCoupon = (StretchImageView) findViewById(R.id.activity_store_image_coupon);
        linkEnabledTextView = (LinkEnabledTextView) findViewById(R.id.activity_store_detail_linktextview);
        imageButtonPlus = (StretchImageView) findViewById(R.id.activity_store_plus);
        btnGoHomePage = (TextView) findViewById(R.id.activity_store_go_site);

        textViewTitle.setText(store.getName());

    }

    private void initUISize() {

        displayManager.changeWidthHeightSameRate(findViewById(R.id.activity_detail_relativelayout_top_panel).getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 96);

        FrameLayout.LayoutParams frameParams = (FrameLayout.LayoutParams) findViewById(R.id.activity_detail_imageview_back).getLayoutParams();
        displayManager.changeWidthHeightSameRate(frameParams, 57, 48);
        frameParams.leftMargin = displayManager.getScaleSizeSameRate(36);
        frameParams.rightMargin = displayManager.getScaleSizeSameRate(36);

        frameParams = (FrameLayout.LayoutParams) findViewById(R.id.activity_detail_imageview_share).getLayoutParams();
        displayManager.changeWidthHeightSameRate(frameParams, 57, 57);
        frameParams.leftMargin = displayManager.getScaleSizeSameRate(36);
        frameParams.rightMargin = displayManager.getScaleSizeSameRate(36);

        frameParams = (FrameLayout.LayoutParams) circleIndicator.getLayoutParams();
        displayManager.changeWidthHeightSameRate(frameParams, ViewGroup.LayoutParams.MATCH_PARENT, 60);
        frameParams.rightMargin = displayManager.getScaleSizeSameRate(24);

        displayManager.changeWidthHeightSameRate(viewPager.getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, VIEWPAGER_HEIGHT);

        displayManager.changeWidthHeightSameRate(findViewById(R.id.activity_store_linearlayout_like).getLayoutParams(), ViewGroup.LayoutParams.WRAP_CONTENT, 60);

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) textViewLikeCount.getLayoutParams();
        linearParams.leftMargin = displayManager.getScaleSizeSameRate(10);
        linearParams.rightMargin = displayManager.getScaleSizeSameRate(10);

        linearParams = (LinearLayout.LayoutParams) findViewById(R.id.activity_store_imageview_like).getLayoutParams();
        linearParams.rightMargin = displayManager.getScaleSizeSameRate(10);
        displayManager.changeWidthHeightSameRate(linearParams, 33, 35);

        textViewTitle.setPadding(displayManager.getScaleSizeSameRate(10), displayManager.getScaleSizeSameRate(10), displayManager.getScaleSizeSameRate(10), displayManager.getScaleSizeSameRate(10));

        linkEnabledTextView.setPadding(displayManager.getScaleSizeSameRate(10), displayManager.getScaleSizeSameRate(10), displayManager.getScaleSizeSameRate(10), displayManager.getScaleSizeSameRate(10));

        linearLayoutMenu.setPadding(displayManager.getScaleSizeSameRate(12), displayManager.getScaleSizeSameRate(12), displayManager.getScaleSizeSameRate(12), displayManager.getScaleSizeSameRate(12));

        /*linearParams = (LinearLayout.LayoutParams) linearLayoutCoupon.getLayoutParams();
        displayManager.changeWidthHeightSameRate(linearParams, ViewGroup.LayoutParams.MATCH_PARENT, 86);

        linearParams.setMargins(displayManager.getScaleSizeSameRate(12), displayManager.getScaleSizeSameRate(8), displayManager.getScaleSizeSameRate(12), displayManager.getScaleSizeSameRate(8));
        linearParams = (LinearLayout.LayoutParams) findViewById(R.id.activity_store_image_coupon).getLayoutParams();
        displayManager.changeWidthHeightSameRate(linearParams,108,63);

        linearParams.leftMargin = displayManager.getScaleSizeSameRate(40);
        linearParams.rightMargin = displayManager.getScaleSizeSameRate(90);*/
    }

    private void setEventListener() {
        loadingview.setOnClickListener(clickListener);
        relativeLayoutBack.setOnClickListener(clickListener);
        relativeLayoutShare.setOnClickListener(clickListener);
        //linearLayoutCoupon.setOnClickListener(clickListener);
        btnCoupon.setOnClickListener(clickListener);
        imageButtonPlus.setOnClickListener(clickListener);
        btnGoHomePage.setOnClickListener(clickListener);
        linkEnabledTextView.setOnTextLinkClickListener(textLinkClickListener);
    }

    private void setAdapter() {

        storeImagePagerAdapter = new StoreImagePagerAdapter(context, store.getArrayListImgUri(), false);
        storeImagePagerAdapter.setOnPageClickListener(pageClickListener);
        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(storeImagePagerAdapter);
        circleIndicator.setViewPager(viewPager);
    }

    protected void setDetailView() {


        textViewLikeCount.setText(store.getLikeCount() + "");
        setUrlText();
        // 매장에서 전달합니다
        //addDetailView(getResources().getString(R.string.store_detil_title1),store.getAnnounce(),4);

        // 영업시간
        addDetailView(getResources().getString(R.string.store_detil_title2), store.getBusinessHour(), 0);

        // 이벤트
        //addDetailView(getResources().getString(R.string.store_detil_title3), store.getEventDetail());

        // 전화번호
        addDetailView(getResources().getString(R.string.store_detil_title4), store.getPhoneNumber(), 0);

        // 주소
        addDetailView(getResources().getString(R.string.store_detil_title5), store.getAddress(), 0);

    }

    protected void addDetailView(String title, String content, int marginBottom) {

        StoreDetailView storeDetailView = new StoreDetailView(context);

        linearLayoutContent.addView(storeDetailView);
        storeDetailView.setView(title, content, marginBottom);
    }

    private void btnOpen(boolean isOpen) {
        if(btnGoHomePage.getVisibility() != View.VISIBLE){
            btnGoHomePage.setVisibility(View.VISIBLE);
        }
        Log.e("isOpen = "+ isOpen);
        if(isOpen){
            btnGoHomePage.animate().translationX(btnGoHomePage.getWidth() + Utils.DPToPX(this, 14)).setDuration(150).start();
            imageButtonPlus.animate().rotation(0f).setDuration(150).start();
            imageButtonPlus.clearColorFilter();
        }else{
            imageButtonPlus.animate().rotation(45f).setDuration(150).start();
            btnGoHomePage.animate().translationX(0).setDuration(150).start();
            imageButtonPlus.setColorFilter(Color.parseColor("#66000000"));
        }

    }

    /**
     * 현재 화면 스크린샷 이미지 얻어오기
     */
    private void screenShot() {
        try {

            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            FileOutputStream outputStream = new FileOutputStream(getScreenShotFile());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();

            sendScreenShot();

        } catch (FileNotFoundException e) {
            Log.d("FileNotFoundException:", e.getMessage());
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }

    private File getScreenShotFile() {
        File file = new File(ImageUtil.getInstance().getImageFolderPath(AppInfo.FILE_PATH) + "/screenshot" + ".jpeg");
        return file;

    }

    /**
     * 스크린샷을 보냄
     */
    private void sendScreenShot() {

        intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(getScreenShotFile()));
        intent.setType("image/*");
        startActivity(intent);
    }

    /**
     * URl 로 되어있는 문자열이 있는지 체크하여 셋팅
     */
    private void setUrlText() {

        linkEnabledTextView.setLinkForText(store.getAnnounce());

        MovementMethod moveMentMethod = linkEnabledTextView.getMovementMethod();
        if ((moveMentMethod == null) || !(moveMentMethod instanceof LinkMovementMethod)) {
            if (linkEnabledTextView.getLinksClickable()) {
                linkEnabledTextView.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }


    protected abstract void couponClick();

    protected abstract void setMenuView();

    public void setCouponVisible(boolean isCouponVisible) {

        if (isCouponVisible) {

            btnCoupon.setVisibility(View.VISIBLE);
        } else {

            btnCoupon.setVisibility(View.GONE);
        }
    }

    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================

}
