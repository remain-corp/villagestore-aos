package com.remain.villagestore.pages.p3_stores.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.remain.villagestore.R;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.pages.p3_stores.adapter.StoreImagePagerAdapter;
import com.remain.villagestore.pages.p3_stores.item.CusViewPager;
import com.remain.villagestore.util.RecycleUtil;

import java.util.ArrayList;


/**
 * Created by woongjaelee on 15. 9. 16..
 */
public class StoreImgDetailActivity extends Activity {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    private Context context;
    private CusViewPager viewPager;

    private StoreImagePagerAdapter storeImagePagerAdapter;
    private ArrayList<String> arrayListImg = new ArrayList<String>();

    private int position;

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_store_img_detail);

        init();

    }

    @Override
    protected void onDestroy() {

        if (storeImagePagerAdapter != null)
            storeImagePagerAdapter.recycle();

        RecycleUtil.recursiveRecycle(viewPager);
        super.onDestroy();
    }

    //================================================================================
    // XXX Listener
    //================================================================================

    private View.OnClickListener clikListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================


    private void init() {

//        imgUrl = getIntent().getStringExtra(ParamInfo.FILENAME);
        context = this;
        arrayListImg = getIntent().getStringArrayListExtra(ParamInfo.FILENAME);
        position = getIntent().getIntExtra(ParamInfo.SELECTED_POS, 0);

        initUI();
        initUISize();
        setEventListener();
        setAdapter();
    }

    private void initUI() {

//        imageView = (ImageView)findViewById(R.id.activity_store_img_detail_imageview);
        viewPager = (CusViewPager) findViewById(R.id.activity_store_detail_viewpager);
    }

    private void initUISize() {


    }

    private void setAdapter() {

        storeImagePagerAdapter = new StoreImagePagerAdapter(context, arrayListImg, true);
        viewPager.setAdapter(storeImagePagerAdapter);
        viewPager.setCurrentItem(position);

    }

    private void setEventListener() {

    }

//    private void setData() {
//
//        ImageLoader.getInstance().displayImage(ImageLoaderManager.URIS_WEB + imgUrl,imageView, options);
//    }

    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================
}
