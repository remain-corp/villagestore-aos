package com.remain.villagestore.pages.p3_stores.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.dto.Store;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.DisplayManager;
import com.remain.villagestore.util.Log;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

import java.util.ArrayList;

/**
 * Created by woongjaelee on 2015. 12. 6..
 */
public class StoreMapActivity extends Activity {

    //================================================================================
    // XXX Constants
    //================================================================================

    private final static String DAUM_API_KEY = "af4599f5f6d7e4cc0300a0b1a101da47";

    //================================================================================
    // XXX Enum
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    // App

    // Content
    private Context context;
    private Intent intent;

    // Widget
    private RelativeLayout toolbar;
    private StretchImageView toolbarBackButton;
    private TextView toolbarTitle;

    private LinearLayout linearLayoutMap;
    private LinearLayout linearLayoutBottomMain;
    private TextView textViewName;
    private TextView textViewAddress;
    private TextView textViewTel;
    private ImageView imageViewCall;
    private ImageView imageViewMap;
    private LinearLayout linearLayoutBottomIcon;
    private TextView textViewEmpty;

    private MapView mapView;

    // View

    // Adapter

    // Array

    // Lang
    private int selectedPos;
    private String titleText;
    private double latitude;
    private double longitude;
    private boolean isSingleMapMode;

    private ArrayList<Store> arrayListStore = new ArrayList<Store>();
    private Store store;

    // Component

    // Util
    private DisplayManager displayManager;

    // Interface

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_store_map);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mapView != null) {

            mapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOff);
            mapView.removeAllPolylines();
            mapView.removeAllPOIItems();
            mapView.setCurrentLocationEventListener(null);
            mapView.clearMapTilePersistentCache();
        }
        linearLayoutMap.removeAllViews();
        System.gc();
    }

    //================================================================================
    // XXX Listener
    //================================================================================

    private View.OnClickListener clickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            if(v == toolbarBackButton){
                finish();
            }

            switch (v.getId()) {
                case R.id.activity_store_map_imageview_call:
                    // 전화걸기

                    if(isSingleMapMode) {

                        if(store != null) {

                            intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + store.getPhoneNumber()));
                            startActivity(intent);
                        }

                    } else {

                        if(arrayListStore != null) {

                            Store currentStore = arrayListStore.get(selectedPos);
                            if(currentStore != null) {

                                intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + currentStore.getPhoneNumber()));
                                startActivity(intent);
                            }
                        }
                    }
                    break;
                case R.id.activity_store_map_imageview_map:
                    // 지도 앱 띄우기

                    startDaumMapApp();
                    break;
            }
        }
    };

    private MapView.MapViewEventListener mapViewEventListener = new MapView.MapViewEventListener() {
        @Override
        public void onMapViewInitialized(MapView mapView) {

            // 맵뷰 사용 준비 완료
            if(latitude != AppInfo.DEFAULT_INT) {

                // 내 위치로 중심잡기
                mapView.setMapCenterPoint(MapPoint.mapPointWithGeoCoord(latitude, longitude),true);
                mapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOnWithoutHeadingWithoutMapMoving);
            }
            else {
                // 매장으로 중심잡기
                if(store != null && mapView != null) {
                    mapView.setMapCenterPoint(MapPoint.mapPointWithGeoCoord(store.getLatitude(), store.getLongitude()),true);
                }
                mapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOnWithoutHeadingWithoutMapMoving);
            }

        }

        @Override
        public void onMapViewCenterPointMoved(MapView mapView, MapPoint mapPoint) {

        }

        @Override
        public void onMapViewZoomLevelChanged(MapView mapView, int i) {

        }

        @Override
        public void onMapViewSingleTapped(MapView mapView, MapPoint mapPoint) {

        }

        @Override
        public void onMapViewDoubleTapped(MapView mapView, MapPoint mapPoint) {

        }

        @Override
        public void onMapViewLongPressed(MapView mapView, MapPoint mapPoint) {

        }

        @Override
        public void onMapViewDragStarted(MapView mapView, MapPoint mapPoint) {

        }

        @Override
        public void onMapViewDragEnded(MapView mapView, MapPoint mapPoint) {

        }

        @Override
        public void onMapViewMoveFinished(MapView mapView, MapPoint mapPoint) {

        }
    };

    private MapView.POIItemEventListener poiItemEventListener = new MapView.POIItemEventListener() {
        @Override
        public void onPOIItemSelected(MapView mapView, MapPOIItem mapPOIItem) {

            selectedPos = mapPOIItem.getTag();

            setSelectedData(selectedPos);
        }

        @Override
        public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem) {

        }

        @Override
        public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem, MapPOIItem.CalloutBalloonButtonType calloutBalloonButtonType) {

        }

        @Override
        public void onDraggablePOIItemMoved(MapView mapView, MapPOIItem mapPOIItem, MapPoint mapPoint) {

        }
    };

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================

    private void init() {

        context = this;
        displayManager = new DisplayManager(context);
        isSingleMapMode = getIntent().getBooleanExtra(ParamInfo.SINGLE_MAP_MODE, false);
        if(isSingleMapMode) {
            store = getIntent().getParcelableExtra(ParamInfo.STORE);
        } else {

            arrayListStore = getIntent().getParcelableArrayListExtra(ParamInfo.STORE_LIST);
            selectedPos = getIntent().getIntExtra(ParamInfo.SELECTED_POS, 0);
            titleText = getIntent().getStringExtra(ParamInfo.MAP_TITLE_TEXT);
            latitude = getIntent().getDoubleExtra(ParamInfo.LAT, 0);
            longitude = getIntent().getDoubleExtra(ParamInfo.LNG,0);
            if(arrayListStore != null && arrayListStore.size() != 0)
                store = arrayListStore.get(selectedPos);
        }
        initUI();
        initUISize();
        setEventListener();
        setMapView();
        setMarker();

        // 현재 위치로 지도 띄워줄때
        if(latitude != AppInfo.DEFAULT_INT)
            setMyLocation();
            // 카테고리로 지도 띄워줄때
        else
            setSelectedData(selectedPos);

    }

    private void initUI() {

        toolbar = (RelativeLayout) findViewById(R.id.map_toolbar);
        toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        linearLayoutMap = (LinearLayout)findViewById(R.id.activity_store_map_linearlayout_map);
        linearLayoutBottomMain = (LinearLayout)findViewById(R.id.activity_store_map_linearlayout_bottom_main);
        linearLayoutBottomIcon = (LinearLayout)findViewById(R.id.activity_store_map_linearlayout_bottom_icon);
        textViewEmpty = (TextView)findViewById(R.id.activity_store_map_textview_empty);
        textViewName = (TextView)findViewById(R.id.activity_store_map_textview_name);
        textViewAddress = (TextView)findViewById(R.id.activity_store_map_textview_address);
        textViewTel = (TextView)findViewById(R.id.activity_store_map_textview_tel);
        imageViewCall = (ImageView)findViewById(R.id.activity_store_map_imageview_call);
        imageViewMap = (ImageView)findViewById(R.id.activity_store_map_imageview_map);


        if(titleText != null && titleText.equals(AppInfo.DEFAULT_STRING)) {
            toolbarTitle.setText(titleText);
        }
        else {
            toolbarTitle.setText(R.string.store_map_title);
        }

    }

    private void initUISize() {

        displayManager.changeWidthHeightSameRate(findViewById(R.id.activity_store_map_linearlayout_bottom).getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 219);
        linearLayoutBottomMain.setPadding(displayManager.getScaleSizeSameRate(35), displayManager.getScaleSizeSameRate(30), displayManager.getScaleSizeSameRate(35), 0);

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) imageViewCall.getLayoutParams();
        displayManager.changeWidthHeightSameRate(linearParams,71,71);
        linearParams.leftMargin = displayManager.getScaleSizeSameRate(21);
        linearParams.rightMargin = displayManager.getScaleSizeSameRate(43);

        linearParams = (LinearLayout.LayoutParams) imageViewMap.getLayoutParams();
        displayManager.changeWidthHeightSameRate(linearParams,70,70);
        linearParams.leftMargin = displayManager.getScaleSizeSameRate(21);
        linearParams.rightMargin = displayManager.getScaleSizeSameRate(43);

    }

    private void setEventListener() {

        toolbarBackButton.setOnClickListener(clickListener);
        imageViewCall.setOnClickListener(clickListener);
        imageViewMap.setOnClickListener(clickListener);

    }

    private void setMapView() throws UnsatisfiedLinkError {

        mapView = new MapView(this);
        mapView.setDaumMapApiKey(DAUM_API_KEY);
        mapView.setMapTilePersistentCacheEnabled(true);
        mapView.setMapType(MapView.MapType.Standard);
        mapView.setShowCurrentLocationMarker(true);
        mapView.setHDMapTileEnabled(true);
        mapView.setZoomLevel(1, false);
        mapView.fitMapViewAreaToShowAllPOIItems();
        mapView.setMapViewEventListener(mapViewEventListener);
        mapView.setPOIItemEventListener(poiItemEventListener);

        linearLayoutMap.addView(mapView);
    }

    private void setMarker() {

        if(isSingleMapMode)  {

            MapPOIItem customMarker = new MapPOIItem();
            customMarker.setItemName(store.getName());
            customMarker.setTag(0);
            customMarker.setMapPoint(MapPoint.mapPointWithGeoCoord(store.getLatitude(), store.getLongitude()));
            customMarker.setMarkerType(MapPOIItem.MarkerType.CustomImage); // 마커타입을 커스텀 마커로 지정.
            customMarker.setCustomImageResourceId(R.drawable.ico_custom_marker); // 마커 이미지.
            customMarker.setCustomImageAutoscale(false); // hdpi, xhdpi 등 안드로이드 플랫폼의 스케일을 사용할 경우 지도 라이브러리의 스케일 기능을 꺼줌.
            customMarker.setCustomImageAnchor(0.5f, 1.0f); // 마커 이미지중 기준이 되는 위치(앵커포인트) 지정 - 마커 이미지 좌측 상단 기준 x(0.0f ~ 1.0f), y(0.0f ~ 1.0f) 값.

            mapView.addPOIItem(customMarker);
        }
        else {

            if(arrayListStore == null || arrayListStore.size() == 0) return;

            for(int i = 0 ; i < arrayListStore.size(); i++) {

                Store currentStore = arrayListStore.get(i);
                MapPOIItem customMarker = new MapPOIItem();
                customMarker.setItemName(currentStore.getName());
                customMarker.setTag(i);
                customMarker.setMapPoint(MapPoint.mapPointWithGeoCoord(currentStore.getLatitude(),currentStore.getLongitude()));
                customMarker.setMarkerType(MapPOIItem.MarkerType.CustomImage); // 마커타입을 커스텀 마커로 지정.
                customMarker.setCustomImageResourceId(R.drawable.ico_custom_marker); // 마커 이미지.
                customMarker.setCustomImageAutoscale(false); // hdpi, xhdpi 등 안드로이드 플랫폼의 스케일을 사용할 경우 지도 라이브러리의 스케일 기능을 꺼줌.
                customMarker.setCustomImageAnchor(0.5f, 1.0f); // 마커 이미지중 기준이 되는 위치(앵커포인트) 지정 - 마커 이미지 좌측 상단 기준 x(0.0f ~ 1.0f), y(0.0f ~ 1.0f) 값.

                mapView.addPOIItem(customMarker);
            }
        }

    }

    private void setSelectedData(int position) {

        if(isSingleMapMode) {

            linearLayoutBottomIcon.setVisibility(View.VISIBLE);
            linearLayoutBottomMain.setVisibility(View.VISIBLE);
            if(textViewEmpty.getVisibility() == View.VISIBLE)
                textViewEmpty.setVisibility(View.GONE);

            toolbarTitle.setText(store.getName());
            textViewName.setText(store.getName());
            textViewAddress.setText(store.getAddress());
            textViewTel.setText(store.getPhoneNumber());

        } else {

            if(arrayListStore == null || arrayListStore.size() == 0) return;

            linearLayoutBottomIcon.setVisibility(View.VISIBLE);
            linearLayoutBottomMain.setVisibility(View.VISIBLE);
            if(textViewEmpty.getVisibility() == View.VISIBLE)
                textViewEmpty.setVisibility(View.GONE);

            Store currentStore = arrayListStore.get(position);

            toolbarTitle.setText(currentStore.getName());
            textViewName.setText(currentStore.getName());
            textViewAddress.setText(currentStore.getAddress());
            textViewTel.setText(currentStore.getPhoneNumber());
        }

    }

    private void setMyLocation() {

        linearLayoutBottomIcon.setVisibility(View.GONE);
        linearLayoutBottomMain.setVisibility(View.GONE);
        textViewEmpty.setVisibility(View.VISIBLE);


    }

    /**
     * 다음 지도 앱 켜기
     */
    private void startDaumMapApp() {


        if(appInstalledOrNot()) {
            Log.e("깔려잇음 = " + isSingleMapMode);
            if(isSingleMapMode) {

                if(store == null) return;

                StringBuffer stringBuffer = new StringBuffer("daummaps://look?p=");
                stringBuffer.append(store.getLatitude());
                stringBuffer.append(",");
                stringBuffer.append(store.getLongitude());

                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(stringBuffer.toString()));
                startActivity(intent);

            } else {

                if(arrayListStore == null || arrayListStore.size() < 1 ||  arrayListStore.get(selectedPos) == null) return;

                Store selectedStore = arrayListStore.get(selectedPos);

                StringBuffer stringBuffer = new StringBuffer("daummaps://look?p=");
                stringBuffer.append(selectedStore.getLatitude());
                stringBuffer.append(",");
                stringBuffer.append(selectedStore.getLongitude());

                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(stringBuffer.toString()));
                startActivity(intent);
            }
        } else {

            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(NetInfo.DAUM_MAP_DOWNLOAD_URL));
            startActivity(intent);
        }
    }

    private boolean appInstalledOrNot() {
        String uri = "net.daum.android.map";
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }


    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================

}
