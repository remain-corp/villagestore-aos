package com.remain.villagestore.pages.p3_stores.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.dto.Coupon;
import com.remain.villagestore.manager.DateFormat;
import com.remain.villagestore.manager.DisplayManager;
import com.remain.villagestore.util.RecycleUtil;

import java.util.ArrayList;

/**
 * Created by woongjaelee on 2015. 12. 17..
 */
public class CouponAdapter extends PagerAdapter {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    private Context context;
    private Intent intent;
    private ArrayList<Coupon> arrayListCoupon = new ArrayList<Coupon>();
    private LayoutInflater layoutInflater;

    // Util
    private DisplayManager displayManager;

    //================================================================================
    // XXX Constructor
    //================================================================================

    public CouponAdapter(Context context, ArrayList<Coupon> arrayListCoupon) {

        this.context = context;
        displayManager = new DisplayManager(context);

        if(arrayListCoupon != null)
            this.arrayListCoupon = new ArrayList<Coupon>(arrayListCoupon);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = layoutInflater.inflate(R.layout.content_coupon_list, null);

        Coupon coupon = arrayListCoupon.get(position);
        init(view,coupon);
        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        RecycleUtil.recursiveRecycle((View) object);

        ((ViewPager) container).removeView((View) object);

    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public int getCount() {
        return arrayListCoupon.size();
    }


    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================

    private void init(View view,Coupon coupon) {

        TextView textViewDate = (TextView)view.findViewById(R.id.content_coupon_textview_regdate);

        textViewDate.setText(String.format(context.getResources().getString(R.string.coupon_regdate_format), DateFormat.getCurrentTime(DateFormat.FORMAT_COUPON_REG_DATE)));

        TextView textViewDetail = (TextView)view.findViewById(R.id.content_coupon_textview_detail);
        textViewDetail.setText(coupon.getCouponDetail());

        TextView textViewUseDate = (TextView)view.findViewById(R.id.content_coupon_textview_issue_date);
        textViewUseDate.setText(String.format(context.getResources().getString(R.string.coupon_usedate_format),
                DateFormat.regDate(coupon.getStartDate(),DateFormat.FORMAT_DATE_COUPON_USE_DATE),
                DateFormat.regDate(coupon.getEndDate(), DateFormat.FORMAT_DATE_COUPON_USE_DATE)
                ));


        initUISize(view);
    }

    private void initUISize(View view) {

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) view.findViewById(R.id.content_coupon_linearlayout_coupon_detail).getLayoutParams();
        linearParams.topMargin = displayManager.getScaleSizeSameRate(20);
        linearParams.bottomMargin = displayManager.getScaleSizeSameRate(20);

        view.findViewById(R.id.content_coupon_linearlayout_coupon_detail).setPadding(displayManager.getScaleSizeSameRate(20),displayManager.getScaleSizeSameRate(20),displayManager.getScaleSizeSameRate(20),displayManager.getScaleSizeSameRate(38));

        linearParams = (LinearLayout.LayoutParams) view.findViewById(R.id.content_coupon_textview_detail).getLayoutParams();
        linearParams.topMargin = displayManager.getScaleSizeSameRate(10);

        linearParams = (LinearLayout.LayoutParams) view.findViewById(R.id.content_coupon_textview_issue_date).getLayoutParams();
        linearParams.topMargin = displayManager.getScaleSizeSameRate(10);


    }

    //================================================================================
    // XXX Override Method
    //================================================================================
}
