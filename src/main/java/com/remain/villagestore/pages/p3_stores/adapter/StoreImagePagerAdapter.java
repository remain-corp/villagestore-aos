package com.remain.villagestore.pages.p3_stores.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.remain.villagestore.R;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.manager.ImageLoaderManager;
import com.remain.villagestore.util.RecycleUtil;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by woongjaelee on 15. 7. 9..
 */
public class StoreImagePagerAdapter extends PagerAdapter{

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    private Context context;
    private Intent intent;
    private ArrayList<String> arrayListImgPath = new ArrayList<String>();
    private LayoutInflater layoutInflater;
    // 원본 이미지 보여주는 모드
    private boolean isOriginMode;

    // Util

    private DisplayImageOptions options;

    private OnPageClickListener pageClickListener;

    private SparseArray<View> views = new SparseArray<View>();

    //================================================================================
    // XXX Constructor
    //================================================================================

    public StoreImagePagerAdapter(Context context, ArrayList<String> arrayListImgPath,boolean isOriginMode) {

        this.context = context;
        if(arrayListImgPath != null)
            this.arrayListImgPath = new ArrayList<String>(arrayListImgPath);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.isOriginMode = isOriginMode;
        options = ImageLoaderManager.getInstance(context).getDisplayImageOptionCache(R.drawable.image_default_photo);

    }


    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = layoutInflater.inflate(R.layout.content_storeimg_pager,null);
        ImageView imageView = (ImageView)view.findViewById(R.id.content_store_pager);

        imageView.setTag(position);
        imageView.setOnClickListener(clickListener);
        ImageLoader.getInstance().displayImage(ImageLoaderManager.URIS_WEB + AppInfo.IMG_PATH + arrayListImgPath.get(position), imageView, options);

        if(isOriginMode) {
            PhotoViewAttacher attacher = new PhotoViewAttacher(imageView);
        } else {
            //ImageLoader.getInstance().displayImage(ImageLoaderManager.URIS_WEB + AppInfo.IMG_THUM_PATH + arrayListImgPath.get(position),imageView, options);
        }

        container.addView(view);

        views.put(position,view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        RecycleUtil.recursiveRecycle((View) object);
        ((ViewPager) container).removeView((View) object);

    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public int getCount() {
        return arrayListImgPath.size();
    }


    //================================================================================
    // XXX Listener
    //================================================================================

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

           if(pageClickListener != null) pageClickListener.onPageClick((int)v.getTag());
        }
    };

    //================================================================================
    // XXX Etc Method
    //================================================================================

    public void setOnPageClickListener (OnPageClickListener pageClickListener) {

        this.pageClickListener = pageClickListener;
    }

    public void recycle() {

        RecycleUtil.recursiveRecycle(views);
    }

    //================================================================================
    // XXX Override Method
    //================================================================================

    public interface OnPageClickListener{
        public void onPageClick(int position);
    }



}
