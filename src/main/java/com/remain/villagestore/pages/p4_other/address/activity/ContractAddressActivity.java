package com.remain.villagestore.pages.p4_other.address.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.pages.p4_other.address.adapter.ContractListAdapter;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by remain on 2016. 6. 2..
 */
public class ContractAddressActivity extends FragmentActivity implements View.OnClickListener {
    private ViewMapper viewMapper;
    private ContractListAdapter contractListAdapter;
    private ArrayList<String> sidoList = new ArrayList<>();
    private ArrayList<String> sigunguList = new ArrayList<>();
    private ArrayList<String> contractList = new ArrayList<>();

    private ArrayList<String> contractSeq = new ArrayList<>();
    //유저가 이전에 선택했던 리스트들
    private String selectedSido = "";
    private String selectedSiGunGu = "";
    private String selectedContSeq = "";
    private ArrayList<String> sigunguBackUpList = new ArrayList<>();
    private ArrayList<String> contractBackUpList = new ArrayList<>();

    private int indicatorPoint = 0;

    private ServerRequest server;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract_address);
        viewMapper = new ViewMapper();
        viewInit();

    }

    final void viewInit() {
        viewMapper.toolbarTitle.setText("약정서 주소 선택");
        server = new ServerRequest(networkResultListener);

        viewMapper.loadingView.setOnClickListener(this);
        viewMapper.toolbarBackButton.setOnClickListener(this);
        viewMapper.imageSido.setOnClickListener(this);
        viewMapper.imageGungu.setOnClickListener(this);
        viewMapper.imageDetail.setOnClickListener(this);

        setIndicatorImage(0);
        setData();

    }

    final void setData() {
        requestSelectContractToServer();

    }

    final void setContractList() {

        contractListAdapter = new ContractListAdapter(this, sidoList);
        viewMapper.listView.setAdapter(contractListAdapter);
        viewMapper.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectAddr = "";
                String info = "";
                switch (indicatorPoint) {
                    case 0:
                        //시도일때
                        selectAddr = sidoList.get(position);
                        info = "시 / 군 / 구 를 선택해주세요.";
                        selectSiDo(selectAddr);
                        setIndicatorImage(1);
                        indicatorPoint = 1;
                        break;
                    case 1:
                        selectAddr = selectedSido + " " + sigunguBackUpList.get(position);
                        info = "해당하는 단체를 선택해주세요.";
                        selectSiGunGu(selectAddr);
                        setIndicatorImage(2);
                        //시군구일때
                        break;
                    case 2:
                        info = "해당하는 단체를 선택해주세요.";
                        selectAddr = contractBackUpList.get(position);
                        String contract = selectAddr.replace(selectedSiGunGu, "");
                        for (int i = 0; i < contractList.size(); i++) {
                            String cont = contractList.get(i);
                            if (cont.contains(contract)) {
                                selectedContSeq = contractSeq.get(i);
                            }
                        }
                        requestInsertLocalToServer(selectedContSeq);
                        Log.e(selectedContSeq);
                        //최종 선택일 경우
                        break;
                }
                viewMapper.textIndicator.setText(info);
                viewMapper.textAddress.setText(selectAddr);
            }
        });
    }

    //유저가 sido를 선택
    final void selectSiDo(String sido) {
        sigunguBackUpList.clear();
        selectedSido = sido;
        for (String siGunGu : sigunguList) {
            if (siGunGu.contains(sido)) {
                String replaceSiGunGu = siGunGu.replace(sido + " ", "");
                sigunguBackUpList.add(replaceSiGunGu);
            }
        }
        contractListAdapter.setData(sigunguBackUpList);
    }

    //유저가 sigungu를 선택
    final void selectSiGunGu(String address) {
        contractBackUpList.clear();
        selectedSiGunGu = address;
        for (String contract : contractList) {
            if (contract.contains(address)) {
                contractBackUpList.add(contract);
            }
        }
        contractListAdapter.setData(contractBackUpList);
    }

    final void setIndicatorImage(int point) {
        ViewGroup.LayoutParams params1 = viewMapper.imageSido.getLayoutParams();
        ViewGroup.LayoutParams params2 = viewMapper.imageGungu.getLayoutParams();
        ViewGroup.LayoutParams params3 = viewMapper.imageDetail.getLayoutParams();

        params1.width = Utils.DPToPX(this, 36);
        params2.width = Utils.DPToPX(this, 36);
        params3.width = Utils.DPToPX(this, 36);

        viewMapper.imageSido.clearColorFilter();
        viewMapper.imageGungu.clearColorFilter();
        viewMapper.imageDetail.clearColorFilter();

        if (point == 0) {
            params1.width = Utils.DPToPX(this, 48);
            viewMapper.imageSido.setLayoutParams(params1);
            viewMapper.imageSido.setColorFilter(Color.parseColor("#f65560"));
        } else if (point == 1) {
            params2.width = Utils.DPToPX(this, 48);
            viewMapper.imageGungu.setLayoutParams(params2);
            viewMapper.imageGungu.setColorFilter(Color.parseColor("#f65560"));
        } else if (point == 2) {
            params3.width = Utils.DPToPX(this, 48);
            viewMapper.imageDetail.setLayoutParams(params3);
            viewMapper.imageDetail.setColorFilter(Color.parseColor("#f65560"));
        }

        viewMapper.imageSido.setLayoutParams(params1);
        viewMapper.imageGungu.setLayoutParams(params2);
        viewMapper.imageDetail.setLayoutParams(params3);

        indicatorPoint = point;
    }

    final void clickIndicatorImage(View view) {
        String textInfo = "";
        String selectAddr = "";
        if (view == viewMapper.imageSido) {
            textInfo = "시 / 도 를 선택해주세요.";
            if (!selectedSiGunGu.equals("")) {
                selectAddr = selectedSido;
            }
            setIndicatorImage(0);
            contractListAdapter.setData(sidoList);
        } else if (view == viewMapper.imageGungu) {
            textInfo = "시 / 군 / 구 를 선택해주세요.";
            if (!selectedSiGunGu.equals("")) {
                selectAddr = selectedSiGunGu;
            }
            setIndicatorImage(1);
            if (sigunguBackUpList != null) {
                contractListAdapter.setData(sigunguBackUpList);
            }
        } else if (view == viewMapper.imageDetail) {
            textInfo = "해당하는 단체를 선택해주세요.";
            setIndicatorImage(2);
            if (!selectedSiGunGu.equals("")) {
                selectAddr = selectedSiGunGu;
            }
            if (contractBackUpList != null) {
                contractListAdapter.setData(contractBackUpList);
            }
        }
        viewMapper.textIndicator.setText(textInfo);
        viewMapper.textAddress.setText(selectAddr);
    }

    private void requestSelectContractToServer() {
        viewMapper.loadingView.setVisibility(View.VISIBLE);
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        server.execute( NetInfo.IP_ADDRESS1, NetInfo.SELECT_CONTRACT, params);

    }

    private void requestInsertLocalToServer(String contractSeq) {
        viewMapper.loadingView.setVisibility(View.VISIBLE);
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_SEQ, Database.getInstance().getUser(this).getSeq());
        params.put(ParamInfo.ADDRESS_INFO, "20");
        params.put(ParamInfo.ADDRESS_ISPRE, "Y");
        params.put(ParamInfo.ADDRESS_PRIO, "1");
        params.put(ParamInfo.ADDRESS_CONT_SEQ, contractSeq);

        //기존에 로컬시큐가 있다면 업데이트
        if (Database.getInstance().getUser(this).getLocalSeq() != 0) {
            params.put(ParamInfo.ADDRESS_LOCAL_SEQ, Database.getInstance().getUser(this).getLocalSeq());
        }

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.INSERT_ADDRESS, params);

    }

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.SELECT_CONTRACT) {
                if (!object.isNull("result")) {
                    sidoList.clear();
                    sigunguList.clear();
                    contractList.clear();
                    contractSeq.clear();
                    try {
                        JSONObject objectResult = object.getJSONObject("result");
                        JSONArray array = objectResult.getJSONArray("addrInfo");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject addr = array.getJSONObject(i);

                            String sido = addr.getString("sido");
                            sidoList.add(sido);
                            JSONArray sigunList = addr.getJSONArray("sigunguList");
                            for (int a = 0; a < sigunList.length(); a++) {
                                JSONObject siGunGuObject = sigunList.getJSONObject(a);
                                String sigun = siGunGuObject.getString("sigungu");
                                sigunguList.add(sido + " " + sigun);
                            }
                        }

                        JSONArray arrayContract = objectResult.getJSONArray("contract");
                        for (int i = 0; i < arrayContract.length(); i++) {
                            JSONObject objContract = arrayContract.getJSONObject(i);
                            String contSeq = objContract.getString("cont_seq");
                            String fullAddr = objContract.getString("full_addr");
                            String detailAddr = objContract.getString("res_name");

                            contractSeq.add(contSeq);
                            contractList.add(fullAddr + " " + detailAddr);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                uiHandler.sendEmptyMessage(NetInfo.WHAT_SELECT_CONTRACT);
            }

            if (url == NetInfo.INSERT_ADDRESS) {
                try {
                    JSONObject objectResult = object.getJSONObject("result");
                    String isOk = objectResult.getString(ParamInfo.IS_OK);
                    if (isOk.equals(ParamInfo.OK)) {
                        Database.getInstance().checkUser(ContractAddressActivity.this, new User(objectResult.getJSONObject("info")));
                        Log.e("isOk=" + isOk);
                        uiHandler.sendEmptyMessage(NetInfo.WHAT_INSERT_ADDRESS);
                    } else {
                        uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                    }
                    Log.e("결과" + objectResult.getJSONObject("info"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            viewMapper.loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Toast.makeText(ContractAddressActivity.this, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    break;

                case NetInfo.WHAT_SELECT_CONTRACT:
                    //약정서 조회 완료
                    setContractList();
                    break;
                case NetInfo.WHAT_INSERT_ADDRESS:
                    //약정서 선택 완료
                    Log.e("여기서 에러인가 있을경우");
                    Intent i = new Intent(ContractAddressActivity.this, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    i.putExtra("signUp", true);
                    startActivity(i);
                    finish();
                    break;

                default:
                    break;
            }

        }
    };

    @Override
    public void onClick(View v) {
        if(viewMapper.loadingView == v){}
        if (v == viewMapper.toolbarBackButton) {
            finish();
        } else if (v == viewMapper.imageSido || v == viewMapper.imageGungu || v == viewMapper.imageDetail) {
            clickIndicatorImage(v);
        }
    }

    private class ViewMapper {
        private RelativeLayout toolbar;
        private StretchImageView toolbarBackButton;
        private TextView toolbarTitle;

        private RelativeLayout loadingView;

        private StretchImageView imageSido;
        private StretchImageView imageGungu;
        private StretchImageView imageDetail;

        private TextView textIndicator;
        private TextView textAddress;

        private ListView listView;

        public ViewMapper() {
            toolbar = (RelativeLayout) findViewById(R.id.contract_toolbar);
            toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

            loadingView = (RelativeLayout) findViewById(R.id.contract_loadingview);

            imageSido = (StretchImageView) findViewById(R.id.contract_image_sido);
            imageGungu = (StretchImageView) findViewById(R.id.contract_image_gungu);
            imageDetail = (StretchImageView) findViewById(R.id.contract_image_detail);

            textIndicator = (TextView) findViewById(R.id.contract_text_indicator);
            textAddress = (TextView) findViewById(R.id.contract_text_address);

            listView = (ListView) findViewById(R.id.contract_listview);
        }

    }

}
