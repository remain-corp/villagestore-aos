package com.remain.villagestore.pages.p4_other.address.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.pages.p4_other.address.adapter.AddressListAdapter;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by remain on 2016. 5. 31..
 */
public class SearchAddressActivity extends Activity implements View.OnClickListener {
    private ViewMapper viewMapper;
    private AddressListAdapter addressListAdapter;
    private ServerRequest server;

    private String putAddress;
    private ArrayList<String> zoneCodeList = new ArrayList<>();
    private ArrayList<String> doroAddressList = new ArrayList<>();
    private ArrayList<String> oldAddressList = new ArrayList<>();

    private String zoneCode = "";
    private String siDo = "";
    private String siGunGu = "";
    private String addressFull = "";
    private String doroDetail = "";
    private String oldDetail = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_address);
        viewMapper = new ViewMapper();
        viewInit();
    }

    final void viewInit() {
        server = new ServerRequest(networkResultListener);
        viewMapper.toolbarTitle.setText("주소지 설정");
        //viewMapper.toolbarTitle.setTextColor(getResources().getColor(R.color.main_color));

        viewMapper.toolbarBackButton.setOnClickListener(this);
        viewMapper.btnSerch.setOnClickListener(this);

        viewMapper.loadingView.setOnClickListener(this);

        viewMapper.selectedConfrim.setOnClickListener(this);
        viewMapper.selectedCancel.setOnClickListener(this);
        viewMapper.addressWrapper.setOnClickListener(this);
        viewMapper.listviewWrapper.setOnClickListener(this);
    }

    final void spliteAddressString(int positon) {
        String oldAddress = oldAddressList.get(positon);
        String doroAddress = doroAddressList.get(positon);

        zoneCode = zoneCodeList.get(positon);
        siDo = "";
        siGunGu = "";
        addressFull = doroAddress;
        doroDetail = "";
        oldDetail = "";

        String sameAddress = "";
        for (int i = 0; i < oldAddress.length(); i++) {
            if (oldAddress.substring(0, i).equals(doroAddress.substring(0, i))) {
                sameAddress = oldAddress.substring(0, i);
            }
        }
        //도로까지 이름이 겹치는경우 제거
        sameAddress = sameAddress.substring(0, sameAddress.lastIndexOf(" ")).trim();

        //시 도, 시 군 구
        String[] sameAddressArray = sameAddress.split(" ");
        for (int i = 0; i < sameAddressArray.length; i++) {
            if (i == 0) {
                siDo = sameAddressArray[i];
            } else {
                if (i == 1) {
                    siGunGu += sameAddressArray[i];
                } else {
                    siGunGu += " " + sameAddressArray[i];
                }
            }
        }
        //도로명 세부 주소
        doroDetail = doroAddress.substring(sameAddress.length() + 1);
        //지번 세부 주소
        oldDetail = oldAddress.substring(sameAddress.length() + 1);

        Log.e("zoneCode = " + zoneCodeList.get(positon) + ";");
        Log.e("도로명 = " + doroAddress + ";");
        Log.e("siDo = " + siDo + ";");
        Log.e("siGunGu = " + siGunGu + ";");
        Log.e("sameAddress = " + sameAddress + ";");
        Log.e("doroDetail = " + doroDetail + ";");
        Log.e("oldDetail = " + oldDetail + ";");

    }

    private void requestInsertLocalToServer(String userDetail) {
        viewMapper.loadingView.setVisibility(View.VISIBLE);
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_SEQ, Database.getInstance().getUser(SearchAddressActivity.this).getSeq());
        params.put(ParamInfo.ADDRESS_INFO, "10");
        params.put(ParamInfo.ADDRESS_ZONECODE, zoneCode);
        params.put(ParamInfo.ADDRESS_SIDO, siDo);
        params.put(ParamInfo.ADDRESS_SIGUNGU, siGunGu);
        params.put(ParamInfo.ADDRESS_FULL, addressFull);
        params.put(ParamInfo.ADDRESS_DORO_DETAIL, doroDetail);
        params.put(ParamInfo.ADDRESS_JIBUN_DETAIL, oldDetail);
        params.put(ParamInfo.ADDRESS_USER_DETAIL, userDetail);
        params.put(ParamInfo.ADDRESS_ISPRE, "Y");
        params.put(ParamInfo.ADDRESS_PRIO, "1");

        //기존에 로컬시큐가 있다면 업데이트
        if (Database.getInstance().getUser(this).getLocalSeq() != 0) {
            params.put(ParamInfo.ADDRESS_LOCAL_SEQ, Database.getInstance().getUser(this).getLocalSeq());
        }

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.INSERT_ADDRESS, params);

    }

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.INSERT_ADDRESS) {
                try {
                    JSONObject objectResult = object.getJSONObject("result");
                    Log.e(" 스트링 = "+objectResult.toString());
                    String isOk = objectResult.getString(ParamInfo.IS_OK);
                    if (isOk.equals(ParamInfo.OK)) {
                        Database.getInstance().checkUser(SearchAddressActivity.this, new User(objectResult.getJSONObject("info")));
                        Log.e("isOk=" + isOk);
                        uiHandler.sendEmptyMessage(NetInfo.WHAT_INSERT_ADDRESS);
                    } else {
                        uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                    }
                    Log.e("결과" + objectResult.getJSONObject("info"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            viewMapper.loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Toast.makeText(SearchAddressActivity.this, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    break;

                case NetInfo.WHAT_INSERT_ADDRESS:
                    //주소지설정 완료
                    Intent i = new Intent(SearchAddressActivity.this, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    i.putExtra("signUp", true);
                    startActivity(i);
                    finish();
                    break;

                default:
                    break;
            }

        }
    };

    private class GetAddressDataTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            final String apiurl = "http://biz.epost.go.kr/KpostPortal/openapi";
            String result = "";

            zoneCodeList.clear();
            doroAddressList.clear();
            oldAddressList.clear();

            HttpURLConnection conn = null;
            try {
                StringBuffer sb = new StringBuffer(3);
                sb.append(apiurl);
                sb.append("?regkey=" + NetInfo.POST_API_KEY + "&target=postNew&query=");
                sb.append(URLEncoder.encode(putAddress, "EUC-KR"));
                String query = sb.toString();
                Log.e("쿼리 = " + query);
                URL url = new URL(query);
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("accept-language", "ko");
                DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                byte[] bytes = new byte[4096];
                InputStream in = conn.getInputStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                while (true) {
                    int red = in.read(bytes);
                    if (red < 0) break;
                    baos.write(bytes, 0, red);
                }
                String xmlData = baos.toString("utf-8");
                baos.close();
                in.close();
                conn.disconnect();
                Document doc = docBuilder.parse(new InputSource(new StringReader(xmlData)));
                Element el = (Element) doc.getElementsByTagName("itemlist").item(0);
                for (int i = 0; i < ((Node) el).getChildNodes().getLength(); i++) {
                    Node node = ((Node) el).getChildNodes().item(i);
                    if (!node.getNodeName().equals("item")) {
                        continue;
                    }
                    zoneCodeList.add(node.getChildNodes().item(1).getFirstChild().getNodeValue());
                    doroAddressList.add(node.getChildNodes().item(3).getFirstChild().getNodeValue());
                    oldAddressList.add(node.getChildNodes().item(5).getFirstChild().getNodeValue());
                }
            } catch (Exception e) {
                result = "에러";
                e.printStackTrace();
            } finally {
                try {
                    if (conn != null) conn.disconnect();
                } catch (Exception e) {
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            viewMapper.loadingView.setVisibility(View.GONE);
            ArrayList addressList = new ArrayList();
            for (int i = 0; i < doroAddressList.size(); i++) {
                addressList.add("도로명 = " + doroAddressList.get(i) + "\n지 번 = " + oldAddressList.get(i));
            }
            if (s.equals("에러")) {
                addressList.clear();
                addressList.add("주소를 찾을 수 없습니다.");
                viewMapper.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(viewMapper.etSerch.getWindowToken(), 0);
                    }
                });
            } else {
                viewMapper.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        spliteAddressString(position);
                        viewMapper.selectedZoneCode.setText("우편번호 : " + zoneCodeList.get(position));
                        viewMapper.selectedDoro.setText("도로명 :\n" + doroAddressList.get(position));
                        viewMapper.selectedZibun.setText("지번 :\n" + oldAddressList.get(position));
                        viewMapper.selectedWrapper.setVisibility(View.VISIBLE);
                        viewMapper.listView.setVisibility(View.GONE);

                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(viewMapper.etSerch.getWindowToken(), 0);
                    }
                });
            }
            addressListAdapter = new AddressListAdapter(SearchAddressActivity.this, addressList);
            viewMapper.listView.setAdapter(addressListAdapter);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == viewMapper.loadingView){

        }
        if (v == viewMapper.toolbarBackButton) {
            finish();
        } else if (v == viewMapper.addressWrapper || v == viewMapper.listviewWrapper ) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(viewMapper.etSerch.getWindowToken(), 0);
        } else if (v == viewMapper.btnSerch) {
            if (!viewMapper.etSerch.getText().toString().equals("")) {
                viewMapper.loadingView.setVisibility(View.VISIBLE);
                putAddress = viewMapper.etSerch.getText().toString();
                new GetAddressDataTask().execute();
                viewMapper.listView.setVisibility(View.VISIBLE);
                viewMapper.selectedWrapper.setVisibility(View.GONE);
            }
        } else if (v == viewMapper.selectedConfrim) {
            //확인 디비작업
            requestInsertLocalToServer(viewMapper.etUserDetailAddress.getText().toString());
        } else if (v == viewMapper.selectedCancel) {
            //리스트뷰 Visible
            viewMapper.listView.setVisibility(View.VISIBLE);
            viewMapper.selectedWrapper.setVisibility(View.GONE);
            viewMapper.etUserDetailAddress.setText("");
        }
    }

    private class ViewMapper {
        private RelativeLayout toolbar;
        private TextView toolbarTitle;
        private StretchImageView toolbarBackButton;

        private RelativeLayout loadingView;

        private LinearLayout addressWrapper;

        private EditText etSerch;
        private StretchImageView btnSerch;

        private RelativeLayout listviewWrapper;
        private ListView listView;

        //주소선택시 페이지
        private LinearLayout selectedWrapper;
        private TextView selectedZoneCode;
        private TextView selectedDoro;
        private TextView selectedZibun;
        private EditText etUserDetailAddress;
        private TextView selectedConfrim;
        private TextView selectedCancel;

        public ViewMapper() {
            toolbar = (RelativeLayout) findViewById(R.id.user_address_toolbar);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
            toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);

            loadingView = (RelativeLayout) findViewById(R.id.user_address_loadingview);

            addressWrapper = (LinearLayout) findViewById(R.id.user_address_wrapper);

            etSerch = (EditText) findViewById(R.id.user_address_et_search);
            btnSerch = (StretchImageView) findViewById(R.id.user_address_image_search);

            listviewWrapper = (RelativeLayout) findViewById(R.id.user_address_listview_wrapper);
            listView = (ListView) findViewById(R.id.user_address_listview);

            //주소선택시 페이지
            selectedWrapper = (LinearLayout) findViewById(R.id.user_address_selected_wrapper);
            selectedZoneCode = (TextView) findViewById(R.id.user_address_selected_zonecode);
            selectedDoro = (TextView) findViewById(R.id.user_address_selected_doro);
            selectedZibun = (TextView) findViewById(R.id.user_address_selected_zibun);
            etUserDetailAddress = (EditText) findViewById(R.id.user_address_selected_et_detail);
            selectedConfrim = (TextView) findViewById(R.id.user_address_selected_confirm);
            selectedCancel = (TextView) findViewById(R.id.user_address_selected_cancel);
        }
    }

}
