package com.remain.villagestore.pages.p4_other.address.activity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.util.Log;

/**
 * Created by remain on 2016. 6. 2..
 */
public class SelectAddressActivity extends FragmentActivity implements View.OnClickListener {
    private ViewMapper viewMapper;

    private int visibleImageNum = 0;
    private boolean isSignUpMode = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_address);
        viewMapper = new ViewMapper();
        checkIntent();
        viewInit();



    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if(isSignUpMode){
                        Intent i = new Intent(this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        i.putExtra("signUp", true);
                        startActivity(i);
                        finish();
                        return true;
                    }else {
                        return super.onKeyDown(keyCode, event);
                    }
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    final void checkIntent(){
       isSignUpMode = getIntent().getBooleanExtra( "isSignMode" ,false);
    }

    final void viewInit() {
        viewMapper.toolbarTitle.setText("적립 단체(단지) 설정");
        viewMapper.image2.setAlpha(0f);
        viewMapper.image3.setAlpha(0f);

        viewMapper.toolbarBackButton.setOnClickListener(this);
        viewMapper.btnContract.setOnClickListener(this);
        viewMapper.btnSearch.setOnClickListener(this);
        viewMapper.btnSelf.setOnClickListener(this);
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        Thread.sleep(3000);
                        chageImages();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    final void chageImages() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                StretchImageView image1 = null;
                StretchImageView image2 = null;

                if (visibleImageNum == 0) {
                    image1 = viewMapper.image1;
                    image2 = viewMapper.image2;
                } else if (visibleImageNum == 1) {
                    image1 = viewMapper.image2;
                    image2 = viewMapper.image3;
                } else if (visibleImageNum == 2) {
                    image1 = viewMapper.image3;
                    image2 = viewMapper.image1;
                }

                ObjectAnimator anim1 = ObjectAnimator.ofFloat(image1, "alpha", 1, 0);
                anim1.setDuration(600);
                anim1.start();

                ObjectAnimator anim2 = ObjectAnimator.ofFloat(image2, "alpha", 0, 1);
                anim2.setDuration(600);
                anim2.start();

                if (visibleImageNum == 2) {
                    visibleImageNum = 0;
                } else {
                    visibleImageNum += 1;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == viewMapper.toolbarBackButton) {
            finish();
        } else if (v == viewMapper.btnContract) {
            //약정서 검색
            Log.e("약정서 검색");
            Intent intent = new Intent(SelectAddressActivity.this, ContractAddressActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else if (v == viewMapper.btnSearch) {
            //주소지 검색
            Intent intent = new Intent(SelectAddressActivity.this, SearchAddressActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else if (v == viewMapper.btnSelf) {
            //직접입력
            Log.e("직적 입력");
            Intent intent = new Intent(SelectAddressActivity.this, SelfAddressActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

    private class ViewMapper {
        private RelativeLayout toolbar;
        private StretchImageView toolbarBackButton;
        private TextView toolbarTitle;

        private StretchImageView image1;
        private StretchImageView image2;
        private StretchImageView image3;

        private TextView btnContract;
        private TextView btnSearch;
        private TextView btnSelf;

        public ViewMapper() {
            toolbar = (RelativeLayout) findViewById(R.id.select_address_toolbar);
            toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

            image1 = (StretchImageView) findViewById(R.id.select_address_toolbar_image1);
            image2 = (StretchImageView) findViewById(R.id.select_address_toolbar_image2);
            image3 = (StretchImageView) findViewById(R.id.select_address_toolbar_image3);

            btnContract = (TextView) findViewById(R.id.select_address_toolbar_text_contract);
            btnSearch = (TextView) findViewById(R.id.select_address_toolbar_text_search);
            btnSelf = (TextView) findViewById(R.id.select_address_toolbar_text_self);

        }
    }
}
