package com.remain.villagestore.pages.p4_other.address.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.pages.p4_other.address.adapter.ContractListAdapter;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by remain on 2016. 6. 2..
 */
public class SelfAddressActivity extends FragmentActivity implements View.OnClickListener {
    private ViewMapper viewMapper;
    private ContractListAdapter contractListAdapter;

    private ServerRequest server;

    private ArrayList<String> arrayCateList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_address);
        viewMapper = new ViewMapper();
        viewInit();

    }

    final void viewInit() {
        server = new ServerRequest(networkResultListener);
        viewMapper.toolbarTitle.setText("주소지 직접입력");

        viewMapper.toolbarBackButton.setOnClickListener(this);
        viewMapper.loadingView.setOnClickListener(this);
        viewMapper.wrapper.setOnClickListener(this);
        viewMapper.imageCate.setOnClickListener(this);
        viewMapper.imageName.setOnClickListener(this);

        viewMapper.textConfirm.setOnClickListener(this);

        setIndicatorImage(0);
        setData();
    }

    final void setData() {
        arrayCateList.add("학교");
        arrayCateList.add("학원");
        arrayCateList.add("회사");
        arrayCateList.add("관공서");
        arrayCateList.add("자선단체");
        arrayCateList.add("동호회");
        arrayCateList.add("기타");

        setListView();
    }

    final void setListView() {
        contractListAdapter = new ContractListAdapter(this, arrayCateList);
        viewMapper.listView.setAdapter(contractListAdapter);
        viewMapper.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectAddr = "";
                String info = "";

                selectAddr = " ( " + arrayCateList.get(position) + " ) ";
                info = "단체명을 입력해 주세요.";
                setIndicatorImage(1);
                viewMapper.wrapperInput.setVisibility(View.VISIBLE);
                viewMapper.listView.setVisibility(View.GONE);

                viewMapper.textIndicator.setText(info);
                viewMapper.textAddress.setText(selectAddr);
            }
        });
    }

    final void setIndicatorImage(int point) {
        ViewGroup.LayoutParams params2 = viewMapper.imageCate.getLayoutParams();
        ViewGroup.LayoutParams params3 = viewMapper.imageName.getLayoutParams();

        params2.width = Utils.DPToPX(this, 36);
        params3.width = Utils.DPToPX(this, 36);

        viewMapper.imageCate.clearColorFilter();
        viewMapper.imageName.clearColorFilter();

        if (point == 0) {
            params2.width = Utils.DPToPX(this, 48);
            viewMapper.imageCate.setLayoutParams(params2);
            viewMapper.imageCate.setColorFilter(Color.parseColor("#f65560"));
        } else if (point == 1) {
            params3.width = Utils.DPToPX(this, 48);
            viewMapper.imageName.setLayoutParams(params3);
            viewMapper.imageName.setColorFilter(Color.parseColor("#f65560"));
        }

        viewMapper.imageCate.setLayoutParams(params2);
        viewMapper.imageName.setLayoutParams(params3);
    }

    final void clickIndicatorImage(View view) {
        String textInfo = "";
        if (view == viewMapper.imageCate) {
            textInfo = "해당하는 단체를 선택해주세요.";
            setIndicatorImage(0);
            contractListAdapter.setData(arrayCateList);
            viewMapper.wrapperInput.setVisibility(View.GONE);
            viewMapper.listView.setVisibility(View.VISIBLE);
        } else if (view == viewMapper.imageName) {
            if (!viewMapper.textAddress.getText().toString().contains("(")) {
                Toast.makeText(this, "단체를 선택해주세요.", Toast.LENGTH_SHORT).show();
                return;
            }
            textInfo = "단체명을 입력해 주세요.";
            setIndicatorImage(1);
            viewMapper.wrapperInput.setVisibility(View.VISIBLE);
            viewMapper.listView.setVisibility(View.GONE);
        }
        viewMapper.textIndicator.setText(textInfo);
    }

    private void requestInsertLocalToServer(String address) {
        viewMapper.loadingView.setVisibility(View.VISIBLE);
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_SEQ, Database.getInstance().getUser(this).getSeq());
        params.put(ParamInfo.ADDRESS_INFO, "30");
        params.put(ParamInfo.ADDRESS_ISPRE, "Y");
        params.put(ParamInfo.ADDRESS_PRIO, "1");
        params.put(ParamInfo.ADDRESS_TEXT, address);

        //기존에 로컬시큐가 있다면 업데이트
        if (Database.getInstance().getUser(this).getLocalSeq() != 0) {
            params.put(ParamInfo.ADDRESS_LOCAL_SEQ, Database.getInstance().getUser(this).getLocalSeq());
        }

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.INSERT_ADDRESS, params);

    }

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.INSERT_ADDRESS) {
                try {
                    JSONObject objectResult = object.getJSONObject("result");
                    String isOk = objectResult.getString(ParamInfo.IS_OK);
                    if (isOk.equals(ParamInfo.OK)) {
                        Database.getInstance().checkUser(SelfAddressActivity.this, new User(objectResult.getJSONObject("info")));
                        Log.e("isOk=" + isOk);
                        uiHandler.sendEmptyMessage(NetInfo.WHAT_INSERT_ADDRESS);
                    } else {
                        uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                    }
                    Log.e("결과" + objectResult.getJSONObject("info"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            viewMapper.loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Toast.makeText(SelfAddressActivity.this, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    break;

                case NetInfo.WHAT_INSERT_ADDRESS:
                    //주소지설정 완료
                    Intent i = new Intent(SelfAddressActivity.this, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    i.putExtra("signUp", true);
                    startActivity(i);
                    finish();
                default:
                    break;
            }

        }
    };

    @Override
    public void onClick(View v) {
        if(v == viewMapper.listView){

        }
        if (v == viewMapper.toolbarBackButton) {
            finish();
        } else if (v == viewMapper.imageCate || v == viewMapper.imageName) {
            clickIndicatorImage(v);
        } else if (v == viewMapper.textConfirm) {
            if (viewMapper.etAddress.getText().toString().equals("")) {
                Toast.makeText(this, "단체명을 입력해주세요.", Toast.LENGTH_SHORT).show();
                return;
            }
            requestInsertLocalToServer(viewMapper.textAddress.getText().toString()+ " " + viewMapper.etAddress.getText().toString());
        } else if (v == viewMapper.wrapper) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(viewMapper.etAddress.getWindowToken(), 0);
        }
    }

    private class ViewMapper {
        private RelativeLayout toolbar;
        private StretchImageView toolbarBackButton;
        private TextView toolbarTitle;

        private RelativeLayout loadingView;

        private LinearLayout wrapper;

        private StretchImageView imageCate;
        private StretchImageView imageName;

        private TextView textIndicator;
        private TextView textAddress;

        private ListView listView;

        private LinearLayout wrapperInput;
        private EditText etAddress;
        private TextView textConfirm;

        public ViewMapper() {
            toolbar = (RelativeLayout) findViewById(R.id.self_toolbar);
            toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

            loadingView = (RelativeLayout) findViewById(R.id.loadingview);

            wrapper = (LinearLayout) findViewById(R.id.self_wrapper);

            imageCate = (StretchImageView) findViewById(R.id.self_image_gungu);
            imageName = (StretchImageView) findViewById(R.id.self_image_detail);

            textIndicator = (TextView) findViewById(R.id.self_text_indicator);
            textAddress = (TextView) findViewById(R.id.self_text_address);

            listView = (ListView) findViewById(R.id.self_listview);

            wrapperInput = (LinearLayout) findViewById(R.id.self_wrapper_input);
            etAddress = (EditText) findViewById(R.id.self_et_address);
            textConfirm = (TextView) findViewById(R.id.self_text_confirm);
        }

    }

}
