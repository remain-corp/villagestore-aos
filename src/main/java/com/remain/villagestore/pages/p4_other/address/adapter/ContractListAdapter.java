package com.remain.villagestore.pages.p4_other.address.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.util.Log;

import java.util.ArrayList;

/**
 * Created by remain on 2016. 5. 25..
 */
public class ContractListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> contractList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private int clickedItem = 999;

    public ContractListAdapter(Context context, ArrayList<String> contractList) {
        this.context = context;
        this.contractList = contractList;
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return contractList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {

            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_contract_list, null);

            holder.bankName = (TextView) convertView.findViewById(R.id.item_contract_text_name);
            holder.imageSwitch = (StretchImageView) convertView.findViewById(R.id.item_contract_image_click);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        setView(holder, position);

        return convertView;
    }

    private void setView(ViewHolder holder, int position) {
        Log.e("포지션은 = " + position);
        Log.e("clickedItem = " + clickedItem);
        holder.bankName.setText(contractList.get(position));
        holder.imageSwitch.setVisibility(View.GONE);
        if (position == clickedItem) {
            holder.imageSwitch.setVisibility(View.VISIBLE);
            holder.imageSwitch.setColorFilter(context.getResources().getColor(R.color.main_color));
        }
    }

    public void setClickItem(int position) {
        this.clickedItem = position;
        notifyDataSetChanged();
    }

    public void setData(ArrayList data){
        contractList = data;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        private TextView bankName;
        private StretchImageView imageSwitch;

    }
}
