package com.remain.villagestore.pages.p4_other.local.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.dto.Address;
import com.remain.villagestore.dto.Subway;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.pages.p4_other.local.adapter.SearchListAdapter;
import com.remain.villagestore.pages.p4_other.local.adapter.SubwayListAdapter;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

/**
 * Created by remain on 2016. 9. 22..
 */
public class SearchActivity extends FragmentActivity implements View.OnClickListener{

    private final static String PATTERN_SUBWAY_SEARCH = "^[ㄱ-ㅎ가-힣0-9]*$";
    private Context context;
    private ViewMapper viewMapper;
    private ServerRequest server;

    // 시 리스트
    private ArrayList<Address> arraySidoList = new ArrayList<>();
    /** 구 리스트  */
    private ArrayList<Address> arrayGuList = new ArrayList<>();
    /** 동 리스트 */
    private ArrayList<Address> arrayDongList = new ArrayList<>();
    private SearchListAdapter searchAdapter;
    private int selSidoPos = AppInfo.NOTHING_INT;
    private int selGunGuPos = AppInfo.NOTHING_INT;
    private Address.AddrType addrType = Address.AddrType.SIDO;

    //지하철모드
    private ArrayList<Subway> subWayList = new ArrayList<>();
    private SubwayListAdapter subwayAdapter;
    private boolean isRequesting = false;

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        context = this;
        viewMapper = new ViewMapper();

        setData();
        viewInit();
    }

    @Override
    public void onClick(View v) {
        Log.e("click : "+v);
        if(v == viewMapper.toolbarBackBtn ){
            finish();
        }else if(v == viewMapper.toolbarSearchBtn || v == viewMapper.toolbarSubwayBtn){
            changeSearchMode(v);
        }else if (v == viewMapper.searchImageSido || v == viewMapper.searchImageGungu || v == viewMapper.searchImageDetail ){

            // 시~도 인디케이터 클릭시
            if(v == viewMapper.searchImageSido)  {
                addrType = Address.AddrType.SIDO;

                searchAdapter.replace(arraySidoList);
            }
            // 시군구인디케이터
            else if(v == viewMapper.searchImageGungu){

                Log.e("군구 클릭 : "+selSidoPos);
                if(selSidoPos == AppInfo.NOTHING_INT) {

                    Toast.makeText(context,R.string.search_gungu_error_message, Toast.LENGTH_SHORT).show();
                    return;
                }
                addrType = Address.AddrType.GUGUN;
                searchAdapter.replace(arrayGuList);
            }
            // 동 인디케이터 클릭시
            else if ( v == viewMapper.searchImageDetail ){

                Log.e("동 클릭 : "+selGunGuPos);
                if( selGunGuPos == AppInfo.NOTHING_INT) {

                    Toast.makeText(context,R.string.search_dong_error_message,Toast.LENGTH_SHORT).show();
                    return;
                }

                addrType = Address.AddrType.DONG;
                searchAdapter.replace(arrayDongList);
            }
            setIndicatorImage(v);
        }

    }

    //================================================================================
    // XXX Listener
    //================================================================================

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if(Pattern.matches(PATTERN_SUBWAY_SEARCH,s)) {
                requestSubwayListToServer(s.toString());
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private AdapterView.OnItemClickListener subwayListItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Intent intent = getIntent();
            try {
                Log.e("역이름 = " + subWayList.get(position).getName());
                intent.putExtra(ParamInfo.STN_NAME, subWayList.get(position).getName());
                intent.putExtra(ParamInfo.LAT, subWayList.get(position).getLat());
                intent.putExtra(ParamInfo.LNG, subWayList.get(position).getLng());

                setResult(NetInfo.WHAT_SEARCH_STATION, intent);

                finish();
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    };

    private AdapterView.OnItemClickListener gridItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (addrType) {
                case SIDO:
                    selSidoPos = position;
                    selGunGuPos = AppInfo.NOTHING_INT;
                    requestGunGulistToServer(arraySidoList.get(position).getAddr_seq(),true);
                    break;
                case GUGUN:
                    selGunGuPos = position;
                    setIndicatorImage(viewMapper.searchImageGungu);
                    try{

                        requestGunGulistToServer(arrayGuList.get(position).getAddr_seq(),true);
                    }
                    catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                    //시군구일때
                    break;
                case DONG:
                    //최종 선택일 경우

                    Intent intent = getIntent();
                    try {
                        intent.putExtra(ParamInfo.SEL_ADDR_SEQ, arrayDongList.get(position).getAddr_seq());

                        // 전체보기
                        if(position == 0) {

                            intent.putExtra(ParamInfo.ADDRESS,arrayGuList.get(selGunGuPos).getName());
                            intent.putExtra(ParamInfo.IS_SIGUNGU,1);
                        }
                        else {

                            intent.putExtra(ParamInfo.ADDRESS,arrayDongList.get(position).getName());
                        }

                        setResult(NetInfo.WHAT_SELECT_ADDRLIST, intent);

                        finish();

                    }
                    catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            Log.e("url = "+url);

            if (error != null) {
                Log.e("error = "+error.getErrorCode());
                Log.e("errorMSG = "+error.getError());
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.SELECT_ADDRLIST) {
                if (!object.isNull("result")) {

                    try {
                        JSONObject objectResult = object.getJSONObject("result");
                        if(!objectResult.isNull(ParamInfo.ADDRESS_LIST)) {
                            JSONArray jsonArray = objectResult.getJSONArray(ParamInfo.ADDRESS_LIST);
                            switch (addrType) {
                                case SIDO:
                                    arrayGuList.clear();
                                    for(int i = 0; i < jsonArray.length(); i++) {

                                        arrayGuList.add(new Address(jsonArray.getJSONObject(i)));
                                    }
                                    break;
                                case GUGUN:
                                    arrayDongList.clear();
                                    arrayDongList.add(new Address(arrayGuList.get(selGunGuPos).getAddr_seq(),"전체보기"));
                                    for(int i = 0; i < jsonArray.length(); i++) {

                                        arrayDongList.add(new Address(jsonArray.getJSONObject(i)));
                                    }
                                    break;

                            }
                        }
                        else {

                            addrType = Address.AddrType.GUGUN;
                            
                            JSONArray guArray = objectResult.getJSONArray(ParamInfo.GU_LIST);
                            JSONArray dongArray = objectResult.getJSONArray(ParamInfo.DONG_LIST);

                            selSidoPos = 0;
                            selGunGuPos = 0;

                            Address guAddr = null;
                            Address dongAddr = null;

                            for (int i = 0; i < guArray.length(); i++) {

                                guAddr = new Address(guArray.getJSONObject(i));
                                if(i == 0) {

                                    for(int j = 0 ; j < arraySidoList.size(); j++) {
                                        // 선택된 시도 seq와 같으면 해당 pos 저장
                                        if(guAddr.getP_addr_seq() == arraySidoList.get(j).getAddr_seq()) {
                                            selSidoPos = j;
                                            break;
                                        }
                                    }
                                }
                                arrayGuList.add(guAddr);

                            }

                            for (int i = 0; i < dongArray.length(); i++) {

                                dongAddr = new Address(dongArray.getJSONObject(i));
                                if(i == 0 ) {

                                    arrayDongList.add(new Address(dongAddr.getP_addr_seq(),"전체보기"));
                                    for(int j = 0 ; j < arrayGuList.size(); j++) {
                                        if(dongAddr.getP_addr_seq() == arrayGuList.get(j).getAddr_seq()) {
                                            selGunGuPos = j;
                                            break;
                                        }
                                    }
                                }
                                arrayDongList.add(dongAddr);
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                uiHandler.sendEmptyMessage(NetInfo.WHAT_SELECT_ADDRLIST);
            }
            else if(url == NetInfo.SEARCH_STATION) {
                if (!object.isNull("result")) {

                    try {
                        JSONObject objectResult = object.getJSONObject("result");
                        JSONArray jsonArray = objectResult.getJSONArray(ParamInfo.STN_LIST);
                        subWayList.clear();

                        for (int i = 0 ;i < jsonArray.length(); i++) {
                            subWayList.add(new Subway(jsonArray.getJSONObject(i)));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                uiHandler.sendEmptyMessage(NetInfo.WHAT_SEARCH_STATION);
            }
        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            viewMapper.loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Toast.makeText(SearchActivity.this, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    break;

                case NetInfo.WHAT_SELECT_ADDRLIST:

                    StringBuffer selectAddr = new StringBuffer();
                    selectAddr.append((selSidoPos != AppInfo.NOTHING_INT) ? arraySidoList.get(selSidoPos).getName():"");
                    if(selGunGuPos != AppInfo.NOTHING_INT) {
                        selectAddr.append(" ");
                        selectAddr.append(arrayGuList.get(selGunGuPos).getName());

                    }

                    viewMapper.searchTextAddr.setText(selectAddr);

                    switch (addrType) {
                        case SIDO:
                            setIndicatorImage(viewMapper.searchImageGungu);
                            searchAdapter.replace(arrayGuList);
                            addrType = Address.AddrType.GUGUN;
                            break;
                        case GUGUN:
                            setIndicatorImage(viewMapper.searchImageDetail);
                            searchAdapter.replace(arrayDongList);
                            addrType = Address.AddrType.DONG;
                            break;
                    }

                    break;
                case NetInfo.WHAT_SEARCH_STATION:

                    if(isRequesting) {

                        setSubwayList();

                        isRequesting = false;
                    }
                    break;
                default:
                    break;
            }

        }
    };

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================


    final void viewInit() {
        server = new ServerRequest(networkResultListener);

        //시군구선택모드
        viewMapper.toolbarTitle.setText("위치 설정");
        viewMapper.toolbarTitle.setTextColor(getResources().getColor(R.color.main_color));
        viewMapper.toolbarSearchBtn.setSelected(true);

        Intent intent = getIntent();
        if(intent != null) {
            int beforeSeq = intent.getIntExtra(ParamInfo.SEARCH_SEQ,AppInfo.DEFAULT_INT);
            if(beforeSeq != AppInfo.DEFAULT_INT) {
                requestResearchToServer(beforeSeq);
            }
        }

        setIndicatorImage(viewMapper.searchImageSido);
        initSearchGridView();

        //지하철
        viewMapper.wrapperSubway.setVisibility(View.GONE);
        viewMapper.subwayEtSearch.addTextChangedListener(textWatcher);
    }

    final void setData() {

        String[] arraySideList = context.getResources().getStringArray(R.array.array_sido_list);

        for(int i = 0; i < arraySideList.length; i++) {

            arraySidoList.add(new Address(i + 1,arraySideList[i]));
        }

    }

    final void initSearchGridView() {

        searchAdapter = new SearchListAdapter(this, arraySidoList);
        viewMapper.searchGridView.setAdapter(searchAdapter);
        viewMapper.searchGridView.setOnItemClickListener(gridItemClickListener);
    }

    final void setSubwayList() {

        if(subwayAdapter == null) {

            subwayAdapter = new SubwayListAdapter(context, subWayList);
            viewMapper.subwayListView.setAdapter(subwayAdapter);
            viewMapper.subwayListView.setOnItemClickListener(subwayListItemClickListener);
        }
        else {
            subwayAdapter.replace(subWayList);
        }

    }

    final void changeSearchMode(View v){

        if(v == viewMapper.toolbarSearchBtn){
            viewMapper.toolbarSearchBtn.setSelected(true);
            viewMapper.toolbarSubwayBtn.setSelected(false);
            viewMapper.wrapperSubway.setVisibility(View.GONE);
            viewMapper.wrapperSelect.setVisibility(View.VISIBLE);
        }
        else if(v == viewMapper.toolbarSubwayBtn){
            viewMapper.toolbarSearchBtn.setSelected(false);
            viewMapper.toolbarSubwayBtn.setSelected(true);
            viewMapper.wrapperSelect.setVisibility(View.GONE);
            viewMapper.wrapperSubway.setVisibility(View.VISIBLE);
        }

    }

    final void setIndicatorImage(View v) {

        ViewGroup.LayoutParams params1 = viewMapper.searchImageSido.getLayoutParams();
        ViewGroup.LayoutParams params2 = viewMapper.searchImageGungu.getLayoutParams();
        ViewGroup.LayoutParams params3 = viewMapper.searchImageDetail.getLayoutParams();

        params1.width = Utils.DPToPX(this, 36);
        params2.width = Utils.DPToPX(this, 36);
        params3.width = Utils.DPToPX(this, 36);

        viewMapper.searchImageSido.clearColorFilter();
        viewMapper.searchImageGungu.clearColorFilter();
        viewMapper.searchImageDetail.clearColorFilter();

        String textInfo = "";

        if (v == viewMapper.searchImageSido) {
            params1.width = Utils.DPToPX(this, 48);
            viewMapper.searchImageSido.setLayoutParams(params1);
            viewMapper.searchImageSido.setColorFilter(Color.parseColor("#f65560"));

            textInfo = getResources().getString(R.string.search_sido_message);

            addrType = Address.AddrType.SIDO;
        } else if (v == viewMapper.searchImageGungu) {
            params2.width = Utils.DPToPX(this, 48);
            viewMapper.searchImageGungu.setLayoutParams(params2);
            viewMapper.searchImageGungu.setColorFilter(Color.parseColor("#f65560"));

            textInfo = getResources().getString(R.string.search_gungu_message);

            addrType = Address.AddrType.GUGUN;
        } else if (v == viewMapper.searchImageDetail) {
            params3.width = Utils.DPToPX(this, 48);
            viewMapper.searchImageDetail.setLayoutParams(params3);
            viewMapper.searchImageDetail.setColorFilter(Color.parseColor("#f65560"));

            textInfo = getResources().getString(R.string.search_dong_message);

            addrType = Address.AddrType.DONG;
        }

        viewMapper.searchTextIndicator.setText(textInfo);
        viewMapper.searchImageSido.setLayoutParams(params1);
        viewMapper.searchImageGungu.setLayoutParams(params2);
        viewMapper.searchImageDetail.setLayoutParams(params3);
    }

    private void requestGunGulistToServer(int sidoSeq,boolean isSido) {
        viewMapper.loadingView.setVisibility(View.VISIBLE);
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.SEARCH_SEQ,sidoSeq);
        if(isSido) {

            params.put(ParamInfo.SEARCH_IS_SIDO,ParamInfo.Y);
        }else {

            params.put(ParamInfo.SEARCH_IS_SIDO,ParamInfo.N);
        }

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.SELECT_ADDRLIST, params);

    }

    private void requestSubwayListToServer(String schStr) {

        isRequesting = true;

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.SEARCH_TEXT,schStr);

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.SEARCH_STATION, params);
    }

    private void requestResearchToServer(int addrSeq) {

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.SEARCH_SEQ,addrSeq);
        params.put(ParamInfo.IS_RESCH,1);

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.SELECT_ADDRLIST, params);
    }

    //================================================================================
    // XXX Inner Class
    //================================================================================

    private class ViewMapper{
        private RelativeLayout loadingView;
        private StretchImageView toolbarBackBtn;
        private StretchImageView toolbarSearchBtn;
        private StretchImageView toolbarSubwayBtn;
        private TextView toolbarTitle;

        //시군구선택모드
        private LinearLayout wrapperSelect;
        private StretchImageView searchImageSido;
        private StretchImageView searchImageGungu;
        private StretchImageView searchImageDetail;
        private TextView searchTextIndicator;
        private TextView searchTextAddr;
        private GridView searchGridView;

        //지하철모드
        private LinearLayout wrapperSubway;
        private EditText subwayEtSearch;
        private ListView subwayListView;

        public ViewMapper() {
            loadingView = (RelativeLayout) findViewById(R.id.search_loadingview);
            toolbarBackBtn = (StretchImageView) findViewById(R.id.search_toolbar_backbutton);
            toolbarSearchBtn = (StretchImageView) findViewById(R.id.search_toolbar_search);
            toolbarSubwayBtn = (StretchImageView) findViewById(R.id.search_toolbar_subway);
            toolbarTitle = (TextView) findViewById(R.id.search_toolbar_title);

            //시군구모드
            wrapperSelect = (LinearLayout) findViewById(R.id.search_wrapper_select);
            searchImageSido = (StretchImageView) findViewById(R.id.search_image_sido);
            searchImageGungu = (StretchImageView) findViewById(R.id.search_image_gungu);
            searchImageDetail = (StretchImageView) findViewById(R.id.search_image_detail);
            searchTextIndicator = (TextView) findViewById(R.id.search_text_indicator);
            searchTextAddr = (TextView) findViewById(R.id.search_text_address);
            searchGridView = (GridView) findViewById(R.id.search_grid);

            //지하철모드
            wrapperSubway = (LinearLayout) findViewById(R.id.search_wrapper_subway);
            subwayEtSearch = (EditText) findViewById(R.id.search_et_subway);
            subwayListView = (ListView) findViewById(R.id.search_listview_subway);

            toolbarBackBtn.setOnClickListener(SearchActivity.this);
            toolbarSearchBtn.setOnClickListener(SearchActivity.this);
            toolbarSubwayBtn.setOnClickListener(SearchActivity.this);
            searchImageSido.setOnClickListener(SearchActivity.this);
            searchImageGungu.setOnClickListener(SearchActivity.this);
            searchImageDetail.setOnClickListener(SearchActivity.this);
        }
    }

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================

}
