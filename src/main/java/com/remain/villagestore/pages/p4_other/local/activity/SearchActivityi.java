package com.remain.villagestore.pages.p4_other.local.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.common.C;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.ClearEditText;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.info.PreferenceInfo;
import com.remain.villagestore.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by remain on 2016. 5. 16..
 */
public class SearchActivityi extends FragmentActivity implements View.OnClickListener {
    private ViewMapper viewMapper;
    private ArrayList<View> arrayPastWord = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchi);
        viewMapper = new ViewMapper();
        viewInit();
        setPastWord();
    }

    final void viewInit() {
        viewMapper.toolbarTitle.setText("위치 설정");
        viewMapper.toolbarTitle.setTextColor(getResources().getColor(R.color.main_color));
    }


    private void setPastWord() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                arrayPastWord.clear();
                viewMapper.wrapperPastWord.removeAllViews();
                String pastWord = (String) Utils.getPreferenceData(SearchActivityi.this, C.pref.SEARCH_PAST_WORD, "");
                String[] pastWords = pastWord.split(",");
                Log.e("과거 기록 =" + pastWord);
                Log.e("과거 기록 사이즈=" + pastWords.length);
                if (!pastWord.equals("")) {
                    //역순으로변경
                    List<String> list = Arrays.asList(pastWords);
                    Collections.reverse(list);
                    for (int i = 0; i < list.size(); i++) {
                        if (!list.get(i).equals("")) {
                            View v = getLayoutInflater().inflate(R.layout.item_search_past, null);
                            RelativeLayout rel = (RelativeLayout) v.findViewById(R.id.item_search_wrapper);
                            TextView text = (TextView) v.findViewById(R.id.item_search_text_loc);
                            StretchImageView image = (StretchImageView) v.findViewById(R.id.item_search_image_cancel);

                            text.setText(list.get(i));

                            rel.setTag(i);
                            image.setTag(i);

                            rel.setOnClickListener(pastItemClick);
                            image.setOnClickListener(pastItemClick);

                            arrayPastWord.add(v);
                            viewMapper.wrapperPastWord.addView(v);
                        }
                    }
                }
                if(arrayPastWord.size() != 0){
                    viewMapper.wrapperDeleteAll.setVisibility(View.VISIBLE);
                }else {
                    viewMapper.wrapperDeleteAll.setVisibility(View.INVISIBLE);
                }
            }
        });
        
    }

    private void searchAddress() {
        if (viewMapper.etAddress.getText().toString().equals("")) {
            Toast.makeText(this, "주소가 올바르지 않습니다.", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = getIntent();
            intent.putExtra(ParamInfo.ADDRESS, viewMapper.etAddress.getText().toString());
            PreferenceInfo.saveLocationSearch(this, viewMapper.etAddress.getText().toString());
            setResult(RESULT_OK, intent);

            //이전기록에 저장
            String pastMsg = (String) Utils.getPreferenceData(this, C.pref.SEARCH_PAST_WORD, "");

            String[] pastMsgs = pastMsg.split(",");
            String prefMsg = "";
            for (int i = 0; i < pastMsgs.length; i++) {
                if (!pastMsgs[i].equals(viewMapper.etAddress.getText().toString())) {
                    prefMsg = prefMsg + pastMsgs[i] + ",";
                }
            }
            Utils.setPreferenceData(this, C.pref.SEARCH_PAST_WORD, prefMsg + viewMapper.etAddress.getText().toString() + ",");
            finish();
        }

    }


    //최근 검색 지역 리스너
    View.OnClickListener pastItemClick = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            View view = arrayPastWord.get((int) v.getTag());
            TextView text = (TextView) view.findViewById(R.id.item_search_text_loc);

            if (v.getId() == R.id.item_search_wrapper) {
                //이전기록 클릭시
                viewMapper.etAddress.setText("");
                viewMapper.etAddress.setText(text.getText().toString());
            } else if (v.getId() == R.id.item_search_image_cancel) {
                //이전기록 지울시
                String pastWord = (String) Utils.getPreferenceData(SearchActivityi.this, C.pref.SEARCH_PAST_WORD, "");
                if (!pastWord.equals("")) {
                    String[] pastWords = pastWord.split(",");
                    String prefMsg = "";

                    for (int i = 0; i < pastWords.length; i++) {
                        if (!pastWords[i].equals(text.getText().toString())) {
                            prefMsg = prefMsg + pastWords[i] + ",";
                        }
                    }

                    Utils.setPreferenceData(SearchActivityi.this, C.pref.SEARCH_PAST_WORD, prefMsg);
                    setPastWord();
                }
            }
        }
    };

    @Override
    public void onClick(View v) {
        if (v == viewMapper.toolbarBackButton) {
            finish();
        } else if (v == viewMapper.btnSearch) {
            searchAddress();
        } else if (v == viewMapper.btnDeleteAll){
            Utils.setPreferenceData(this, C.pref.SEARCH_PAST_WORD, "");
            setPastWord();
        }

    }

    private class ViewMapper {
        RelativeLayout toolbar;
        TextView toolbarTitle;
        StretchImageView toolbarBackButton;
        RelativeLayout wrapperDeleteAll;
        TextView btnDeleteAll;
        ClearEditText etAddress;
        StretchImageView btnSearch;
        LinearLayout wrapperPastWord;

        public ViewMapper() {
            toolbar = (RelativeLayout) findViewById(R.id.search_toolbar);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
            toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
            wrapperDeleteAll = (RelativeLayout) findViewById(R.id.search_wrapper_delete_all);
            btnDeleteAll = (TextView) findViewById(R.id.search_text_delete_all);
            etAddress = (ClearEditText) findViewById(R.id.search_et_address);
            btnSearch = (StretchImageView) findViewById(R.id.search_image_search);
            wrapperPastWord = (LinearLayout) findViewById(R.id.search_wrapper_past_word);

            //리스너
            toolbarBackButton.setOnClickListener(SearchActivityi.this);
            btnSearch.setOnClickListener(SearchActivityi.this);
            btnDeleteAll.setOnClickListener(SearchActivityi.this);
        }
    }
}
