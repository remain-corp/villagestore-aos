package com.remain.villagestore.pages.p4_other.local.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.dto.Address;

import java.util.ArrayList;

/**
 * Created by remain on 2016. 5. 25..
 */
public class SearchListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Address> dataList = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public SearchListAdapter(Context context, ArrayList<Address> dataList) {
        this.context = context;
        this.dataList = new ArrayList<Address>(dataList);

        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {

            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_search_grid, null);

            holder.dataName = (TextView) convertView.findViewById(R.id.item_search_text);
            //holder.imageSwitch = (StretchImageView) convertView.findViewById(R.id.item_contract_image_click);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        setView(holder, position);

        return convertView;
    }

    private void setView(ViewHolder holder, int position) {

        holder.dataName.setText(dataList.get(position).getName());
    }

    public void replace(ArrayList<Address> addrList) {

        ArrayList<Address> newAddrList = new ArrayList<Address>(addrList);
        dataList.clear();
        dataList.addAll(newAddrList);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        private TextView dataName;

    }
}
