package com.remain.villagestore.pages.p4_other.local.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.dto.Address;
import com.remain.villagestore.dto.Subway;
import com.remain.villagestore.util.Log;

import java.util.ArrayList;

/**
 * Created by remain on 2016. 5. 25..
 */
public class SubwayListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Subway> dataList = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public SubwayListAdapter(Context context, ArrayList<Subway> dataList) {
        this.context = context;
        this.dataList = new ArrayList<Subway>(dataList);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Subway getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {

            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_search_list, null);

            holder.dataName = (TextView) convertView.findViewById(R.id.item_search_text);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        setView(holder, position,convertView);

        return convertView;
    }

    private void setView(ViewHolder holder, int position,View convertView) {
//        holder.linearLayoutLineNum.removeAllViews();
        holder.dataName.setText(dataList.get(position).getName());

        LinearLayout linearLayoutLineNum = (LinearLayout)convertView.findViewById(R.id.item_search_linearlayout_lineview);
        linearLayoutLineNum.removeAllViews();
        Subway subway = getItem(position);
        if(subway != null && subway.getArrayListLineName() != null) {

            TextView textView;
            Drawable background;
            LinearLayout.LayoutParams layoutParams;
                for(int i = 0 ;i < subway.getArrayListLineName().size(); i++ ) {

                    textView = new TextView(context);
                    textView.setText(subway.getArrayListLineName().get(i));
                    textView.setTextColor(Color.WHITE);
                    textView.setPadding(20,20,20,20);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,11);

                    textView.setBackgroundResource(R.drawable.shape_line_bg);

                    background = textView.getBackground();

                    if (background instanceof ShapeDrawable) {
                        ((ShapeDrawable)background).getPaint().setColor(Color.parseColor(subway.getArrayListLineColor().get(i)));
                    } else if (background instanceof GradientDrawable) {
                        ((GradientDrawable)background).setColor(Color.parseColor(subway.getArrayListLineColor().get(i)));
                    } else if (background instanceof ColorDrawable) {
                        ((ColorDrawable)background).setColor(Color.parseColor(subway.getArrayListLineColor().get(i)));
                    }

                    linearLayoutLineNum.addView(textView);
                    layoutParams = (LinearLayout.LayoutParams)textView.getLayoutParams();
                    layoutParams.rightMargin = 20;
                    textView.setLayoutParams(layoutParams);
                }

        }

    }



    public void replace(ArrayList<Subway> subwayList) {

        ArrayList<Subway> newSubList = new ArrayList<Subway>(subwayList);
        dataList.clear();
        dataList.addAll(newSubList);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        private TextView dataName;
       // private StretchImageView imageSwitch;

    }
}
