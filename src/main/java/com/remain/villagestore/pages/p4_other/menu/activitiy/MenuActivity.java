package com.remain.villagestore.pages.p4_other.menu.activitiy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.Coupon;
import com.remain.villagestore.dto.Delivery;
import com.remain.villagestore.dto.StoreMenu;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.DisplayManager;
import com.remain.villagestore.manager.StringTokenizerManager;
import com.remain.villagestore.pages.p4_other.menu.adapter.StoreMenuListAdapter;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ImageUtil;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by woongjaelee on 2015. 11. 21..
 */
public class MenuActivity extends Activity {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Enum
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    // App

    // Content
    private Context context;
    private Intent intent;

    // Widget
    private FrameLayout relativeLayoutBack;
    private TextView textViewTitle;
    private FrameLayout relativeLayoutShare;
    private ListView listView;

    // View
    private ViewMapper viewMapper;

    // Adapter
    private StoreMenuListAdapter storeMenuListAdapter;

    // Array
    private ArrayList<StoreMenu> arrayListMenu;
    private ArrayList<Coupon> arrayListCoupon;
    private ArrayList<View> arrayListDrawerMenu = new ArrayList<>();

    // Lang
    private boolean isGetMenu;
    private Delivery eatery;
    private boolean isEnableDelivery = false;

    // Component
    RelativeLayout loadingView;

    // Util
    private ServerRequest server;
    private DisplayManager displayManager;

    // Interface

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu);
        viewMapper = new ViewMapper();

        checkIntent();
        init();

    }

    @Override
    protected void onDestroy() {

        if (storeMenuListAdapter != null)
            storeMenuListAdapter.recycle();

        super.onDestroy();
    }

    @Override
    public void finish() {

        if (isGetMenu) {

            Intent intent = getIntent();
            intent.putExtra(ParamInfo.STORE_MENU_LIST, arrayListMenu);
            setResult(RESULT_OK, intent);

        }
        super.finish();
    }

    //================================================================================
    // XXX Listener
    //================================================================================
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == loadingView){

            }
            if (v == viewMapper.drawerTextDeleteAll) {
                //드로어 모두삭제
                deleteMenuAll();
            } else if (v == viewMapper.drawerBtnOrder) {
                //드로어 주문하기 클릭
                if (Database.getInstance().getUser(context) != null) {
                    if (Database.getInstance().getUser(context).getAddress() == null || Database.getInstance().getUser(context).getAddress().equals("")) {
                        //주소지가 설정되어 있지 않을때
                        Toast.makeText(context, getResources().getString(R.string.fail_search_address), Toast.LENGTH_SHORT).show();
                    } else {
                        intent = new Intent(context, OrderActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra(ParamInfo.STORE, eatery);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    //로그인이 되어있지 않을경우
                    Toast.makeText(context, getResources().getString(R.string.fail_unlogin_message), Toast.LENGTH_SHORT).show();
                }
            }

            switch (v.getId()) {
                case R.id.activity_menu_relativelayout_back:
                    finish();
                    break;
                case R.id.activity_menu_relativelayout_share:
                    viewMapper.drawerLayout.openDrawer(Gravity.RIGHT);
                    //screenShot();
                    break;

                /*case R.id.activity_menu_image_cart:
                    if (Database.getInstance().getUser(context) != null) {
                        if (Database.getInstance().getUser(context).getAddress() == null || Database.getInstance().getUser(context).getAddress().equals("")) {
                            //주소지가 설정되어 있지 않을때
                            Toast.makeText(context, getResources().getString(R.string.fail_search_address), Toast.LENGTH_SHORT).show();
                        } else {
                            if (isEnableDelivery && eatery != null) {
                                intent = new Intent(context, MenuSelectActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                intent.putExtra(ParamInfo.STORE, eatery);
                                startActivity(intent);
                                finish();
                            }
                        }
                    } else {
                        //로그인이 되어있지 않을때
                        Toast.makeText(context, getResources().getString(R.string.fail_unlogin_message), Toast.LENGTH_SHORT).show();
                    }
                    break;
*/
            }
        }
    };

    StoreMenuListAdapter.OnMenuClickListener menuClickListener = new StoreMenuListAdapter.OnMenuClickListener() {
        @Override
        public void onMenuClick(StoreMenu storeMenu, int position) {
            if (!isEnableDelivery) {
                //배달이 아닐 경우 메뉴 선택 불가
                return;
            }

            if (arrayListDrawerMenu.size() == 0) {
                //장바구니가 하나도 없을경우
                StoreMenu menu = arrayListMenu.get(position);
                menu.setCount(menu.getCount() + 1);

                notifiyDrawerItem();
                toastOn(arrayListMenu.get(position).getName());
            } else {
                boolean isEnabledAdd = true;
                for (View v : arrayListDrawerMenu) {
                    if (position == (int) v.getTag()) {
                        //같은 메뉴가 있을 경우
                        isEnabledAdd = false;
                        StoreMenu menu = arrayListMenu.get(position);
                        menu.setCount(menu.getCount() + 1);
                    }
                }
                if (isEnabledAdd) {
                    //같은 메뉴가 없을 경우
                    StoreMenu menu = arrayListMenu.get(position);
                    menu.setCount(menu.getCount() + 1);
                }
                notifiyDrawerItem();
                toastOn(arrayListMenu.get(position).getName());
            }
        }
    };

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {

                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.READ_MENU) {

                if (!object.isNull(ParamInfo.MENU_LIST)) {

                    try {

                        arrayListMenu = new ArrayList<StoreMenu>();

                        JSONArray array = object.getJSONArray(ParamInfo.MENU_LIST);
                        for (int i = 0; i < array.length(); i++) {
                            arrayListMenu.add(new StoreMenu(array.getJSONObject(i)));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                uiHandler.sendEmptyMessage(NetInfo.WHAT_READ_MENU);

            }

            if (url == NetInfo.SELECT_COUPON) {

                arrayListCoupon = new ArrayList<Coupon>();

                try {
                    if (!object.isNull(ParamInfo.RESULT)) {

                        JSONArray jsonArray = object.getJSONArray(ParamInfo.RESULT);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            arrayListCoupon.add(new Coupon(jsonArray.getJSONObject(i)));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                uiHandler.sendEmptyMessage(NetInfo.WHAT_SELECT_COUPON);


            }

        }
    };

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    private Handler uiHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:

                    Toast.makeText(context, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    break;
                case NetInfo.WHAT_READ_MENU:
                    isGetMenu = true;
                    setAdapter();
                    requestCouponListToServer(getIntent().getIntExtra(ParamInfo.STORE_SEQ, AppInfo.NOTHING_INT));
                    break;
                case NetInfo.WHAT_SELECT_COUPON:
                    if (isEnableDelivery && eatery != null) {
                        eatery.setArrayListCoupon(arrayListCoupon);
                    }
                    break;
            }
        }
    };

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================

    private void checkIntent() {
        Intent intent = getIntent();
        isEnableDelivery = intent.getBooleanExtra("isEnableDelivery", false);
        if (isEnableDelivery) {
            eatery = getIntent().getParcelableExtra(ParamInfo.STORE);
        }
    }

    private void init() {

        context = this;
        displayManager = new DisplayManager(context);

        arrayListMenu = getIntent().getParcelableArrayListExtra(ParamInfo.STORE_MENU_LIST);

        initUI();
        initUISize();
        setEventListener();

        server = new ServerRequest(networkResultListener);
        if (arrayListMenu != null && arrayListMenu.size() > 0) {
            setAdapter();
            requestCouponListToServer(getIntent().getIntExtra(ParamInfo.STORE_SEQ, AppInfo.NOTHING_INT));
        } else {
            requestMenuListToServer(getIntent().getIntExtra(ParamInfo.STORE_SEQ, AppInfo.NOTHING_INT));

        }
    }

    private void initUI() {
        loadingView = (RelativeLayout) findViewById(R.id.menu_loadingview);

        relativeLayoutBack = (FrameLayout) findViewById(R.id.activity_menu_relativelayout_back);
        relativeLayoutShare = (FrameLayout) findViewById(R.id.activity_menu_relativelayout_share);
        textViewTitle = (TextView) findViewById(R.id.activity_menu_textview_title);
        listView = (ListView) findViewById(R.id.activity_menu_listview);

        textViewTitle.setText(getIntent().getStringExtra(ParamInfo.ST_NAME));

        //배달가능에서는
        if (isEnableDelivery) {
            viewMapper.orderText.setText("주문하실 메뉴를 선택 해 주세요.");
        } else {
            relativeLayoutShare.setVisibility(View.GONE);
            viewMapper.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }

        viewMapper.ordercount.setText("0");
        viewMapper.wrapperOrderCount.setAlpha(0);
        viewMapper.menuToast.setAlpha(0);
    }

    private void initUISize() {

        displayManager.changeWidthHeightSameRate(findViewById(R.id.activity_menu_top_panel).getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 96);

        FrameLayout.LayoutParams frameParams = (FrameLayout.LayoutParams) findViewById(R.id.activity_menu_imageview_back_arrow).getLayoutParams();

        displayManager.changeWidthHeightSameRate(frameParams, 58, 49);
        frameParams.leftMargin = displayManager.getScaleSizeSameRate(30);
        frameParams.rightMargin = displayManager.getScaleSizeSameRate(30);

        frameParams = (FrameLayout.LayoutParams) findViewById(R.id.activity_menu_imageview_share).getLayoutParams();
        displayManager.changeWidthHeightSameRate(frameParams, 57, 57);
        frameParams.leftMargin = displayManager.getScaleSizeSameRate(30);
        frameParams.rightMargin = displayManager.getScaleSizeSameRate(30);

        findViewById(R.id.activity_menu_textview_list_top).setPadding(displayManager.getScaleSizeSameRate(18), displayManager.getScaleSizeSameRate(18), displayManager.getScaleSizeSameRate(18), displayManager.getScaleSizeSameRate(18));
        listView.setDividerHeight(displayManager.getScaleSizeSameRate(1));
    }

    private void setEventListener() {
        loadingView.setOnClickListener(clickListener);
        relativeLayoutBack.setOnClickListener(clickListener);
        relativeLayoutShare.setOnClickListener(clickListener);

        viewMapper.drawer.setOnClickListener(clickListener);
        viewMapper.drawerTextDeleteAll.setOnClickListener(clickListener);
        viewMapper.drawerBtnOrder.setOnClickListener(clickListener);
        //btnCart.setOnClickListener(clickListener);

    }

    private void setAdapter() {

        storeMenuListAdapter = new StoreMenuListAdapter(context, arrayListMenu, StoreMenuListAdapter.StoreMenuType.ONLY_MENU, Glide.with(this));
        storeMenuListAdapter.setOnMenuClickListener(menuClickListener);
        listView.setAdapter(storeMenuListAdapter);

        /*
        * 플로팅버튼 제어를 위한 스크롤링 이벤트
        * *//*
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (totalItemCount == 6) {
                    Log.e("scroll" + scrollState);
                }

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                MenuActivity.this.totalItemCount = totalItemCount;

                //isSeeLastitem = (totalItemCount > 0) && ((firstVisibleItem + visibleItemCount) >= totalItemCount);
                int count = totalItemCount - visibleItemCount;
                if (totalItemCount > 6) {
                    Log.e("여기오나1");
                    if (firstVisibleItem >= count && totalItemCount != 0) {
                        //마지막 바닥일때
                        if (!isHideButton) {
                            Log.e("여기오나2");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                                Log.e("여기오나3");
                                btnCart.animate().setDuration(200).translationYBy(cartY).translationY(cartY + Utils.DPToPX(MenuActivity.this, 80)).start();
                                isHideButton = true;
                            } else {
                                Log.e("여기오나4");
                                btnCart.setY(cartY + Utils.DPToPX(MenuActivity.this, 80));
                                isHideButton = true;
                            }
                        }
                    } else {
                        //바닥이 아닐때
                        if (isHideButton) {
                            Log.e("여기오나5");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                                Log.e("여기오나6");
                                btnCart.animate().setDuration(200).translationYBy(cartY - Utils.DPToPX(MenuActivity.this, 80)).translationY(cartY).start();
                                isHideButton = false;

                            } else {
                                Log.e("여기오나7");
                                btnCart.setY(cartY);
                                isHideButton = false;
                            }
                        }
                    }
                }

            }
        });*/

    }

    private void notifiyDrawerItem() {
        eatery.setArrayListMenu(arrayListMenu);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                viewMapper.drawerOrderList.removeAllViews();
                arrayListDrawerMenu.clear();
                int i = 0;
                int totalOrder = 0;
                for (StoreMenu menu : arrayListMenu) {
                    if (menu.getCount() != 0) {
                        View view = getLayoutInflater().inflate(R.layout.item_menu_drawer_list, null);
                        TextView textDrawerMenuName = (TextView) view.findViewById(R.id.item_menu_drawer_text_name);
                        TextView textDrawerMenuPrice = (TextView) view.findViewById(R.id.item_menu_drawer_text_price);
                        TextView textDrawerMenuCount = (TextView) view.findViewById(R.id.item_menu_drawer_text_count);
                        StretchImageView btnDrawerMenuPlus = (StretchImageView) view.findViewById(R.id.item_menu_drawer_image_plus);
                        StretchImageView btnDrawerMenuMinus = (StretchImageView) view.findViewById(R.id.item_menu_drawer_image_minus);

                        textDrawerMenuName.setText(menu.getName());
                        textDrawerMenuPrice.setText(String.format(getResources().getString(R.string.store_menu_price_form),
                                StringTokenizerManager.getInstance().addChar(menu.getPrice(), ",", 3, false)));
                        textDrawerMenuCount.setText(String.valueOf(menu.getCount()));

                        btnDrawerMenuMinus.setTag(i);
                        btnDrawerMenuPlus.setTag(i);
                        view.setTag(i);

                        btnDrawerMenuMinus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                countUpAndDown((int) v.getTag(), false);
                            }
                        });

                        btnDrawerMenuPlus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                countUpAndDown((int) v.getTag(), true);
                            }
                        });
                        arrayListDrawerMenu.add(view);
                        viewMapper.drawerOrderList.addView(view);

                        totalOrder = totalOrder + menu.getCount();
                    }
                    i++;
                }
                viewMapper.drawerTotalPrice.setText(String.format(getResources().getString(R.string.store_menu_price_form),
                        StringTokenizerManager.getInstance().addChar(eatery.getTotalPrice(), ",", 3, false)));
                //툴바 메뉴 개수
                if (totalOrder == 0) {
                    viewMapper.ordercount.setText("0");
                    viewMapper.wrapperOrderCount.setAlpha(0);
                } else if (totalOrder == 1) {
                    viewMapper.ordercount.setText("1");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                        viewMapper.wrapperOrderCount.animate().alpha(1).setDuration(300).start();
                    } else {
                        viewMapper.wrapperOrderCount.setAlpha(1);
                    }
                } else {
                    viewMapper.ordercount.setText(String.valueOf(totalOrder));
                }
            }
        });

    }

    private void toastOn(String menuName) {
        final String msg = menuName + " 이(가)\n장바구니에 추가 되었습니다.";
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                viewMapper.menuToastText.setText(msg);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                    viewMapper.menuToast.animate().alpha(1).setDuration(300).start();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                                viewMapper.menuToast.animate().alpha(0).setDuration(300).start();
                            }
                        }
                    }, 1200);
                } else {
                    viewMapper.menuToast.setAlpha(1);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            viewMapper.menuToast.setAlpha(0);
                        }
                    }, 1500);
                }

            }
        });
    }

    /*
    * 드로어 아이템 카운트 이벤트 처리
    * */
    private void countUpAndDown(int position, boolean isUp) {
        //데이터 처리
        StoreMenu menu = arrayListMenu.get(position);
        if (isUp) {
            menu.setCount(menu.getCount() + 1);
        } else {
            menu.setCount(menu.getCount() - 1);
        }
        notifiyDrawerItem();
    }

    /*
    * 드로어 아이템 카운트 이벤트 처리
    * */
    private void deleteMenuAll() {
        //데이터 처리
        for (StoreMenu menu : arrayListMenu) {
            menu.setCount(0);
        }
        notifiyDrawerItem();
    }

    /**
     * 현재 화면 스크린샷 이미지 얻어오기
     */
    private void screenShot() {
        try {

            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            FileOutputStream outputStream = new FileOutputStream(getScreenShotFile());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();

            sendScreenShot();

        } catch (FileNotFoundException e) {
            Log.d("FileNotFoundException:", e.getMessage());
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }

    private File getScreenShotFile() {
        File file = new File(ImageUtil.getInstance().getImageFolderPath(AppInfo.FILE_PATH) + "/screenshot" + ".jpeg");
        return file;

    }

    /**
     * 스크린샷을 보냄
     */
    private void sendScreenShot() {

        intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(getScreenShotFile()));
        intent.setType("image/*");
        startActivity(intent);
    }

    private void requestMenuListToServer(int storeSeq) {

        loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.ST_SEQ, storeSeq);

        server.execute(NetInfo.READ_MENU, params);

    }

    private void requestCouponListToServer(int storeSeq) {

        loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.STORE_SEQ, storeSeq);

        server.execute(NetInfo.SELECT_COUPON, params);

    }

    //================================================================================
    // XXX Inner Class
    //================================================================================

    private class ViewMapper {
        //액티비티
        private RelativeLayout wrapperOrderCount;
        private TextView ordercount;
        private TextView orderText;
        private LinearLayout menuToast;
        private TextView menuToastText;

        //드로어
        private DrawerLayout drawerLayout;
        private RelativeLayout drawer;
        private TextView drawerTextDeleteAll;
        private StretchImageView drawerBtnOrder;
        private TextView drawerToolbarTitle;
        private LinearLayout drawerOrderList;
        private TextView drawerTotalPrice;

        public ViewMapper() {
            //액티비티
            wrapperOrderCount = (RelativeLayout) findViewById(R.id.menu_wrapper_ordercount);
            ordercount = (TextView) findViewById(R.id.menu_text_ordercount);
            orderText = (TextView) findViewById(R.id.activity_menu_textview_list_top);
            menuToast = (LinearLayout) findViewById(R.id.menu_toast);
            menuToastText = (TextView) findViewById(R.id.menu_toast_text);

            //드로어
            drawerLayout = (DrawerLayout) findViewById(R.id.activity_menu_drawerlayout);
            drawer = (RelativeLayout) findViewById(R.id.manu_right_drawer);
            drawerTextDeleteAll = (TextView) drawer.findViewById(R.id.menu_text_delete_all);
            drawerBtnOrder = (StretchImageView) drawer.findViewById(R.id.menu_image_order);
            drawerToolbarTitle = (TextView) drawer.findViewById(R.id.toolbar_title);
            drawerOrderList = (LinearLayout) drawer.findViewById(R.id.menu_wrapper_orderlist);
            drawerTotalPrice = (TextView) drawer.findViewById(R.id.menu_text_totalprice);

            drawerToolbarTitle.setText("장바구니");
            drawerToolbarTitle.setTextColor(getResources().getColor(R.color.main_color));

        }

    }

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================

}
