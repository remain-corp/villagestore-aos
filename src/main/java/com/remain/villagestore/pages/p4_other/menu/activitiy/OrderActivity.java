package com.remain.villagestore.pages.p4_other.menu.activitiy;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.component.CircleIndicator;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.Coupon;
import com.remain.villagestore.dto.Delivery;
import com.remain.villagestore.dto.StoreMenu;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.StringTokenizerManager;
import com.remain.villagestore.pages.p3_stores.adapter.CouponAdapter;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;
import com.remain.villagestore.component.image.StretchImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by remain on 2016. 5. 6..
 */
public class OrderActivity extends FragmentActivity implements View.OnClickListener {
    private ViewMapper viewMapper;
    private CouponAdapter couponAdapter;
    private ArrayList<Coupon> arrayListCoupon;
    private ArrayList<StoreMenu> arrayListMenus;
    private ServerRequest server;
    private Delivery eatery;
    private User me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        viewMapper = new ViewMapper();
        checkIntent();
        viewInit();
    }

    final void checkIntent() {
        eatery = getIntent().getParcelableExtra(ParamInfo.STORE);
    }

    final void viewInit() {
        viewMapper.toolbarTitle.setText("주문하기");

        server = new ServerRequest(networkResultListener);
        me = User.getMyInfo(this);

        // 쿠폰을 받아왔으면 바로 뿌리기
        if (eatery.getArrayListCoupon() != null && eatery.getArrayListCoupon().size() > 0) {
            arrayListCoupon = eatery.getArrayListCoupon();
            setPagerAdapter();
        } else {
            requestCouponListToServer(eatery.getSeq());
        }

        //주문금액
        if (eatery.getPrice() != 0) {
            viewMapper.textOrderCost.setText(String.format(getResources().getString(R.string.store_menu_price_form),
                    StringTokenizerManager.getInstance().addChar(eatery.getPrice(), ",", 3, false)));
        } else {
            viewMapper.textOrderCost.setText(String.format(getResources().getString(R.string.store_menu_price_form),
                    StringTokenizerManager.getInstance().addChar(eatery.getTotalPrice(), ",", 3, false)));
        }

        //주문메뉴
        if (eatery.getArrayListMenu() != null) {
            arrayListMenus = eatery.getArrayListMenu();
        }

        setMenus();
    }

    final void setMenus() {
        String menu = "";
        String price = "";
        if (eatery.getArrayListMenu() != null) {
            Log.e("ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ");
            for (StoreMenu storeMenu : arrayListMenus) {
                if (storeMenu.getCount() != 0) {
                    String oneMenu = storeMenu.getName();
                    if (oneMenu.length() > 12) {
                        oneMenu = oneMenu.substring(0, 12) + "..";
                        menu = menu + oneMenu + " X " + storeMenu.getCount() + "\n";
                    } else {
                        menu = menu + oneMenu + " X " + storeMenu.getCount() + "\n";
                    }
                    int pri = storeMenu.getPrice() * storeMenu.getCount();
                    String pric = String.format(getResources().getString(R.string.store_menu_price_form),
                            StringTokenizerManager.getInstance().addChar(pri, ",", 3, false));
                    price = price + pric + "\n";
                }
            }
            Log.e("ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ");
        }
        viewMapper.textOrderListMenu.setText(menu);
        viewMapper.textOrderListPrice.setText(price);
    }

    private void setPagerAdapter() {

        couponAdapter = new CouponAdapter(this, arrayListCoupon);
        viewMapper.viewPager.setOffscreenPageLimit(5);
        viewMapper.viewPager.setAdapter(couponAdapter);
        viewMapper.circleIndicator.setViewPager(viewMapper.viewPager);

        eatery.setPrice(eatery.getTotalPrice());
        if (arrayListCoupon.size() > 0) {

            eatery.setCouponInfo(arrayListCoupon.get(0).getCouponDetail());
            eatery.setCouponLimit(arrayListCoupon.get(0).getEndDate());

        }
        Database.getInstance().checkStore(this, eatery);
    }

    /**
     * 쿠폰 리스트 요청
     */
    private void requestCouponListToServer(int storeSeq) {

        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.STORE_SEQ, storeSeq);

        server.execute(NetInfo.SELECT_COUPON, params);
    }

    /**
     * 쿠폰 사용 요청
     */
    private void requestUseCouponToServer(int couponSeq, String useYN) {

//        progressDialog = TransProgressDialog.show(context,false);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.CP_SEQ, couponSeq);
        params.put(ParamInfo.CNT, useYN);

        server.execute(NetInfo.DISCOUNT_COUPON, params);

    }

    /**
     * 주문 내용 전송
     */
    private void requestInsertUseCouponToServer(int couponSeq, String userLoc, int price, int userSeq) {

        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.CP_SEQ, couponSeq);
        params.put(ParamInfo.USE_LOC, userLoc);
        params.put(ParamInfo.PRICE, price);
        params.put(ParamInfo.USER_SEQ, userSeq);

        server.execute(NetInfo.INSERT_COUPONUSE, params);
    }

    //전화 횟수 저장
    private void requestCallCntToServer(int storeSeq) {

        LinkedHashMap<String, Object> params = new LinkedHashMap<>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.ST_SEQ, storeSeq);

        server.execute(NetInfo.CALL_CNT, params);
    }

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {

                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }
            if (url == NetInfo.SELECT_COUPON) {
                Log.e("NetInfo.SELECT_COUPON");
                try {

                    arrayListCoupon = new ArrayList<Coupon>();
                    JSONArray jsonArray = object.getJSONArray(ParamInfo.RESULT);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        arrayListCoupon.add(new Coupon(jsonArray.getJSONObject(i)));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                uiHandler.sendEmptyMessage(NetInfo.WHAT_SELECT_COUPON);

            } else if (url == NetInfo.DISCOUNT_COUPON) {
                Log.e("NetInfo.DISCOUNT_COUPON");
//                uiHandler.sendEmptyMessage(NetInfo.WHAT_DISCOUNT_COUPON);
            } else if (url == NetInfo.INSERT_COUPONUSE) {
                Log.e("NetInfo.INSERT_COUPONUSE");
                // 쿠폰 사용내역 전송
                uiHandler.sendEmptyMessage(NetInfo.WHAT_INSERT_COUPONUSE);

            } else if (url == NetInfo.CALL_CNT) {
                Log.e("NetInfo.CALL_CNT");
                uiHandler.sendEmptyMessage(NetInfo.WHAT_CALL_CNT);

            }
        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            viewMapper.loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Log.e("네트워크에러");
                    //Toast.makeText(OrderActivity.this, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    break;

                case NetInfo.WHAT_SELECT_COUPON:

                    setPagerAdapter();

                    // 쿠폰 리스트 받으면 바로 사용 처리
//                    for(int i = 0 ;i <  arrayListCoupon.size(); i++) {
//                        Coupon coupon = arrayListCoupon.get(i);
//                        requestUseCouponToServer(coupon.getCouponSeq(), ParamInfo.Y);
//                    }

                    break;
                case NetInfo.WHAT_DISCOUNT_COUPON:

                    break;
                case NetInfo.WHAT_CALL_CNT:
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + eatery.getPhoneNumber()));
                    startActivity(intent);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    },2000);

                    break;
                case NetInfo.WHAT_INSERT_COUPONUSE:

                    Log.e("handleMessage = WHAT_INSERT_COUPONUSE");
                    /*intent = new Intent(context , SavingCompleteActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra(ParamInfo.ADDRESS,textViewLoc.getText().toString());
                    startActivity(intent);
                    finish();*/
                    break;
            }
        }
    };

    /*
    * 쿠폰 발급 내용 저장
    * */
    private void insertToServerCouponInfo() {
        int couponSeq = arrayListCoupon.get(viewMapper.viewPager.getCurrentItem()).getCouponSeq();
        String strPrice = viewMapper.textOrderCost.getText().toString().replace("원", "").trim();
        strPrice = strPrice.replace(",", "").trim();
        int price = 0;
        if (strPrice != null && !strPrice.equals("")) {
            try {
                price = Integer.parseInt(strPrice);
                Log.e(price+"");
            } catch (NumberFormatException e) {
                Toast.makeText(this, "가격이 올바르지 않습니다.", Toast.LENGTH_LONG).show();
            }
        }
        if (arrayListCoupon != null && arrayListCoupon.size() > 0) {
            //쿠폰이 있을경우
            requestInsertUseCouponToServer(couponSeq, me.getAddress(), price, me.getSeq());
        } else {
            //쿠폰이 없을경우
            requestInsertUseCouponToServer(0, me.getAddress(), price, me.getSeq());
        }

    }

    @Override
    public void onClick(View v) {
        if(v == viewMapper.loadingView){

        }
        if (v == viewMapper.toolbarBackButton) {
            finish();
        } else if (v == viewMapper.btnCall) {
            //쿠폰이 있을경우
            requestUseCouponToServer(arrayListCoupon.get(viewMapper.viewPager.getCurrentItem()).getCouponSeq(), "Y");
            //쿠폰 발급 내용 저장
            insertToServerCouponInfo();
            //d_store통화량
            requestCallCntToServer(eatery.getSeq());

        }
    }

    private class ViewMapper {
        //툴바
        private RelativeLayout toolbar;
        private TextView toolbarTitle;
        private StretchImageView toolbarBackButton;

        //로딩뷰
        private RelativeLayout loadingView;

        //뷰페이져
        private ViewPager viewPager;
        private CircleIndicator circleIndicator;

        //주문정보
        private TextView textOrderCost;
        private TextView textOrderListMenu;
        private TextView textOrderListPrice;

        //주문하기
        private StretchImageView btnCall;

        public ViewMapper() {
            toolbar = (RelativeLayout) findViewById(R.id.order_toolbar);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
            toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);

            loadingView = (RelativeLayout) findViewById(R.id.order_loadingview);

            viewPager = (ViewPager) findViewById(R.id.order_viewpager_coup);
            circleIndicator = (CircleIndicator) findViewById(R.id.order_indicator_coup);

            textOrderCost = (TextView) findViewById(R.id.order_text_order_cost);
            textOrderListMenu = (TextView) findViewById(R.id.order_text_list_menu);
            textOrderListPrice = (TextView) findViewById(R.id.order_text_list_price);

            btnCall = (StretchImageView) findViewById(R.id.order_image_call);

            toolbarBackButton.setOnClickListener(OrderActivity.this);
            loadingView.setOnClickListener(OrderActivity.this);
            btnCall.setOnClickListener(OrderActivity.this);
        }
    }
}
