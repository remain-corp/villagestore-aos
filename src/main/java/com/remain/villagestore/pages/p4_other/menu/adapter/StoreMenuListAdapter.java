package com.remain.villagestore.pages.p4_other.menu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.remain.villagestore.R;
import com.remain.villagestore.dto.StoreMenu;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.manager.DisplayManager;
import com.remain.villagestore.manager.StringTokenizerManager;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.RecycleUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by woongjaelee on 2015. 11. 21..
 */
public class StoreMenuListAdapter extends BaseAdapter {

    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Enum
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    // App

    // Content
    private Context context;

    // Widget


    // View
    private LayoutInflater layoutInflater;

    // Adapter

    // Array
    private ArrayList<StoreMenu> arrayListMenu;
    private List<WeakReference<View>> mRecycleList = new ArrayList<WeakReference<View>>();

    // Lang

    // Component

    // Util
    //private DisplayImageOptions options;
    private DisplayManager displayManager;
    private RequestManager glideRequestManager;

    // Interface
    private OnCountClickListener countClickListener;
    private OnMenuClickListener menuClickListener;

    // Enum
    private StoreMenuType storeMenuType;

    //================================================================================
    // XXX Enum
    //================================================================================

    public enum StoreMenuType {
        /* 메뉴판 */
        ONLY_MENU,
        /* 메뉴선택 */
        CONTAIN_COUNT,
    }

    //================================================================================
    // XXX Constructor
    //================================================================================

    public StoreMenuListAdapter(Context context, ArrayList<StoreMenu> arrayListMenu, StoreMenuType storeMenuType, RequestManager glideRequestManager) {

        this.context = context;
        this.glideRequestManager = glideRequestManager;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        displayManager = new DisplayManager(context);

        if (arrayListMenu != null) {

//            this.arrayListMenu = new ArrayList<StoreMenu>(arrayListMenu);
            this.arrayListMenu = arrayListMenu;
        } else {

            this.arrayListMenu = new ArrayList<StoreMenu>();

        }

        this.storeMenuType = storeMenuType;


        //options = ImageLoaderManager.getInstance(context).getDisplayImageOptionCache(R.drawable.image_default_photo);
    }

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    public int getCount() {
        return arrayListMenu.size();
    }

    @Override
    public StoreMenu getItem(int position) {
        return arrayListMenu.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.content_store_menu_list, null);
            holder = new ViewHolder();
            init(convertView, holder, position);
            convertView.setTag(holder);
            mRecycleList.add(new WeakReference<View>(convertView));

        } else {

            holder = (ViewHolder) convertView.getTag();

        }

        setView(holder, position);

        return convertView;
    }


    //================================================================================
    // XXX Listener
    //================================================================================

    //================================================================================
    // XXX Thread, Handler
    //================================================================================

    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    public void setOnCountClickListener(OnCountClickListener countClickListener) {
        this.countClickListener = countClickListener;
    }

    public void setOnMenuClickListener(OnMenuClickListener menuClickListener) {
        this.menuClickListener = menuClickListener;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================

    private void init(View convertView, ViewHolder holder, int position) {

        initUI(convertView, holder);
        initUISize(convertView, holder);

    }

    private void initUI(View convertView, ViewHolder holder) {

        holder.menuWrapper = (LinearLayout) convertView.findViewById(R.id.content_store_menu_wrapper);
        holder.imageViewThum = (ImageView) convertView.findViewById(R.id.content_store_menu_imageview_thum);
        holder.textViewName = (TextView) convertView.findViewById(R.id.content_store_menu_textview_name);
        holder.textViewCount = (TextView) convertView.findViewById(R.id.content_store_menu_textview_count);
        holder.textViewPrice = (TextView) convertView.findViewById(R.id.content_store_menu_textview_price);
        holder.imageViewMinus = (ImageView) convertView.findViewById(R.id.content_store_menu_imageview_minus);
        holder.imageViewPlus = (ImageView) convertView.findViewById(R.id.content_store_menu_imageview_plue);
        holder.linearLayoutCount = (LinearLayout) convertView.findViewById(R.id.content_store_menu_linearlayout_count);

    }

    private void initUISize(View convertView, ViewHolder holder) {

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) holder.imageViewThum.getLayoutParams();
        displayManager.changeWidthHeightSameRate(linearParams, 230, 140);
        linearParams.setMargins(displayManager.getScaleSizeSameRate(14), displayManager.getScaleSizeSameRate(22), displayManager.getScaleSizeSameRate(14), displayManager.getScaleSizeSameRate(22));

        linearParams = (LinearLayout.LayoutParams) holder.textViewName.getLayoutParams();
        linearParams.leftMargin = displayManager.getScaleSizeSameRate(28);
        linearParams.rightMargin = displayManager.getScaleSizeSameRate(28);

        linearParams = (LinearLayout.LayoutParams) holder.linearLayoutCount.getLayoutParams();
        linearParams.setMargins(0, displayManager.getScaleSizeSameRate(38), displayManager.getScaleSizeSameRate(48), displayManager.getScaleSizeSameRate(38));

        displayManager.changeWidthHeightSameRate(holder.imageViewMinus.getLayoutParams(), 39, 39);
        displayManager.changeWidthHeightSameRate(holder.textViewCount.getLayoutParams(), 86, ViewGroup.LayoutParams.MATCH_PARENT);

        displayManager.changeWidthHeightSameRate(holder.imageViewPlus.getLayoutParams(), 39, 39);


        if (storeMenuType == StoreMenuType.ONLY_MENU) {

            holder.imageViewThum.setVisibility(View.VISIBLE);
            holder.linearLayoutCount.setVisibility(View.GONE);
            displayManager.changeWidthHeightSameRate(holder.textViewName.getLayoutParams(), 230, ViewGroup.LayoutParams.WRAP_CONTENT);


        } else {

            holder.imageViewThum.setVisibility(View.GONE);
            holder.linearLayoutCount.setVisibility(View.VISIBLE);
            displayManager.changeWidthHeightSameRate(holder.textViewName.getLayoutParams(), 280, ViewGroup.LayoutParams.WRAP_CONTENT);

        }

        linearParams = (LinearLayout.LayoutParams) holder.textViewPrice.getLayoutParams();
        linearParams.rightMargin = displayManager.getScaleSizeSameRate(36);

    }

    private void setEventListener(View convertView, ViewHolder holder) {

    }

    private void setView(final ViewHolder holder, final int position) {

        final StoreMenu storeMenu = getItem(position);
        Log.e("이즈 디폴트" + storeMenu.getIsDefault());

        holder.menuWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menuClickListener != null) {
                    menuClickListener.onMenuClick(storeMenu, position);
                }
            }
        });

        if (storeMenuType == StoreMenuType.ONLY_MENU) {

            if (storeMenu.getImgPath() != null && !storeMenu.getImgPath().equals("")) {
                //ImageLoader.getInstance().displayImage(ImageLoaderManager.URIS_WEB + AppInfo.IMG_MENU_PATH + storeMenu.getImgPath(), holder.imageViewThum, options);
                Log.e("이미지경로 = " + storeMenu.getImgPath());
                /*
                * 디폴트이미지인지 선택인지 구분하여 경로분기
                * */
                String imagePath = "";
                if (storeMenu.getIsDefault().equals("S")) {
                    imagePath = "http://" + AppInfo.IMG_MENU_PATH + storeMenu.getImgPath();
                } else {
                    imagePath = "http://" + AppInfo.IMG_D_MENU_PATH + storeMenu.getImgPath();
                }

                glideRequestManager
                        .load(imagePath)
                        .error(R.drawable.image_default_photo)
                        .thumbnail(0.1f)
                        .into(holder.imageViewThum);
            } else {
                //ImageLoader.getInstance().displayImage(ImageLoaderManager.URIS_DRAWABLES + R.drawable.image_default_photo, holder.imageViewThum, options);
                holder.imageViewThum.setImageResource(R.drawable.image_default_photo);
            }
        } else {

            holder.textViewCount.setText(storeMenu.getCount() + "");

            holder.imageViewMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (storeMenu.getCount() != 0) {

                        storeMenu.setCount(storeMenu.getCount() - 1);
                        holder.textViewCount.setText(storeMenu.getCount() + "");

                        if (countClickListener != null) {
                            countClickListener.onMinusClick();
                        }
                    }
                }
            });

            holder.imageViewPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    storeMenu.setCount(storeMenu.getCount() + 1);
                    holder.textViewCount.setText(storeMenu.getCount() + "");

                    if (countClickListener != null) {
                        countClickListener.onPlusClick();
                    }
                }
            });


        }

        holder.textViewName.setText(storeMenu.getName());
        holder.textViewPrice.setText(String.format(context.getResources().getString(R.string.store_menu_price_form),
                StringTokenizerManager.getInstance().addChar(storeMenu.getPrice(), ",", 3, false)));

    }

    public void resetView() {

        for (StoreMenu storeMenu : arrayListMenu) {

            storeMenu.setCount(0);

        }

        notifyDataSetChanged();
    }

    public void recycle() {

        RecycleUtil.recursiveRecycle(mRecycleList);
    }


    //================================================================================
    // XXX Inner Class
    //================================================================================

    public class ViewHolder {

        private LinearLayout menuWrapper;
        private ImageView imageViewThum;
        private TextView textViewName;
        private TextView textViewCount;
        private TextView textViewPrice;
        private ImageView imageViewMinus;
        private ImageView imageViewPlus;
        private LinearLayout linearLayoutCount;

    }

    //================================================================================
    // XXX Interface
    //================================================================================

    public interface OnCountClickListener {
        public void onMinusClick();

        public void onPlusClick();
    }

    public interface OnMenuClickListener {
        public void onMenuClick(StoreMenu storeMenu, int position);
    }

    //================================================================================
    // XXX Debug
    //================================================================================

}
