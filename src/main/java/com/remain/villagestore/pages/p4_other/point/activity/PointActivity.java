package com.remain.villagestore.pages.p4_other.point.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.image.CircleImageView;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.OrderInfo;
import com.remain.villagestore.dto.Store;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.StringTokenizerManager;
import com.remain.villagestore.pages.p4_other.point.adapter.PointPageAdapter;
import com.remain.villagestore.pages.p4_other.point.fragment.PointFragment;
import com.remain.villagestore.pages.p4_other.setting.activity.UserSettingActivity;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by remain on 2016. 5. 9..
 */
public class PointActivity extends FragmentActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private ViewMapper viewMapper;
    private PointPageAdapter pageAdapter = new PointPageAdapter(getSupportFragmentManager());
    private ArrayList<PointFragment> coupons = new ArrayList<>();
    private ArrayList<CircleImageView> indicators = new ArrayList<>();

    private ArrayList<OrderInfo> arrayListOrderInfo = new ArrayList<>();
    private ArrayList<Store> arrayListStore = new ArrayList<>();
    private ArrayList<String> arrayListImagePath = new ArrayList<>();
    private ArrayList<String> arrayListCouponInfo = new ArrayList<>();

    private ServerRequest server;

    private int currentPosition = 0;
    private boolean isVisibleConfirmWrapper = false;
    private boolean isSavedPoint = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point);

        viewMapper = new ViewMapper();
        viewInit();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isVisibleConfirmWrapper) {
                viewMapper.wrapperConfirm.setVisibility(View.GONE);
                isVisibleConfirmWrapper = false;
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    final void viewInit() {
        viewMapper.toolbarTitle.setText("포인트 적립");
        viewMapper.toolbarTitle.setTextColor(getResources().getColor(R.color.main_color));

        server = new ServerRequest(networkResultListener);
        Log.e("셀렉트1");

        requestCouponListToServer(Database.getInstance().getUser(this).getSeq());

    }

    final void setViewPager() {
        pageAdapter.setFragments(coupons);
        viewMapper.viewPager.setAdapter(pageAdapter);
        viewMapper.viewPager.addOnPageChangeListener(this);

        if (arrayListOrderInfo.size() == 0) {
            //주문 리스트가 없다면
            viewMapper.wrapperPoint.setVisibility(View.GONE);
            viewMapper.wrapperContentNo.setVisibility(View.VISIBLE);
        } else {
            viewMapper.wrapperPoint.setVisibility(View.VISIBLE);
            viewMapper.wrapperContentNo.setVisibility(View.GONE);
        }

    }

    private void setIndicators() {
        viewMapper.indicatorWrapper.removeAllViews();
        for (int i = 0; i < coupons.size(); i++) {
            View view = getLayoutInflater().inflate(R.layout.item_indicator_circle, null);
            CircleImageView indicator = (CircleImageView) view.findViewById(R.id.item_indicator_circle);
            ViewGroup.LayoutParams params = indicator.getLayoutParams();
            if (i == 0) {
                indicator.setImageResource(R.color.main_color);
                params.width = Utils.DPToPX(this, 20);
            } else {
                indicator.setImageResource(R.drawable.ico_indicator_black);
            }
            indicator.setLayoutParams(params);
            indicators.add(indicator);
            viewMapper.indicatorWrapper.addView(view);
        }
    }


    public void visibleConfirmWrapper(boolean isSavedPoint) {
        this.isSavedPoint = isSavedPoint;

        if (isSavedPoint) {
            viewMapper.textTitle.setText("직원 적립 확인");
            viewMapper.textMsg.setText("적립처리를 하시겠습니까?");
        } else {
            viewMapper.textTitle.setText("주문금액 변경");
            viewMapper.textMsg.setText("금액을 변경 하시겠습니까?");
        }
        viewMapper.wrapperConfirm.setVisibility(View.VISIBLE);
        isVisibleConfirmWrapper = true;
    }

    private void changePage(int position) {
        for (int i = 0; i < indicators.size(); i++) {
            CircleImageView indicator = indicators.get(i);
            ViewGroup.LayoutParams params = indicator.getLayoutParams();
            if (i == position) {
                indicator.setImageResource(R.color.main_color);
                params.width = Utils.DPToPX(this, 20);
                indicator.setLayoutParams(params);
            } else {
                indicator.setImageResource(R.drawable.ico_indicator_black);
                params.width = Utils.DPToPX(this, 15);
                indicator.setLayoutParams(params);
            }
        }
    }

    private void inputNum(int num) {
        String alreadyNum = (viewMapper.textNum.getText().toString()).replace(",", "");
        if (alreadyNum.length() < 9) {
            if (isSavedPoint) {
                if (num == 10) {
                    if (!viewMapper.textNum.getText().toString().equals("")) {
                        String inputNum = alreadyNum.substring(0, alreadyNum.length() - 1);
                        viewMapper.textNum.setText(inputNum);
                    }
                } else {
                    String inputNum = alreadyNum + num;
                    viewMapper.textNum.setText(inputNum);
                }
            } else {
                if (num == 10) {
                    if (!viewMapper.textNum.getText().toString().equals("")) {
                        String inputNum = alreadyNum.substring(0, alreadyNum.length() - 1);
                        if (inputNum.equals("")) {
                            viewMapper.textNum.setText("");
                        } else {
                            String price = StringTokenizerManager.getInstance().addChar(inputNum, ",", 3, false);
                            viewMapper.textNum.setText(price);
                        }
                    }
                } else {
                    String inputNum = StringTokenizerManager.getInstance().addChar(alreadyNum + num, ",", 3, false);
                    viewMapper.textNum.setText(inputNum);
                }
            }
        } else {
            //10억이상이때 int 자료형으로 변환 불가능
            if(isSavedPoint){
                Toast.makeText(this, "인증번호를 확인해 주세요.", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, "금액을 확인해 주세요.", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void clickConfrimItem(View v) {
        if (v == viewMapper.wrapperConfirm) {
            //뒤에 이벤트 가져오기
        } else if (v == viewMapper.btnConfirmPad0) {
            inputNum(0);
        } else if (v == viewMapper.btnConfirmPad1) {
            inputNum(1);
        } else if (v == viewMapper.btnConfirmPad2) {
            inputNum(2);
        } else if (v == viewMapper.btnConfirmPad3) {
            inputNum(3);
        } else if (v == viewMapper.btnConfirmPad4) {
            inputNum(4);
        } else if (v == viewMapper.btnConfirmPad5) {
            inputNum(5);
        } else if (v == viewMapper.btnConfirmPad6) {
            inputNum(6);
        } else if (v == viewMapper.btnConfirmPad7) {
            inputNum(7);
        } else if (v == viewMapper.btnConfirmPad8) {
            inputNum(8);
        } else if (v == viewMapper.btnConfirmPad9) {
            inputNum(9);
        } else if (v == viewMapper.btnConfirmPadX) {
            inputNum(10);
        } else if (v == viewMapper.btnConfirmOk) {
            if (isSavedPoint) {
                //포인트 적립시
                PointFragment currentCoupon = coupons.get(currentPosition);
                if (viewMapper.textNum.getText().equals(String.valueOf(currentCoupon.getStoreAuthKey()))) {
                    String price = currentCoupon.getOrderPrice();
                    price = price.replace("주문금액", "");
                    price = price.replace(":", "");
                    price = price.replace("원", "").trim();
                    Log.e("fd = " + price);
                    requestSavePointToServer(Database.getInstance().getUser(this).getSeq(), arrayListOrderInfo.get(currentPosition).getCouponSeq(), Integer.parseInt(price));
                } else {
                    Toast.makeText(this, "인증번호가 올바르지 않습니다.", Toast.LENGTH_SHORT).show();
                }
            } else {
                //금액 변경시
                if (!viewMapper.textNum.getText().equals("")) {
                    coupons.get(currentPosition).changeMoney(viewMapper.textNum.getText().toString());
                } else {
                    Toast.makeText(this, "금액이 올바르지 않습니다.", Toast.LENGTH_SHORT).show();
                }
            }
            viewMapper.textNum.setText("");
            viewMapper.wrapperConfirm.setVisibility(View.GONE);
            isVisibleConfirmWrapper = false;
        } else if (v == viewMapper.btnConfirmCancel) {
            viewMapper.textNum.setText("");
            viewMapper.wrapperConfirm.setVisibility(View.GONE);
            isVisibleConfirmWrapper = false;
        }
    }

    /**
     * 주문 쿠폰 정보 가져오기
     */
    private void requestCouponListToServer(int user_seq) {

        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_SEQ, user_seq);
        Log.e("셀렉트2");
        server.execute(NetInfo.IP_ADDRESS1, NetInfo.SELECT_ORDER, params);
    }

    /**
     * 결제 후 포인트 업데이트
     */
    private void requestSavePointToServer(int user_seq, int coupon_seq, int finalPrice) {

        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_SEQ, user_seq);
        params.put(ParamInfo.CP_USE_SEQ, coupon_seq);
        params.put(ParamInfo.FINAL_PRICE, finalPrice);

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.SAVE_POINT, params);

    }


    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.SELECT_ORDER) {
                Log.e("셀렉트3");
                Log.e("네트워크정보 = " + ParamInfo.RESULT);

                try {
                    if (object.getJSONArray(ParamInfo.RESULT) != null) {
                        JSONArray jsonArray = object.getJSONArray(ParamInfo.RESULT);

                        //JSONObject objectStore = object.getJSONObject(ParamInfo.RESULT);

                        //주문내역이 있다면
                        arrayListOrderInfo.clear();
                        arrayListStore.clear();
                        arrayListImagePath.clear();
                        arrayListCouponInfo.clear();

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject objectDetail = jsonArray.getJSONObject(i);

                            arrayListOrderInfo.add(new OrderInfo(objectDetail.getJSONObject("order_info")));
                            arrayListStore.add(new Store(objectDetail.getJSONObject("store")));
                            arrayListImagePath.add(((JSONObject) objectDetail.getJSONArray("img").get(0)).getString("filename"));
                            arrayListCouponInfo.add(objectDetail.getString("cp_info"));

                            Log.e(arrayListOrderInfo.get(i).getOrderDate());

                        }

                    } else {
                        Log.e("주문내역 없음");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                uiHandler.sendEmptyMessage(NetInfo.WHAT_SELECT_ORDER);
            } else if (url == NetInfo.SAVE_POINT) {

                try {
                    if (object.getString(ParamInfo.RESULT) != null) {
                        String result = object.getString(ParamInfo.RESULT);
                        if (result.equals("SUCCESS")) {
                            uiHandler.sendEmptyMessage(NetInfo.WHAT_SAVE_POINT);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return;
            }


        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            viewMapper.loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Toast.makeText(PointActivity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                    break;
                case NetInfo.WHAT_SAVE_POINT:

                    Toast.makeText(PointActivity.this, "포인트적립이 되었습니다.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PointActivity.this, SavingCompleteActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);

                    finish();

                    break;

                case NetInfo.WHAT_SELECT_ORDER:
                    Log.e("셀렉트1");
                    coupons.clear();



                    for (int i = 0; i < arrayListOrderInfo.size(); i++) {
                        OrderInfo orderInfo = arrayListOrderInfo.get(i);
                        Store store = arrayListStore.get(i);
                        String imagePath = arrayListImagePath.get(i);
                        String cpInfo = arrayListCouponInfo.get(i);

                        PointFragment fragment = new PointFragment();
                        fragment.setPointActivity(PointActivity.this);
                        String orderPrice = StringTokenizerManager.getInstance().addChar(orderInfo.getFirstPrice(), ",", 3, false);
                        fragment.setInitData(store.getName(), imagePath, cpInfo, orderInfo.getOrderDate(), orderPrice, store.getAuthKey());

                        coupons.add(fragment);
                    }
                    setViewPager();
                    setIndicators();
                    break;
            }
        }
    };

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        changePage(position);
        currentPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {
        if(v == viewMapper.loadingView){

        }
        //적립화면
        if (v == viewMapper.wrapperConfirm || v == viewMapper.btnConfirmPad0 || v == viewMapper.btnConfirmPad1
                || v == viewMapper.btnConfirmPad2 || v == viewMapper.btnConfirmPad3 || v == viewMapper.btnConfirmPad4
                || v == viewMapper.btnConfirmPad5 || v == viewMapper.btnConfirmPad6 || v == viewMapper.btnConfirmPad7
                || v == viewMapper.btnConfirmPad8 || v == viewMapper.btnConfirmPad9 || v == viewMapper.btnConfirmPadX
                || v == viewMapper.btnConfirmCancel || v == viewMapper.btnConfirmOk) {
            clickConfrimItem(v);
        }

        if (v == viewMapper.btnGoSetting) {
            Intent intent = new Intent(this, UserSettingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

        if (v == viewMapper.toolbarBackButton) {
            finish();
        }

    }


    private class ViewMapper {
        //툴바
        RelativeLayout toolbar;
        StretchImageView toolbarBackButton;
        TextView toolbarTitle;

        //로딩뷰
        RelativeLayout loadingView;

        //포인트페이지
        LinearLayout wrapperPoint;
        ViewPager viewPager;
        LinearLayout indicatorWrapper;
        LinearLayout wrapperContentNo;
        TextView btnGoSetting;

        //적립확인
        RelativeLayout wrapperConfirm;
        TextView textTitle;
        TextView textMsg;
        TextView textNum;
        StretchImageView btnConfirmPad0;
        StretchImageView btnConfirmPad1;
        StretchImageView btnConfirmPad2;
        StretchImageView btnConfirmPad3;
        StretchImageView btnConfirmPad4;
        StretchImageView btnConfirmPad5;
        StretchImageView btnConfirmPad6;
        StretchImageView btnConfirmPad7;
        StretchImageView btnConfirmPad8;
        StretchImageView btnConfirmPad9;
        StretchImageView btnConfirmPadX;
        StretchImageView btnConfirmCancel;
        StretchImageView btnConfirmOk;

        public ViewMapper() {
            //툴바
            toolbar = (RelativeLayout) findViewById(R.id.point_toolbar);
            toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

            //로딩뷰
            loadingView = (RelativeLayout) findViewById(R.id.point_loadingview);

            //포인트페이지
            wrapperPoint = (LinearLayout) findViewById(R.id.point_wrapper);
            viewPager = (ViewPager) findViewById(R.id.point_viewpager);
            indicatorWrapper = (LinearLayout) findViewById(R.id.point_wrapper_indicator);
            wrapperContentNo = (LinearLayout) findViewById(R.id.point_wrapper_content_no);
            btnGoSetting = (TextView) findViewById(R.id.point_text_go_setting);

            //적립확인
            wrapperConfirm = (RelativeLayout) findViewById(R.id.point_wrapper_confirm);
            textTitle = (TextView) findViewById(R.id.point_confirm_text_title);
            textMsg = (TextView) findViewById(R.id.point_confirm_text_msg);
            textNum = (TextView) findViewById(R.id.point_confirm_text);
            btnConfirmPad0 = (StretchImageView) findViewById(R.id.point_confirm_pad_0);
            btnConfirmPad1 = (StretchImageView) findViewById(R.id.point_confirm_pad_1);
            btnConfirmPad2 = (StretchImageView) findViewById(R.id.point_confirm_pad_2);
            btnConfirmPad3 = (StretchImageView) findViewById(R.id.point_confirm_pad_3);
            btnConfirmPad4 = (StretchImageView) findViewById(R.id.point_confirm_pad_4);
            btnConfirmPad5 = (StretchImageView) findViewById(R.id.point_confirm_pad_5);
            btnConfirmPad6 = (StretchImageView) findViewById(R.id.point_confirm_pad_6);
            btnConfirmPad7 = (StretchImageView) findViewById(R.id.point_confirm_pad_7);
            btnConfirmPad8 = (StretchImageView) findViewById(R.id.point_confirm_pad_8);
            btnConfirmPad9 = (StretchImageView) findViewById(R.id.point_confirm_pad_9);
            btnConfirmPadX = (StretchImageView) findViewById(R.id.point_confirm_pad_x);
            btnConfirmCancel = (StretchImageView) findViewById(R.id.point_confirm_cancel);
            btnConfirmOk = (StretchImageView) findViewById(R.id.point_confirm_confirm);

            //리스너
            toolbarBackButton.setOnClickListener(PointActivity.this);
            loadingView.setOnClickListener(PointActivity.this);
            wrapperConfirm.setOnClickListener(PointActivity.this);
            btnGoSetting.setOnClickListener(PointActivity.this);
            btnConfirmPad0.setOnClickListener(PointActivity.this);
            btnConfirmPad1.setOnClickListener(PointActivity.this);
            btnConfirmPad2.setOnClickListener(PointActivity.this);
            btnConfirmPad3.setOnClickListener(PointActivity.this);
            btnConfirmPad4.setOnClickListener(PointActivity.this);
            btnConfirmPad5.setOnClickListener(PointActivity.this);
            btnConfirmPad6.setOnClickListener(PointActivity.this);
            btnConfirmPad7.setOnClickListener(PointActivity.this);
            btnConfirmPad8.setOnClickListener(PointActivity.this);
            btnConfirmPad9.setOnClickListener(PointActivity.this);
            btnConfirmPadX.setOnClickListener(PointActivity.this);
            btnConfirmCancel.setOnClickListener(PointActivity.this);
            btnConfirmOk.setOnClickListener(PointActivity.this);

        }

    }
}
