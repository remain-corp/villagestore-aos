package com.remain.villagestore.pages.p4_other.point.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kakao.kakaotalk.KakaoTalkService;
import com.kakao.kakaotalk.callback.TalkResponseCallback;
import com.kakao.network.ErrorResult;
import com.kakao.util.helper.log.Logger;
import com.remain.villagestore.R;
import com.remain.villagestore.common.C;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.pages.p4_other.point.adapter.BankListAdapter;
import com.remain.villagestore.pages.p4_other.policy.activity.PolicyInfoActivity;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by remain on 2016. 5. 23..
 */
public class PointRefundActivity extends FragmentActivity implements View.OnClickListener {
    private ViewMapper viewMapper;
    private User user;
    private ServerRequest server;

    private ArrayList<String> banklist = new ArrayList<>();
    private String selectedBank = "";
    private boolean isPolicyAgree = false;

    private boolean isKakao = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_refund);
        viewMapper = new ViewMapper();
        viewInit();
        setData();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (viewMapper.listView.getVisibility() == View.VISIBLE) {
            if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                viewMapper.listView.setVisibility(View.GONE);
                viewMapper.bgBlack.setVisibility(View.GONE);
                return true;
            }
        }

        if (viewMapper.alertWrapper.getVisibility() == View.VISIBLE) {
            if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                viewMapper.alertWrapper.setVisibility(View.GONE);
                viewMapper.bgBlack.setVisibility(View.GONE);
                return true;
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    final void viewInit() {
        viewMapper.toolbarTitle.setText("캐쉬 아웃");
        viewMapper.toolbarTitle.setTextColor(getResources().getColor(R.color.main_color));

        viewMapper.userPoint.setText(String.valueOf(Database.getInstance().getUser(this).getPoint() + " P"));
        viewMapper.alertWrapper.setVisibility(View.GONE);
        viewMapper.bgBlack.setVisibility(View.GONE);
        viewMapper.listView.setVisibility(View.GONE);

        viewMapper.wrapper.setOnClickListener(this);
        viewMapper.loadingView.setOnClickListener(this);
        viewMapper.toolbarBackbutton.setOnClickListener(this);
        viewMapper.btnBankCate.setOnClickListener(this);
        viewMapper.btnReFund.setOnClickListener(this);
        viewMapper.bgBlack.setOnClickListener(this);
        viewMapper.alertBtnConfirm.setOnClickListener(this);
        viewMapper.alertBtnCancel.setOnClickListener(this);
        viewMapper.btnPolicyText.setOnClickListener(this);
        viewMapper.btnPolicyCheckbox.setOnClickListener(this);

        user = Database.getInstance().getUser(this);
        server = new ServerRequest(networkResultListener);

        setBankList();
    }

    final void setData() {
        if (Utils.getPreferenceData(this, C.pref.IS_KAKAO, ParamInfo.NO).equals(ParamInfo.OK)) {
            isKakao = true;
        }
        if (isKakao) {
            viewMapper.alertTextSubTitle.setText("정말 환급신청을 하시겠습니까?");
            viewMapper.alertEtPass.setVisibility(View.GONE);
            viewMapper.alertInputWrapper.setVisibility(View.GONE);
        }
    }

    final void setBankList() {

        banklist.add("국민은행");
        banklist.add("기업은행");
        banklist.add("농협");
        banklist.add("신한(구조흥은행포함)");
        banklist.add("외환");
        banklist.add("우체국");
        banklist.add("SC제일");
        banklist.add("하나");
        banklist.add("한국씨티(구한미)");
        banklist.add("우리");
        banklist.add("경남");
        banklist.add("광주");
        banklist.add("대구");
        banklist.add("도이치");
        banklist.add("부산");
        banklist.add("산업");
        banklist.add("수협");
        banklist.add("전북");
        banklist.add("제주");
        banklist.add("새마을금고");
        banklist.add("신용협동조합");
        banklist.add("홍콩상하이(HSBC)");
        banklist.add("상호저축은행중앙회");

        final BankListAdapter bankListAdapter = new BankListAdapter(this, banklist);
        viewMapper.listView.setAdapter(bankListAdapter);
        viewMapper.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedBank = banklist.get(position);
                Log.e("은행이름은 = " + selectedBank);
                viewMapper.listView.setVisibility(View.GONE);
                viewMapper.bgBlack.setVisibility(View.GONE);
                viewMapper.textBankCate.setText(selectedBank);
                bankListAdapter.setClickItem(position);
            }
        });
    }

    final boolean checkRefundForm() {
        String failMSG = "";
        View failView = null;

        if (!isPolicyAgree) {
            failMSG = "포인트 이용약관에 동의해 주세요.";
            failView = viewMapper.btnPolicyCheckbox;
        }

        //핸드폰번호
        if (viewMapper.etPhoneNum.getText().toString().equals("")) {
            failMSG = "휴대폰번호를 입력해 주세요.";
            failView = viewMapper.etPhoneNum;
        }

        //환급금액
        if (viewMapper.etCash.getText().toString().equals("")) {
            failMSG = "금액을 입력해 주세요.";
            failView = viewMapper.etCash;
        } else {
            try {
                double cash = Double.parseDouble(viewMapper.etCash.getText().toString());
                if (cash < 10000) {
                    failMSG = "환급금액은 10000원 이상 가능합니다.";
                    failView = viewMapper.etCash;
                }
                if (cash > user.getPoint()) {
                    failMSG = "포인트가 부족합니다.";
                    failView = viewMapper.etCash;
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                failMSG = "환급금액을 숫자로 입력해 주세요.";
                failView = viewMapper.etCash;
            }
        }

        //계좌번호
        if (viewMapper.etAccountNum.getText().toString().equals("")) {
            failMSG = "계좌번호를 입력해 주세요.";
            failView = viewMapper.etAccountNum;
        }

        //은행 선택여부
        if (selectedBank.equals("")) {
            failMSG = "은행을 선택해 주세요.";
            failView = viewMapper.textBankCate;
        }
        //예금주 입력
        if (viewMapper.etName.getText().toString().equals("")) {
            failMSG = "예금주를 입력해 주세요.";
            failView = viewMapper.etName;
        }

        if (!failMSG.equals("")) {
            Toast.makeText(this, failMSG, Toast.LENGTH_SHORT).show();
            Utils.shakeView(failView);
            Utils.vibrate(this, 300);
            return false;
        } else {
            return true;
        }
    }

    //카카오
    public void requestSendMemo() {
        KakaoTalkMessageBuilder builder = new KakaoTalkMessageBuilder();
        if(user != null){
            builder.addParam("userName", user.getUserNickName());
            builder.addParam("refundCode", "123456");
        }else{
            builder.addParam("userName", "유저");
            builder.addParam("DATE", "880526");
        }

        KakaoTalkService.requestSendMemo(new KakaoTalkResponseCallback<Boolean>() {
                                             @Override
                                             public void onSuccess(Boolean result) {
                                                 Logger.d("send message to my chatroom : " + result);
                                             }
                                         }
                , "1166"
                , builder.build());

    }

    public class KakaoTalkMessageBuilder {
        public Map<String, String> messageParams = new HashMap<String, String>();

        public KakaoTalkMessageBuilder addParam(String key, String value) {
            messageParams.put("${" + key + "}", value);
            return this;
        }

        public Map<String, String> build() {
            return messageParams;
        }
    }

    private abstract class KakaoTalkResponseCallback<T> extends TalkResponseCallback<T> {
        @Override
        public void onNotKakaoTalkUser() {
            Log.e("onNotKakaoTalkUser(): not a KakaoTalk user");
        }

        @Override
        public void onFailure(ErrorResult errorResult) {
            Log.e("onFailure() : " + errorResult);
        }

        @Override
        public void onSessionClosed(ErrorResult errorResult) {
            Log.e("onSessionClosed() : " + errorResult);
        }

        @Override
        public void onNotSignedUp() {
            Log.e("onNotSignedUp()");
        }
    }

    /*
    * 유저 비밀번호 확인
    * */
    private void requestCheckPasswordToServer(String pass) {
        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.ID, user.getUserId());
        params.put(ParamInfo.PASS, pass);

        server.execute(NetInfo.LOGIN_USER, params);
    }

    /**
     * 환급 신청
     */
    private void requestRefundToServer() {
        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_SEQ, user.getSeq());
        params.put(ParamInfo.REFUND_NAME, viewMapper.etName.getText().toString());
        params.put(ParamInfo.REFUND_BANK, viewMapper.textBankCate.getText().toString());
        params.put(ParamInfo.REFUND_ACCOUNT_NUM, viewMapper.etAccountNum.getText().toString());
        params.put(ParamInfo.REFUND_PRICE, viewMapper.etCash.getText().toString());
        params.put(ParamInfo.REFUND_PHONE_NUM, viewMapper.etPhoneNum.getText().toString());

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.POINT_REFUND, params);
    }

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                Log.e("Error = " + error);
                return;
            }

            if (url == NetInfo.LOGIN_USER) {
                Bundle data = new Bundle();
                Message msg = Message.obtain();

                if (!object.isNull("result")) {
                    try {
                        if (object.getString("result").equals("FAIL")) {
                            Log.e("FAIL 진입");
                            data.putString("failMSG", "비밀번호를 확인해 주세요.");
                            msg.setData(data);
                            uiHandler.sendMessage(msg);
                            return;
                        } else {
                            //최종 승인 서버 작업
                            uiHandler.sendEmptyMessage(NetInfo.WHAT_LOGIN_USER);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (url == NetInfo.POINT_REFUND) {
                if (!object.isNull("result")) {
                    try {
                        Log.e(object.getString("result"));
                        uiHandler.sendEmptyMessage(NetInfo.WHAT_POINT_REFUND);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("result = null");
                }
            }
        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            viewMapper.loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Toast.makeText(PointRefundActivity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                    break;
                case NetInfo.WHAT_LOGIN_USER:
                    Log.e("WHAT_LOGIN_USER");
                    requestRefundToServer();
                    break;
                case NetInfo.WHAT_POINT_REFUND:
                    Log.e("WHAT_POINT_REFUND 진입");
                    finish();
                    break;
                default:
                    Log.e("핸들러 에러 진입.");
                    Utils.vibrate(PointRefundActivity.this, 400);
                    if (viewMapper.alertEtPass.getVisibility() != View.GONE) {
                        Utils.shakeView(viewMapper.alertEtPass);
                    }
                    Toast.makeText(PointRefundActivity.this, msg.getData().getString("failMSG"), Toast.LENGTH_SHORT).show();
                    break;

            }
        }
    };

    @Override
    public void onClick(View v) {
        if(v == viewMapper.loadingView){

        }

        if (viewMapper.bgBlack.getVisibility() == View.VISIBLE) {
            if (v == viewMapper.bgBlack) {
                if (viewMapper.listView.getVisibility() == View.VISIBLE) {
                    //은행선택시 배경누르면 없어지도록
                    viewMapper.listView.setVisibility(View.GONE);
                    viewMapper.bgBlack.setVisibility(View.GONE);
                }
                //활성화 되었을시 뒤에 이벤트 뺏기
            }
        }

        if (v == viewMapper.wrapper) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(viewMapper.etName.getWindowToken(), 0);
        } else if (v == viewMapper.toolbarBackbutton) {
            finish();
        } else if (v == viewMapper.btnBankCate) {
            //카테고리 클릭
            viewMapper.listView.setVisibility(View.VISIBLE);
            viewMapper.bgBlack.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            if (viewMapper.alertEtPass.getVisibility() != View.GONE) {
                imm.hideSoftInputFromWindow(viewMapper.alertEtPass.getWindowToken(), 0);
            }
        } else if (v == viewMapper.btnReFund) {
            //환급신청
            if (checkRefundForm()) {
                viewMapper.etName.setFocusable(false);
                viewMapper.etAccountNum.setFocusable(false);
                viewMapper.etCash.setFocusable(false);
                viewMapper.etPhoneNum.setFocusable(false);

                viewMapper.alertWrapper.setVisibility(View.VISIBLE);
                viewMapper.bgBlack.setVisibility(View.VISIBLE);

                if (viewMapper.alertEtPass.getVisibility() != View.GONE) {
                    viewMapper.alertEtPass.requestFocus();
                }
                if (!isKakao) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
            }
        } else if (v == viewMapper.alertBtnConfirm) {
            //얼럿확인
            if (isKakao) {
                //그냥 환급신청
                requestRefundToServer();
                //requestSendMemo();
            } else {
                String pass = viewMapper.alertEtPass.getText().toString().trim();
                if (pass == null || pass.equals("")) {
                    Toast.makeText(this, "비밀번호를 입력해 주세요.", Toast.LENGTH_SHORT).show();
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(viewMapper.alertEtPass.getWindowToken(), 0);
                    requestCheckPasswordToServer(pass);
                }
            }
        } else if (v == viewMapper.alertBtnCancel) {
            //얼럿취소
            viewMapper.etName.setFocusableInTouchMode(true);
            viewMapper.etAccountNum.setFocusableInTouchMode(true);
            viewMapper.etCash.setFocusableInTouchMode(true);
            viewMapper.etPhoneNum.setFocusableInTouchMode(true);
            viewMapper.etName.setFocusable(true);
            viewMapper.etAccountNum.setFocusable(true);
            viewMapper.etCash.setFocusable(true);
            viewMapper.etPhoneNum.setFocusable(true);

            if (viewMapper.alertEtPass.getVisibility() != View.GONE) {
                viewMapper.alertEtPass.setText("");
            }
            viewMapper.bgBlack.setVisibility(View.GONE);
            viewMapper.alertWrapper.setVisibility(View.GONE);
        } else if (v == viewMapper.btnPolicyText) {
            Intent i = new Intent(this, PolicyInfoActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.putExtra("signUp", PolicyInfoActivity.USE_POINT_POLICY);
            startActivity(i);
        } else if (v == viewMapper.btnPolicyCheckbox) {
            if (isPolicyAgree) {
                viewMapper.btnPolicyCheckbox.setImageResource(R.drawable.image_check_no);
            } else {
                viewMapper.btnPolicyCheckbox.setImageResource(R.drawable.image_check_yes);
            }
            isPolicyAgree = !isPolicyAgree;
        }
    }

    private class ViewMapper {
        LinearLayout wrapper;

        RelativeLayout toolbar;
        StretchImageView toolbarBackbutton;
        TextView toolbarTitle;

        RelativeLayout loadingView;

        TextView userPoint;
        EditText etName;
        LinearLayout btnBankCate;
        TextView textBankCate;
        EditText etAccountNum;
        EditText etCash;
        EditText etPhoneNum;
        TextView btnReFund;
        StretchImageView btnPolicyCheckbox;
        TextView btnPolicyText;

        //최종 확인창
        View bgBlack;
        RelativeLayout alertWrapper;
        TextView alertTextSubTitle;
        LinearLayout alertInputWrapper;
        EditText alertEtPass;
        TextView alertBtnConfirm;
        TextView alertBtnCancel;

        //은행목록 리스트뷰
        ListView listView;

        public ViewMapper() {
            wrapper = (LinearLayout) findViewById(R.id.refund_wrapper);

            toolbar = (RelativeLayout) findViewById(R.id.refund_toolbar);
            toolbarBackbutton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

            loadingView = (RelativeLayout) findViewById(R.id.refund_loadingview);

            userPoint = (TextView) findViewById(R.id.refund_point);
            btnBankCate = (LinearLayout) findViewById(R.id.refund_wrapper_bank_cate);
            textBankCate = (TextView) findViewById(R.id.refund_text_bank_cate);
            etName = (EditText) findViewById(R.id.refund_name);
            etAccountNum = (EditText) findViewById(R.id.refund_account_num);
            etCash = (EditText) findViewById(R.id.refund_cash);
            etPhoneNum = (EditText) findViewById(R.id.refund_phone_num);
            btnReFund = (TextView) findViewById(R.id.refund_text_refund);
            btnPolicyCheckbox = (StretchImageView) findViewById(R.id.refund_checkbox);
            btnPolicyText = (TextView) findViewById(R.id.refund_text_policy);

            //최종 확인창
            bgBlack = findViewById(R.id.refund_bg_black);
            alertWrapper = (RelativeLayout) findViewById(R.id.refund_alert);
            alertTextSubTitle = (TextView) alertWrapper.findViewById(R.id.alert_text_subtitle);
            alertInputWrapper = (LinearLayout) alertWrapper.findViewById(R.id.alert_wrapper_input);
            alertEtPass = (EditText) alertWrapper.findViewById(R.id.alert_et_input);
            alertBtnConfirm = (TextView) alertWrapper.findViewById(R.id.alert_text_confirm);
            alertBtnCancel = (TextView) alertWrapper.findViewById(R.id.alert_text_cancel);

            //은행 목록
            listView = (ListView) findViewById(R.id.refund_listView);
        }
    }
}
