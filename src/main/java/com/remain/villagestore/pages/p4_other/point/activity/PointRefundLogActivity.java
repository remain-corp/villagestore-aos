package com.remain.villagestore.pages.p4_other.point.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by remain on 2016. 5. 27..
 */
public class PointRefundLogActivity extends FragmentActivity implements View.OnClickListener {
    private ViewMapper viewMapper;
    private ServerRequest server;
    private User user;

    private String refundDate = "";
    private String refundPrice = "";
    private String refundBank = "";
    private String refundEtc = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_refund_log);
        viewMapper = new ViewMapper();

        viewInit();
    }

    final void viewInit() {
        user = Database.getInstance().getUser(this);
        server = new ServerRequest(networkResultListener);

        viewMapper.toolbarTitle.setText("포인트 환급 내역");

        requestRefundToServer();
    }

    /**
     * 환급 신청
     */
    private void requestRefundToServer() {
        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_SEQ, user.getSeq());

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.POINT_REFUND_LOG, params);
    }

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                Log.e("Error = " + error);
                return;
            }
            if (url == NetInfo.POINT_REFUND_LOG) {
                if (!object.isNull("result")) {
                    try {
                        refundBank = "";
                        refundDate = "";
                        refundPrice = "";
                        refundEtc = "";
                        JSONArray array = object.getJSONArray("result");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object1 = (JSONObject) array.get(i);

                            String date = object1.getString("refund_time");
                            String price = object1.getString("reg_bill");
                            String bank = object1.getString("bank_name");
                            String etc = object1.getString("iscomplete");

                            refundDate += date.substring(0, 10) + "\n";
                            refundPrice += price + " 원" + "\n";
                            //은행명이 길 경우
                            if (bank.length() > 4) {
                                refundBank += bank.substring(0, 4) + "..\n";
                            } else {
                                refundBank += bank + "\n";
                            }
                            //승인여부
                            if (etc.equals("P")) {
                                refundEtc += "대기\n";
                            } else if (etc.equals("C")) {
                                refundEtc += "승인\n";
                            } else if (etc.equals("R")) {
                                refundEtc += "거부\n";
                            }

                            Log.e(object1.getString("bank_name"));
                            Log.e(object1.getString("reg_bill"));
                            Log.e(object1.getString("refund_time"));
                            Log.e(object1.getString("iscomplete")); // P = 미처리 C = 처리완료 R = 거부
                        }
                        /*for (int i = 0; i < 30; i++) {
                            refundDate += "2016-05-26" + "\n";
                            refundPrice += "10000" + " 원" + "\n";
                            //은행명이 길 경우

                            refundBank += "우체국" + "\n";
                            //승인여부

                            refundEtc += "승인\n";

                        }*/
                        uiHandler.sendEmptyMessage(NetInfo.WHAT_POINT_REFUND_LOG);
                        //uiHandler.sendMessage(new Message());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("result = null");
                    uiHandler.sendMessage(new Message());
                }
            }
        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            viewMapper.loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Toast.makeText(PointRefundLogActivity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                    break;
                case NetInfo.WHAT_POINT_REFUND_LOG:
                    Log.e("WHAT_POINT_REFUND 진입");
                    viewMapper.wrapperNo.setVisibility(View.GONE);
                    viewMapper.wrapperYes.setVisibility(View.VISIBLE);
                    viewMapper.refundDate.setText(refundDate);
                    viewMapper.refundPrice.setText(refundPrice);
                    viewMapper.refundBank.setText(refundBank);
                    viewMapper.refundEtc.setText(refundEtc);
                    break;
                default:
                    Log.e("여기?");
                    viewMapper.wrapperNo.setVisibility(View.VISIBLE);
                    viewMapper.wrapperYes.setVisibility(View.GONE);
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {
        if (v == viewMapper.toolbarBackButton) {
            finish();
        }
    }

    private class ViewMapper {
        private RelativeLayout toolbar;
        private StretchImageView toolbarBackButton;
        private TextView toolbarTitle;

        private RelativeLayout loadingView;

        private LinearLayout wrapperYes;
        private LinearLayout wrapperNo;

        private TextView refundDate;
        private TextView refundPrice;
        private TextView refundBank;
        private TextView refundEtc;


        public ViewMapper() {

            toolbar = (RelativeLayout) findViewById(R.id.refundlog_toolbar);
            toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

            loadingView = (RelativeLayout) findViewById(R.id.refundlog_loadingview);

            wrapperYes = (LinearLayout) findViewById(R.id.refundlog_wrapper_yes);
            wrapperNo = (LinearLayout) findViewById(R.id.refundlog_wrapper_no);

            refundDate = (TextView) findViewById(R.id.refundlog_text_date);
            refundPrice = (TextView) findViewById(R.id.refundlog_text_price);
            refundBank = (TextView) findViewById(R.id.refundlog_text_bank);
            refundEtc = (TextView) findViewById(R.id.refundlog_text_etc);

            toolbarBackButton.setOnClickListener(PointRefundLogActivity.this);
            loadingView.setOnClickListener(PointRefundLogActivity.this);
        }
    }
}
