package com.remain.villagestore.pages.p4_other.point.activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.DisplayManager;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by woongjaelee on 2015. 12. 23..
 */
public class SavingCompleteActivity extends Activity {

    //================================================================================
    // XXX Constants
    //================================================================================

    private final static float ROTATE_ROT = 2;
    private final static long ROTATE_DURATION = 100;
    //================================================================================
    // XXX Enum
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================

    // App

    // Content
    private Context context;
    private Intent intent;

    // Widget
    private TextView textViewMessage;
    private ImageView imageViewCoin;
    private TextView textViewBubble;
    private ImageView imageViewPig;
    private RelativeLayout toolbar;
    private StretchImageView toolbarBackButton;
    private TextView toolbarTitle;
    private TextView btnGoMAin;

    // View

    // Adapter

    // Array

    // Lang
    private User me;

    // Component
    private RelativeLayout loadingView;

    // Util
    private ServerRequest server;
    private DisplayManager displayManager;

    // Interface

    //================================================================================
    // XXX Constructor
    //================================================================================

    //================================================================================
    // XXX Override Method
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_saving_complete);

        init();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    intent = new Intent(context, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("saving", true);
                    startActivity(intent);
                    finish();

                    return true;
            }

        }

        return super.onKeyDown(keyCode, event);
    }

    private void requestSelectUserToServer(String id) {

        loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.ID, id);

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.SELECT_USER, params);

    }

    //================================================================================
    // XXX Listener
    //================================================================================

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == loadingView){

            }
            if (v == toolbarBackButton || v == btnGoMAin) {
                intent = new Intent(context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("saving", true);
                startActivity(intent);
                finish();
            }
            /*switch (v.getId()) {
                case R.id.activity_saving_relativelayout_back:
                case R.id.activity_saving_linearlayout_back:
                    intent = new Intent(context, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("saving", true);
                    startActivity(intent);
                    finish();
                    break;
            }*/
        }
    };

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {

            if (error != null) {

                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.SELECT_USER) {
                try {
                    if (!object.isNull(ParamInfo.USERLIST)) {

                        JSONArray array = object.getJSONArray(ParamInfo.USERLIST);

                        if (array.length() > 0) {
                            User user = new User(array.getJSONObject(0));
                            Log.e("세이빙 포인트2 = " + user.getPoint());
                            Database.getInstance().checkUser(SavingCompleteActivity.this, user);
                        }
                        uiHandler.sendEmptyMessage(NetInfo.WHAT_SELECT_USER);
                    } else {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    };

    //================================================================================
    // XXX Thread, Handler
    //================================================================================
    private Handler uiHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:

                    Toast.makeText(context, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    break;

                case NetInfo.WHAT_SELECT_USER:
                    startAnim();

                    break;

            }
        }
    };
    //================================================================================
    // XXX Getter, Setter
    //================================================================================

    //================================================================================
    // XXX Etc Method
    //================================================================================

    private void init() {

        context = this;

        displayManager = new DisplayManager(context);
        server = new ServerRequest(networkResultListener);

        initUI();
        initUISize();
        setEventListener();
        setView();

        requestSelectUserToServer(Database.getInstance().getUser(this).getUserId());
    }

    private void initUI() {

        loadingView = (RelativeLayout) findViewById(R.id.saving_loadingview);

        imageViewCoin = (ImageView) findViewById(R.id.actvity_saving_imageview_coin);
        imageViewPig = (ImageView) findViewById(R.id.activity_saving_imageview_pig);
        textViewMessage = (TextView) findViewById(R.id.activity_saving_textview_message);
        textViewBubble = (TextView) findViewById(R.id.activity_saving_textview_bubble);
        toolbar = (RelativeLayout) findViewById(R.id.saving_toolbar);
        toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        btnGoMAin = (TextView) findViewById(R.id.activity_saving_text_go_main);

    }

    private void initUISize() {

        displayManager.changeWidthHeightSameRate(findViewById(R.id.acitvity_saving_relativelayout_top_panel).getLayoutParams(), ViewGroup.LayoutParams.MATCH_PARENT, 96);

        FrameLayout.LayoutParams frameParams = (FrameLayout.LayoutParams) findViewById(R.id.activity_saving_imageview_icon).getLayoutParams();
        displayManager.changeWidthHeightSameRate(frameParams, 57, 48);
        frameParams.leftMargin = displayManager.getScaleSizeSameRate(30);
        frameParams.rightMargin = displayManager.getScaleSizeSameRate(30);

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) textViewMessage.getLayoutParams();
        linearParams.topMargin = displayManager.getScaleSizeSameRate(80);

        displayManager.changeWidthHeightSameRate(findViewById(R.id.activity_saving_imageview_coin_bg).getLayoutParams(), 708, 208);
        displayManager.changeWidthHeightSameRate(imageViewPig.getLayoutParams(), 397, 299);

        frameParams = (FrameLayout.LayoutParams) textViewBubble.getLayoutParams();
        frameParams.topMargin = displayManager.getScaleSizeSameRate(180);
        frameParams.rightMargin = displayManager.getScaleSizeSameRate(40);
        textViewBubble.setPadding(0, displayManager.getScaleSizeSameRate(34), 0, 0);

        displayManager.changeWidthHeightSameRate(frameParams, 202, 132);

        displayManager.changeWidthHeightSameRate(imageViewCoin.getLayoutParams(), 80, 80);

    }

    private void setEventListener() {
        toolbarBackButton.setOnClickListener(clickListener);
        loadingView.setOnClickListener(clickListener);
        btnGoMAin.setOnClickListener(clickListener);

    }

    private void setView() {
        toolbarTitle.setText("포인트 적립");
        textViewBubble.setVisibility(View.GONE);

    }

    private void startAnim() {

        TranslateAnimation translateAnimation = new TranslateAnimation(Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, 280);
        translateAnimation.setDuration(1000);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                imageViewCoin.setAlpha(0.0f);
                textViewBubble.setVisibility(View.VISIBLE);
                startBubbleAnim();
                startPigAnim();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        translateAnimation.setFillAfter(true);
        imageViewCoin.startAnimation(translateAnimation);
    }

    private void startPigAnim() {

        AnimationSet animationSet = new AnimationSet(true);

        final RotateAnimation rotateAnimation = new RotateAnimation(0, ROTATE_ROT, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(ROTATE_DURATION);
        rotateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                RotateAnimation rotateAnimation2 = new RotateAnimation(0, -ROTATE_ROT, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                rotateAnimation2.setDuration(ROTATE_DURATION);
                rotateAnimation2.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        rotateAnimation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {

                                RotateAnimation rotateAnimation2 = new RotateAnimation(0, -ROTATE_ROT, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                rotateAnimation2.setDuration(ROTATE_DURATION);
                                imageViewPig.startAnimation(rotateAnimation2);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        imageViewPig.startAnimation(rotateAnimation);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                imageViewPig.startAnimation(rotateAnimation2);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animationSet.addAnimation(rotateAnimation);

        imageViewPig.startAnimation(animationSet);


    }

    private void startBubbleAnim() {

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(400);
        alphaAnimation.setFillAfter(true);

        // 원 Scale 애니메이션
        ObjectAnimator scaleXAnimator1 = ObjectAnimator.ofFloat(textViewBubble, "ScaleX", 0.0f, 1.0f);
        ObjectAnimator scaleYAnimator1 = ObjectAnimator.ofFloat(textViewBubble, "ScaleY", 0.0f, 1.0f);

        animatorSet.playTogether(scaleXAnimator1, scaleYAnimator1);
        textViewBubble.startAnimation(alphaAnimation);
        animatorSet.start();
    }


    //================================================================================
    // XXX Inner Class
    //================================================================================

    //================================================================================
    // XXX Interface
    //================================================================================

    //================================================================================
    // XXX Debug
    //================================================================================

}
