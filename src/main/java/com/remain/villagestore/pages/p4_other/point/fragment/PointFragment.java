package com.remain.villagestore.pages.p4_other.point.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.remain.villagestore.R;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.pages.p4_other.point.activity.PointActivity;
import com.remain.villagestore.util.Log;

/**
 * Created by remain on 2016. 5. 9..
 */
public class PointFragment extends Fragment implements View.OnClickListener {
    private PointActivity pointActivity;
    private ViewMapper viewMapper;

    private String storeName;
    private String storeImgPath;
    private String couponInfo;
    private String orderDate;
    private String orderPrice;
    private int storeAuthKey;
    private RequestManager glideRequestManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_point, container, false);
        glideRequestManager = Glide.with(this);
        viewMapper = new ViewMapper(view);
        viewInit();
        return view;
    }

    @Override
    public void onClick(View v) {
        if (v == viewMapper.btnSavePoint) {
            pointActivity.visibleConfirmWrapper(true);
        } else if (v == viewMapper.btnUpdateMoney) {
            pointActivity.visibleConfirmWrapper(false);
        }
    }

    private void viewInit() {
        viewMapper.textStore.setText(storeName);
        viewMapper.textPrice.setText("주문금액 : " + orderPrice + " 원");
        viewMapper.textCouponInfo.setText(couponInfo);
        viewMapper.textOrderDate.setText("주문 일시 : " + orderDate);
        viewMapper.imageStore.post(new Runnable() {
            @Override
            public void run() {
                glideRequestManager
                        .load("http://" + AppInfo.IMG_PATH + storeImgPath)
                        .thumbnail(0.1f)
                        .error(R.drawable.image_default_photo)
                        .into(viewMapper.imageStore);
            }
        });
    }

    public void setInitData(String storeName, String imgPath, String couponInfo, String orderDate, String orderPrice, int storeAuthKey) {
        this.storeName = storeName;
        this.storeImgPath = imgPath;
        this.couponInfo = couponInfo;
        this.orderDate = orderDate;
        this.orderPrice = orderPrice;
        this.storeAuthKey = storeAuthKey;
    }

    public void setPointActivity(PointActivity pointActivity) {
        this.pointActivity = pointActivity;
    }

    public String getOrderPrice() {
        return viewMapper.textPrice.getText().toString().replace(",", "");
    }

    public int getStoreAuthKey() {
        return storeAuthKey;
    }

    public void changeMoney(String price) {
        String result = "주문금액 : " + price + " 원";
        Log.e(result);
        viewMapper.textPrice.setText(result);
    }

    private class ViewMapper {
        TextView textStore;
        ImageView imageStore;
        TextView textOrderDate;
        TextView textCouponInfo;
        TextView textPrice;
        TextView btnUpdateMoney;
        TextView btnSavePoint;

        public ViewMapper(View v) {
            textStore = (TextView) v.findViewById(R.id.point_text_store);
            imageStore = (ImageView) v.findViewById(R.id.point_image_store);
            textOrderDate = (TextView) v.findViewById(R.id.point_text_order_date);
            textCouponInfo = (TextView) v.findViewById(R.id.point_text_coupon_info);
            textPrice = (TextView) v.findViewById(R.id.point_confirm_text_money);
            btnUpdateMoney = (TextView) v.findViewById(R.id.point_update_money);
            btnSavePoint = (TextView) v.findViewById(R.id.point_save_point);

            btnUpdateMoney.setOnClickListener(PointFragment.this);
            btnSavePoint.setOnClickListener(PointFragment.this);
        }
    }

}
