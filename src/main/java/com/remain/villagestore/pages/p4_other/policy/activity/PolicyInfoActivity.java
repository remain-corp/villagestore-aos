package com.remain.villagestore.pages.p4_other.policy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;

/**
 * Created by remain on 2016. 5. 2..
 */
public class PolicyInfoActivity extends FragmentActivity implements View.OnClickListener {
    private ViewMapper viewMapper;
    //인텐트정보
    private String intentInfo;
    public static String USE_SERVICE_POLICY = "useService";
    public static String USE_PERSONER_INFO_POLICY = "usePersonerInfo";
    public static String USE_PERSONER_INFO_POLICY2 = "usePersonerInfo2";
    public static String USE_POINT_POLICY = "usePoint";
    public static String USE_LOCAL_POLICY = "useLocalPolicy";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policyinfo);

        viewMapper = new ViewMapper();

        checkIntent();
        viewInit();
    }

    private void checkIntent() {
        Intent i = getIntent();
        intentInfo = i.getStringExtra("signUp");
    }

    private void viewInit() {
        //viewMapper.toolbarTitle.setTextColor(getResources().getColor(R.color.main_color));
        viewMapper.toolbar.setBackgroundColor(getResources().getColor(R.color.main_color));
        if (intentInfo.equals(USE_SERVICE_POLICY)) {
            viewMapper.toolbarTitle.setText("서비스 이용약관");
            viewMapper.contents.setText(getResources().getString(R.string.policy_use_service));
        } else if (intentInfo.equals(USE_PERSONER_INFO_POLICY)) {
            viewMapper.toolbarTitle.setText("개인정보 취급방침");
            viewMapper.contents.setText(getResources().getString(R.string.policy_person_info));
        }else if (intentInfo.equals(USE_POINT_POLICY)) {
            viewMapper.toolbarTitle.setText("포인트 이용약관");
            viewMapper.contents.setText(getResources().getString(R.string.policy_use_point));
        }else if(intentInfo.equals(USE_PERSONER_INFO_POLICY2)){
            viewMapper.toolbarTitle.setText("개인정보 수집 및 이용동의");
            viewMapper.contents.setText(getResources().getString(R.string.policy_personal_info2));
        }
    }

    @Override
    public void onClick(View v) {
        if (v == viewMapper.toolbarBackbutton) {
            finish();
        }
    }

    private class ViewMapper {
        private RelativeLayout toolbar;
        private TextView toolbarTitle;
        private StretchImageView toolbarBackbutton;

        private TextView contents;

        public ViewMapper() {
            toolbar = (RelativeLayout) findViewById(R.id.policy_toolbar);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
            toolbarBackbutton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
            contents = (TextView) findViewById(R.id.policy_text_contents);

            toolbarBackbutton.setOnClickListener(PolicyInfoActivity.this);
        }

    }

}
