package com.remain.villagestore.pages.p4_other.setting.activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.AlarmManager;
import com.remain.villagestore.manager.DataManager;
import com.remain.villagestore.pages.commonActivity.WebviewActivity;
import com.remain.villagestore.pages.p4_other.address.activity.SelectAddressActivity;
import com.remain.villagestore.pages.p4_other.point.activity.PointRefundActivity;
import com.remain.villagestore.pages.p4_other.point.activity.PointRefundLogActivity;
import com.remain.villagestore.pages.p4_other.policy.activity.PolicyInfoActivity;
import com.remain.villagestore.pages.p4_other.sign.activity.SelectSignActivity;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by remain on 2016. 4. 26..
 */
public class UserSettingActivity extends FragmentActivity implements View.OnClickListener {
    private ViewMapper viewMapper;
    private User me;
    private boolean isPushOn = true;
    private boolean isLogin = false;
    private boolean isPointMode = false;

    private ServerRequest server;

    //포인트로그
    private ArrayList<String> arrayListStore = new ArrayList<>();
    private ArrayList<String> arrayListPoint = new ArrayList<>();
    private ArrayList<String> arrayListDate = new ArrayList<>();
    private int totalPoint = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_setting);
        viewMapper = new ViewMapper();


        checkInitInfo();
        viewInit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        //업데이트후 진입시
        if (intent.getBooleanExtra("updateOK", true)) {
            viewMapper.textAddress.setText(Database.getInstance().getUser(this).getAddress());
        }

    }

    final void checkInitInfo() {
        //로그인체크
        me = User.getMyInfo(this);
        if (me != null && me.getDeviceId() != null && !me.getDeviceId().equals("")) {
            isLogin = true;
        } else {
            isLogin = false;
        }
        //푸쉬 스위치 체크
        isPushOn = AlarmManager.getInstance(this).isNoticeAlarm();

        //포인트 모드인지
        isPointMode = getIntent().getBooleanExtra("isPointMode", false);
    }

    final void viewInit() {
        server = new ServerRequest(networkResultListener);
        //로그인페이지
        setLogin(isLogin);

        //스위치
        float imageX = viewMapper.imageSwitch.getX();
        if (isPushOn) {
            viewMapper.imageSwitch.setX(imageX + Utils.DPToPX(this, 24));
            viewMapper.imageSwitch.clearColorFilter();
        } else {
            viewMapper.imageSwitch.setColorFilter(Color.parseColor("#999999"));
        }

        //유저기본정보입력
        if (me != null) {
            viewMapper.textNickName.setText(me.getUserNickName());
            viewMapper.textPoint.setText(String.valueOf(me.getPoint()));
            if (me.getAddress() == null || me.getAddress().equals("")) {
                viewMapper.textAddress.setText(getString(R.string.main_user_no_address));
            } else {
                viewMapper.textAddress.setText(me.getAddress());
            }
        }

        //포인트로그화면일시
        if (isPointMode) {
            clickCate(viewMapper.btnCatePointLog);
        }
    }

    //중복 터치시 에니메이션 중복을 막기 위해
    boolean switchClickable = true;

    private void checkSwitch() {
        if (switchClickable) {
            switchClickable = false;
            float imageX = viewMapper.imageSwitch.getX();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                if (isPushOn) {
                    viewMapper.imageSwitch.animate().setDuration(350).translationX(imageX - Utils.DPToPX(this, 24)).start();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            viewMapper.imageSwitch.setColorFilter(Color.parseColor("#999999"));
                            switchClickable = true;
                        }
                    }, 340);
                } else {
                    viewMapper.imageSwitch.animate().setDuration(350).translationX(imageX + Utils.DPToPX(this, 24)).start();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            viewMapper.imageSwitch.clearColorFilter();
                            switchClickable = true;
                        }
                    }, 340);
                }
                isPushOn = !isPushOn;
                AlarmManager.getInstance(this).setNoticeAlarm(isPushOn);
            }
        }
    }

    private void setLogin(boolean isLogin) {
        if (isLogin) {
            viewMapper.wrapperSignIn.setVisibility(View.GONE);
            viewMapper.listLogOut.setVisibility(View.VISIBLE);
        } else {
            viewMapper.wrapperSignIn.setVisibility(View.VISIBLE);
            viewMapper.listLogOut.setVisibility(View.GONE);
        }
    }

    private void clickCate(View v) {
        if (!isLogin) {
            if (v == viewMapper.btnCateRefund || v == viewMapper.btnCatePointLog) {
                Toast.makeText(this, "로그인이 필요한 기능입니다.", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        viewMapper.indicatorSetting.setBackgroundColor(Color.parseColor("#FAFAFA"));
        viewMapper.indicatorPrepare.setBackgroundColor(Color.parseColor("#FAFAFA"));
        viewMapper.indicatorCoup.setBackgroundColor(Color.parseColor("#FAFAFA"));
        viewMapper.textCateSetting.setTextColor(Color.parseColor("#999999"));
        viewMapper.textCatePrepare.setTextColor(Color.parseColor("#999999"));
        viewMapper.textCateCoup.setTextColor(Color.parseColor("#999999"));

        if (v == viewMapper.btnCateSetting) {
            viewMapper.indicatorSetting.setBackgroundColor(Color.parseColor("#ffb266"));
            viewMapper.textCateSetting.setTextColor(Color.parseColor("#000000"));

            viewMapper.wrapperSetting.setVisibility(View.VISIBLE);
            viewMapper.wrapperPointLog.setVisibility(View.GONE);
            viewMapper.wrapperPointRefund.setVisibility(View.GONE);
        } else if (v == viewMapper.btnCateRefund) {
            viewMapper.indicatorPrepare.setBackgroundColor(Color.parseColor("#ffb266"));
            viewMapper.textCatePrepare.setTextColor(Color.parseColor("#000000"));

            viewMapper.wrapperPointRefund.setVisibility(View.VISIBLE);
            viewMapper.wrapperSetting.setVisibility(View.GONE);
            viewMapper.wrapperPointLog.setVisibility(View.GONE);
        } else if (v == viewMapper.btnCatePointLog) {
            viewMapper.indicatorCoup.setBackgroundColor(Color.parseColor("#ffb266"));
            viewMapper.textCateCoup.setTextColor(Color.parseColor("#000000"));

            viewMapper.wrapperPointLog.setVisibility(View.VISIBLE);
            viewMapper.wrapperSetting.setVisibility(View.GONE);
            viewMapper.wrapperPointRefund.setVisibility(View.GONE);
            requestGetPointLogToServer(me.getSeq());
        }
    }

    private void clickListItem(View v) {
        if (v == viewMapper.listNoti) {
            //공지사항
            Intent intent = new Intent(this, WebviewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(ParamInfo.SITE_ADDRESS, NetInfo.NOTICE_URL);
            startActivity(intent);
            /*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(NetInfo.NOTICE_URL));
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);*/
        } else if (v == viewMapper.listTerms) {
            //서비스 이용약관
            Intent i = new Intent(this, PolicyInfoActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.putExtra("signUp", PolicyInfoActivity.USE_SERVICE_POLICY);
            startActivity(i);
        } else if (v == viewMapper.listAsk) {
            //문의하기
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppInfo.KAKAO_PLUS_LINK));
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else if (v == viewMapper.listLogOut) {
            //로그아웃
            me = null;
            Database.getInstance().deleteUser(this);
            DataManager.getInstance().setUser(null);
            isLogin = false;
            setLogin(isLogin);
        }
    }

    /**
     * 쿠폰 로그 가져오기
     */
    private void requestGetPointLogToServer(int user_seq) {

        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_SEQ, user_seq);
        server.execute(NetInfo.IP_ADDRESS1, NetInfo.GET_POINT_LOG, params);
    }

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.GET_POINT_LOG) {
                try {
                    if (object.getJSONArray(ParamInfo.RESULT) != null) {
                        JSONArray jsonArray = object.getJSONArray(ParamInfo.RESULT);

                        //JSONObject objectStore = object.getJSONObject(ParamInfo.RESULT);

                        arrayListStore.clear();
                        arrayListPoint.clear();
                        arrayListDate.clear();

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject detail = jsonArray.getJSONObject(i);

                            String store = detail.getString("loc");
                            int point = detail.getInt("p_point");
                            String date = detail.getString("time");
                            String isPlus = detail.getString("isPlus");

                            date = date.replace("-", ".");
                            date = date.substring(5, 10);

                            String realPoint = "";
                            if (isPlus.equals("P")) {
                                realPoint = "+ " + point;
                            } else {
                                realPoint = "- " + point;
                            }

                            totalPoint = totalPoint + point;

                            arrayListStore.add(store);
                            arrayListPoint.add(realPoint);
                            arrayListDate.add(date);

                            Log.e("store = " + store);
                            Log.e("point = " + point);
                            Log.e("date = " + date);
                            Log.e("isPlus = " + isPlus);

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                uiHandler.sendEmptyMessage(NetInfo.WHAT_GET_POINT_LOG);
            }

        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            viewMapper.loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Toast.makeText(UserSettingActivity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                    break;
                case NetInfo.WHAT_GET_POINT_LOG:
                    String store = "";
                    String point = "";
                    String date = "";
                    for (int i = 0; i < arrayListStore.size(); i++) {
                        store = store + arrayListStore.get(i) + "\n";
                        point = point + arrayListPoint.get(i) + "\n";
                        date = date + arrayListDate.get(i) + "\n";
                    }

                    viewMapper.textPointLogStore.setText(store);
                    viewMapper.textPointLogPoint.setText(point);
                    viewMapper.textPointLogDate.setText(date);
                    viewMapper.textPointLogTotal.setText("총 포인트 : " + totalPoint);
                    totalPoint = 0;

                    break;

            }
        }
    };

    @Override
    public void onClick(View v) {
        if (v == viewMapper.loadingView) {

        }
        if (v == viewMapper.btnBack2) {
            finish();
        } else if (v == viewMapper.infoUser) {

            clickCate(viewMapper.btnCateSetting);

        } else if (v == viewMapper.infoPoint) {

            clickCate(viewMapper.btnCatePointLog);

        } else if (v == viewMapper.btnSignIn) {
            Intent intent = new Intent(this, SelectSignActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else if (v == viewMapper.btnBack) {
            finish();
        } else if (v == viewMapper.btnEditAddress || v == viewMapper.textBtnAddress) {
            //수정하는 메서드
            Intent intent = new Intent(this, SelectAddressActivity.class);
            intent.putExtra(ParamInfo.DETAIL_MODE, true);
            intent.putExtra("isUpdateMode", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else if (v == viewMapper.btnCateSetting || v == viewMapper.btnCateRefund || v == viewMapper.btnCatePointLog) {
            clickCate(v);
        } else if (v == viewMapper.listNoti || v == viewMapper.listTerms || v == viewMapper.listAsk || v == viewMapper.listLogOut) {
            clickListItem(v);
        } else if (v == viewMapper.btnSwitch) {
            checkSwitch();
        } else if (v == viewMapper.btnContactUs) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.setData(Uri.parse("tel:0314517552"));
            startActivity(intent);
        } else if (v == viewMapper.textRefund) {
            Intent intent = new Intent(this, PointRefundActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else if (v == viewMapper.textRefundLog) {
            Intent intent = new Intent(this, PointRefundLogActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

    private class ViewMapper {
        //로딩뷰
        RelativeLayout loadingView;

        //메인상단(로그인시)
        StretchImageView btnBack;
        LinearLayout infoUser;
        LinearLayout infoPoint;
        TextView textNickName;
        TextView textAddress;
        TextView textPoint;
        TextView textBtnAddress;
        LinearLayout btnEditAddress;

        //메인상단2(비로그인시)
        RelativeLayout wrapperSignIn;
        LinearLayout btnSignIn;
        StretchImageView btnBack2;

        //카테고리
        LinearLayout btnCateSetting;
        LinearLayout btnCateRefund;
        LinearLayout btnCatePointLog;
        TextView textCateSetting;
        TextView textCatePrepare;
        TextView textCateCoup;
        View indicatorSetting;
        View indicatorPrepare;
        View indicatorCoup;

        //리스트
        LinearLayout wrapperSetting;
        LinearLayout listNoti;
        LinearLayout listTerms;
        LinearLayout listAsk;
        LinearLayout listLogOut;

        //스위치
        RelativeLayout btnSwitch;
        StretchImageView imageSwitch;

        //etc
        StretchImageView btnContactUs;

        //포인트환급
        RelativeLayout wrapperPointRefund;
        TextView textRefund;
        TextView textRefundLog;

        //포인트적립로그
        RelativeLayout wrapperPointLog;
        TextView textPointLogTotal;
        TextView textPointLogStore;
        TextView textPointLogPoint;
        TextView textPointLogDate;

        public ViewMapper() {
            //로딩뷰
            loadingView = (RelativeLayout) findViewById(R.id.loadingview);

            //메인상단
            btnBack = (StretchImageView) findViewById(R.id.user_setting_image_back);
            infoUser = (LinearLayout) findViewById(R.id.user_setting_info_user);
            infoPoint = (LinearLayout) findViewById(R.id.user_setting_info_point);
            btnEditAddress = (LinearLayout) findViewById(R.id.user_setting_lin_address);
            textNickName = (TextView) findViewById(R.id.user_setting_text_nickName);
            textAddress = (TextView) findViewById(R.id.user_setting_text_address);
            textPoint = (TextView) findViewById(R.id.user_setting_text_point);
            textBtnAddress = (TextView) findViewById(R.id.user_setting_text_address_edit);

            //메인상단2(비로그인시)
            wrapperSignIn = (RelativeLayout) findViewById(R.id.setting_user_login);
            btnSignIn = (LinearLayout) findViewById(R.id.setting_user_login_btn);
            btnBack2 = (StretchImageView) findViewById(R.id.setting_user_login_back2);

            //카테고리
            btnCateSetting = (LinearLayout) findViewById(R.id.setting_user_cate1);
            btnCateRefund = (LinearLayout) findViewById(R.id.setting_user_cate2);
            btnCatePointLog = (LinearLayout) findViewById(R.id.setting_user_cate3);
            textCateSetting = (TextView) findViewById(R.id.setting_user_text_cate1);
            textCatePrepare = (TextView) findViewById(R.id.setting_user_text_cate2);
            textCateCoup = (TextView) findViewById(R.id.setting_user_text_cate3);
            indicatorSetting = findViewById(R.id.setting_user_indicator1);
            indicatorPrepare = findViewById(R.id.setting_user_indicator2);
            indicatorCoup = findViewById(R.id.setting_user_indicator3);

            //리스트
            wrapperSetting = (LinearLayout) findViewById(R.id.user_setting_wrapper_setting);
            listNoti = (LinearLayout) findViewById(R.id.setting_list_item1);
            listTerms = (LinearLayout) findViewById(R.id.setting_list_item2);
            listAsk = (LinearLayout) findViewById(R.id.setting_list_item3);
            listLogOut = (LinearLayout) findViewById(R.id.setting_list_item5);

            //스위치
            btnSwitch = (RelativeLayout) findViewById(R.id.setting_user_switch);
            imageSwitch = (StretchImageView) findViewById(R.id.setting_user_switch_circle);

            //etc
            btnContactUs = (StretchImageView) findViewById(R.id.setting_user_contact);

            //포인트환급
            wrapperPointRefund = (RelativeLayout) findViewById(R.id.user_setting_wrapper_refund);
            textRefund = (TextView) findViewById(R.id.user_setting_text_refund);
            textRefundLog = (TextView) findViewById(R.id.user_setting_text_refund_log);

            //포인트로그
            wrapperPointLog = (RelativeLayout) findViewById(R.id.user_setting_wrapper_point);
            textPointLogTotal = (TextView) findViewById(R.id.user_setting_text_log_total);
            textPointLogStore = (TextView) findViewById(R.id.user_setting_text_log_store);
            textPointLogPoint = (TextView) findViewById(R.id.user_setting_text_log_point);
            textPointLogDate = (TextView) findViewById(R.id.user_setting_text_log_date);

            //클릭리스너연결
            loadingView.setOnClickListener(UserSettingActivity.this);
            btnBack.setOnClickListener(UserSettingActivity.this);
            infoUser.setOnClickListener(UserSettingActivity.this);
            infoPoint.setOnClickListener(UserSettingActivity.this);
            btnEditAddress.setOnClickListener(UserSettingActivity.this);
            textBtnAddress.setOnClickListener(UserSettingActivity.this);
            btnSignIn.setOnClickListener(UserSettingActivity.this);
            btnBack2.setOnClickListener(UserSettingActivity.this);
            btnCateSetting.setOnClickListener(UserSettingActivity.this);
            btnCateRefund.setOnClickListener(UserSettingActivity.this);
            textRefund.setOnClickListener(UserSettingActivity.this);
            textRefundLog.setOnClickListener(UserSettingActivity.this);
            btnCatePointLog.setOnClickListener(UserSettingActivity.this);
            listNoti.setOnClickListener(UserSettingActivity.this);
            listTerms.setOnClickListener(UserSettingActivity.this);
            listAsk.setOnClickListener(UserSettingActivity.this);
            listLogOut.setOnClickListener(UserSettingActivity.this);
            btnSwitch.setOnClickListener(UserSettingActivity.this);
            btnContactUs.setOnClickListener(UserSettingActivity.this);

        }
    }
}
