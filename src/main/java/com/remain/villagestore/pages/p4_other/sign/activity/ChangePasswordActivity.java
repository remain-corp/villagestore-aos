package com.remain.villagestore.pages.p4_other.sign.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.GmailSendManager;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by remain on 2016. 5. 20..
 */
public class ChangePasswordActivity extends FragmentActivity implements View.OnClickListener {
    private ViewMapper viewMapper;

    private ServerRequest server;
    private String validationKey = "";
    private String emailAddress = "";

    private boolean isEmailChek = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepass);
        viewMapper = new ViewMapper();
        viewInit();

    }

    private void viewInit() {
        viewMapper.toolbarTitle.setText("비밀번호 초기화");
        viewMapper.toolbarTitle.setTextColor(getResources().getColor(R.color.main_color));

        viewMapper.wrapperValidation.setAlpha(0);
        viewMapper.wrapperPassword.setAlpha(0);
        viewMapper.wrapperValidation.setVisibility(View.GONE);
        viewMapper.wrapperPassword.setVisibility(View.GONE);

        viewMapper.toolbarBackButton.setOnClickListener(this);
        viewMapper.loadingView.setOnClickListener(this);
        viewMapper.sendMail.setOnClickListener(this);
        viewMapper.textValidation.setOnClickListener(this);
        viewMapper.textConfirm.setOnClickListener(this);

        server = new ServerRequest(networkResultListener);

    }

    @Override
    public void onClick(View v) {
        if(viewMapper.loadingView == v){}
        if (v == viewMapper.toolbarBackButton) {
            finish();
        } else if (v == viewMapper.sendMail) {

            emailAddress = viewMapper.etEmail.getText().toString().trim();
            validationKey = Utils.getRandomPassword();
            if (emailAddress.equals("") || !Utils.isValidEmail(emailAddress)) {
                Toast.makeText(this, "이메일 형식이 올바르지 않습니다.", Toast.LENGTH_SHORT).show();
                return;
            }
            requestCheckIdToServer(emailAddress);
        } else if (v == viewMapper.textValidation) {
            String msg = "";
            if (isEmailChek) {
                if (validationKey.equals(viewMapper.etValidation.getText().toString().trim())) {
                    msg = "인증이 완료되었습니다.";
                    endValidation();
                    //새로운 비밀번호로 변경할작업
                } else {
                    Utils.vibrate(this, 400);
                    Utils.shakeView(viewMapper.etValidation);
                    msg = "인증번호를 확인후 다시 입력해주세요.";
                }
            }
            Toast.makeText(ChangePasswordActivity.this, msg, Toast.LENGTH_SHORT).show();
        } else if (v == viewMapper.textConfirm) {
            String pw = viewMapper.etPassword.getText().toString().trim();
            String confirmPw = viewMapper.etPasswordConfirm.getText().toString().trim();
            if(pw.length()<6){
                Utils.vibrate(this, 400);
                Utils.shakeView(viewMapper.etPassword);
                Toast.makeText(ChangePasswordActivity.this, "비밀번호를 6자리 이상 입력해주세요.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (pw.equals(confirmPw)) {
                requestChangePwToServer(pw);
            } else {
                Utils.vibrate(this, 400);
                Utils.shakeView(viewMapper.etPassword);
                Toast.makeText(ChangePasswordActivity.this, "비밀번호가 일치하지 않습니다.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void endValidation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            viewMapper.wrapperValidation.animate().translationXBy(viewMapper.wrapperValidation.getX()).translationX(viewMapper.wrapperValidation.getWidth()).setDuration(300).start();
        } else {
            viewMapper.wrapperValidation.setVisibility(View.GONE);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                viewMapper.wrapperValidation.setVisibility(View.GONE);
                viewMapper.wrapperPassword.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                    viewMapper.wrapperPassword.animate().setDuration(400).alpha(1).start();
                } else {
                    viewMapper.wrapperPassword.setAlpha(1);
                }
            }
        }, 600);
    }

    private void endSendingMail() {
        //이메일 보내기 성공 후
        viewMapper.loadingView.setVisibility(View.GONE);
        Toast.makeText(ChangePasswordActivity.this, "요청하신 " + emailAddress + " 로\n인증번호가 전송 되었습니다.", Toast.LENGTH_SHORT).show();
        isEmailChek = true;
        viewMapper.etEmail.setEnabled(false);
        viewMapper.textInfo.setVisibility(View.GONE);
        viewMapper.wrapperValidation.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            viewMapper.wrapperValidation.animate().alpha(1).setDuration(300).start();
        } else {
            viewMapper.wrapperValidation.setAlpha(1);
        }
    }

    /**
     * 아이디 확인
     */
    private void requestCheckIdToServer(String user_id) {
        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_ID, user_id);
        server.execute(NetInfo.IP_ADDRESS1, NetInfo.USER_ID_CHECK, params);
    }
    /**
     * 비밀번호 변경
     */
    private void requestChangePwToServer(String user_pass) {
        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.USER_ID, emailAddress);
        params.put(ParamInfo.USER_PASS, user_pass);
        server.execute(NetInfo.IP_ADDRESS1, NetInfo.UPDATE_PASSWORD, params);
    }

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);

                return;
            }
            if (url == NetInfo.USER_ID_CHECK) {

                try {
                    if (object.getString(ParamInfo.RESULT) != null) {
                        String result = object.getString(ParamInfo.RESULT);
                        if (result.equals("ok")) {
                            Log.e("이메일 인증완료");
                            uiHandler.sendEmptyMessage(NetInfo.WHAT_USER_ID_CHECK);
                        } else {
                            uiHandler.sendEmptyMessage(9999);
                            Log.e("인증 실패");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return;
            } else if (url == NetInfo.UPDATE_PASSWORD) {

                uiHandler.sendEmptyMessage(NetInfo.WHAT_UPDATE_PASSWORD);
                try {
                    if (object.getString(ParamInfo.RESULT) != null) {
                        Database.getInstance().checkUser(ChangePasswordActivity.this, new User(object.getJSONObject(ParamInfo.RESULT)));
                    }

                    Intent i = new Intent(ChangePasswordActivity.this, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    i.putExtra("signUp", true);
                    startActivity(i);
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return;
            }


        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            viewMapper.loadingView.setVisibility(View.GONE);
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:

                    Toast.makeText(ChangePasswordActivity.this, "네트워크가 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
                    break;
                case NetInfo.WHAT_USER_ID_CHECK:
                    new MailSender().execute();
                    Log.e(validationKey);
                    break;
                case NetInfo.WHAT_UPDATE_PASSWORD:
                    break;
                case 9999:
                    Toast.makeText(ChangePasswordActivity.this, "가입되지 않은 이메일 입니다.", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private class ViewMapper {
        private RelativeLayout toolbar;
        private StretchImageView toolbarBackButton;
        private TextView toolbarTitle;

        private RelativeLayout loadingView;

        private EditText etEmail;
        private TextView sendMail;

        private LinearLayout wrapperValidation;
        private EditText etValidation;
        private TextView textValidation;

        private LinearLayout wrapperPassword;
        private EditText etPassword;
        private EditText etPasswordConfirm;
        private TextView textConfirm;

        private TextView textInfo;

        public ViewMapper() {
            toolbar = (RelativeLayout) findViewById(R.id.changepass_toolbar);
            toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

            loadingView = (RelativeLayout) findViewById(R.id.changepass_loadingview);

            etEmail = (EditText) findViewById(R.id.changepass_et_email);
            sendMail = (TextView) findViewById(R.id.changepass_text_email);

            wrapperValidation = (LinearLayout) findViewById(R.id.changepass_wrapper_validation);
            etValidation = (EditText) findViewById(R.id.changepass_et_validation);
            textValidation = (TextView) findViewById(R.id.changepass_text_validation);

            wrapperPassword = (LinearLayout) findViewById(R.id.changepass_wrapper_pass);
            etPassword = (EditText) findViewById(R.id.changepass_et_pass);
            etPasswordConfirm = (EditText) findViewById(R.id.changepass_et_pass_confirm);
            textConfirm = (TextView) findViewById(R.id.changepass_text_confirm);

            textInfo = (TextView) findViewById(R.id.changepass_text_info);
        }
    }

    protected class MailSender extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            viewMapper.loadingView.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... params) {
            GmailSendManager sender = new GmailSendManager("remain.kr@gmail.com", "cpykfivybdlqafqm"); // SUBSTITUTE HERE
            try {
                sender.sendMail(
                        "동네로App 인증키",   //subject.getText().toString(),
                        "동네로App 인증\n\n" + validationKey + " 를 정확하게 기입해 주세요.",
                        "remain.kr@gmail.com",
                        emailAddress
                );
            } catch (Exception e) {
                Log.e("error = " + e);
            }
            return emailAddress;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            endSendingMail();
        }
    }
}
