package com.remain.villagestore.pages.p4_other.sign.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.kakao.auth.AuthType;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.exception.KakaoException;
import com.remain.villagestore.GCMIntentService;
import com.remain.villagestore.R;
import com.remain.villagestore.common.C;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.TransProgressDialog;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.DeviceInfo;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.DataManager;
import com.remain.villagestore.pages.p4_other.address.activity.SelectAddressActivity;
import com.remain.villagestore.pages.p4_other.sign.adapter.PageAdapter;
import com.remain.villagestore.pages.p4_other.sign.fragment.SignExampleFragment;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;
import com.remain.villagestore.component.image.CircleImageView;
import com.remain.villagestore.component.image.StretchImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by remain on 2016. 4. 19..
 */
public class SelectSignActivity extends FragmentActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private ViewMapper viewMapper;
    private PageAdapter pageAdapter;

    private ServerRequest server;
    private TransProgressDialog progressDialog;

    //카카오
    private SessionCallback mKakaocallback;
    private User user;

    private String userName;
    private String userId;
    private String profileUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_sign);
        viewMapper = new ViewMapper();
        executeRegisterGcm();
        viewInit();
        setData();

        Log.e("hash=" + Utils.getAppKeyHash(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
            Log.e("requestCode = " + requestCode);
            Log.e("resultCode = " + resultCode);
            Log.e("data = " + data);
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("onDestroy");
        Session.getCurrentSession().removeCallback(mKakaocallback);
    }

    final void viewInit() {
        setFragment();
        server = new ServerRequest(networkResultListener);

        viewMapper.textToolbar.setText("로그인");
    }

    final void setData() {

    }


    final void setFragment() {
        ArrayList<SignExampleFragment> arrayList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            SignExampleFragment frag = new SignExampleFragment();
            switch (i) {
                case 0:
                    frag.setImage(R.drawable.image_sign_example1);
                    break;
                case 1:
                    frag.setImage(R.drawable.image_sign_example2);
                    break;
                case 2:
                    frag.setImage(R.drawable.image_sign_example3);
                    break;
            }
            arrayList.add(frag);
        }
        pageAdapter = new PageAdapter(getSupportFragmentManager());
        pageAdapter.setFragments(arrayList);
        viewMapper.viewPager.setAdapter(pageAdapter);
        viewMapper.viewPager.addOnPageChangeListener(this);
    }

    private void pageChanged(int page) {
        ViewGroup.LayoutParams params1 = viewMapper.indicator1.getLayoutParams();
        ViewGroup.LayoutParams params2 = viewMapper.indicator2.getLayoutParams();
        ViewGroup.LayoutParams params3 = viewMapper.indicator3.getLayoutParams();

        params1.width = Utils.DPToPX(this, 15);
        params2.width = Utils.DPToPX(this, 15);
        params3.width = Utils.DPToPX(this, 15);

        viewMapper.indicator1.setImageResource(R.drawable.ico_indicator_black);
        viewMapper.indicator2.setImageResource(R.drawable.ico_indicator_black);
        viewMapper.indicator3.setImageResource(R.drawable.ico_indicator_black);

        switch (page) {
            case 0:
                viewMapper.textTitle.setText("매장에서 제공하는 쿠폰을\n더욱 편리하게 확인하세요.");
                params1.width = Utils.DPToPX(this, 20);
                viewMapper.indicator1.setLayoutParams(params1);
                viewMapper.indicator1.setImageResource(R.color.main_color);
                break;
            case 1:
                viewMapper.textTitle.setText("동네로 에서 진행하는\n다양한 이벤트에 참여해보세요.");
                params2.width = Utils.DPToPX(this, 20);
                viewMapper.indicator2.setLayoutParams(params2);
                viewMapper.indicator2.setImageResource(R.color.main_color);
                break;
            case 2:
                viewMapper.textTitle.setText("맘에 드는 매장을\n한 눈에 모아보세요!");
                params3.width = Utils.DPToPX(this, 20);
                viewMapper.indicator3.setLayoutParams(params3);
                viewMapper.indicator3.setImageResource(R.color.main_color);
                break;
        }
    }

    //카카오관련메서드
    private void isKakaoLogin() {

        // 카카오 세션을 오픈한다
        mKakaocallback = new SessionCallback();
        com.kakao.auth.Session.getCurrentSession().addCallback(mKakaocallback);
        com.kakao.auth.Session.getCurrentSession().checkAndImplicitOpen();
        com.kakao.auth.Session.getCurrentSession().open(AuthType.KAKAO_TALK_EXCLUDE_NATIVE_LOGIN, SelectSignActivity.this);
        Log.e("isKakaoLogin");
    }

    /**
     * 사용자의 상태를 알아 보기 위해 me API 호출을 한다.
     */
    boolean isOneSelect = true;

    protected void KakaorequestMe() {

        UserManagement.requestMe(new MeResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                int ErrorCode = errorResult.getErrorCode();
                int ClientErrorCode = -777;

                if (ErrorCode == ClientErrorCode) {
                    Log.e("카카오톡 서버의 네트워크가 불안정합니다. 잠시 후 다시 시도해주세요.");
                } else {
                    Log.e("오류로 카카오로그인 실패 ");
                }
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                Log.e("오류로 카카오로그인 실패 ");
            }

            @Override
            public void onSuccess(UserProfile userProfile) {
                Log.e("성공 ");
                profileUrl = userProfile.getProfileImagePath();
                userId = String.valueOf(userProfile.getId());
                userName = userProfile.getNickname();

                Log.e("profileUrl =  " + profileUrl);
                Log.e("userId =  " + userId);
                Log.e("userName =  " + userName);

                if(userName == null || userName.equals("")){
                    Log.e("유저정보는? ="+ userName);
                    userName = userId;
                }

                if (isOneSelect) {
                    Log.e("isOneSelect =  " + isOneSelect);
                    isOneSelect = false;
                    requestSelectUserToServer(userId);
                }
            }

            @Override
            public void onNotSignedUp() {
                // 자동가입이 아닐경우 동의창
                Log.e("오류로 카카오로그인 실패 ");
            }
        });
    }

    // 유저 확인
    private void requestSelectUserToServer(String id) {
        if (progressDialog == null) {
            progressDialog = TransProgressDialog.show(this, false);
        }
        Log.e("requestSelectUserToServer =  " + "유저확인" + id);
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.ID, id);
        server.execute(NetInfo.IP_ADDRESS1, NetInfo.SELECT_USER, params);

    }

    // 유저 가입
    private void requestInsertUserToServer() {
        Log.e("유저 가입");
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.DEVICE_ID, DeviceInfo.getInstance().getDeviceId(this));
        params.put(ParamInfo.ID, userId);
        params.put(ParamInfo.PASS, "");
        params.put(ParamInfo.ADDRESS1, "");
        params.put(ParamInfo.ADDRESS2, userName);  //닉네임테스트
        params.put(ParamInfo.PUSHID, DataManager.getInstance().getPushID());
        server.execute(NetInfo.INSERT_USER, params);
    }

    // 푸쉬아이디 업데이트
    private void requestUpdatePushIdToServer(String userSeq) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.SEQ, userSeq);
        params.put(ParamInfo.COLUMN, "pushid");
        params.put(ParamInfo.DATA, DataManager.getInstance().getPushID());

        server.execute(NetInfo.IP_ADDRESS1 , NetInfo.UPDATE_ALL_CUL_USER, params);
    }

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            if (error != null) {
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.INSERT_USER) {
                Log.e("INSERT_USER");
                uiHandler.sendEmptyMessage(NetInfo.WHAT_INSERT_USER);
            }

            if (url == NetInfo.SELECT_USER) {
                if (progressDialog != null) progressDialog.dismiss();
                try {
                    if (!object.isNull(ParamInfo.USERLIST)) {

                        JSONArray array = object.getJSONArray(ParamInfo.USERLIST);
                        if(!array.isNull(0)){
                            //이미 등록되어있는 유저
                            if (array.length() > 0) {
                                user = new User(array.getJSONObject(0));
                            }
                            Log.e("getAddress"+user.getAddress());
                            Log.e("length"+array.length());
                            DataManager.getInstance().setUser(user);
                            Database.getInstance().checkUser(SelectSignActivity.this, user);
                            isOneSelect = true;

                            Utils.setPreferenceData(SelectSignActivity.this, C.pref.IS_KAKAO, ParamInfo.OK);

                            //푸쉬아이디 업데이트
                            requestUpdatePushIdToServer(String.valueOf(user.getSeq()));

                            if (user.getAddress() == null || user.getAddress().equals("")) {
                                //주소가없을경우
                                Log.e("주소가 있을 경우로 나오나");
                                Intent intent = new Intent(SelectSignActivity.this, SelectAddressActivity.class);
                                intent.putExtra(ParamInfo.DETAIL_MODE, true);
                                intent.putExtra("isSignMode", true);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                            } else {
                                //주소가 있을 경우
                                Log.e("여기서 에러인가 있을경우");
                                Intent i = new Intent(SelectSignActivity.this, MainActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                i.putExtra("signUp", true);
                                startActivity(i);
                                finish();
                            }
                        }else{
                            //등록해야되는 유저
                            Log.e("등록해야되는 유저");
                            requestInsertUserToServer();
                        }
                    } else {
                        //등록해야되는 유저
                        Log.e("등록해야되는 유저");
                        requestInsertUserToServer();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (progressDialog != null) progressDialog.dismiss();
        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e(msg.what+"");
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Toast.makeText(SelectSignActivity.this, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    break;

                case NetInfo.WHAT_INSERT_USER:
                    Log.e("WHAT_INSERT_USER");
                    requestSelectUserToServer(userId);
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {

        if (v == viewMapper.btnSignKakao) {
            isKakaoLogin();
        } else if (v == viewMapper.btnSignEmail) {
            Intent i = new Intent(this, SignActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.putExtra("signUp", false);
            startActivity(i);
        } else if (v == viewMapper.btnSignUp) {
            Intent i = new Intent(this, SignActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.putExtra("signUp", true);
            startActivity(i);
        } else if (v == viewMapper.btnBackBtn) {
            finish();
        }

    }

    //ViewPager
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        pageChanged(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class SessionCallback implements ISessionCallback {
        @Override
        public void onSessionOpened() {
            Log.e("세션 오픈됨");
            // 사용자 정보를 가져옴, 회원가입 미가입시 자동가입 시킴
            KakaorequestMe();
        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            if (exception != null) {
                Log.e("세션오픈실패 : " + exception.getMessage());
            }
        }
    }

    /**
     * GCM Id 등록 비동기 작업
     */
    @SuppressLint("NewApi")
    public void executeRegisterGcm() {

        RegisterGcmAsyncTask registerGcmAsyncTask = new RegisterGcmAsyncTask();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            registerGcmAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            registerGcmAsyncTask.execute();
        }
    }

    /**
     * GCM Id 등록 비동기 작업
     */
    class RegisterGcmAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... doParams) {

            GCMRegistrar.checkDevice(SelectSignActivity.this);
            GCMRegistrar.checkManifest(SelectSignActivity.this);

            String pushId = GCMRegistrar.getRegistrationId(SelectSignActivity.this);

            if (AppInfo.DEFAULT_STRING.equals(pushId)) {

                GCMRegistrar.register(SelectSignActivity.this, GCMIntentService.PROJECT_ID);
                DataManager.getInstance().setPushID(GCMRegistrar.getRegistrationId(SelectSignActivity.this));
            } else {
                DataManager.getInstance().setPushID(pushId);
            }
            return null;
        }
    }

    private class ViewMapper {
        RelativeLayout toolbar;
        StretchImageView btnBackBtn;
        TextView textToolbar;

        TextView textTitle;
        ViewPager viewPager;

        StretchImageView btnSignKakao;
        StretchImageView btnSignEmail;
        StretchImageView btnSignUp;

        CircleImageView indicator1;
        CircleImageView indicator2;
        CircleImageView indicator3;

        public ViewMapper() {
            toolbar = (RelativeLayout) findViewById(R.id.sign_toolbar);
            btnBackBtn = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
            textToolbar = (TextView) toolbar.findViewById(R.id.toolbar_title);

            textTitle = (TextView) findViewById(R.id.sign_text_title);
            viewPager = (ViewPager) findViewById(R.id.sign_viewpager);

            btnSignKakao = (StretchImageView) findViewById(R.id.sign_image_kakao);
            btnSignEmail = (StretchImageView) findViewById(R.id.sign_image_email);
            btnSignUp = (StretchImageView) findViewById(R.id.sign_image_signup);
            indicator1 = (CircleImageView) findViewById(R.id.sign_indicator1);
            indicator2 = (CircleImageView) findViewById(R.id.sign_indicator2);
            indicator3 = (CircleImageView) findViewById(R.id.sign_indicator3);

            //setListener
            btnSignKakao.setOnClickListener(SelectSignActivity.this);
            btnSignEmail.setOnClickListener(SelectSignActivity.this);
            btnSignUp.setOnClickListener(SelectSignActivity.this);
            btnBackBtn.setOnClickListener(SelectSignActivity.this);
        }
    }
}
