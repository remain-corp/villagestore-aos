package com.remain.villagestore.pages.p4_other.sign.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.common.C;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.ClearEditText;
import com.remain.villagestore.component.TransProgressDialog;
import com.remain.villagestore.component.image.StretchImageView;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.DeviceInfo;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.DataManager;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.pages.p4_other.address.activity.SelectAddressActivity;
import com.remain.villagestore.pages.p4_other.policy.activity.PolicyInfoActivity;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by remain on 2016. 8. 1..
 */
public class SignActivity extends FragmentActivity implements View.OnClickListener {
    private ViewMapper viewMapper;
    private ServerRequest server;

    private boolean isSignUp = false;
    private String userId = "";
    private String userNick = "";
    private String userPass = "";
    private boolean isPolicyAgree = false;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        viewMapper = new ViewMapper();
        checkIntent();
        viewInit();
        setData();
    }

    final void checkIntent() {
        Intent i = getIntent();
        isSignUp = i.getBooleanExtra("signUp", false);
    }

    final void viewInit() {
        server = new ServerRequest(networkResultListener);

        viewMapper.loadingView.setVisibility(View.GONE);

        viewMapper.wrapperSignUp1.setVisibility(View.GONE);
        viewMapper.wrapperSignUp2.setVisibility(View.GONE);
        viewMapper.wrapperSignUp3.setVisibility(View.GONE);
        viewMapper.wrapperSignIn.setVisibility(View.GONE);
        viewMapper.textFindIdInfo.setVisibility(View.GONE);

        if (isSignUp) {
            viewMapper.toolbarTitle.setText("회원가입");
            viewMapper.wrapperSignUp1.setVisibility(View.VISIBLE);
        } else {
            viewMapper.toolbarTitle.setText("로그인");
            viewMapper.wrapperSignIn.setVisibility(View.VISIBLE);
            viewMapper.textFindIdInfo.setVisibility(View.VISIBLE);
        }
    }

    final void setData() {

    }

    private void changeWrapper(boolean next, final View currentWrapper, final View targetWrapper) {
        if (next) {

            if (currentWrapper != null && targetWrapper != null) {
                targetWrapper.setVisibility(View.INVISIBLE);
                targetWrapper.setAlpha(0f);
                targetWrapper.setVisibility(View.VISIBLE);
                currentWrapper.animate().translationX(-currentWrapper.getWidth() - 10).setDuration(280).start();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        targetWrapper.animate().alpha(1f).setDuration(300);
                    }
                }, 260);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        currentWrapper.setVisibility(View.GONE);
                    }
                }, 580);
            }

        } else {
            currentWrapper.animate().alpha(0f).setDuration(300);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    targetWrapper.setVisibility(View.VISIBLE);
                    targetWrapper.animate().translationX(0).setDuration(280).start();
                }
            }, 260);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentWrapper.setVisibility(View.GONE);
                }
            }, 580);
        }
    }

    private boolean verifySign(int verifyWhat) {
        /*
        * 1 == 이메일, 닉네임 검증
        * 2 == 비밀번호 검증
        * 3 == 약관동의 검증
        * 4 == 로그인 검증
        * */
        View target = null;
        boolean verifyResult = true;
        String msg = "";

        if (verifyWhat == 1) {
            if (viewMapper.etSignUpNickName.getText().toString().equals("")) {
                target = viewMapper.etSignUpNickName;
                verifyResult = false;
                msg = "닉네임을 입력해 주세요.";
            }
            if (!Utils.isValidEmail(viewMapper.etSignUpEmail.getText().toString())) {
                target = viewMapper.etSignUpEmail;
                verifyResult = false;
                msg = "이메일 형식이 올바르지 않습니다.";
            }
        } else if (verifyWhat == 2) {
            if (!viewMapper.etPass.getText().toString().equals(viewMapper.etPassConfirm.getText().toString())) {
                target = viewMapper.etPassConfirm;
                verifyResult = false;
                msg = "비밀번호가 일치하지 않습니다.";
            }
            if (viewMapper.etPass.getText().toString().equals("") || viewMapper.etPass.getText().toString().length() < 6) {
                if (viewMapper.etPass.getText().toString().equals("")) {
                    target = viewMapper.etPass;
                    verifyResult = false;
                    msg = "비밀번호를 입력해 주세요.";
                } else if (viewMapper.etPass.getText().toString().length() < 6) {
                    target = viewMapper.etPass;
                    verifyResult = false;
                    msg = "비밀번호는 6자 이상입니다.";
                }
            }
        } else if (verifyWhat == 3) {
            if (!isPolicyAgree) {
                target = viewMapper.imageAgreePolicy;
                verifyResult = false;
                msg = "개인정보 수집 및 이용에 동의해 주세요.";
            }
        } else if (verifyWhat == 4) {
            if (viewMapper.etSignInPass.getText().toString().equals("") || viewMapper.etSignInPass.getText().toString().length() < 6) {
                if (viewMapper.etSignInPass.getText().toString().equals("")) {
                    target = viewMapper.etSignInPass;
                    verifyResult = false;
                    msg = "비밀번호를 입력해 주세요.";
                } else if (viewMapper.etSignInPass.getText().toString().length() < 6) {
                    target = viewMapper.etSignInPass;
                    verifyResult = false;
                    msg = "비밀번호는 6자 이상입니다.";
                }
            }
            if (!Utils.isValidEmail(viewMapper.etSignInEmail.getText().toString())) {
                target = viewMapper.etSignInEmail;
                verifyResult = false;
                msg = "이메일 형식이 올바르지 않습니다.";
            }
        }

        Log.e("veri = " + verifyResult);
        if (!verifyResult) {
            Utils.vibrate(this, 400);
            Utils.shakeView(target);
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
        return verifyResult;
    }

    /*
    * 아이디, 닉네임 체크
    * */
    private void requestCheckUserToServer(String id, String nickName) {
        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.ID, id);
        params.put(ParamInfo.ADDRESS2, nickName);

        server.execute(NetInfo.ID_CHECK, params);

    }

    /*
    * 회원가입
    * */
    private void requestInsertUserToServer() {
        viewMapper.loadingView.setVisibility(View.VISIBLE);
        Log.e("id = "+userId);
        Log.e("userPass = "+userPass);
        Log.e("userNick = "+userNick);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.DEVICE_ID, DeviceInfo.getInstance().getDeviceId(this));
        params.put(ParamInfo.ID, userId);
        params.put(ParamInfo.PASS, userPass);
        params.put(ParamInfo.ADDRESS1, "");
        params.put(ParamInfo.ADDRESS2, userNick);
        params.put(ParamInfo.PUSHID, DataManager.getInstance().getPushID());

        server.execute(NetInfo.INSERT_USER, params);
    }

    /*
    * 가입된 유저정보 셀렉트
    * */
    private void requestSelectUserToServer(String id) {
        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.ID, id);

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.SELECT_USER, params);

    }

    /*
    * 로그인 요청
    * */
    private void requestLoginToServer(String id, String pass) {
        viewMapper.loadingView.setVisibility(View.VISIBLE);

        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.ID, id);
        params.put(ParamInfo.PASS, pass);

        server.execute(NetInfo.LOGIN_USER, params);
    }

    // 푸쉬아이디 업데이트
    private void requestUpdatePushIdToServer(String userSeq) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.SEQ, userSeq);
        params.put(ParamInfo.COLUMN, "pushid");
        params.put(ParamInfo.DATA, DataManager.getInstance().getPushID());

        server.execute(NetInfo.IP_ADDRESS1 , NetInfo.UPDATE_ALL_CUL_USER, params);
    }

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {

                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.INSERT_USER) {
                uiHandler.sendEmptyMessage(NetInfo.WHAT_INSERT_USER);

            }else if (url == NetInfo.ID_CHECK) {
                //uiHandler.sendEmptyMessage(NetInfo.WHAT_INSERT_USER);
                if (!object.isNull("result")) {
                    Bundle data = new Bundle();
                    Message msg = Message.obtain();
                    try {
                        Log.e(object.getString("result"));
                        String result = object.getString("result");

                        if (result.equals("ID")) {
                            //아이디중복
                            data.putString("failMSG", "중복된 아이디 입니다.");
                            data.putString("view", "id");
                            msg.setData(data);
                            uiHandler.sendMessage(msg);
                        } else if (result.equals("NICK")) {
                            //닉네임중복
                            data.putString("failMSG", "중복된 닉네임 입니다.");
                            data.putString("view", "nick");
                            msg.setData(data);
                            uiHandler.sendMessage(msg);
                        } else if (result.equals("OK")) {
                            //가입가능
                            uiHandler.sendEmptyMessage(NetInfo.WHAT_ID_CHECK);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }else if (url == NetInfo.SELECT_USER) {
                try {
                    if (!object.isNull(ParamInfo.USERLIST)) {
                        JSONArray array = object.getJSONArray(ParamInfo.USERLIST);
                        //가입후 유저정보를 저장하기 위해 검색
                        if (array.length() > 0) {
                            user = new User(array.getJSONObject(0));
                            Database.getInstance().checkUser(SignActivity.this, user);
                        }
                        uiHandler.sendEmptyMessage(NetInfo.WHAT_SELECT_USER);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }if (url == NetInfo.LOGIN_USER) {
                Bundle data = new Bundle();
                Message msg = Message.obtain();
                try {
                    if (!object.isNull("result")) {
                        if (object.getString("result").equals("ID")) {
                            Log.e("ID없음 진입");
                            data.putString("view", "signInId");
                            data.putString("failMSG", "아이디를 확인해 주세요.");
                            msg.setData(data);
                            uiHandler.sendMessage(msg);
                        } else if (object.getString("result").equals("FAIL")) {
                            Log.e("FAIL 진입");
                            data.putString("view", "signInPass");
                            data.putString("failMSG", "비밀번호를 확인해 주세요.");
                            msg.setData(data);
                            uiHandler.sendMessage(msg);
                        } else if (object.getString("result").equals("OK")) {
                            Log.e("OK 진입");
                            if (!object.isNull("info")) {
                                Log.e("info 진입");
                                JSONArray array = object.getJSONArray("info");
                                user = new User(array.getJSONObject(0));

                                Log.e("디테일" + user.getDetailAddress());
                                Log.e("닉넴" + user.getUserNickName());

                                Database.getInstance().checkUser(SignActivity.this, user);
                                uiHandler.sendEmptyMessage(NetInfo.WHAT_LOGIN_USER);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            viewMapper.loadingView.setVisibility(View.GONE);

            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Toast.makeText(SignActivity.this, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    break;

                case NetInfo.WHAT_INSERT_USER:
                    //회원가입 완료
                    requestSelectUserToServer(userId);
                    break;

                case NetInfo.WHAT_SELECT_USER:
                    if(user != null){
                        Intent intent = new Intent(SignActivity.this, SelectAddressActivity.class);
                        intent.putExtra("isSignMode", true);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        finish();
                    }
                    break;
                case NetInfo.WHAT_LOGIN_USER:
                    Utils.setPreferenceData(SignActivity.this, C.pref.IS_KAKAO, ParamInfo.NO);

                    //푸쉬업데이트
                    requestUpdatePushIdToServer(String.valueOf(user.getSeq()));

                    //로그인 성공
                    if (user.getAddress() == null || user.getAddress().equals("")) {
                        //주소설정이 안된경우
                        Intent i = new Intent(SignActivity.this, SelectAddressActivity.class);
                        i.putExtra("isSignMode", true);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                        finish();
                        break;

                    } else {
                        //주소설정이 된 경우
                        Intent i = new Intent(SignActivity.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        i.putExtra("signUp", true);
                        startActivity(i);
                        finish();
                        break;

                    }
                case NetInfo.WHAT_ID_CHECK:
                    //아이디 닉네임체크 완료
                    viewMapper.wrapperSignUp2.setVisibility(View.INVISIBLE);
                    userId = viewMapper.etSignUpEmail.getText().toString();
                    userNick = viewMapper.etSignUpNickName.getText().toString();
                    viewMapper.textInputId.setText("아이디 : " + userId);
                    viewMapper.textInputNick.setText("닉네임 : " + userNick);
                    
                    changeWrapper(true, viewMapper.wrapperSignUp1, viewMapper.wrapperSignUp2);
                    break;
                default:
                    Log.e("핸들러 에러 진입.");
                    Toast.makeText(SignActivity.this, msg.getData().getString("failMSG"), Toast.LENGTH_SHORT).show();
                    if (msg.getData().getString("view").equals("id")) {
                        Utils.vibrate(SignActivity.this, 400);
                        Utils.shakeView(viewMapper.etSignUpEmail);
                    } else if (msg.getData().getString("view").equals("nick")) {
                        Utils.vibrate(SignActivity.this, 400);
                        Utils.shakeView(viewMapper.etSignUpNickName);
                    } else if (msg.getData().getString("view").equals("signInId")) {
                        Utils.vibrate(SignActivity.this, 400);
                        Utils.shakeView(viewMapper.etSignInEmail);
                    }else if (msg.getData().getString("view").equals("signInPass")) {
                        Utils.vibrate(SignActivity.this, 400);
                        Utils.shakeView(viewMapper.etSignInPass);
                    }
                    break;

            }

        }
    };

    @Override
    public void onClick(View v) {
        if (v == viewMapper.toolbarBackButton) {
            finish();
        } else if (v == viewMapper.textWrapper1Confirm) {
            Log.e("오는가");
            if (verifySign(1)) {
                requestCheckUserToServer(viewMapper.etSignUpEmail.getText().toString().trim(), viewMapper.etSignUpNickName.getText().toString().trim());
            }
        } else if (v == viewMapper.textWrapper2Cancel) {
            changeWrapper(false, viewMapper.wrapperSignUp2, viewMapper.wrapperSignUp1);
        } else if (v == viewMapper.textWrapper2Confirm) {
            if(verifySign(2)){
                userPass = viewMapper.etPass.getText().toString();
                changeWrapper(true, viewMapper.wrapperSignUp2, viewMapper.wrapperSignUp3);
            }
        } else if (v == viewMapper.textWrapper3Cancel) {
            changeWrapper(false, viewMapper.wrapperSignUp3, viewMapper.wrapperSignUp2);
        } else if (v == viewMapper.textWrapper3Confirm) {
            //회원가입 시작
            if(verifySign(3)){
                requestInsertUserToServer();
            }
        } else if (v == viewMapper.imageAgreePolicy){
            isPolicyAgree = !isPolicyAgree;
            if(isPolicyAgree){
                viewMapper.imageAgreePolicy.setImageResource(R.drawable.image_check_yes);
            }else {
                viewMapper.imageAgreePolicy.setImageResource(R.drawable.image_check_no);
            }
        } else if (v == viewMapper.btnAgreeService) {
            Intent i = new Intent(this, PolicyInfoActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.putExtra("signUp", PolicyInfoActivity.USE_SERVICE_POLICY);
            startActivity(i);
        } else if (v == viewMapper.btnAgreePersonerInfo1) {
            Intent i = new Intent(this, PolicyInfoActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.putExtra("signUp", PolicyInfoActivity.USE_PERSONER_INFO_POLICY2);
            startActivity(i);
        } else if (v == viewMapper.btnAgreePersonerInfo2) {
            Intent i = new Intent(this, PolicyInfoActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.putExtra("signUp", PolicyInfoActivity.USE_PERSONER_INFO_POLICY);
            startActivity(i);
        }else if(v == viewMapper.textSignInConfirm){
            if(verifySign(4)){
                requestLoginToServer(viewMapper.etSignInEmail.getText().toString(), viewMapper.etSignInPass.getText().toString());
            }
        }else if(v == viewMapper.imageFindPass){
            Intent intent = new Intent(this, ChangePasswordActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

    private class ViewMapper {
        //로딩뷰
        private RelativeLayout loadingView;

        //툴바
        private RelativeLayout toolbar;
        private StretchImageView toolbarBackButton;
        private TextView toolbarTitle;

        //signUp wrapper1
        private LinearLayout wrapperSignUp1;
        private ClearEditText etSignUpEmail;
        private ClearEditText etSignUpNickName;
        private TextView textWrapper1Confirm;

        //signUp wrapper2
        private LinearLayout wrapperSignUp2;
        private TextView textWrapper2Confirm;
        private TextView textWrapper2Cancel;
        private TextView textInputId;
        private TextView textInputNick;
        private EditText etPass;
        private EditText etPassConfirm;

        //signUp wrapper3
        private LinearLayout wrapperSignUp3;
        private TextView textWrapper3Confirm;
        private TextView textWrapper3Cancel;
        private StretchImageView imageAgreePolicy;
        private StretchImageView btnAgreeService;
        private StretchImageView btnAgreePersonerInfo1;
        private StretchImageView btnAgreePersonerInfo2;


        //signIn
        private LinearLayout wrapperSignIn;
        private ClearEditText etSignInEmail;
        private EditText etSignInPass;
        private TextView textSignInConfirm;
        private TextView textFindIdInfo;
        private StretchImageView imageFindPass;

        public ViewMapper() {
            //로딩뷰
            loadingView = (RelativeLayout) findViewById(R.id.loadingview);

            //툴바
            toolbar = (RelativeLayout) findViewById(R.id.toolbar);
            toolbarBackButton = (StretchImageView) toolbar.findViewById(R.id.toolbar_backbutton);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

            //signUp wrapper1
            wrapperSignUp1 = (LinearLayout) findViewById(R.id.signup_wrapper1);
            etSignUpEmail = (ClearEditText) findViewById(R.id.signup_et_email);
            etSignUpNickName = (ClearEditText) findViewById(R.id.signup_et_nickname);
            textWrapper1Confirm = (TextView) findViewById(R.id.signup_text_wrapper1_confirm);

            //signUp wrapper2
            wrapperSignUp2 = (LinearLayout) findViewById(R.id.signup_wrapper2);
            textWrapper2Confirm = (TextView) findViewById(R.id.signup_text_wrapper2_confirm);
            textWrapper2Cancel = (TextView) findViewById(R.id.signup_text_wrapper2_cancel);
            textInputId = (TextView) findViewById(R.id.signup_text_input_id);
            textInputNick = (TextView) findViewById(R.id.signup_text_input_nick);
            etPass = (EditText) findViewById(R.id.signup_et_pass);
            etPassConfirm = (EditText) findViewById(R.id.signup_et_pass_confirm);
            
            //signUp wrapper3
            wrapperSignUp3 = (LinearLayout) findViewById(R.id.signup_wrapper3);
            textWrapper3Confirm = (TextView) findViewById(R.id.signup_text_wrapper3_confirm);
            textWrapper3Cancel = (TextView) findViewById(R.id.signup_text_wrapper3_cancel);
            imageAgreePolicy = (StretchImageView) findViewById(R.id.signup_image_policy_agree);
            btnAgreeService = (StretchImageView) findViewById(R.id.signup_image_policy_service);
            btnAgreePersonerInfo1 = (StretchImageView) findViewById(R.id.signup_image_policy_personerinfo1);
            btnAgreePersonerInfo2 = (StretchImageView) findViewById(R.id.signup_image_policy_personerinfo2);

            //signIn
            wrapperSignIn = (LinearLayout) findViewById(R.id.signin_wrapper);
            etSignInEmail = (ClearEditText) findViewById(R.id.signin_et_email);
            etSignInPass = (EditText) findViewById(R.id.signin_et_pass);
            textSignInConfirm = (TextView) findViewById(R.id.signin_text_confirm);
            textFindIdInfo = (TextView) findViewById(R.id.signin_text_find_id);
            imageFindPass = (StretchImageView) findViewById(R.id.signup_find_pw);

            //listener
            toolbarBackButton.setOnClickListener(SignActivity.this);
            textWrapper1Confirm.setOnClickListener(SignActivity.this);
            textWrapper2Confirm.setOnClickListener(SignActivity.this);
            textWrapper2Cancel.setOnClickListener(SignActivity.this);
            textWrapper3Confirm.setOnClickListener(SignActivity.this);
            textWrapper3Cancel.setOnClickListener(SignActivity.this);
            imageAgreePolicy.setOnClickListener(SignActivity.this);
            btnAgreeService.setOnClickListener(SignActivity.this);
            btnAgreePersonerInfo1.setOnClickListener(SignActivity.this);
            btnAgreePersonerInfo2.setOnClickListener(SignActivity.this);
            textSignInConfirm.setOnClickListener(SignActivity.this);
            imageFindPass.setOnClickListener(SignActivity.this);
        }
    }


}
