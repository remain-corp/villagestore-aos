package com.remain.villagestore.pages.p4_other.sign.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.remain.villagestore.GCMIntentService;
import com.remain.villagestore.R;
import com.remain.villagestore.common.C;
import com.remain.villagestore.pages.p2_main.activity.MainActivity;
import com.remain.villagestore.common.Utils;
import com.remain.villagestore.component.TransProgressDialog;
import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;
import com.remain.villagestore.info.AppInfo;
import com.remain.villagestore.info.DeviceInfo;
import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;
import com.remain.villagestore.info.ParamInfo;
import com.remain.villagestore.manager.DataManager;
import com.remain.villagestore.pages.p4_other.address.activity.SelectAddressActivity;
import com.remain.villagestore.pages.p4_other.policy.activity.PolicyInfoActivity;
import com.remain.villagestore.util.Log;
import com.remain.villagestore.util.ServerRequest;
import com.remain.villagestore.component.PagingScrollView.PagingScrollView;
import com.remain.villagestore.component.image.StretchImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by remain on 2016. 4. 18..
 */
public class SignUp extends FragmentActivity implements View.OnClickListener, PagingScrollView.OnPagineScrollViewListener {

    private ViewMapper viewMapper;
    private ArrayList<View> pages = new ArrayList<>();

    private TransProgressDialog progressDialog;
    private ServerRequest server;

    private boolean signUp = true;

    //pageSignUp
    private boolean serviceAgree = false;
    private LinearLayout wrapperSignUp;
    private StretchImageView btnAgreeCheck;
    private StretchImageView btnAgreeService;
    private StretchImageView btnAgreePersonerInfo1;
    private StretchImageView btnAgreePersonerInfo2;
    private EditText etSignUpEmail;
    private EditText etSignUpNickName;
    private EditText etSignUpPass;
    private EditText etSignUpPassConfirm;

    private TextView btnSignUp;

    //pageSignIn
    private TextView btnSignIn;
    private TextView textSignUpEmail;
    private TextView etSignInEmail;
    private TextView etSignInPass;
    private StretchImageView btnFindPw;
    private TextView textFindId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        viewMapper = new ViewMapper();

        checkIntent();
        viewInit();
        setData();
        executeRegisterGcm();
    }

    final void checkIntent() {
        Intent i = getIntent();
        signUp = i.getBooleanExtra("signUp", true);
    }

    final void viewInit() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //signUpPage
        View viewSignUp = inflater.inflate(R.layout.layout_signup_page, null);
        wrapperSignUp = (LinearLayout) viewSignUp.findViewById(R.id.item_signup_wrapper);
        btnAgreeCheck = (StretchImageView) viewSignUp.findViewById(R.id.item_signup_image_agree);
        btnAgreePersonerInfo1 = (StretchImageView) viewSignUp.findViewById(R.id.signup_image_personerinfo1);
        btnAgreePersonerInfo2 = (StretchImageView) viewSignUp.findViewById(R.id.signup_image_personerinfo2);

        btnAgreeService = (StretchImageView) viewSignUp.findViewById(R.id.signup_image_service);
        btnSignUp = (TextView) viewSignUp.findViewById(R.id.signup_text_start);
        etSignUpEmail = (EditText) viewSignUp.findViewById(R.id.item_signup_et_email);
        etSignUpNickName = (EditText) viewSignUp.findViewById(R.id.item_signup_et_nickname);
        etSignUpPass = (EditText) viewSignUp.findViewById(R.id.item_signup_et_pass);
        etSignUpPassConfirm = (EditText) viewSignUp.findViewById(R.id.item_signup_et_pass_confirm);

        wrapperSignUp.setOnClickListener(this);
        btnAgreeCheck.setOnClickListener(this);
        btnAgreePersonerInfo1.setOnClickListener(this);
        btnAgreePersonerInfo2.setOnClickListener(this);
        btnAgreeService.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);

        //signInPage
        View viewSignIn = inflater.inflate(R.layout.layout_signup_page, null);
        LinearLayout linearNickName = (LinearLayout) viewSignIn.findViewById(R.id.item_signup_wrapper_nickname);
        LinearLayout linearPassConfirm = (LinearLayout) viewSignIn.findViewById(R.id.item_signup_wrapper_pass_confirm);
        LinearLayout linearAgree = (LinearLayout) viewSignIn.findViewById(R.id.item_signup_wrapper_agree);

        etSignInEmail = (EditText) viewSignIn.findViewById(R.id.item_signup_et_email);
        textSignUpEmail = (TextView) viewSignIn.findViewById(R.id.item_signup_text_email);
        etSignInPass = (EditText) viewSignIn.findViewById(R.id.item_signup_et_pass);
        etSignInPass.setImeOptions(EditorInfo.IME_ACTION_DONE);
        btnSignIn = (TextView) viewSignIn.findViewById(R.id.signup_text_start);
        btnFindPw = (StretchImageView) viewSignIn.findViewById(R.id.signup_find_pw);
        textFindId = (TextView) viewSignIn.findViewById(R.id.signup_text_find_id);

        linearNickName.setVisibility(View.GONE);
        linearPassConfirm.setVisibility(View.GONE);
        linearAgree.setVisibility(View.GONE);
        textSignUpEmail.setVisibility(View.GONE);
        btnFindPw.setVisibility(View.VISIBLE);
        textFindId.setVisibility(View.VISIBLE);

        btnSignIn.setOnClickListener(this);
        btnFindPw.setOnClickListener(this);

        pages.add(viewSignUp);
        pages.add(viewSignIn);

        viewMapper.viewPager.setObjects(pages);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (signUp) {
                    viewMapper.viewPager.setPage(0);
                } else {
                    viewMapper.viewPager.setPage(1);
                }

            }
        }, 200);

    }

    final void setData() {
        server = new ServerRequest(networkResultListener);
    }

    private void changePage(int page) {
        viewMapper.btnSignUp.setTextColor(Color.parseColor("#999999"));
        viewMapper.btnSignIn.setTextColor(Color.parseColor("#999999"));

        if (page == 0) {
            viewMapper.btnSignUp.setTextColor(Color.parseColor("#ffb266"));
            viewMapper.indicator.setX(Utils.DPToPX(this, 55));
            signUp = true;
        } else {
            viewMapper.btnSignIn.setTextColor(Color.parseColor("#ffb266"));
            viewMapper.indicator.setX(Utils.DPToPX(this, 182));
            signUp = false;
        }

    }

    /*
    * 회원가입 데이터 검증작업
    * */
    private boolean verifySignUp() {
        String msg = "";
        View target = null;
        boolean verifyResult = true;

        if (!serviceAgree) {
            target = btnAgreeCheck;
            verifyResult = false;
            msg = "개인정보 수집 및 이용에 동의해 주세요.";
        }
        if (!etSignUpPassConfirm.getText().toString().equals(etSignUpPass.getText().toString())) {
            target = etSignUpPassConfirm;
            verifyResult = false;
            msg = "비밀번호가 일치하지 않습니다.";
        }
        if (etSignUpPass.getText().toString().equals("") || etSignUpPass.getText().toString().length() < 6) {
            if (etSignUpPass.getText().toString().equals("")) {
                target = etSignUpPass;
                verifyResult = false;
                msg = "비밀번호를 입력해 주세요.";
            } else if (etSignUpPass.getText().toString().length() < 6) {
                target = etSignUpPass;
                verifyResult = false;
                msg = "비밀번호는 6자 이상입니다.";
            }
        }
        if (etSignUpNickName.getText().toString().equals("")) {
            target = etSignUpNickName;
            verifyResult = false;
            msg = "닉네임을 입력해 주세요.";
        }
        if (!Utils.isValidEmail(etSignUpEmail.getText().toString())) {
            target = etSignUpEmail;
            verifyResult = false;
            msg = "이메일 형식이 올바르지 않습니다.";
        }

        if (verifyResult) {

        } else {
            Utils.vibrate(this, 400);
            shakeView(target);
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
        return verifyResult;
    }

    /*
    * 로그인 데이터 검증작업
    * */
    private boolean verifySignIn() {
        String msg = "";
        View target = null;
        boolean verifyResult = true;

        if (etSignInPass.getText().toString().equals("") || etSignInPass.getText().toString().length() < 6) {
            if (etSignInPass.getText().toString().equals("")) {
                target = etSignInPass;
                verifyResult = false;
                msg = "비밀번호를 입력해 주세요.";
            } else if (etSignInPass.getText().toString().length() < 6) {
                target = etSignInPass;
                verifyResult = false;
                msg = "비밀번호는 6자 이상입니다.";
            }
        }
        if (!Utils.isValidEmail(etSignInEmail.getText().toString())) {
            target = etSignInEmail;
            verifyResult = false;
            msg = "이메일 형식이 올바르지 않습니다.";
        }

        if (verifyResult) {

        } else {
            Utils.vibrate(this, 500);
            shakeView(target);
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
        return verifyResult;
    }

    private void requestInsertUserToServer() {
        if (progressDialog == null) {
            progressDialog = TransProgressDialog.show(this, false);
        }
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.DEVICE_ID, DeviceInfo.getInstance().getDeviceId(this));
        params.put(ParamInfo.ID, etSignUpEmail.getText().toString());
        params.put(ParamInfo.PASS, etSignUpPass.getText().toString());
        params.put(ParamInfo.ADDRESS1, "");
        params.put(ParamInfo.ADDRESS2, etSignUpNickName.getText().toString());

        params.put(ParamInfo.PUSHID, DataManager.getInstance().getPushID());

        server.execute(NetInfo.INSERT_USER, params);
    }

    // 푸쉬아이디 업데이트
    private void requestUpdatePushIdToServer(String userSeq) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.SEQ, userSeq);
        params.put(ParamInfo.COLUMN, "pushid");
        params.put(ParamInfo.DATA, DataManager.getInstance().getPushID());

        server.execute(NetInfo.IP_ADDRESS1 , NetInfo.UPDATE_ALL_CUL_USER, params);
    }

    //로그인요청
    private void requestLoginToServer(String id, String pass) {
        if (progressDialog == null) {
            progressDialog = TransProgressDialog.show(this, false);
        }
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.ID, id);
        params.put(ParamInfo.PASS, pass);

        server.execute(NetInfo.LOGIN_USER, params);
    }

    private void requestCheckUserToServer(String id, String nickName) {
        if (progressDialog == null) {
            progressDialog = TransProgressDialog.show(this, false);
        }
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.ID, id);
        params.put(ParamInfo.ADDRESS2, nickName);

        server.execute(NetInfo.ID_CHECK, params);

    }

    private void requestSelectUserToServer(String id) {
        if (progressDialog == null) {
            progressDialog = TransProgressDialog.show(this, false);
        }
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        params.put(ParamInfo.HASHKEY, NetInfo.HASH_KEY);
        params.put(ParamInfo.ID, id);

        server.execute(NetInfo.IP_ADDRESS1, NetInfo.SELECT_USER, params);

    }

    private ServerRequest.OnNetworkResultListener networkResultListener = new ServerRequest.OnNetworkResultListener() {
        @Override
        public void onNetworkResult(JSONObject object, String url, ErrorInfo error) {
            if (error != null) {
                if (progressDialog != null) progressDialog.dismiss();
                uiHandler.sendEmptyMessage(ErrorInfo.FAILED_CONNECTED);
                return;
            }

            if (url == NetInfo.INSERT_USER) {
                if (progressDialog != null) progressDialog.dismiss();
                uiHandler.sendEmptyMessage(NetInfo.WHAT_INSERT_USER);

            }

            if (url == NetInfo.ID_CHECK) {
                //uiHandler.sendEmptyMessage(NetInfo.WHAT_INSERT_USER);
                if (progressDialog != null) progressDialog.dismiss();
                if (!object.isNull("result")) {
                    Bundle data = new Bundle();
                    Message msg = Message.obtain();
                    try {
                        Log.e(object.getString("result"));
                        String result = object.getString("result");

                        if (result.equals("ID")) {
                            //아이디중복
                            Utils.vibrate(SignUp.this, 400);
                            shakeView(etSignUpEmail);
                            data.putString("failMSG", "중복된 아이디 입니다.");
                            msg.setData(data);
                            uiHandler.sendMessage(msg);
                        } else if (result.equals("NICK")) {
                            //닉네임중복
                            Utils.vibrate(SignUp.this, 400);
                            shakeView(etSignUpNickName);
                            data.putString("failMSG", "중복된 닉네임 입니다.");
                            msg.setData(data);
                            uiHandler.sendMessage(msg);
                        } else if (result.equals("OK")) {
                            //가입가능
                            requestInsertUserToServer();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (url == NetInfo.LOGIN_USER) {
                Log.e("로그인 응답");
                if (progressDialog != null) progressDialog.dismiss();
                Bundle data = new Bundle();
                Message msg = Message.obtain();
                try {
                    if (!object.isNull("result")) {
                        if (object.getString("result").equals("ID")) {
                            Log.e("ID없음 진입");
                            Utils.vibrate(SignUp.this, 400);
                            shakeView(etSignInEmail);
                            data.putString("failMSG", "아이디를 확인해 주세요.");
                            msg.setData(data);
                            uiHandler.sendMessage(msg);
                        } else if (object.getString("result").equals("FAIL")) {
                            Log.e("FAIL 진입");
                            Utils.vibrate(SignUp.this, 400);
                            shakeView(etSignInPass);
                            data.putString("failMSG", "비밀번호를 확인해 주세요.");
                            msg.setData(data);
                            uiHandler.sendMessage(msg);
                        } else if (object.getString("result").equals("OK")) {
                            Log.e("OK 진입");
                            if (!object.isNull("info")) {
                                Log.e("info 진입");
                                JSONArray array = object.getJSONArray("info");
                                User selectUser = new User(array.getJSONObject(0));

                                Log.e("디테일" + selectUser.getDetailAddress());
                                Log.e("닉넴" + selectUser.getUserNickName());

                                DataManager.getInstance().setUser(selectUser);
                                Database.getInstance().checkUser(SignUp.this, selectUser);
                                uiHandler.sendEmptyMessage(NetInfo.WHAT_LOGIN_USER);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (url == NetInfo.SELECT_USER) {
                if (progressDialog != null) progressDialog.dismiss();
                try {
                    if (!object.isNull(ParamInfo.USERLIST)) {
                        JSONArray array = object.getJSONArray(ParamInfo.USERLIST);
                        //가입후 유저정보를 저장하기 위해 검색
                        if (array.length() > 0) {
                            User user = new User(array.getJSONObject(0));

                            Log.e("signUp SELECT 닉 디비이전 " + user.getUserNickName());

                            DataManager.getInstance().setUser(user);
                            Database.getInstance().checkUser(SignUp.this, user);

                            Log.e("signUp SELECT 닉 디비이후 " + Database.getInstance().getUser(SignUp.this).getUserNickName());
                        }
                        uiHandler.sendEmptyMessage(NetInfo.WHAT_SELECT_USER);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ErrorInfo.FAILED_CONNECTED:
                    Toast.makeText(SignUp.this, getResources().getString(R.string.network_connection_error), Toast.LENGTH_SHORT).show();
                    break;

                case NetInfo.WHAT_INSERT_USER:
                    //회원가입 성공
                    Log.e("핸들러 회원가입 성공 진입.");
                    requestSelectUserToServer(etSignUpEmail.getText().toString());
                    break;

                case NetInfo.WHAT_SELECT_USER:
                    //가입 후 저장된 회원의 정보
                    Intent intent = new Intent(SignUp.this, SelectAddressActivity.class);
                    intent.putExtra(ParamInfo.DETAIL_MODE, true);
                    intent.putExtra("isSignMode", true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;

                case NetInfo.WHAT_LOGIN_USER:

                    Utils.setPreferenceData(SignUp.this, C.pref.IS_KAKAO, ParamInfo.NO);

                    //푸쉬업데이트
                    requestUpdatePushIdToServer(String.valueOf(DataManager.getInstance().getUser().getSeq()));

                    //로그인 성공
                    if (DataManager.getInstance().getUser().getAddress() == null || DataManager.getInstance().getUser().getAddress().equals("")) {
                        //주소설정이 안된경우
                        Intent i = new Intent(SignUp.this, SelectAddressActivity.class);
                        i.putExtra(ParamInfo.DETAIL_MODE, true);
                        i.putExtra("isSignMode", true);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                        break;

                    } else {
                        //주소설정이 된 경우
                        Intent i = new Intent(SignUp.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        i.putExtra("signUp", true);
                        startActivity(i);
                        finish();
                        break;

                    }
                default:
                    Log.e("핸들러 에러 진입.");
                    Toast.makeText(SignUp.this, msg.getData().getString("failMSG"), Toast.LENGTH_SHORT).show();
                    break;

            }

        }
    };

    private void shakeView(final View v) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Utils.shakeView(v);
            }
        });
    }

    /**
     * GCM Id 등록 비동기 작업
     */
    @SuppressLint("NewApi")
    public void executeRegisterGcm() {

        RegisterGcmAsyncTask registerGcmAsyncTask = new RegisterGcmAsyncTask();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            registerGcmAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            registerGcmAsyncTask.execute();
        }
    }

    /**
     * GCM Id 등록 비동기 작업
     */
    class RegisterGcmAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... doParams) {

            GCMRegistrar.checkDevice(SignUp.this);
            GCMRegistrar.checkManifest(SignUp.this);

            String pushId = GCMRegistrar.getRegistrationId(SignUp.this);

            if (AppInfo.DEFAULT_STRING.equals(pushId)) {

                GCMRegistrar.register(SignUp.this, GCMIntentService.PROJECT_ID);
                DataManager.getInstance().setPushID(GCMRegistrar.getRegistrationId(SignUp.this));
            } else {
                DataManager.getInstance().setPushID(pushId);
            }
            return null;
        }
    }

    @Override
    public void onPagingChanged(int page) {
        changePage(page);
    }

    @Override
    public void onClick(View v) {
        if (v == wrapperSignUp) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etSignUpPass.getWindowToken(), 0);
        } else if (v == viewMapper.btnSignUp) {
            viewMapper.viewPager.setPage(0, 400);
        } else if (v == viewMapper.btnSignIn) {
            viewMapper.viewPager.setPage(1, 400);
        } else if (v == btnAgreeCheck) {
            if (serviceAgree) {
                btnAgreeCheck.setImageResource(R.drawable.image_check_no);
            } else {
                btnAgreeCheck.setImageResource(R.drawable.image_check_yes);
            }
            serviceAgree = !serviceAgree;
        } else if (v == btnAgreeService) {
            Intent i = new Intent(this, PolicyInfoActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.putExtra("signUp", PolicyInfoActivity.USE_SERVICE_POLICY);
            startActivity(i);
        } else if (v == btnAgreePersonerInfo1) {
            Intent i = new Intent(this, PolicyInfoActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.putExtra("signUp", PolicyInfoActivity.USE_PERSONER_INFO_POLICY2);
            startActivity(i);
        } else if (v == btnAgreePersonerInfo2) {
            Intent i = new Intent(this, PolicyInfoActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.putExtra("signUp", PolicyInfoActivity.USE_PERSONER_INFO_POLICY);
            startActivity(i);
        } else if (v == btnSignUp) {
            if (verifySignUp()) {
                requestCheckUserToServer(etSignUpEmail.getText().toString(), etSignUpNickName.getText().toString());
            }
        } else if (v == btnSignIn) {
            if (verifySignIn()) {
                requestLoginToServer(etSignInEmail.getText().toString(), etSignInPass.getText().toString());
                //requestSelectUserToServer(etSignInEmail.getText().toString());
            }
        }

        if (v == btnFindPw) {
            Intent intent = new Intent(SignUp.this, ChangePasswordActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }


    private class ViewMapper {
        PagingScrollView viewPager;
        TextView btnSignUp;
        TextView btnSignIn;
        View indicator;

        public ViewMapper() {
            viewPager = (PagingScrollView) findViewById(R.id.signup_viewpager);
            btnSignUp = (TextView) findViewById(R.id.signup_btn_signup);
            btnSignIn = (TextView) findViewById(R.id.signup_btn_signin);
            indicator = findViewById(R.id.signup_indicator);

            //리스너연결
            viewPager.setOnPagineScrollViewListener(SignUp.this);
            btnSignUp.setOnClickListener(SignUp.this);
            btnSignIn.setOnClickListener(SignUp.this);

        }
    }


}
