package com.remain.villagestore.pages.p4_other.sign.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.remain.villagestore.pages.p4_other.sign.fragment.SignExampleFragment;

import java.util.ArrayList;

/**
 * Created by wefun on 15. 12. 5..
 */
public class PageAdapter extends android.support.v4.app.FragmentPagerAdapter {

    private ArrayList<SignExampleFragment> fragments = new ArrayList<>();

    public PageAdapter(FragmentManager fm) {
        super( fm );
    }

    public void setFragments(ArrayList fragments){
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem( int position ) {
        return fragments.get(position);
    }

    @Override
    public int getCount( ) {
        return fragments.size();
    }

}
