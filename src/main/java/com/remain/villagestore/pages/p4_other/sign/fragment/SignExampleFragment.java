package com.remain.villagestore.pages.p4_other.sign.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;

/**
 * Created by wefun on 15. 12. 5..
 */
public class SignExampleFragment extends Fragment {
    private StretchImageView stretchImageView;
    private int imageRes;

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        View view = inflater.inflate(R.layout.fragment_sign, container, false );
        stretchImageView = (StretchImageView) view.findViewById(R.id.sign_image_example);
        stretchImageView.setImageResource(imageRes);
        return view;
    }

    public void setImage(int res){
        this.imageRes = res;
    }

}
