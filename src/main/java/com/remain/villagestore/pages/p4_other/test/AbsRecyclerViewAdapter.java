package com.remain.villagestore.pages.p4_other.test;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by wefun on 16. 1. 28..
 */
public abstract class AbsRecyclerViewAdapter extends RecyclerView.Adapter {
    private static final int TYPE_HEADER = Integer.MIN_VALUE;
    private static final int TYPE_FOOTER = Integer.MIN_VALUE + 1;
    private static final int TYPE_ADAPTEE_OFFSET = 2;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType ) {
        if ( viewType == TYPE_HEADER ) {
            return onCreateHeaderViewHolder( parent, viewType );
        } else if ( viewType == TYPE_FOOTER ) {
            return onCreateFooterViewHolder( parent, viewType );
        }
        return onCreateBasicItemViewHolder( parent, viewType - TYPE_ADAPTEE_OFFSET );
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position ) {
        if ( position == 0 && holder.getItemViewType( ) == TYPE_HEADER ) {
            onBindHeaderView( holder, position );
        } else if ( holder.getItemViewType( ) == TYPE_FOOTER ) {
            if ( useHeader( ) ) {
                if ( position == getBasicItemCount( ) + 1 ) {
                    onBindFooterView( holder, position );
                }
            } else {
                if ( position == getBasicItemCount( ) ) {
                    onBindFooterView( holder, position );
                }
            }
        } else {
            if ( useHeader( ) ) {
                onBindBasicItemView( holder, position - 1 );
            } else {
                onBindBasicItemView( holder, position );
            }
        }
    }

    @Override
    public int getItemCount( ) {
        int itemCount = getBasicItemCount( );
        if ( useHeader( ) ) {
            itemCount += 1;
        }
        if ( useFooter( ) ) {
            itemCount += 1;
        }
        return itemCount;
    }

    @Override
    public int getItemViewType( int position ) {
        if ( position == 0 && useHeader( ) ) {
            return TYPE_HEADER;
        }
        if ( useHeader( ) ) {
            if ( position == getBasicItemCount( ) + 1 && useFooter( ) ) {
                return TYPE_FOOTER;
            }
        } else {
            if ( position == getBasicItemCount( ) && useFooter( ) ) {
                return TYPE_FOOTER;
            }
        }
        if ( getBasicItemType( position ) != Integer.MAX_VALUE - TYPE_ADAPTEE_OFFSET ) {
            new IllegalStateException( "HeaderRecyclerViewAdapter offsets your BasicItemType by " + TYPE_ADAPTEE_OFFSET + "." );
        }

        return getBasicItemType( position ) + TYPE_ADAPTEE_OFFSET;
    }

    public abstract boolean useHeader( );

    public abstract RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType );

    public abstract void onBindHeaderView(RecyclerView.ViewHolder holder, int position );

    public abstract boolean useFooter( );

    public abstract RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType );

    public abstract void onBindFooterView(RecyclerView.ViewHolder holder, int position );

    public abstract RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType );

    public abstract void onBindBasicItemView(RecyclerView.ViewHolder holder, int position );

    public abstract int getBasicItemCount( );

    public abstract int getBasicItemType( int position );
}