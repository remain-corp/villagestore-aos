package com.remain.villagestore.pages.p4_other.test;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.remain.villagestore.R;
import com.remain.villagestore.pages.p4_other.test.decorations.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by remain on 2016. 6. 21..
 */
public class TestActivity extends FragmentActivity {

    private RecyclerView lecyclerView;
    private TestAdapter testAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        viewInit();
        setData();
    }

    final void viewInit() {
        lecyclerView = (RecyclerView) findViewById(R.id.test_recyclerView);
        initRecyclerView(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void initRecyclerView(RecyclerView.LayoutManager manager) {
        //init

        lecyclerView.setHasFixedSize(true);
        lecyclerView.setLayoutManager(manager);

        testAdapter = new TestAdapter(this, setData());
        lecyclerView.setAdapter(testAdapter);

        //useItemDecoration( new GridSpacingItemDecoration( spanSize, 20, true ) );
        useItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        lecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void useItemDecoration(RecyclerView.ItemDecoration customItemDecoration) {
        RecyclerView.ItemDecoration itemDecoration = customItemDecoration;
        lecyclerView.addItemDecoration(itemDecoration);
    }

    final List setData() {
        List<TestVo> dataList = new ArrayList<TestVo>();

        TypedArray typedArray = getResources().obtainTypedArray(R.array.array_life_menu_cate_res_item);

        for (int i = 0; i < typedArray.length(); i++) {

            TestVo vo = new TestVo();
            vo.setTitle("어느 멋진 날 " + i);
            vo.setWho("정용 " + i);
            vo.setImageRes((int) typedArray.getResourceId(i, 0));
            dataList.add(vo);
        }
        return dataList;
    }

}
