package com.remain.villagestore.pages.p4_other.test;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.remain.villagestore.R;
import com.remain.villagestore.component.image.StretchImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wefun on 16. 1. 27..
 */
public class TestAdapter extends AbsRecyclerViewAdapter {
    private Context context;
    private ArrayList<TestVo> dataList;
    private View headerView;
    private View footerView;
    private boolean isHeader = false;
    private boolean isFooter = false;

    public TestAdapter(Context context, List<TestVo> myDataset) {
        this.context = context;
        dataList = (ArrayList) myDataset;
    }

    public void setHeaderView(View headerView) {
        this.headerView = headerView;
        isHeader = true;
    }

    public void removeHeaderView() {
        if (headerView != null) {
            headerView = null;
        }
        isHeader = false;
        notifyDataSetChanged();
    }

    public void removeFooterView() {
        if (footerView != null) {
            footerView = null;
        }
        isFooter = false;
        notifyDataSetChanged();
    }

    public View getHeaderView() {
        return headerView;
    }

    public void setFooterView(View footerView) {
        this.footerView = footerView;
        isFooter = true;
    }

    public View getFooterView() {
        return footerView;
    }

    @Override
    public boolean useHeader() {
        return isHeader;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        if (isHeader) {
            viewHolder = new HeaderViewHolder(headerView);
        }
        return viewHolder;
    }

    @Override
    public void onBindHeaderView(RecyclerView.ViewHolder holder, int position) {
    }

    @Override
    public boolean useFooter() {
        return isFooter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        if (isFooter) {
            viewHolder = new FooterViewHolder(footerView);
        }
        return viewHolder;
    }

    @Override
    public void onBindFooterView(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_row, parent, false);
        return new BasicViewHolder(view);
    }

    @Override
    public void onBindBasicItemView(RecyclerView.ViewHolder holder, int position) {
        final TestVo recyclerVO = dataList.get(position);
        ((BasicViewHolder) holder).cardImage.setImageResource(recyclerVO.getImageRes());
        ((BasicViewHolder) holder).cartTextTitle.setText(recyclerVO.getTitle());
        ((BasicViewHolder) holder).cartTextWho.setText(recyclerVO.getWho());
        ((BasicViewHolder) holder).cardWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, recyclerVO.getTitle().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getBasicItemCount() {
        return dataList.size();
    }

    @Override
    public int getBasicItemType(int position) {
        return 0;
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }

    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class BasicViewHolder extends RecyclerView.ViewHolder {
        View cardWrapper;
        ImageView cardImage;
        TextView cartTextTitle;
        TextView cartTextWho;

        public BasicViewHolder(View itemView) {
            super(itemView);
            cardWrapper = itemView;
            cardImage = (ImageView) itemView.findViewById(R.id.test_img);
            cartTextTitle = (TextView) itemView.findViewById(R.id.test_text_title);
            cartTextWho = (TextView) itemView.findViewById(R.id.test_text_who);
        }
    }

}
