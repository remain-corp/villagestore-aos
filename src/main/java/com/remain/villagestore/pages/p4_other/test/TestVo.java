package com.remain.villagestore.pages.p4_other.test;

/**
 * Created by remain on 2016. 6. 21..
 */
public class TestVo {
    private String title;
    private String who;
    private int imageRes;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public int getImageRes() {
        return imageRes;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }
}
