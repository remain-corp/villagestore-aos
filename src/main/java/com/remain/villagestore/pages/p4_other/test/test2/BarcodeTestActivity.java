package com.remain.villagestore.pages.p4_other.test.test2;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.*;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.remain.villagestore.R;

/**
 * Created by remain on 2016. 7. 28..
 */
public class BarcodeTestActivity extends Activity{
    private ImageView barcodeImg;
    private EditText barcodeEt;
    private TextView barcodeTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);

        barcodeEt = (EditText) findViewById(R.id.barcode_et);
        barcodeImg = (ImageView) findViewById(R.id.barcode_image);
        barcodeTextView = (TextView) findViewById(R.id.barcode_text);
        barcodeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap = createBarcode(barcodeEt.getText().toString());
                        barcodeImg.setImageBitmap(bitmap);
                    }
                });
            }
        });
    }

    public Bitmap createBarcode(String code){
        Bitmap bitmap =null;
        MultiFormatWriter gen = new MultiFormatWriter();
        try {
            final int WIDTH = 460;
            final int HEIGHT = 90;
            BitMatrix bytemap = gen.encode(code, BarcodeFormat.MAXICODE, WIDTH, HEIGHT);
            bitmap = Bitmap.createBitmap(WIDTH, HEIGHT, Bitmap.Config.ARGB_8888);
            for (int i = 0 ; i < WIDTH ; ++i)
                for (int j = 0 ; j < HEIGHT ; ++j) {
                    bitmap.setPixel(i, j, bytemap.get(i,j) ? Color.BLACK : Color.WHITE);
                }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }
}
