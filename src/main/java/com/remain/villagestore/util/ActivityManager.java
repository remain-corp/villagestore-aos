package com.remain.villagestore.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;

import com.remain.villagestore.info.AppInfo;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by woongjaelee on 15. 8. 21..
 */
public class ActivityManager {

    //================================================================================
    // XXX Variables
    //================================================================================
    // This
    private static ActivityManager instance;

    // Array
    private ArrayList<Activity> activityList;
    private ArrayList<Activity> activityListWrite;

    //================================================================================
    // XXX Constructor
    //================================================================================
    private ActivityManager() {

        activityList = new ArrayList<Activity>();
        activityListWrite = new ArrayList<Activity>();
    }

    //================================================================================
    // XXX Getter, Setter
    //================================================================================
    public static ActivityManager getInstance() {

        if (instance == null) instance = new ActivityManager();
        return instance;
    }

    /**
     * 현재 리스트를 리턴한다
     * @return
     */
    public ArrayList<Activity> getActivityList() {
        return activityList;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================
    /**
     * 새로운 액티비티를 추가한다
     * @param activity
     */
    public void addActivity(Activity activity) {
        activityList.add(activity);
    }

    /**
     * 액티비티를 삭제한다
     * @param activity
     */
    public void removeActivity(Activity activity) {
        activityList.remove(activity);
    }

    /**
     * 저장되어 있는 액티비티의 개수를 리턴한다
     * @return
     */
    public int size() {
        return activityList.size();
    }

    /**
     * 리스트에 저장된 모든 액티비티를 끝낸다
     */
    public void finishAllActivity() {

        for (Activity activity : activityList) {
            activity.finish();
        }
        activityList.clear();
    }

    /**
     * 게시글 작성 액티비티를 추가한다
     * @param activity
     */
    public void addWriteActivity(Activity activity) {
        activityListWrite.add(activity);
    }

    /**
     * 게시글 작성 액티비티를 삭제한다
     * @param activity
     */
    public void removeWriteActivity(Activity activity) {
        activityListWrite.remove(activity);
    }

    /**
     * 리스트에 저장된 모든 게시글 작성 액티비티를 끝낸다
     */
    public void finishWriteActivity() {

        for (Activity activity : activityListWrite) {
            activity.finish();
        }
        activityListWrite.clear();
    }


    /**
     * 저장되어 있는 게시글 작성 액티비티의 개수를 리턴한다
     * @return
     */
    public int sizeWrite() {
        return activityListWrite.size();
    }

    /**
     * 앱 실행 중 여부를 구한다.
     * @return 앱 실행 여부
     */
    public boolean isPackage(Context context) {

        android.app.ActivityManager am = (android.app.ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<android.app.ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        ComponentName topActivity = taskInfo.get(0).topActivity;
        String packageName = topActivity.getPackageName();

        if (packageName.equals(AppInfo.PACKAGE_NAME)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 현재 액티비티를 반환한다.
     * @return 현재 실행 액티비티 정보
     */
    public ComponentName getCurrentActivity(Context context) {

        android.app.ActivityManager am = (android.app.ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<android.app.ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        ComponentName topActivity = taskInfo.get(0).topActivity;

        return topActivity;
    }
}