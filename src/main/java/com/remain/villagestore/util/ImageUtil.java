package com.remain.villagestore.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import com.remain.villagestore.manager.DisplayManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created by woongjaelee on 15. 8. 28..
 */
public class ImageUtil {

    //================================================================================
    // Constants
    //================================================================================
    private static final int PHOTO_MAX_WIDTH = 1280;
    private static final int PHOTO_MAX_HEIGHT = 1280;
    public static final int RESCALE_MAX_WIDTH = 480;
    public static final int RESCALE_MAX_HEIGHT = 800;

    /** 기본 폴더명 */
    private static final String PARENT_FOLDER_NAME = "/DailyDrink";

    //================================================================================
    // Variables
    //================================================================================
    /** Singleton을 위한 객체 */
    private static ImageUtil imageUtil;

    //================================================================================
    // Constructor
    //================================================================================

    public static ImageUtil getInstance() {
        if (imageUtil == null) {
            imageUtil = new ImageUtil();
        }
        return imageUtil;
    }

    //================================================================================
    // Etc Method
    //================================================================================

    /**
     * 미디어 스캐닝 되는 폴더 /프로젝트 명
     *
     * @return 미디어 스캐닝 되는 폴더 경로
     */
    public String getImageFolderPath(String subFolderName) {
        makeImageFolder(subFolderName);
        File path = new File(Environment.getExternalStorageDirectory(), PARENT_FOLDER_NAME + "/" + subFolderName);
        return path.getPath();
    }

    /**
     * 미디어 스캐닝 되는 폴더 /프로젝트 명
     *
     * @return 미디어 스캐닝 되는 폴더 경로
     */
    public String getImageFolderPath(String subFolderName, boolean isMediaFolder) {
        makeImageFolder(subFolderName, isMediaFolder);
        File path = new File(Environment.getExternalStorageDirectory(), PARENT_FOLDER_NAME + "/" + subFolderName);
        return path.getPath();
    }

    /**
     * 파일 하나에 대한 미디어 스캐닝
     * @param context : Context
     * @param filePath : 미디어 스캐닝 할 파일의 경로
     */
    public void doMediaScaner(Context context, String filePath) {

        com.remain.villagestore.util.Log.d("path : " + filePath);
        //context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"+ filePath)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(new File(filePath));
            mediaScanIntent.setData(contentUri);
            context.sendBroadcast(mediaScanIntent);

        } else {
            //context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));

            MediaScannerConnection.scanFile(context, new String[]{filePath}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(final String path, final Uri uri) {
                            Log.e(String.format("Scanned path %s -> URI = %s", path, uri.toString()));
                        }
                    });
        }
    }

    /**
     * 폴더 생성여부 검사, 폴더 생성
     */
    public void makeImageFolder(String subFolderName) {
        File sd = Environment.getExternalStorageDirectory();
        // sdcard/Wehub 폴더
        String folderPath = sd.getAbsolutePath() + PARENT_FOLDER_NAME + "/" + subFolderName;
        File Folder = new File(folderPath);
        if (!Folder.isDirectory()) {
            Log.e("MakeFolder = " + sd.getAbsolutePath());
            Folder.mkdirs();
            String noMediaStr = folderPath + "/.nomedia";
            File noMedia = new File(noMediaStr);
            noMedia.mkdirs();
        }
    }

    /**
     * 해당 경로에 폴더 생성, 미디어 스캔 금지 파일 생성
     * @param path 생성할 경로
     * @param isMediaFolder 미디어 폴더인지(미디어 폴더라면 갤러리에 노출)
     */
    public void makeImageFolder(String path, boolean isMediaFolder) {
        File Folder = new File(path);
        if (!Folder.isDirectory()) {
            Folder.mkdirs();
            if(!isMediaFolder){
                String noMediaStr = path + "/.nomedia";
                File noMedia = new File(noMediaStr);
                noMedia.mkdirs();
            }
        }
    }

    /**
     * URI 실제 경로 가져오기
     *
     * @param uri
     * @return
     */
    public String getRealPath(Activity activity, Uri uri) {

        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /**
     * 폴더 파일 존재 여부 체크
     */
    public boolean checkFile(String subFolderName) {

        File sd = Environment.getExternalStorageDirectory();
        String folderPath = sd.getAbsolutePath() + PARENT_FOLDER_NAME + "/" + subFolderName;

        File setDir = new File(folderPath);

        int count = 0;

        if (setDir.isDirectory()) {
            File[] allFiles = setDir.listFiles();

            for (File delAllDir : allFiles) {
                if (delAllDir.isFile()) {
                    count++;
                }
            }
        }

        if (count > 0)
            return true;
        else
            return false;
    }

    /**
     * 폴더 파일 사진 모두 삭제
     *
     * ( 디렉토리 및 파일 삭제를 위한 재귀함수 디렉토리 내에 존재하는 모든 디렉토리와 파일을 포함하여 전부 삭제 )
     */
    public void deleteFile(String path) {

        File delDir = new File(path);

        if (delDir.isDirectory()) {
            File[] allFiles = delDir.listFiles();

            for (File delAllDir : allFiles) {
                deleteFile(delAllDir.getAbsolutePath());
            }
        }

        delDir.delete();
    }

    /**
     * 이미지 축소
     *            축소할 비율 (예. resize = 2, 1/2 축소)
     * @return
     */
    public Bitmap resizeBitmap(String absolutePath, int ratio) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = ratio;
        Bitmap resizeBitmap = BitmapFactory.decodeFile(absolutePath, options);

        return resizeBitmap;
    }

    public void copyUriToFile(Uri srcUri, File target, Context context) {

        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;
        FileChannel fcin = null;
        FileChannel fcout = null;
        try {
            // 스트림 생성
            inputStream = (FileInputStream) context.getContentResolver().openInputStream(srcUri);
            outputStream = new FileOutputStream(target);

            // 채널 생성
            fcin = inputStream.getChannel();
            fcout = outputStream.getChannel();

            // 채널을 통한 스트림 전송
            long size = fcin.size();
            fcin.transferTo(0, size, fcout);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(fcout != null) fcout.close();
            } catch (IOException ioe) {
            }
            try {
                if(fcin != null) fcin.close();
            } catch (IOException ioe) {
            }
            try {
                if(outputStream != null) outputStream.close();
            } catch (IOException ioe) {
            }
            try {
                if(inputStream != null) inputStream.close();
            } catch (IOException ioe) {
            }
        }
    }

    /** 이미지 사이즈 수정 후, 카메라 rotation 정보가 있으면 회전 보정함. */
    public int correctCameraOrientation(String imagePath) {

        int exifDegree = 0;

        try {
            // 이미지를 상황에 맞게 회전시킨다
            ExifInterface exif = new ExifInterface(imagePath);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            exifDegree = exifOrientationToDegrees(exifOrientation);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return exifDegree;
    }

    /** 이미지 사이즈 수정 후, 카메라 rotation 정보가 있으면 회전 보정함. */
    public Bitmap correctCameraOrientation(File imgFile) {
        //해상도 조정
        Bitmap bitmap = loadImageWithSampleSize(imgFile);
        //크기보정(위의 해상도 조정이랑 같은 기능 일수도 있음..
        //Bitmap resizeBitmap = resizeBitmapByBaseLength(bitmap);

        if(bitmap == null) return null;
        //Log.d("correct fileSize = " + bitmap.getWidth() + " : " + bitmap.getHeight());
        try {
            ExifInterface exif = new ExifInterface(imgFile.getAbsolutePath());
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int exifRotateDegree = exifOrientationToDegrees(exifOrientation);
            bitmap = rotateImage(bitmap, exifRotateDegree);
            saveBitmapToFile(bitmap, imgFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * Bitmap를 메모리에 올리지 않고 크기 변경
     */
    public Bitmap loadBitmap(String fileName) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, options);

        int width = options.outWidth;
        int height = options.outHeight;

        int scaleFactor = Math.min(width / RESCALE_MAX_WIDTH, height / RESCALE_MAX_HEIGHT);
        options.inJustDecodeBounds = false;
        options.inSampleSize = 2;

        return BitmapFactory.decodeFile(fileName, options);
    }

    /**
     * Bitmap를 메모리에 올리지 않고 크기 변경
     */
    public Bitmap loadBitmap(String fileName, int wantedWidth, int wantedHeight) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, options);

//        int width = options.outWidth;
//		int height = options.outHeight;

//		int scaleFactor = Math.min(width / wantedWidth, height / wantedHeight);
        options.inSampleSize = calculateInSampleSize(options, wantedWidth, wantedHeight);
        options.inJustDecodeBounds = false;
//		options.inPurgeable = true;
//		options.inDither = false;

        return BitmapFactory.decodeFile(fileName, options);
    }

    /** 원하는 크기의 이미지로 options 설정. */
    public Bitmap loadImageWithSampleSize(File file) {

        return loadImageWithSize(file, PHOTO_MAX_WIDTH, PHOTO_MAX_HEIGHT);
    }

    /** 원하는 크기의 이미지로 options 설정. */
    public Bitmap loadImageWithSize(File file, int maxWidth, int maxHeight) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        int optionWidth = options.outWidth;
        int optionHeight = options.outHeight;
        int longSide = Math.max(optionWidth, optionHeight);
        int sampleSize = 1 ;
        if (longSide > maxWidth) {
            sampleSize = longSide / maxWidth;
        }
        options.inJustDecodeBounds = false;
        options.inSampleSize = sampleSize;
        options.inPurgeable = true;
        options.inDither = false;

        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        return bitmap;
    }

    /** Get Bitmap's Width **/
    public int getBitmapOfWidth(String fileName) {

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(fileName, options);
            return options.outWidth;
        } catch(Exception e) {
            return 0;
        }
    }

    /** Get Bitmap's height **/
    public int getBitmapOfHeight(String fileName) {

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(fileName, options);
            return options.outHeight;
        } catch(Exception e) {
            return 0;
        }
    }

    /**
     * EXIF정보를 회전각도로 변환하는 메서드
     *
     * @param exifOrientation
     *            EXIF 회전각
     * @return 실제 각도
     */
    private int exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    /**
     * 이미지 회전
     * @param bitmap 회전시킬 비트맵
     * @param degrees 회전할 각도
     * @return
     */
    public Bitmap rotateImage(Bitmap bitmap, int degrees) {
        if (bitmap == null)
            return null;

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int longSide = Math.max(width, height);

        float resizeFactor = PHOTO_MAX_HEIGHT * 1f / longSide;

        int targetWidth, targetHeight;

        if (resizeFactor > 1) {
            targetWidth = width;
            targetHeight = height;
        } else {
            targetWidth = (int) (width * resizeFactor);
            targetHeight = (int) (height * resizeFactor);
        }

        Log.d(width + ", " + height + "/" + targetWidth + "," + targetHeight);
        if (degrees != 0 && bitmap != null) {
            Matrix m = new Matrix();
            m.postRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
            float scaleWidth = ((float) targetWidth) / width;
            float scaleHeight = ((float) targetHeight) / height;
            m.postScale(scaleWidth, scaleHeight);
//			m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
//			m.setScale(targetWidth, targetHeight);
            try {
                Bitmap converted = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
//				Bitmap converted = Bitmap.createBitmap(bitmap, 0, 0, targetWidth, targetHeight, m, true);
//				Bitmap converted = Bitmap.createScaledBitmap(bitmap, targetWidth, targetHeight, true);
                if (bitmap != converted) {
                    bitmap.recycle();
                    bitmap = converted;
                }
            } catch (OutOfMemoryError ex) {
            }
        }
        return bitmap;
    }

    /**
     * 비트맵을 파일로 저장하기
     *
     * @param bitmap
     *            : 저장할 비트맵
     * @param target
     *            : 저장할 파일 경로
     */
    public void saveBitmapToFile(Bitmap bitmap, File target) {
        try {
            FileOutputStream fos = new FileOutputStream(target, false);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//	/**
//	 * 비트맵 모서리 동그랗게 자르기
//	 *
//	 * @param bitmap
//	 * @return
//	 */
//	public Bitmap getRoundedCornerBitmap(Bitmap bitmap, float roundPx, int thumbnailSize) {
//
//		bitmap = makeSquareImage(bitmap, thumbnailSize);
//		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
//
//		Log.e("bitmap.getWidth() : " + bitmap.getWidth() + ", bitmap.getHeight() : " + bitmap.getHeight());
//
//		Canvas canvas = new Canvas(output);
//		final int color = 0xff424242;
//		final Paint paint = new Paint();
//		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
//		final RectF rectF = new RectF(rect);
//		paint.setAntiAlias(true);
//		canvas.drawARGB(0, 0, 0, 0);
//		paint.setColor(color);
//		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
//		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
//		canvas.drawBitmap(bitmap, rect, rect, paint);
//
//		return output;
//	}

    /**
     * 비트맵 모서리 동그랗게 자르기
     *
     * @param bitmap 비트맵 이미지
     * @param thumbnailSize 썸네일 사이즈
     * @param roundPx 동그랗게 할 수치
     * @return
     */
    public Bitmap getRoundedCornerBitmap(Bitmap bitmap, int thumbnailSize, int roundPx) {

        if(bitmap == null) return null;
        //bitmap = resizeBitmapWithWidthHeight(bitmap, thumbnailSize, thumbnailSize);
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(output);
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, thumbnailSize, thumbnailSize);
        final Rect srect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, srect, rect, paint);

        return output;
    }

    /**
     * 비트맵 모서리 동그랗게 자르기
     *
     * @param bitmap 비트맵 이미지
     * @param thumbnailSize 썸네일 사이즈
     * @param roundPx 동그랗게 할 수치
     * @return
     */
    public Bitmap getRoundedCornerBitmap(DisplayManager display, Bitmap bitmap, int thumbnailSize, int roundPx) {

        //bitmap = resizeBitmapWithWidthHeight(bitmap, thumbnailSize, thumbnailSize);
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(output);
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, display.getWidth(thumbnailSize), display.getHeight(thumbnailSize));
        final Rect srect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, display.getWidth(roundPx), display.getHeight(roundPx), paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, srect, rect, paint);

        return output;
    }

    /**
     * 비트맵 모서리 동그랗게 자른 후 테두리 있는 배경 입히기
     *
     * @param context
     * @param bitmap 비트맵 이미지
     * @param bgId 배경 이미지
     * @param bgSize 배경 사이즈
     * @param thumbnailSize 썸네일 사이즈
     * @param roundPx 동그랗게 할 수치
     * @return
     */
    public Bitmap getRoundedCornerBitmap(DisplayManager display, Context context, Bitmap bitmap, int bgId, int bgSize, int thumbnailSize, int roundPx) {

        if (bitmap == null) return bitmap;

        Bitmap output = getRoundedCornerBitmap(display, bitmap, thumbnailSize, roundPx);

        // 최소 여백 1
        int thumb = (bgSize - thumbnailSize) / 2;
        int widthThumb = display.getWidth(thumb);
        if (widthThumb == 0) {
            widthThumb = 1;
        }
        int heightThumb = display.getHeight(thumb);
        if (heightThumb == 0) {
            heightThumb = 1;
        }
        BitmapDrawable drawable = (BitmapDrawable) context.getResources().getDrawable(bgId);
        Bitmap circle = drawable.getBitmap().copy(Bitmap.Config.ARGB_8888, true);
        circle = Bitmap.createScaledBitmap(circle, display.getWidth(bgSize), display.getHeight(bgSize), true);
        Bitmap bmOverlay = Bitmap.createBitmap(circle.getWidth(), circle.getHeight(), circle.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(circle, 0, 0, null);
        canvas.drawBitmap(output, widthThumb, heightThumb, null);

        return bmOverlay;
    }


    /**
     * 정 사각형 썸네일 만들기
     * @param src : 원본 이미지 파일
     * @param dest : 썸네일 이미지 파일
     * @param thumbnailSize : 썸네일 사이즈
     */
    public void makeSquareThumbImage(File src, File dest, String fileName ,int thumbnailSize) {

        Bitmap srcBitmap = loadBitmap(fileName, thumbnailSize, thumbnailSize);
        //Bitmap destBitmap = srcBitmap.copy(Bitmap.Config.ARGB_8888, true);

        srcBitmap = rotateImage(makeSquareImage(srcBitmap, thumbnailSize), correctCameraOrientation(fileName));

        saveBitmapToFile(srcBitmap, dest);
        srcBitmap.recycle();
    }

    /**
     * 정 사각형 썸네일 만들기
     * @param src : 원본 이미지 파일
     * @param dest : 썸네일 이미지 파일
     * @param thumbnailSize : 썸네일 사이즈
     */
    public void makeSquareThumbImage(File src, File dest, int thumbnailSize) {
        Bitmap srcBitmap = BitmapFactory.decodeFile(src.getAbsolutePath());
        //Bitmap destBitmap = srcBitmap.copy(Bitmap.Config.ARGB_8888, true);

        srcBitmap = makeSquareImage(srcBitmap, thumbnailSize);

        saveBitmapToFile(srcBitmap, dest);
        srcBitmap.recycle();
    }

    /**
     * 비트맵 정사각형으로 자르기
     *
     * @param origin
     *            : 원본 비트맵
     * @param size
     *            : 가로 혹은 세로 길이 (가로 == 세로)
     * @return 정사각형으로 자른 비트맵
     */
    public Bitmap makeSquareImage(Bitmap origin, int size) {

        Bitmap target = null;
        try {

            int srcWidth = origin.getWidth();
            int srcHeight = origin.getHeight();

            if (srcWidth < srcHeight) {
                target = resizeBitmapWithWidth(origin, size);
            } else {
                target = resizeBitmapWithHeight(origin, size);
            }

            int targetWidth = target.getWidth();
            int targetHeight = target.getHeight();

            if (targetWidth < targetHeight) {
                // 가로 < 세로
                int extra = (targetHeight - size) / 2;
                target = Bitmap.createBitmap(target, 0, extra, targetWidth, targetWidth);
            } else if (targetWidth > targetHeight) {
                // 가로 > 세로
                int extra = (targetWidth - size) / 2;
                target = Bitmap.createBitmap(target, extra, 0, targetHeight, targetHeight);
            }
            return target;
        }
        catch (OutOfMemoryError ex) {
            Log.e("Out of Memory");
            System.gc();
            return target;
        }
    }

    /**
     * 이미지 비율대로 크기 수정 (가로크기 기준으로)
     * @param source 수정할 비트맵 객채
     * @param wantedWidth 원하는 가로 크기
     * @return 수정된 Bitmap 객채
     */
    public Bitmap resizeBitmapWithWidth(Bitmap source, int wantedWidth) {
        if (source == null)
            return null;

        int width = source.getWidth();
        int height = source.getHeight();

        float resizeFactor = wantedWidth * 1f / width;

        int targetWidth, targetHeight;
        targetWidth = (int) (width * resizeFactor);
        targetHeight = (int) (height * resizeFactor);

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, true);

        return resizedBitmap;
    }

    /**
     * 이미지 비율대로 크기 수정 (세로크기 기준으로)
     * @param source 수정할 비트맵 객채
     * @param wantedHeight 원하는 세로 크기
     * @return 수정된 Bitmap 객채
     */
    public Bitmap resizeBitmapWithHeight(Bitmap source, int wantedHeight) {
        if (source == null)
            return null;

        int width = source.getWidth();
        int height = source.getHeight();

        float resizeFactor = wantedHeight * 1f / height;

        int targetWidth, targetHeight;
        targetWidth = (int) (width * resizeFactor);
        targetHeight = (int) (height * resizeFactor);

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, true);

        return resizedBitmap;
    }


//	/**
//	 * 이미지 비율대로 크기 수정
//	 * @param source 수정할 비트맵 객채
//	 * @param wantedWidth 원하는 가로 크기
//	 * @param wantedHeight 원하는 세로 크기
//	 * @return 수정된 Bitmap 객채
//	 */
//	public Bitmap resizeBitmapWithWidthHeight(Bitmap source, int wantedWidth, int wantedHeight) {
//
//		DisplayManager display = DisplayManager.getInstance();
//
//		if (source == null)
//			return null;
//
//		int targetWidth = 0;
//		int targetHeight = 0;
//
//		targetWidth = display.getWidth(wantedWidth);
//		targetHeight = display.getHeight(wantedHeight);
//
//		Bitmap resizedBitmap = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, true);
//
//		return resizedBitmap;
//	}

    /**
     * DP를 Pixel을 변경해주는 메서드
     *
     * @param dp 변경할 DP
     * @return dp에서 변경된 Pixel
     */
    public int getPxFromDp(Context context, float dp) {
        int px = 0;
        px = (int) (dp * context.getResources().getDisplayMetrics().density);
        return px;
    }

    /**
     * Drawable 이미지 리사이즈
     * @param image 리사이즈할 Drawable 객체
     * @param scaleFactor 조정할 비율
     * @return
     */
    public Drawable reSizeScaleDrawableImage (Context context, Drawable image, float scaleFactor) {

        if ((image == null) || !(image instanceof BitmapDrawable)) {
            return image;
        }

        Bitmap b = ((BitmapDrawable)image).getBitmap();

        int sizeX = Math.round(image.getIntrinsicWidth() / scaleFactor);
        int sizeY = Math.round(image.getIntrinsicHeight() / scaleFactor);

        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, sizeX, sizeY, false);

        image = new BitmapDrawable(context.getResources(), bitmapResized);

        return image;

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            //
            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        Log.e("inSampleSize : " + inSampleSize);

        return inSampleSize;
    }

    /** 카메라 rotation 정보가 있으면 회전 보정함. */
    public Bitmap correctCameraOrientation(String filePath, Bitmap bitmap) {
        try {
            ExifInterface exif = new ExifInterface(filePath);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int exifRotateDegree = exifOrientationToDegrees(exifOrientation);
            Log.e("Degree : " + exifRotateDegree);
            bitmap = rotateImage(bitmap, exifRotateDegree);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.postRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    public static Bitmap flip(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.preScale(-1, 1);
        mtx.postRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    public static Bitmap flipVertical(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.preScale(1, -1);
        mtx.postRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    /**
     * 동영상에서 그룹 이미지 노티용
     * @param origin
     * @param thumSize
     * @param roundPx
     * @return
     */
    public Bitmap makeSquareCornerThumbImage(Bitmap origin , int thumSize,int roundPx) {

        Bitmap target = null;
        try {

            int srcWidth = origin.getWidth();
            int srcHeight = origin.getHeight();

            if (srcWidth < srcHeight) {
                target = resizeBitmapWithWidth(origin, thumSize);
            } else {
                target = resizeBitmapWithHeight(origin, thumSize);
            }

            int targetWidth = target.getWidth();
            int targetHeight = target.getHeight();

            if (targetWidth < targetHeight) {
                // 가로 < 세로
                int extra = (targetHeight - thumSize) / 2;
                target = Bitmap.createBitmap(target, 0, extra, targetWidth, targetWidth);
            } else if (targetWidth > targetHeight) {
                // 가로 > 세로
                int extra = (targetWidth - thumSize) / 2;
                target = Bitmap.createBitmap(target, extra, 0, targetHeight, targetHeight);
            }

            Bitmap output = Bitmap.createBitmap(target.getWidth(), target.getHeight(), Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(output);
            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, thumSize, thumSize);
            final Rect srect = new Rect(0, 0, target.getWidth(), target.getHeight());
            final RectF rectF = new RectF(rect);
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(target, srect, rect, paint);

            return output;
        }
        catch (OutOfMemoryError ex) {
            Log.e("Out of Memory");
            System.gc();
            return target;
        }
    }

}
