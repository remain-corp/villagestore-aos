package com.remain.villagestore.util;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import com.remain.villagestore.db.Database;
import com.remain.villagestore.dto.User;

import java.io.IOException;
import java.util.List;

public class LocationUtil extends Service {
	//================================================================================
    // XXX Constants
    //================================================================================
	
	// The minimum distance to change Updates in meters
//	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 100; // 100 meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 100 meters
	// The minimum time between updates in milliseconds
//	private static final long MIN_TIME_BW_UPDATES = 1000; // 1 second
	private static final long MIN_TIME_BW_UPDATES = 0; // 1 second
	// The minimum time between updates in milliseconds
	private static final long MIN_TIME_GPS_UPDATES = 100;  // 0.1 second
	/** 초기 위도 */
	public static final float DEFAULT_LATITUDE = 0.0f;
	/** 초기 경도 */
	public static final float DEFAULT_LONGITUDE = 0.0f;
	/** float 거리 결과값 최소치 */
	private final int MIN_DISTANCE = -1;
	/** float 거리 결과값 최대치 */
	private final int MAX_DISTANCE = -1;
	/** String 거리 결과값 최소치 */
	private final int MIN_DISTANCE_STRING = 1;
	/** String 거리 결과값 최대치 */
	private final int MAX_DISTANCE_STRING = 999;

	//================================================================================
	// XXX Variables
	//================================================================================
		
	private static LocationUtil locationUtil;
	private Context mContext;

	// flag for GPS status
	private boolean isGPSEnabled = false;

	// flag for network status
	private boolean isNetworkEnabled = false;

	// flag for GPS status
	private static boolean canGetLocation = false;
	private boolean isLocationStarted;

	private Location location;
	private Location gpsLocation; // location
	private Location networkLocation; 
	private double latitude; // latitude
	private double longitude; // longitude

	private User me;
	
	// Thread 
	private Handler handler;
	// Declaring a Location Manager
	private OnGpsSettingListener gpsSettingListener;
	private OnLocationChangedListener locationChangedListener;
	protected LocationManager locationManager;
	// alert dialog
	AlertDialog.Builder alertDialog;
	//================================================================================
	// XXX Constructor
	//================================================================================
	//================================================================================
    // XXX Override Method
    //================================================================================
	
	public LocationUtil() {
	}
	
	public LocationUtil(Context context) {
		this.mContext = context;

		me = User.getMyInfo(context);
//		init();
	}
	
	
	//================================================================================
    // XXX Listener
    //================================================================================
	
	private LocationListener gpsLocationListener = new LocationListener() {
		
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			Log.i("GPS Status Changed");
		}

		@Override
		public void onProviderEnabled(String provider) {
			Log.i("GPS Provider Enabled");
		}

		@Override
		public void onProviderDisabled(String provider) {
			Log.i("GPS Provider Disabled");
		}

		@Override
		public void onLocationChanged(Location location) {
			boolean isBetter = isBetterLocation(location, gpsLocation);
			Log.i("GPS Changed", "" + isBetter);

			if(isBetter){
				gpsLocation = location;
				setLocation();
				if(locationChangedListener != null)
					locationChangedListener.onLocationChanged();

				if(me != null) {
					me.setLatitude(location.getLatitude());
					me.setLongitude(location.getLongitude());
					Database.getInstance().updateUser(mContext,me);
				}
				else {
					me = User.getMyInfo(mContext);
				}


			}
		}
	};

	private LocationListener networkLocationListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			Log.e("network Status change");
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			Log.e("network enable");
			if(gpsSettingListener != null)
				gpsSettingListener.onGpsSetting(true);

		}

		@Override
		public void onProviderDisabled(String provider) {
			Log.e("network disable");

		}

		@Override
		public void onLocationChanged(Location location) {
			boolean isBetter = isBetterLocation(location, networkLocation);
			Log.i("netWork Changed", "" + isBetter);

			if(isBetter){
				networkLocation = location;
				setLocation();
				if(locationChangedListener != null)
					locationChangedListener.onLocationChanged();

				if(me != null) {

					me.setLatitude(location.getLatitude());
					me.setLongitude(location.getLongitude());
					Database.getInstance().updateUser(mContext,me);

				} else {
					me = User.getMyInfo(mContext);
				}

			}
		}
	};

	//================================================================================
    // XXX Thread, Handler
    //================================================================================

	//================================================================================
    // XXX Getter, Setter
    //================================================================================

	public static LocationUtil getInstance(Context context){
		if(locationUtil == null){
			Log.e("null");
			locationUtil = new LocationUtil(context);
			locationUtil.init(context);
		} else {
			Log.e("not null");

			locationUtil.init(context);
		}
		return locationUtil;
	}

	public void setOnGpsSettingListener(OnGpsSettingListener gpsSettingListener ) {

		this.gpsSettingListener = gpsSettingListener;
	}

	public OnLocationChangedListener getLocationChangedListener () {
		return this.locationChangedListener;
	}

	public void setOnLocationChangedListener(OnLocationChangedListener locationChangedListener ) {

		this.locationChangedListener = locationChangedListener;
	}

	//================================================================================
    // XXX Etc Method
    //================================================================================
	private void init(Context context) {

		canGetLocation = false;

		me = User.getMyInfo(context);
		if(locationManager == null)
			locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

		// getting GPS status
		isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

		// getting network status
		isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		Log.e("GPs = " + isGPSEnabled + " network = " + isNetworkEnabled + " isStart = " + isLocationStarted);
		if (!isGPSEnabled && !isNetworkEnabled) {
			// no network provider is enabled
			canGetLocation = false;
		}
		else {
			canGetLocation = true;
		}
		if(canGetLocation) {

			if(!isLocationStarted)
				startLocation();
		}

		// FIXME 2015. 3. 1 다이얼로그 계속뜸 요청으로 수정
//		else {
//			if(isNetworkEnabled)
//				canGetLocation = true;
//		}
//		if(isNetworkEnabled) {
//			if(!isLocationStarted)
//				startLocation();
//		}
	}

	private void startLocation() {

		try {

			if (!canGetLocation) {
				// no network provider is enabled
				Toast.makeText(mContext, "Any Provider Network Enabled", Toast.LENGTH_SHORT);
			} else {
				isLocationStarted = true;
				// if GPS Enabled get lat/long using GPS Services
				if (isGPSEnabled) {

					locationManager.requestLocationUpdates(
							LocationManager.GPS_PROVIDER,
							MIN_TIME_GPS_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, gpsLocationListener);

					Log.d("GPS Enabled", "GPS Enabled");

					if (locationManager != null) {
						Location lastGPSLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						if(lastGPSLocation != null) {
							gpsLocation = lastGPSLocation;

						} else {
							Log.e("lastGPS NULL");
						}

					}
					else {
						Log.e("locationManager null");
					}
				} else {
					Log.e("isGPSEnabled = " + isGPSEnabled) ;
				}

				if (isNetworkEnabled) {
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER,
							MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, networkLocationListener);

					Log.d("Network Enabled", "Network Enabled");

					if (locationManager != null) {
						Location lastNetworkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if(lastNetworkLocation != null) {
							networkLocation = lastNetworkLocation ;
						}else {
							Log.e("lastNetWork NULL");
						}
					} else {
						Log.e("locationManager null");
					}
				}

				/// GPS 옵션 계속 켜져있을때 간헐적으로 아무것도 못받아오는 삼성폰 버그로 인해 수정 (2015. 3. 14)
				// GoogleApi 요청
				if(networkLocation == null && gpsLocation == null) {
					Log.e("위치 좌표 둘다 null 이면  ");

				}
				// 있으면 두 좌표 비교
				else {
					Log.e("둘중 하나라도 있으면 ");
					if(isBetterLocation(networkLocation,gpsLocation)){
						location = networkLocation;
					} else {
						location = gpsLocation;
					}
				}



			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setLocation () {
		if(isBetterLocation(networkLocation,gpsLocation)){
			Log.e("networkLocation is Better");
			location = networkLocation;
		} else {
			Log.e("gpsLocation is Better");
			location = gpsLocation;
		}
	}
	public Location getLocation() {


		if(isBetterLocation(networkLocation,gpsLocation)){
			Log.e("networkLocation is Better");
			location = networkLocation;
		} else {
			Log.e("gpsLocation is Better");
			location = gpsLocation;
		}

		return location;
	}

	/**
	 * Stop using GPS listener
	 * Calling this function will stop using GPS in your app
	 * */
	public void stopLocationUpdates(){
		if(locationManager != null){
			Log.e("remove Updates remove Updates remove Updates remove Updates remove Updates remove Updates remove Updates");
			locationManager.removeUpdates(networkLocationListener);
			locationManager.removeUpdates(gpsLocationListener);

			isLocationStarted = false;

		}
	}

	/**
	 * Function to get latitude
	 * */
	public double getLatitude(){
		if(location != null){
			latitude = location.getLatitude();
		}

		// return latitude
		return latitude;
	}

	/**
	 * Function to get longitude
	 * */
	public double getLongitude(){
		if(location != null){
			longitude = location.getLongitude();
		}

		// return longitude
		return longitude;
	}

	/**
	 * Function to check GPS/wifi enabled
	 * @return boolean
	 * */
	public boolean canGetLocation() {
		return canGetLocation;
	}

	public boolean canGetLocation(Context context) {
		if(locationManager == null) {
			locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
		}

		isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

		// getting network status
		isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		if (!isGPSEnabled && !isNetworkEnabled) {
			canGetLocation = false;
		} else {
			if(isNetworkEnabled)
				canGetLocation = true;
		}

		return canGetLocation;
	}

	private static final int ONE_MINUTES = 1000 * 60;

	/** Determines whether one Location reading is better than the current Location fix
	  * @param location  The new Location that you want to evaluate
	  * @param currentBestLocation  The current Location fix, to which you want to compare the new one
	  */
	protected boolean isBetterLocation(Location location, Location currentBestLocation) {
	    if (currentBestLocation == null) {
	        // A new location is always better than no location
	        return true;
	    }

	    if(location == null) {
	    	Log.i("", "location null");
	    	return false;
	    }
	    // Check whether the new location fix is newer or older
	    long timeDelta = location.getTime() - currentBestLocation.getTime();
	    Log.i("", "location = " + location.getTime() + " currentLocation =" + currentBestLocation.getTime());
	    boolean isSignificantlyNewer = timeDelta > ONE_MINUTES;
	    boolean isSignificantlyOlder = timeDelta < -ONE_MINUTES;
	    boolean isNewer = timeDelta > 0;

	    Log.i("", "signNewer = " + isSignificantlyNewer + " signOlder = " + isSignificantlyOlder + " , isNewer = " + isNewer);
	    // If it's been more than two minutes since the current location, use the new location
	    // because the user has likely moved
	    if (isSignificantlyNewer) {
	        return true;
	    // If the new location is more than two minutes older, it must be worse
	    } else if (isSignificantlyOlder) {
	        return false;
	    }

	    // Check whether the new location fix is more or less accurate
	    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
	    boolean isLessAccurate = accuracyDelta > 0;
	    boolean isMoreAccurate = accuracyDelta < 0;
	    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

	    Log.i("", "location Accu = " + location.getAccuracy() + "current Accu = " + currentBestLocation.getAccuracy());
	    Log.i("", "Sign Accu = " + isSignificantlyLessAccurate);
	    // Check if the old and new location are from the same provider
	    boolean isFromSameProvider = isSameProvider(location.getProvider(),
	            currentBestLocation.getProvider());

	    // Determine location quality using a combination of timeliness and accuracy
	    if (isMoreAccurate) {
	        return true;
	    } else if (isNewer && !isLessAccurate) {
	        return true;
	    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
	        return true;
	    }
	    return false;
	}

	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) {
	    if (provider1 == null) {
	      return provider2 == null;
	    }
	    return provider1.equals(provider2);
	}


	public boolean isNetWorkEnable() {
		if(locationManager != null)
			isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		return isNetworkEnabled;
	}

	public static String getAddress(Context context,final double latitude , final double longitude) {

		Geocoder geocoder = new Geocoder(context);
		StringBuffer stringBuffer = new StringBuffer();

		List<Address> list = null;

		try {
			list = geocoder.getFromLocation(latitude, longitude, 1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(list == null) {

			Log.e("주소 데이터 얻기 실패");
			return null;
		}

		if(list.size() > 0) {

			Address add = list.get(0);
			Log.e("countryName : " + add.getCountryName());
			Log.e("getFeatureName : " + add.getFeatureName());
			Log.e("getSubLocality : " + add.getSubLocality());
			Log.e("getLocality : " + add.getLocality());
			Log.e("getPremises : " + add.getPremises());
			Log.e("getLocale : " + add.getLocale());
			Log.e("getExtras : " + add.getExtras());
			// 하위 구분이 있을때 붙여서 표시
			if(add.getSubLocality() != null &&  !add.getSubLocality().equals("")) {

				stringBuffer.append(add.getLocality());
				stringBuffer.append(" ");
				stringBuffer.append(add.getSubLocality());
			}
			// 없으면 그냥 표시
			else {
				stringBuffer.append(add.getLocality());

			}
		}

		return stringBuffer.toString();
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	//================================================================================
	// XXX Inner Class
	//================================================================================
	
	//================================================================================
	// XXX Interface
	//================================================================================
	
	public interface OnGpsSettingListener {
		public void onGpsSetting(boolean isGpsSetting);
	}
	
	public interface OnLocationChangedListener {
		public void onLocationChanged();
	}
	
	//================================================================================
    // XXX Debug
    //================================================================================
	

}
