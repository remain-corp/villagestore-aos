package com.remain.villagestore.util;

/**************************************************
*
* Log
* Log 관리 및 기존 Log에 클래스명과 라인을 추가
*
**************************************************/
public class Log {

	private static final String TAG = "VillageStore";

	public static final boolean D = true;//apk 추출하기 전에는 항상 false로 해놓고 해야함.
	
	public static void i (String msg) {
		if (D) android.util.Log.i(TAG, makeDebugString(msg));
	}
	
	public static void v (String msg) {
		if (D) android.util.Log.v(TAG, makeDebugString(msg));
	}

	public static void d (String msg) {
		if (D) android.util.Log.d(TAG, makeDebugString(msg));
	}

	public static void w (String msg) {
		if (D) android.util.Log.w(TAG, makeDebugString(msg));
	}

	public static void e (String msg) {
		if (D) android.util.Log.e(TAG, makeDebugString(msg));
	}
	
	public static void i (String tag, String msg) {
		if (D) android.util.Log.i(tag, makeDebugString(msg));
	}
	
	public static void v (String tag, String msg) {
		if (D) android.util.Log.v(tag, makeDebugString(msg));
	}

	public static void d (String tag, String msg) {
		if (D) android.util.Log.d(tag, makeDebugString(msg));
	}

	public static void w (String tag, String msg) {
		if (D) android.util.Log.w(tag, makeDebugString(msg));
	}

	public static void e (String tag, String msg) {
		if (D) android.util.Log.e(tag, makeDebugString(msg));
	}
	
	/**
	 * 디버깅 스트링 만들기 (FileName (line) : message)
	 * @param message
	 * @return
	 */
	private static String makeDebugString(String message) {
		
        Exception e = new Exception();
		StackTraceElement element = e.getStackTrace()[2];
       
		String[] a = element.getFileName().split("\\.");
        String className = a[0];
        
        String str = className + "(" + element.getLineNumber() + "): " + message;
        
        return str;
	}
}
