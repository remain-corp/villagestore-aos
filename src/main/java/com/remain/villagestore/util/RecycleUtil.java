package com.remain.villagestore.util;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class RecycleUtil {

	private RecycleUtil() {};

	public static void recursiveRecycle(View root) {
		
		if (root == null) return;
		
		root.setBackgroundDrawable(null);
		if (root instanceof ViewGroup) {
			
			ViewGroup group = (ViewGroup) root;
			int count = group.getChildCount();
			for (int i = 0; i < count; i++) {
				recursiveRecycle(group.getChildAt(i));
			}

			if (!(root instanceof AdapterView)) {
				group.removeAllViews();
			}
		}

		if (root instanceof ImageView) {
			((ImageView) root).setImageDrawable(null);
		}

		root = null;

		return;
	}

	/**
	 * ViewPager에 사용
	 * @param views
	 */
	public static void recursiveRecycle(SparseArray<View> views) {

		int key = 0;
		for (int i = 0; i < views.size(); i++) {

			key = views.keyAt(i);
			View v = views.get(key);
			recursiveRecycle(v);
		}
	}

	/**
	 * ListView, GridView등에 사용
	 * @param recycleList
	 */
	public static void recursiveRecycle(List<WeakReference<View>> recycleList) {

		for (WeakReference<View> ref : recycleList) {
			recursiveRecycle(ref.get());
		}
	}

	/**
	 * ImageView 해제
	 */
	public static void recursiveRecycle(ArrayList<ImageView> recycleList) {

		for (ImageView imageView : recycleList) {

			recursiveRecycle(imageView);
		}
	}
	/**
	 * ImageView 해제
	 */
	public static void recursiveRecycle(ImageView imageView) {

		if(imageView == null) return;

		BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();

		if(bitmapDrawable == null) return;

		Bitmap bitmap = bitmapDrawable.getBitmap();
		if(bitmap == null) return;
		bitmap.recycle();
		imageView.setImageDrawable(null);
	}
}
