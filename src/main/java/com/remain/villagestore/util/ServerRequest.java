package com.remain.villagestore.util;

import android.app.ProgressDialog;
import android.content.Context;

import com.remain.villagestore.info.ErrorInfo;
import com.remain.villagestore.info.NetInfo;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.entity.mime.MultipartEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;


/**************************************************
 * 
 * ServerRequest
 * 서버로 값을 전송하고 결과 값을 받는 클래스
 * 
 **************************************************/

public class ServerRequest {
	
	//================================================================================
    // Constants
    //================================================================================
	/** GET 방식 */
	public static final int REQUEST_GET = 101;
	/** POST 방식 */
	public static final int REQUEST_POST = 102;
	
	/** 재연결 시도 횟수 */
	private static final int RECONNECT_COUNT = 1;
	/** 타임아웃 시간 */
	public static final int DEFAULT_TIMEOUT	= 60000;

	//================================================================================
    // Constructors
    //================================================================================
	/**
	 * 기본 생성자
	 */
	public ServerRequest() {
		
	}
	
	/** 
	 * 리스너를 바로 추가하는 생성자
	 * @param onNetworkResultListener
	 */
	public ServerRequest(OnNetworkResultListener onNetworkResultListener) {
		this.onNetworkResultListener = onNetworkResultListener;
	}

	//================================================================================
    // Variables
    //================================================================================
	private ProgressDialog progressDialog;
	private OnNetworkResultListener onNetworkResultListener;
	
	//================================================================================
    // Getter, Setter
    //================================================================================
	/**
	 * 실행 결과 리스너 
	 * @param listener
	 */
	public void setOnNetworkResultListener (OnNetworkResultListener listener) {
		this.onNetworkResultListener = listener;
	}
	
	//================================================================================
    // Etc Method
    //================================================================================
	// 실행 메소드
	/**
	 * 실행. GET방식
	 * @param url
	 * 	접속할 주소 (포트번호 다음부터)
	 */
	public void execute(String url) {
		int methodState = REQUEST_GET;
		ConnectThread thread = new ConnectThread(url, methodState, null, null);
		thread.start();
	}
	
	/**
	 * 실행. Get, Post를 선택할 수 있음.
	 * @param method
	 * 	Get-ServerRequest.REQUEST_GET, Post-ServerRequest.REQUEST_POST
	 * @param url
	 * 	접속할 주소 (포트번호 다음부터)
	 */
	public void execute(int method, String url) {
		int methodState = method;
		ConnectThread thread = new ConnectThread(url, methodState, null, null);
		thread.start();
	}
	
	/**
	 * 실행. POST방식, parameter 필요.
	 * @param url
	 * 	접속할 주소 (포트번호 다음부터)
	 * @param params
	 * 	post 방식에서 보낼 값들의 Collection.
	 */
	public void execute(String url, LinkedHashMap<String, Object> params) {
		int methodState = REQUEST_POST;
		ConnectThread thread = new ConnectThread(url, methodState, params, null);
		thread.start();
	}
	
	/**
	 * 실행. POST방식, parameter 필요.
	 * 기존 주소가 아닌 다른 주소를 사용할때
	 * @param 다른 주소
	 * @param url
	 * 	접속할 주소 (포트번호 다음부터)
	 * @param params
	 * 	post 방식에서 보낼 값들의 Collection.
	 */
	public void execute(String address, String url, LinkedHashMap<String, Object> params) {
		int methodState = REQUEST_POST;
		ConnectThread thread = new ConnectThread(address, url, methodState, params, null);
		thread.start();
	}
	
	/**
	 * 실행. POST방식, 일반 parameter, file parameter
	 * @param url
	 * 	접속할 주소 (포트번호 다음부터)
	 * @param normalParams
	 * 	일반 parameter Collection.
	 * @param fileParams
	 * 	File parameter Collection.
	 */
	public void execute(String url, LinkedHashMap<String, Object> normalParams, LinkedHashMap<String, Object> fileParams) {
		int methodState = REQUEST_POST;
		ConnectThread thread = new ConnectThread(url, methodState, normalParams, fileParams);
		thread.start();
	}
	
	public void execute(Context context, String url, LinkedHashMap<String, Object> normalParams, LinkedHashMap<String, Object> fileParams) {
		
		progressDialog = new ProgressDialog(context);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setMessage("Uploading...");
		progressDialog.setCancelable(false);
		progressDialog.show();
		
		int methodState = REQUEST_POST;
		ConnectThread thread = new ConnectThread(url, methodState, normalParams, fileParams);
		thread.start();
	}
	
    //================================================================================
    // Inner Class, Interface
    //================================================================================
    /**
     * 서버의 응답 결과를 받아오는 인터페이스(리스너)
     */
    public interface OnNetworkResultListener {
    	public void onNetworkResult(JSONObject object, String url, ErrorInfo error);
    }
    
    /**
     * 서버와 연결하고 그 결과를 받아오는 내용을 새로운 쓰레드에서 실행한다.
     */
    private class ConnectThread extends Thread {
    	//================================================================================
        // Variables
        //================================================================================
		private String url;
		private String address;
		private long totalSize;
		private int methodState;
		private LinkedHashMap<String, Object> postParams;
		private LinkedHashMap<String, Object> postFileParams;
		
		//================================================================================
        // Constructors
        //================================================================================
		public ConnectThread(String url, int methodState, LinkedHashMap<String, Object> postParams, LinkedHashMap<String, Object> postFileParams) {
			this.url = url;
			this.methodState = methodState;
			this.postParams = postParams;
			this.postFileParams = postFileParams;
		}
		
		public ConnectThread(String address, String url, int methodState, LinkedHashMap<String, Object> postParams, LinkedHashMap<String, Object> postFileParams) {
			this.address = address;
			this.url = url;
			this.methodState = methodState;
			this.postParams = postParams;
			this.postFileParams = postFileParams;
		}
		
		//================================================================================
        // Override Method
        //================================================================================
		@Override
		public void run() {
			
			// URL Decode
    		if(methodState == REQUEST_GET) {
    			connectGet(url);
    		} else if(methodState == REQUEST_POST) {
    			connectPost(url, postParams, postFileParams, 0);
    		}
		}
		
		//================================================================================
        // Etc Method
        //================================================================================
		/**
		 * POST 방식으로 연결
		 * @param url: 연결 URL
		 * @param normalParams : 일반 parameters
		 * @param fileParams : 파일 parameters
		 * @param reconnectionCount : 반복한 횟수
		 */
	    private void connectPost(String url, LinkedHashMap<String, Object> normalParams, LinkedHashMap<String, Object> fileParams, int reconnectionCount){
	    	JSONObject object = null;
	    	reconnectionCount++;
	        HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = null;
	        
	        String ip = NetInfo.IP_ADDRESS;
	        if(address == null){
	        	ip = NetInfo.IP_ADDRESS;
	        }else{
	        	ip = address;
	        }
	        String apiAddress = ip + url;
	        
	        httppost = new HttpPost(apiAddress);
	        
	        //parameters setting
	        HttpParams params = httppost.getParams();
			HttpConnectionParams.setConnectionTimeout(params, DEFAULT_TIMEOUT);
			HttpConnectionParams.setSoTimeout(params, DEFAULT_TIMEOUT);
			try {
				makeParams(httppost, fileParams, normalParams);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	 
	        ErrorInfo error = null;
	 
	        try {
	            // Open the webpage.
	        	HttpResponse response = httpclient.execute(httppost);
	            if(response.getStatusLine().getStatusCode() == 200){
	            	HttpEntity entity = response.getEntity();
	                if (entity != null) {
	                	InputStream instream = entity.getContent();
	                    // Load the requested page converted to a string into a JSONObject.
	                    object = new JSONObject(convertStreamToString(instream));	//JSON Parsing
	                    instream.close();
	                    com.remain.villagestore.util.Log.d(url + ":" + object.toString());
	                }
	            }
	            else {
	            	error = new ErrorInfo(ErrorInfo.INVALID_ADDRESS, "Unable to load page - " + response.getStatusLine());
	            	//CustomToast.show(context, "Invalid Address : 서버 문제");
	            }
	        } 
	        catch (UnknownHostException uhe) {
	        	error = new ErrorInfo(ErrorInfo.NOT_CONNECT, "Not Connect; " + uhe.getMessage() + " / " + uhe);
	        }
	        catch (IOException  ex) {
	            // Connection was not established
	        	error = new ErrorInfo(ErrorInfo.FAILED_CONNECTED, "Connection failed; " + ex.getMessage() + " / " + ex);
	        	if(reconnectionCount < RECONNECT_COUNT)
	        		connectPost(url, normalParams, fileParams, reconnectionCount);
	        	//else CustomToast.show(context, context.getString(R.string.toast_fail_network));
	        }
	        catch (JSONException ex){
	            // JSON errors
	        	error = new ErrorInfo(ErrorInfo.JSON_ERROR, "JSON failed; " + ex.getMessage());
	        }
	       
	        com.remain.villagestore.util.Log.d("count : " + reconnectionCount);
	        if (error != null && reconnectionCount == 1) com.remain.villagestore.util.Log.e(url + " /// " + error.getError());
	        if (progressDialog != null) {
	        	
	        	progressDialog.dismiss();
	        	progressDialog = null;
	        }
	        if (onNetworkResultListener != null && reconnectionCount == 1) onNetworkResultListener.onNetworkResult(object, url, error);
	    }
	 
	    /** 
	     * GET 방식으로 연결
	     * @param url
	     */
	    private void connectGet(String url){
	    	JSONObject object = null;

	    	HttpClient httpclient = new DefaultHttpClient();
	    	
	        String ip = NetInfo.IP_ADDRESS;
	        if(address == null){

	        	ip = NetInfo.IP_ADDRESS;

	        }else{
	        	ip = address;
	        }
	        String apiAddress = ip + url;
	        
	        HttpGet httpget = new HttpGet(apiAddress);
	        HttpResponse response;

	        ErrorInfo error = null;
	 
	        try {
	            response = httpclient.execute(httpget);
	 
	            if(response.getStatusLine().getStatusCode() == 200){
	                HttpEntity entity = response.getEntity();
	                if (entity != null) {
	                    InputStream instream = entity.getContent();
	                    // Load the requested page converted to a string into a JSONObject.
						String msg = convertStreamToString(instream);
						Log.e("msg = "+msg);
	                    object = new JSONObject(msg);
	                    instream.close();
	                    
	                    com.remain.villagestore.util.Log.d(url + ":" + object.toString());
	                }
	            } else {
	            	error = new ErrorInfo(ErrorInfo.INVALID_ADDRESS, "Unable to load page - " + response.getStatusLine());
	            }
	        }
	        catch (IOException  ex) {
	            // Connection was not established
	        	error = new ErrorInfo(ErrorInfo.FAILED_CONNECTED, "Connection failed; " + ex.getMessage() + " / " + ex);
	        }
	        catch (JSONException ex){
	            // JSON errors
	        	error = new ErrorInfo(ErrorInfo.JSON_ERROR, "JSON failed; " + ex.getMessage());
	        }
	        if (error != null) com.remain.villagestore.util.Log.e(error.getError());
	        if (progressDialog != null) {
	        	
	        	progressDialog.dismiss();
	        	progressDialog = null;
	        }
	        if (onNetworkResultListener != null) onNetworkResultListener.onNetworkResult(object, url, error);
	    }
	    
	    /** 
	     * 파라미터 만들기
	     * @param post
	     * @param fileParam : 파일 파라미터 
	     * @param param : 텍스트 파라미터
	     * @throws UnsupportedEncodingException
	     */
	    private void makeParams(HttpPost post, LinkedHashMap<String, Object> fileParam, LinkedHashMap<String, Object> param) 
	    		throws UnsupportedEncodingException {
			if (fileParam == null && param == null) {
				return;
			}
			String hashKey = null;
			Iterator<String> iter = null;
			List<NameValuePair> nameValueParis = makeTextParameter(param); // 파라미터를 담는 리스트
			
			MultipartEntity reqEntity = new MultipartEntity();
			
			if(fileParam != null) {
				iter = fileParam.keySet().iterator();
				for (int i = 0; i < fileParam.size(); i++){
					hashKey = iter.next();
					String paramKey = hashKey;
					
					if(hashKey.contains(":")) {
						int index = paramKey.indexOf(":");
						paramKey = paramKey.substring(0, index);
					}
					
					com.remain.villagestore.util.Log.d("파일 파라미터 조립중 - " + hashKey + " : " + (String) fileParam.get(hashKey));
					File file = new File((String)fileParam.get(hashKey));
					reqEntity.addPart(paramKey, new FileBody(file));
					com.remain.villagestore.util.Log.e("reqEntity.getContentLength()1 : " + reqEntity.getContentLength());
					com.remain.villagestore.util.Log.e("totalSize1 : " + totalSize);
				}
				
				com.remain.villagestore.util.Log.e("reqEntity.getContentLength()2 : " + reqEntity.getContentLength());
				com.remain.villagestore.util.Log.e("totalSize2 : " + totalSize);
				totalSize = reqEntity.getContentLength();
			}
			
		 	if (nameValueParis != null) {
		 		
				for (NameValuePair nvp : nameValueParis) {
					reqEntity.addPart(nvp.getName(), 
					new StringBody(nvp.getValue(), Charset.forName("UTF-8")));
		        }
			 }
			
			post.setEntity(reqEntity);
		}
	    
	    /**
	     * 일반 파라미터 만들기 (텍스트)
	     * @param param : 텍스트 파라미터
	     * @return
	     * @throws UnsupportedEncodingException
	     */
	    private List<NameValuePair> makeTextParameter(LinkedHashMap<String, Object> param) throws UnsupportedEncodingException {
			if (param == null) {
				return null;
			}
			List<NameValuePair> nameValueParis = null; // 파라미터를 담는 리스트

			String hashKey = null;
			Iterator<String> iter = null;
			nameValueParis = new ArrayList<NameValuePair>();

			iter = param.keySet().iterator();

			while (iter.hasNext()) {
				hashKey = iter.next();
				String paramKey = hashKey;
				
				if(hashKey.contains(":")) {
					int index = paramKey.indexOf(":");
					paramKey = paramKey.substring(0, index);
				}
				com.remain.villagestore.util.Log.d("파라미터 조립중 - " + hashKey + " : " + param.get(hashKey).toString());
				nameValueParis.add(new BasicNameValuePair(paramKey, param.get(hashKey).toString()));
			}
			return nameValueParis;
		}
	 
	    /**
	     * InputStream을 String으로 변환한다.
	     * @param is : InputStream
	     * @return 변환한 String
	     */
	    private String convertStreamToString(InputStream is) {
	        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	        StringBuilder sb = new StringBuilder();
	 
	        String line = null;
	        try {
	            while ((line = reader.readLine()) != null) {
	            	
	                sb.append(line + "\n");
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                is.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	        return sb.toString();
	    }
	}
}
