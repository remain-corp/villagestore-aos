package com.remain.villagestore.util;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.remain.villagestore.R;


/**
 * Created by woongjaelee on 15. 8. 15..
 */
public class ToggleAnimation extends TranslateAnimation {

    //================================================================================
    // XXX Constants
    //================================================================================
	/* 애니메이션 기본 설정 */
    /** 실행 시간 */
    public final int DEFAULT_DURATION = 100;
    /** 이동 비율 */
    public final static float DEFAULT_FROM_TO_RATIO = 1f;
    /** 설정 스위치 이동 비율 */
	public final static float SETTING_FROM_TO_RATIO = 1.28f;

    public final static int LEFT = 3;
    public final static int RIGHT = 5;

    //================================================================================
    // XXX Variables
    //================================================================================
    // View
    private View viewSwitch;
    private View viewBackground;

    // Lang
    private int direct;
    private int startPos;
    /** 사용자 지정 이동 비율 */
    private float fromToRatio;

    /** 좌우 이동 스위치 리소스 (OFF) */
    private int switchOffImgId = R.drawable.switch_push;
    /** 스위치 배경 리소스 (OFF) */
    private int bgOffId = R.drawable.switch_panel_off;
    /** 좌우 이동 스위치 리소스 (ON) */
    private int switchOnImgId = R.drawable.switch_push;
    /** 스위치 배경 리소스 (ON) */
    private int bgOnId = R.drawable.switch_panel_on;

    // Interface
    private OnToggleAnimationListener toggleAnimationListener;

    //================================================================================
    // XXX Constructor
    //================================================================================
    public ToggleAnimation(View viewSwitch, View viewBackground, int direct, int startPos) {
        this(viewSwitch, viewBackground, direct, startPos, DEFAULT_FROM_TO_RATIO);
    }

    public ToggleAnimation(View viewSwitch, View viewBackground, int direct, int startPos, float fromToRatio) {
        this(Animation.RELATIVE_TO_SELF, getFromX(direct, startPos, fromToRatio), Animation.RELATIVE_TO_SELF, getXDelta(direct, startPos, fromToRatio), Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        setFillAfter(true);
        setDuration(DEFAULT_DURATION);
        this.viewSwitch = viewSwitch;
        this.viewBackground = viewBackground;
        this.direct = direct;
        this.startPos = startPos;
        this.fromToRatio = fromToRatio;
        setAnimationListener(animationListener);
    }

    public ToggleAnimation(int fromXType, float fromXValue, int toXType, float toXValue, int fromYType, float fromYValue, int toYType, float toYValue) {
        super(fromXType, fromXValue, toXType, toXValue, fromYType, fromYValue, toYType,	toYValue);
    }

    //================================================================================
    // XXX Listener
    //================================================================================
    AnimationListener animationListener = new AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {

            if (direct == LEFT) {

                if (viewBackground != null) {

                    ((ImageView) viewSwitch).setBackgroundResource(0);
                    ((ImageView) viewSwitch).setBackgroundResource(switchOffImgId);
                }

                if (viewBackground != null) {

                    viewBackground.setBackgroundResource(0);
                    viewBackground.setBackgroundResource(bgOffId);
                }

            } else if (direct == RIGHT) {

                if (viewBackground != null) {

                    ((ImageView) viewSwitch).setBackgroundResource(0);
                    ((ImageView) viewSwitch).setBackgroundResource(switchOnImgId);
                }

                if (viewBackground != null) {

                    viewBackground.setBackgroundResource(0);
                    viewBackground.setBackgroundResource(bgOnId);
                }
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {}

        @Override
        public void onAnimationEnd(Animation animation) {
            if (toggleAnimationListener != null) toggleAnimationListener.onEnd(direct, direct == LEFT ? RIGHT : LEFT);
        }
    };

    //================================================================================
    // XXX Getter, Setter
    //================================================================================
    public void setOnToggleAnimationListener(OnToggleAnimationListener toggleAnimationListener) {
        this.toggleAnimationListener = toggleAnimationListener;
    }

    public int getSwitchOffImgId() {
        return switchOffImgId;
    }

    public void setSwitchOffImgId(int switchOffImgId) {
        this.switchOffImgId = switchOffImgId;
    }

    public int getBgOffId() {
        return bgOffId;
    }

    public void setBgOffId(int bgOffId) {
        this.bgOffId = bgOffId;
    }

    public int getSwitchOnImgId() {
        return switchOnImgId;
    }

    public void setSwitchOnImgId(int switchOnImgId) {
        this.switchOnImgId = switchOnImgId;
    }

    public int getBgOnId() {
        return bgOnId;
    }

    public void setBgOnId(int bgOnId) {
        this.bgOnId = bgOnId;
    }

    //================================================================================
    // XXX Etc Method
    //================================================================================
    private static float getFromX(int direct, int startPos, float fromToRatio) {

        if (startPos == RIGHT) {

            if (direct == LEFT) {
                return 0.0f;
            } else if (direct == RIGHT) {
                return -fromToRatio;
            } else {
                return 0.0f;
            }

        } else {

            if (direct == LEFT) {
                return fromToRatio;
            } else if (direct == RIGHT) {
                return 0.0f;
            } else {
                return 0.0f;
            }
        }
    }

    private static float getXDelta(int direct, int startPos, float fromToRatio) {

        if (startPos == RIGHT) {

            if (direct == LEFT) {
                return -fromToRatio;
            } else if (direct == RIGHT) {
                return 0.0f;
            } else {
                return 0.0f;
            }

        } else {

            if (direct == LEFT) {
                return 0.0f;
            } else if (direct == RIGHT) {
                return fromToRatio;
            } else {
                return 0.0f;
            }
        }
    }

    //================================================================================
    // XXX Interface
    //================================================================================
    public interface OnToggleAnimationListener {
        public void onEnd(int prevDirect, int nextDirect);
    }
}